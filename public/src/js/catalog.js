$('body').on('click', '#newItem', function () {
    $.ajax({
        dataType: "html",
        method: "GET",
        url: "/" + attr + "/new",
        success: function (newData) {
            $("#catalogTable").prepend(newData);

            $('.saveItem').on('click', function () {
                var form = $(this).closest('form');
                $.ajax({
                    type: "POST",
                    url: form.attr('action'),
                    data: form.serialize(),
                    success: function () {
                        updateList(attr, form);
                    }
                });
            });

            $('.formCancel').on('click', function () {
                var form = $(this).closest('form');
                updateList(attr, form);
            });
        }
    });
});

$('body').on('click', '.deleteItem', function (event) {
    var element = $(this).closest('tr');
    $.ajax({
        type: "GET",
        url: "/delete/" + attr + "/" + element.attr('data-id'),
        success: function () {
            element.remove();
        },
        error: function (error) {
            deleteModal(error);
        }
    });
    event.preventDefault();
});

$('body').on('click', '#searchCatalog', function () {
    search = $('#searchCatalogField').val();
    var href = attr + '?search=' + search;
    $('#catalogModal .modal-content').empty();
    loadContent(href);
});

$('body').on('click', '.editItem', function () {
    var parent = $(this).closest( "tr" );
    var href = $(this).attr('data-href');
    $.ajax({
        dataType: "html",
        method: "GET",
        url: href,
        success: function (newData2) {
            parent.replaceWith(newData2);
            if (!permission) {
                parent.find('input, select, textarea').each(function () {
                    $(this).prop('disabled', true);
                });
            }

            $('.saveItem').on('click', function () {
                var form = $(this).closest('form');
                $.ajax({
                    type: "POST",
                    url: form.attr('action'),
                    data: form.serialize(),
                    success: function (data) {
                        updateList(attr, form);
                    }
                });
            });

            $('.formCancel').on('click', function () {
                var form = $(this).closest('form');
                updateList(attr, form);
            });
        }
    });
});

function loadContent (attr) {
    $.ajax({
        dataType: "html",
        method: "GET",
        url: "/" + attr,
        success: function (data) {
            $("#catalogModal .modal-content").append(data);
            if(search)
                $('#searchCatalogField').val(search);
        }
    });
}

function CatalogModal () {
    $('.showModals a').on('click', function () {
        $('#catalogModal .modal-content').empty();
        $('#catalogModal').modal('show');
        attr = $(this).attr('data-id');

        loadContent(attr);
    });
}

function updateList (url, form) {
    $.ajax({
        dataType: "html",
        method: "GET",
        url: "/" + url,
        success: function (newData) {
            form.closest(".modal-content").empty().append(newData);
        }
    });
}