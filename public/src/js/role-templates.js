var roleTemplates = {
    admin: {
        name: 'Admin',
        roles: {
            all: true
        }
    },
    operator: {
        name: 'Operator',
        roles: [
            'edit_directories', 'show_directories', 'edit_catalog', 'show_catalog', 'edit_cities', 'show_cities'
        ]
    }
};

function rolesInit() {
    for (var role in roleTemplates)
        $('#userRole').append('<option value="' + role + '">' + roleTemplates[role]['name']  + '</option>');

    $('#userRole').on('change', function() {
        if (roleTemplates[this.value]) {
            if (roleTemplates[this.value]['all'])
                $('#userDataAccess input[type=checkbox]').prop('checked', true);
            else {
                roleTemplates[this.value]['roles'].forEach(function (roleName) {
                    $('#userDataAccess input[name=' + roleName + ']').prop('checked', true);
                });
            }
        }
    });
}