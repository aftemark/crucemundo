function modelReservations(id, type, token, tab) {
    tab.on('change', '.reservations-filters select', function () {
        var form_data = $(this.form).serializeArray();
        form_data.push(
            {name: 'id', value: id},
            {name: 'type', value: type},
            {name: '_token', value: token}
        );
        $.ajax({
            dataType: "html",
            method: "POST",
            cache: false,
            url: '/model/reservations',
            data: form_data,
            success: function (html) {
                tab.empty().append(html);
            }
        });
    });
}

function deleteModal(error) {
    var modal = $('#deleteModal');
    modal.find('.modal-body').html('<p>' + error.responseText + '</p>');
    modal.modal('show');
}

$(document).ready(function() {
    $('body').on('click', '.delete-check-element', function (e) {
        var element = $(this);
        $.get(element.attr('href'), function() {
            element.closest('tr').remove();
        }).error(function(error) {
            deleteModal(error);
        });
        e.preventDefault();
    });
});