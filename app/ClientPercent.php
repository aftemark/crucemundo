<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ClientPercent extends Model
{
    public function client ()
    {
        return $this->belongsTo('App\Client', 'client_id', 'id');
    }

    public function ship ()
    {
        return $this->belongsTo('App\Ship', 'ship_id', 'id');
    }

    public function hotel ()
    {
        return $this->belongsTo('App\Hotel', 'hotel_id', 'id');
    }
}
