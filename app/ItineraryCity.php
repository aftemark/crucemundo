<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ItineraryCity extends Model
{
    public function city()
    {
        return $this->belongsTo('App\City', 'city_id', 'id');
    }

    public function itinerary()
    {
        return $this->belongsTo('App\Itinerary', 'itinerary_id', 'id');
    }

    public function non_compatibles()
    {
        return $this->hasMany('App\NonCompatible');
    }

    protected static function boot() {
        parent::boot();

        static::deleting(function($itineraryCity) {
            foreach ($itineraryCity->non_compatibles as $non_compatible) {
                $non_compatible->services()->delete();
                $non_compatible->delete();
            }
        });
    }
}
