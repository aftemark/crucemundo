<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Sofa\Eloquence\Eloquence;

class TypeCategory extends Model
{
    use Eloquence;

    protected $searchableColumns = ['id', 'name', 'pax_amount', 'balcony', 'bed', 'type.name'];

    protected $searchable = [
        'columns' => [
            'type_categories.id' => 1,
            'type_categories.name' => 10,
            'type_categories.pax_amount' => 5,
            'type_categories.balcony' => 2,
            'type_categories.bed' => 2,
            'placement_types.name' => 5
        ],
        'joins' => [
            'placement_types' => ['type_categories.placement_type_id','placement_types.id'],
        ]
    ];

    public function type()
    {
        return $this->belongsTo('App\PlacementType', 'placement_type_id', 'id');
    }

    public function cabins()
    {
        return $this->hasMany('App\Cabin');
    }

    public function rooms()
    {
        return $this->hasMany('App\Room');
    }

    public function cruise_types()
    {
        return $this->hasMany('App\CruiseType');
    }

    protected static function boot()
    {
        parent::boot();

        static::deleting(function($typeCategory) {
            foreach ($typeCategory->rooms as $room) {
                foreach ($room->placements as $placement) {
                    $placement->special_relations()->delete();
                }
                $room->placements()->delete();
            }
            $typeCategory->rooms()->delete();
        });
    }
}
