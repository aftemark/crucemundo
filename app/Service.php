<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Sofa\Eloquence\Eloquence;

class Service extends Model
{
    use Eloquence;

    /*protected $searchable = [
        'columns' => [
            'services.service_type' => 1,
            'services.name' => 10,
            'cities.name' => 5
        ],
        'joins' => [
            'cities' => ['services.city_id','cities.id']
        ]
    ];*/
    protected $searchableColumns = ['service_type', 'journey.code'];

    public function ship()
    {
        return $this->belongsTo('App\Ship', 'ship_id', 'id');
    }

    public function hotel()
    {
        return $this->belongsTo('App\Hotel', 'hotel_id', 'id');
    }

    public function city()
    {
        return $this->belongsTo('App\City', 'city_id', 'id');
    }

    public function year()
    {
        return $this->belongsTo('App\Year', 'year_id', 'id');
    }

    public function packages()
    {
        return $this->hasMany('App\PackageService');
    }

    public function journey_services()
    {
        return $this->hasMany('App\JourneyService');
    }

    protected static function boot()
    {
        parent::boot();

        static::deleting(function($special) {
            $special->journey_services()->delete();
        });
    }
}
