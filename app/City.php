<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Sofa\Eloquence\Eloquence;

class City extends Model
{
    use Eloquence;

    protected $searchableColumns = ['id', 'name', 'description'];

    public function country()
    {
        return $this->belongsTo('App\Country', 'country_id', 'id');
    }

    public function airports()
    {
        return $this->hasMany('App\Airport');
    }

    public function hotels()
    {
        return $this->hasMany('App\Hotel');
    }

    public function services()
    {
        return $this->hasMany('App\Service');
    }

    public function itinerary_cities()
    {
        return $this->hasMany('App\ItineraryCity');
    }
}
