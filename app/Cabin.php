<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Cabin extends Model
{
    public function deck()
    {
        return $this->belongsTo('App\Deck', 'deck_id', 'id');
    }

    public function type()
    {
        return $this->belongsTo('App\TypeCategory', 'type_category_id', 'id');
    }

    public function placements()
    {
        return $this->hasMany('App\Placement');
    }
}
