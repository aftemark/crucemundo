<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Sofa\Eloquence\Eloquence;

class PlacementType extends Model
{
    use Eloquence;

    protected $searchableColumns = ['id', 'name'];

    /*protected $searchable = [
        'columns' => [
            'placement_types.id' => 1,
            'placement_types.name' => 1
        ]
    ];*/

    public function categories()
    {
        return $this->hasMany('App\TypeCategory');
    }
}
