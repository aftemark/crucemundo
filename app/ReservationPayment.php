<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ReservationPayment extends Model
{
    public function reservation ()
    {
        return $this->belongsTo('App\Reservation', 'reservation_id', 'id');
    }
}
