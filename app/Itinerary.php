<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Sofa\Eloquence\Eloquence;

class Itinerary extends Model
{
    use Eloquence;

    protected $searchableColumns = ['id', 'name', 'from_to'];

    public function relations()
    {
        return $this->hasMany('App\ItineraryCity');
    }

    public function journeys()
    {
        return $this->hasMany('App\Journey');
    }

    protected static function boot() {
        parent::boot();

        static::deleting(function($itinerary) {
            $itinerary->relations()->delete();
        });
    }
}
