<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ReservationPaxService extends Model
{
    public function placement()
    {
        return $this->belongsTo('App\ReservationPax', 'reservation_pax_id', 'id');
    }

    public function service()
    {
        return $this->belongsTo('App\JourneyService', 'journey_service_id', 'id');
    }

    public function notifications()
    {
        return $this->hasMany('App\Notification');
    }
}
