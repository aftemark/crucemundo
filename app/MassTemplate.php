<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class MassTemplate extends Model
{
    public function receivers()
    {
        return $this->hasMany('App\MassTemplateReceiver');
    }
}
