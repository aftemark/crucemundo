<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SupplementGroup extends Model
{
    public function placements()
    {
        return $this->hasMany('App\Placement');
    }

    public function journey ()
    {
        return $this->belongsTo('App\Journey', 'journey_id', 'id');
    }
}
