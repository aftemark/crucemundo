<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use App\Year;
use DB;
use Schema;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        if (Schema::hasTable('years')) {
            $years['active'] = Year::where('status', 'active')->get();
            $years['archive'] = Year::where('status', 'archive')->get();

            view()->share('nav_years', $years);
        }
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }
}
