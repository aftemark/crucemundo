<?php

namespace App\Providers;

use Illuminate\Routing\Router;
use Illuminate\Foundation\Support\Providers\RouteServiceProvider as ServiceProvider;
use App\Ship;
use App\Reservation;
use App\Placement;
use App\Journey;
use App\Year;
use Session;

class RouteServiceProvider extends ServiceProvider
{
    /**
     * This namespace is applied to your controller routes.
     *
     * In addition, it is set as the URL generator's root namespace.
     *
     * @var string
     */
    protected $namespace = 'App\Http\Controllers';

    /**
     * Define your route model bindings, pattern filters, etc.
     *
     * @param  \Illuminate\Routing\Router  $router
     * @return void
     */
    public function boot(Router $router)
    {
        $router->bind('ship', function ($ship) {
            if ($ship != 'new')
                return Ship::findOrFail($ship);
            else
                return new Ship;
        });

        $router->bind('reservation', function ($reservation) {
            return $reservation == 'new' ? new Reservation : Reservation::withTrashed()->whereYearId(Session::get('catalog_year_id'))->findOrFail($reservation);
        });

        $router->bind('journey', function ($journey) {
            if ($journey != 'new')
                return Journey::findOrFail($journey);
            else
                return new Journey;
        });

        $router->bind('placement', function ($placement) {
            if ($placement != 'new')
                return Placement::findOrFail($placement);
            else
                return new Placement;
        });

        $router->bind('year', function ($year) {
            return Year::where('year', $year)->firstOrFail();
        });

        parent::boot($router);
    }

    /**
     * Define the routes for the application.
     *
     * @param  \Illuminate\Routing\Router  $router
     * @return void
     */
    public function map(Router $router)
    {
        $this->mapWebRoutes($router);

        //
    }

    /**
     * Define the "web" routes for the application.
     *
     * These routes all receive session state, CSRF protection, etc.
     *
     * @param  \Illuminate\Routing\Router  $router
     * @return void
     */
    protected function mapWebRoutes(Router $router)
    {
        $router->group([
            'namespace' => $this->namespace, 'middleware' => 'web',
        ], function ($router) {
            require app_path('Http/routes.php');
        });
    }
}
