<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Special extends Model
{
    public function journey()
    {
        return $this->belongsTo('App\Journey', 'journey_id', 'id');
    }

    public function relations()
    {
        return $this->hasMany('App\SpecialRelation');
    }

    protected static function boot()
    {
        parent::boot();

        static::deleting(function($special) {
            $special->relations()->delete();
        });
    }
}
