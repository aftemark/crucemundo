<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Sofa\Eloquence\Eloquence;

class Hotel extends Model
{
    use Eloquence;

    protected $searchableColumns = ['id', 'name', 'rating'];

    /*protected $searchable = [
        'columns' => [
            'hotels.id' => 10,
            'hotels.name' => 10,
            'hotels.rating' => 5,
            'cities.name' => 5,
            'countries.name' => 5,
        ],
        'joins' => [
            'cities' => ['hotels.city_id','cities.id'],
            'countries' => ['cities.country_id','countries.id']
        ]
    ];*/

    public function city()
    {
        return $this->belongsTo('App\City', 'city_id', 'id');
    }

    public function rooms()
    {
        return $this->hasMany('App\Room');
    }

    public function services()
    {
        return $this->hasMany('App\Service');
    }

    public function percents()
    {
        return $this->hasMany('App\ClientPercent');
    }

    public function journeys()
    {
        return $this->hasMany('App\Journey');
    }

    protected static function boot() {
        parent::boot();

        static::deleting(function($hotel) {
            $hotel->rooms()->delete();
            $hotel->percents()->delete();
        });
    }
}
