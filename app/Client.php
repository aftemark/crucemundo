<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Sofa\Eloquence\Eloquence;

class Client extends Model
{
    use Eloquence;

    protected $searchableColumns = ['id', 'name', 'country.name', 'language.name', 'percent', 'loyal_number'];

    public function country()
    {
        return $this->belongsTo('App\Country', 'country_id', 'id');
    }

    public function language()
    {
        return $this->belongsTo('App\Language', 'language_id', 'id');
    }

    public function percents()
    {
        return $this->hasMany('App\ClientPercent');
    }

    public function users()
    {
        return $this->hasMany('App\ClientUser');
    }

    public function reservations()
    {
        return $this->hasMany('App\Reservation');
    }

    protected static function boot() {
        parent::boot();

        static::deleting(function($client) {
            $client->percents()->delete();
        });
    }
}
