<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CruiseType extends Model
{
    public function journey()
    {
        return $this->belongsTo('App\Journey', 'journey_id', 'id');
    }

    public function type()
    {
        return $this->belongsTo('App\TypeCategory', 'type_category_id', 'id');
    }

    public function placements()
    {
        return $this->hasMany('App\Placement');
    }
}
