<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Room extends Model
{
    public function hotel()
    {
        return $this->belongsTo('App\Hotel', 'hotel_id', 'id');
    }

    public function type()
    {
        return $this->belongsTo('App\TypeCategory', 'type_category_id', 'id');
    }

    public function placements()
    {
        return $this->hasMany('App\Placement');
    }

    protected static function boot()
    {
        parent::boot();

        static::deleting(function($room) {
            foreach ($room->placements() as $placement)
                $placement->special_relations()->delete();

            $room->placements()->delete();
        });
    }
}
