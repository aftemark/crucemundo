<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Sofa\Eloquence\Eloquence;

class Journey extends Model
{
    use Eloquence;

    protected $searchableColumns = ['depart_date', 'hotel.name', 'ship.name', 'itinerary.name', 'itinerary.from_to', 'itinerary.nights', 'season'];

    public function ship ()
    {
        return $this->belongsTo('App\Ship', 'ship_id', 'id');
    }

    public function hotel ()
    {
        return $this->belongsTo('App\Hotel', 'hotel_id', 'id');
    }

    public function itinerary ()
    {
        return $this->belongsTo('App\Itinerary', 'itinerary_id', 'id');
    }

    public function services()
    {
        return $this->hasMany('App\JourneyService');
    }

    public function nonDays()
    {
        return $this->hasMany('App\NonCompatible');
    }

    public function groups()
    {
        return $this->hasMany('App\SupplementGroup');
    }

    public function placements()
    {
        return $this->hasMany('App\Placement');
    }

    public function fees()
    {
        return $this->hasMany('App\JourneyFee');
    }

    public function specials()
    {
        return $this->hasMany('App\Special');
    }

    public function reservations()
    {
        return $this->hasMany('App\Reservation');
    }

    public function types()
    {
        return $this->hasMany('App\CruiseType');
    }

    protected static function boot()
    {
        parent::boot();

        static::deleting(function($journey) {
            $journey->services()->delete();
            $journey->nonDays()->delete();
            $journey->groups()->delete();
            $journey->placements()->delete();
            $journey->fees()->delete();
            foreach ($journey->specials as $special)
                $special->relations()->delete();
            $journey->specials()->delete();
            $journey->types()->delete();
        });
    }
}
