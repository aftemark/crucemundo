<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

Route::pattern('id', '[0-9]+');

Route::pattern('item', 'new|[0-9]+');

Route::pattern('itinerary_id', '[0-9]+');

Route::get('', function () {
    if (!Auth::check())
        return view('login');
    else {
        $year = Session::get('catalog_year');
        if ($year !== NULL)
            return redirect()->route('statistics', ['year' => $year]);
        else
            return redirect()->route('years');
    }
});

Route::get('login', function () {
    return redirect('/');
});

Route::get('logout', function () {
    Auth::logout();
    return redirect('/');
});

Route::post('signin', [
    'uses' => 'UserController@SignIn',
    'as' => 'signin'
]);

Route::get('{year}/statistics', ['uses' => 'YearController@Statistics', 'as' => 'statistics', 'middleware' => 'auth']);

/*YEAR*/
Route::group(['middleware' => 'auth', 'prefix' => 'years'], function () {

    Route::get('', ['uses' => 'YearController@YearIndex', 'as' => 'years']);

    Route::post('add', ['uses' => 'YearController@YearCopy', 'middleware' => 'role:edit_year']);

    Route::post('archive', ['uses' => 'YearController@ArchiveYear', 'middleware' => 'role:edit_year']);

    Route::get('select/{year}', ['uses' => 'YearController@YearSelect']);

});

Route::group(['middleware' => ['auth', 'role:show_directories']], function () {

    Route::group(['middleware' => 'role:show_cities'], function () {
        /*CITY*/
        Route::get('cities/setnumber', ['uses' => 'CityController@SetNumber']);

        Route::get('cities', ['middleware' => 'auth', 'uses' => 'CityController@Index', 'as' => 'cities_list']);

        Route::get('cities/{item}', ['middleware' => 'ajax', 'uses' => 'CityController@Info']);

        Route::post('save/cities', ['middleware' => ['ajax', 'role:edit_cities', 'role:edit_directories'], 'uses' => 'CityController@Save']);

        Route::get('delete/city/{id}', ['middleware' => ['ajax', 'role:edit_cities', 'role:edit_directories'], 'uses' => 'CityController@Delete']);
    });

    Route::group(['middleware' => 'role:show_catalog'], function () {
        /*COUNTRY*/
        Route::get('countries', ['middleware' => 'ajax', 'uses' => 'CountryController@Index']);

        Route::get('countries/{item}', ['middleware' => 'ajax', 'uses' => 'CountryController@Info']);

        Route::post('save/countries', ['middleware' => ['ajax', 'role:edit_catalog'], 'uses' => 'CountryController@Save']);

        Route::get('delete/countries/{id}', ['middleware' => ['ajax', 'role:edit_catalog'], 'uses' => 'CountryController@Delete']);

        /*LANGUAGE*/
        Route::get('languages', ['middleware' => 'ajax', 'uses' => 'LanguageController@Index']);

        Route::get('languages/{item}', ['middleware' => 'ajax', 'uses' => 'LanguageController@Info']);

        Route::post('save/languages', ['middleware' => ['ajax', 'role:edit_catalog'], 'uses' => 'LanguageController@Save']);

        Route::get('delete/languages/{id}', ['middleware' => ['ajax', 'role:edit_catalog'], 'uses' => 'LanguageController@Delete']);

        /*PLACEMENT_TYPES*/
        Route::get('placementtypes', ['middleware' => 'ajax', 'uses' => 'PlacementTypeController@Index']);

        Route::get('placementtypes/{item}', ['middleware' => 'ajax', 'uses' => 'PlacementTypeController@Info']);

        Route::post('save/placementtypes', ['middleware' => ['ajax', 'role:edit_catalog'], 'uses' => 'PlacementTypeController@Save']);

        Route::get('delete/placementtypes/{id}', ['middleware' => ['ajax', 'role:edit_catalog'], 'uses' => 'PlacementTypeController@Delete']);

        /*TYPE_CATEGORIES*/
        Route::get('hotelcategories', ['middleware' => 'ajax', 'uses' => 'TypeCategoryController@HotelIndex']);

        Route::get('hotelcategories/{item}', ['middleware' => 'ajax', 'uses' => 'TypeCategoryController@HotelInfo']);

        Route::get('delete/hotelcategories/{id}', ['middleware' => ['ajax', 'role:edit_catalog'], 'uses' => 'TypeCategoryController@Delete']);

        /*ajaxinfos went to special*/

        Route::post('save/categories', ['middleware' => ['ajax', 'role:edit_catalog'], 'uses' => 'TypeCategoryController@Save']);

        Route::get('delete/shipcategories/{id}', ['middleware' => ['ajax', 'role:edit_catalog'], 'uses' => 'TypeCategoryController@Delete']);

        /*AIRPORTS*/
        Route::get('airports', ['middleware' => 'ajax', 'uses' => 'AirportController@Index']);

        Route::get('airports/{item}', ['middleware' => 'ajax', 'uses' => 'AirportController@Info']);

        Route::post('save/airports', ['middleware' => ['ajax', 'role:edit_catalog'], 'uses' => 'AirportController@Save']);

        Route::get('delete/airports/{id}', ['middleware' => ['ajax', 'role:edit_catalog'], 'uses' => 'AirportController@Delete']);

    });

    /*SPECIAL*/
    Route::group(['middleware' => ['role:edit_ships|edit_hotels', 'role:show_ships|show_hotels', 'role:show_directories']], function () {
        Route::get('shipcategories', ['middleware' => 'ajax', 'uses' => 'TypeCategoryController@ShipIndex']);

        Route::get('shipcategories/{item}', ['middleware' => 'ajax', 'uses' => 'TypeCategoryController@ShipInfo']);

        Route::get('typecategory/{item}', ['middleware' => 'auth', 'uses' => 'TypeCategoryController@AjaxInfo']);

        Route::get('placementtypeship/{item}', ['middleware' => 'auth', 'uses' => 'PlacementTypeController@AjaxInfoShip']);

        Route::get('placementtypehotel/{item}', ['middleware' => 'auth', 'uses' => 'PlacementTypeController@AjaxInfoHotel']);
    });

    Route::group(['middleware' => ['role:show_directories', 'role:show_ships|show_hotels']], function () {
        /*ACCOMODATIONS*/
        Route::get('accommodations', ['uses' => 'ShipController@Index', 'as' => 'accommodations_list']);

        Route::get('accommodations/setnumber', ['middleware' => 'auth', 'uses' => 'ShipController@SetNumber']);

        Route::group(['middleware' => 'role:show_ships'], function () {
            /*SHIPS*/
            Route::get('ship/{ship}', ['uses' => 'ShipController@Info', 'as' => 'ship']);

            Route::post('save/ship/{item}', ['middleware' => ['ajax', 'role:edit_ships'], 'uses' => 'ShipController@Save']);

            Route::get('delete/ship/{id}', ['middleware' => ['auth', 'role:edit_ships'], 'uses' => 'ShipController@Delete']);

            Route::get('shipdata/{ship}', ['uses' => 'ShipController@ShipData']);

            /*DECKS*/
            Route::get('decks/{ship}', ['middleware' => 'auth', 'uses' => 'ShipController@CabinsIndex']);

            Route::post('save/deck/{item}', ['middleware' => ['auth', 'role:edit_ships'], 'uses' => 'DeckController@Save']);

            /*CABINS*/
            Route::get('cabin/{item}', ['middleware' => 'ajax', 'uses' => 'CabinController@Info']);

            Route::post('save/cabin', ['middleware' => ['ajax', 'role:edit_ships'], 'uses' => 'CabinController@Save']);

            Route::get('delete/cabin/{id}', ['middleware' => ['ajax', 'role:edit_ships'], 'uses' => 'CabinController@Delete']);
        });

        Route::group(['middleware' => 'role:show_hotels'], function () {
            /*HOTELS*/
            Route::get('hotel/{item}', ['uses' => 'HotelController@Info', 'as' => 'hotel']);

            Route::post('save/hotel/{item}', ['middleware' => 'role:edit_hotels', 'uses' => 'HotelController@Save']);

            Route::get('delete/hotel/{id}', ['middleware' => 'role:edit_hotels', 'uses' => 'HotelController@Delete']);

            //Route::get('hoteldata/{id}', ['middleware' => 'auth', 'uses' => 'HotelController@HotelData']);

            /*ROOMS*/
            Route::get('rooms/{id}', ['middleware' => 'auth', 'uses' => 'HotelController@RoomsIndex']);

            Route::get('room/{item}', ['middleware' => 'ajax', 'uses' => 'RoomController@Info']);

            Route::post('save/room', ['middleware' => ['ajax', 'role:edit_hotels'], 'uses' => 'RoomController@Save']);

            Route::get('delete/room/{id}', ['middleware' => ['ajax', 'role:edit_hotels'], 'uses' => 'RoomController@Delete']);
        });

    });

    Route::group(['middleware' => ['role:show_services', 'year']], function () {
        /*SERVICE*/
        Route::get('services', ['uses' => 'ServiceController@Index', 'as' => 'services_list']);

        Route::get('service/{item}', ['middleware' => 'ajax', 'uses' => 'ServiceController@Info']);

        Route::post('save/service', ['middleware' => ['ajax', 'role:edit_services', 'role:edit_directories'], 'uses' => 'ServiceController@Save']);

        Route::get('delete/service/{id}', ['middleware' => ['role:edit_services', 'role:edit_directories'], 'uses' => 'ServiceController@Delete']);

        Route::get('services/setnumber', ['uses' => 'ServiceController@SetNumber']);

        Route::get('venues', ['middleware' => ['ajax', 'role:edit_services', 'role:edit_directories'], 'uses' => 'ServiceController@GetVenues']);
    });

    Route::group(['middleware' => 'role:show_packages'], function () {
        /*PACKAGE*/
        Route::get('packages', ['uses' => 'ServicePackageController@Index', 'as' => 'packages_list']);

        Route::get('package/{item}', ['uses' => 'ServicePackageController@Info', 'as' => 'package']);

        Route::post('save/package', ['middleware' => ['role:edit_packages', 'role:edit_directories'], 'uses' => 'ServicePackageController@Save']);

        Route::get('delete/package/{id}', ['middleware' => ['ajax', 'role:edit_packages', 'role:edit_directories'], 'uses' => 'ServicePackageController@Delete']);

        Route::get('packages/setnumber', ['uses' => 'ServicePackageController@SetNumber']);


        Route::get('serviceinfo/{id}', ['middleware' => 'ajax', 'uses' => 'ServiceController@Info']);

        Route::get('packages/venues/{id}', ['middleware' => 'ajax', 'uses' => 'ServicePackageController@PackageVenues']);

        Route::post('save/packageservices/{id}', ['middleware' => ['ajax', 'role:edit_packages', 'role:edit_directories'], 'uses' => 'ServicePackageController@SaveService']);

        Route::get('delete/packageservices/{id}', ['middleware' => ['ajax', 'role:edit_packages', 'role:edit_directories'], 'uses' => 'ServicePackageController@ServiceDelete']);

        Route::get('packageservices/{id}', ['middleware' => 'ajax', 'uses' => 'ServicePackageController@PackageServices']);

        Route::get('type/services/{id}', ['middleware' => 'ajax', 'uses' => 'ServicePackageController@TypeServices']);
    });

    Route::group(['middleware' => 'role:show_itineraries'], function () {
        /*ITINERARY*/
        Route::get('itineraries', ['uses' => 'ItineraryController@Index', 'as' => 'itineraries_list']);

        Route::get('itinerary/{item}', ['middleware' => 'ajax', 'uses' => 'ItineraryController@Info']);

        Route::post('save/itinerary', ['middleware' => ['ajax', 'role:edit_itineraries', 'role:edit_directories'], 'uses' => 'ItineraryController@Save']);

        Route::get('delete/itinerary/{id}', ['middleware' => ['ajax', 'role:edit_itineraries', 'role:edit_directories'], 'uses' => 'ItineraryController@Delete']);

        Route::get('itineraries/setnumber', ['uses' => 'ItineraryController@SetNumber']);
    });

    Route::group(['middleware' => 'role:show_b2c|show_b2b'], function () {
        /*CLIENT*/
        Route::get('clients', ['uses' => 'ClientController@Index', 'as' => 'clients_list']);

        Route::get('clients/setnumber', ['uses' => 'ClientController@SetNumber']);

        Route::group(['middleware' => 'role:show_b2c'], function () {

            Route::get('b2c/{id}', ['uses' => 'ClientController@B2CInfo', 'as' => 'b2c']);

            Route::get('b2c/new', ['uses' => 'ClientController@B2CNew', 'as' => 'b2c.new']);

        });

        Route::group(['middleware' => 'role:show_b2b'], function () {

            Route::get('b2b/{id}', ['uses' => 'ClientController@B2BInfo', 'as' => 'b2b']);

            Route::get('b2b/new', ['uses' => 'ClientController@B2BNew', 'as' => 'b2b.new']);

            Route::get('b2b/users/{id}', ['middleware' => 'ajax', 'uses' => 'ClientController@Users']);

            Route::get('b2b/user/{item}', ['uses' => 'ClientUserController@Info']);

            Route::post('b2b/save/user', ['middleware' => ['ajax', 'role:edit_b2b', 'role:edit_directories'], 'uses' => 'ClientUserController@Save']);

            Route::get('b2b/delete/user/{id}', ['middleware' => ['ajax', 'role:edit_b2b', 'role:edit_directories'], 'uses' => 'ClientUserController@Delete']);

            Route::get('clientusers/setnumber', ['uses' => 'ClientController@SetClientUsersNumber']);

        });

        Route::post('save/client', ['middleware' => ['ajax', 'role:edit_b2c|edit_b2b', 'role:edit_directories'], 'uses' => 'ClientController@Save']);

        Route::get('delete/client/{id}', ['middleware' => ['role:edit_b2c|edit_b2b', 'role:edit_directories'], 'uses' => 'ClientController@Delete']);

    });

    Route::group(['middleware' => ['role:show_tours', 'year']], function () {
        /*TOUR*/
        Route::get('tours/setnumber', ['uses' => 'TourController@SetNumber']);

        Route::get('tours', ['middleware' => 'auth', 'uses' => 'TourController@Index', 'as' => 'tours_list']);

        Route::get('tour/{item}', ['uses' => 'TourController@Info', 'as' => 'tour']);

        Route::get('tour/{item}/{itinerary_id}', ['uses' => 'TourController@Info']);

        Route::post('save/tour', ['middleware' => ['ajax', 'role:edit_tours', 'role:edit_directories'], 'uses' => 'TourController@Save']);

        Route::get('services/tour', ['middleware' => ['ajax', 'role:edit_tours'], 'uses' => 'JourneyServiceController@Index']);

        Route::get('delete/tour/{id}', ['middleware' => ['role:edit_tours', 'role:edit_directories'], 'uses' => 'TourController@Delete']);

        Route::get('copy/tour/{id}', ['middleware' => ['role:edit_tours', 'role:edit_directories'], 'uses' => 'TourController@Copy']);
    });

    Route::group(['middleware' => ['role:show_cruises', 'year']], function () {
        /*TOUR*/
        Route::get('cruises/setnumber', ['uses' => 'CruiseController@SetNumber']);

        Route::get('cruises', ['middleware' => 'auth', 'uses' => 'CruiseController@Index', 'as' => 'cruises_list']);

        Route::get('cruise/{item}', ['uses' => 'CruiseController@Info', 'as' => 'cruise']);

        Route::get('cruise/{item}/{itinerary_id}', ['uses' => 'CruiseController@Info']);

        Route::post('save/cruise', ['middleware' => ['ajax', 'role:edit_cruises', 'role:edit_directories'], 'uses' => 'CruiseController@Save']);

        Route::get('services/cruise', ['middleware' => ['ajax', 'role:edit_cruises'], 'uses' => 'JourneyServiceController@Index']);

        Route::get('delete/cruise/{id}', ['middleware' => ['role:edit_cruises', 'role:edit_directories'], 'uses' => 'CruiseController@Delete']);

        Route::get('copy/cruise/{id}', ['middleware' => ['role:edit_cruises', 'role:edit_directories'], 'uses' => 'CruiseController@Copy']);
    });

    Route::group(['middleware' => ['role:show_cruises|show_tours', 'role:edit_cruises|edit_tours', 'year', 'ajax']], function () {
        /*TOUR CRUISE Special*/
        Route::get('special/{item}/{id}', ['uses' => 'SpecialController@Info']);

        Route::get('special/{item}', ['uses' => 'SpecialController@Info']);

        Route::get('delete/special/{id}', ['uses' => 'SpecialController@Delete']);

        Route::post('save/special', ['uses' => 'SpecialController@Save']);
    });

    Route::group(['middleware' => ['role:show_reservations', 'year']], function () {
        /*TOUR*/
        Route::get('reservations/setnumber', ['uses' => 'ReservationController@SetNumber']);

        Route::get('reservations', ['uses' => 'ReservationController@Index', 'as' => 'reservations_list']);

        Route::post('model/reservations', ['uses' => 'ReservationController@GetReservations', 'middleware' => 'ajax']);

        Route::get('assignment/{journey}', ['uses' => 'ReservationController@CabinsAssignment', 'middleware' => ['role:show_cruise_bookings', 'role:show_cruise_bookings_assignment']]);

        Route::get('assignment/placement/{placement}', ['uses' => 'ReservationController@PlacementAssignment', 'middleware' => ['role:show_cruise_bookings', 'role:show_cruise_bookings_assignment', 'ajax']]);

        Route::get('payment/{item}', ['uses' => 'ReservationPaymentController@Info', 'middleware' => ['ajax', 'role:show_cruise_bookings|show_tour_bookings']]);

        Route::group(['middleware' => [
            'role:edit_reservations',
            'role:show_cruise_options|show_tour_options|show_cruise_bookings|show_tour_bookings|show_cruise_inquiries|show_tour_inquiries|show_cruise_bookings_canceled|show_tour_bookings_canceled|show_cruise_options_canceled|show_tour_options_canceled|show_cruise_inquiries_canceled|show_tour_inquiries_canceled',
            'role:edit_cruise_options|edit_tour_options|edit_cruise_bookings|edit_tour_bookings|edit_cruise_inquiries|edit_tour_inquiries|edit_cruise_options_canceled|edit_tour_options_canceled|edit_cruise_bookings_canceled|edit_tour_bookings_canceled|edit_cruise_inquiries_canceled|edit_tour_inquiries_canceled'
            ]], function () {
            
            Route::get('copy/reservation/{id}', ['uses' => 'ReservationController@Copy']);

            Route::post('reservation/status', ['uses' => 'ReservationController@SaveReservationField', 'middleware' => 'ajax']);

        });

        Route::group(['middleware' => 'role:show_cruise_options|show_tour_options|show_cruise_bookings|show_tour_bookings|show_cruise_inquiries|show_tour_inquiries'], function () {

            Route::get('reservation/noncompatibles/{id}', ['middleware' => 'ajax', 'uses' => 'ReservationPlacementController@NonCompatibles']);

            Route::get('placement/pax/{id}', ['middleware' => 'ajax', 'uses' => 'ReservationPaxController@PaxList']);

            Route::group(['middleware' => 'role:edit_reservations'], function () {

                Route::group(['middleware' => ['role:create_cruise_bookings', 'role:edit_cruise_bookings', 'role:show_cruise_bookings', 'role:show_cruise_bookings_assignment', 'ajax']], function () {

                    Route::post('save/assignment', ['uses' => 'ReservationController@SaveAssignment']);

                    Route::get('clear/assignment/{placement}', ['uses' => 'ReservationController@ClearAssignment']);

                });

                Route::group(['middleware' => ['role:create_cruise_options|create_tour_options|create_cruise_bookings|create_tour_bookings|create_cruise_inquiries|create_tour_inquiries']], function () {

                    Route::post('save/reservation', ['uses' => 'ReservationController@Save', 'middleware' => 'ajax']);

                    Route::post('save/payment', ['uses' => 'ReservationPaymentController@Save', 'middleware' => ['role:edit_cruise_bookings|edit_tour_bookings', 'ajax']]);

                    Route::get('delete/payment/{id}', ['uses' => 'ReservationPaymentController@Delete', 'middleware' => ['role:edit_cruise_bookings|edit_tour_bookings', 'ajax']]);

                    Route::group(['middleware' => ['role:edit_cruise_options|edit_tour_options|edit_cruise_bookings|edit_tour_bookings|edit_cruise_inquiries|edit_tour_inquiries']], function () {

                        Route::post('save/placement/{id}', ['middleware' => 'ajax', 'uses' => 'ReservationPaxController@SavePax']);

                        Route::get('cancel/reservation/{id}', ['uses' => 'ReservationController@Cancel']);

                        Route::get('convert/{id}', ['uses' => 'ReservationController@ConvertReservation']);

                        Route::group(['middleware' => 'role:show_cruise_bookings_rooms_cabins|show_tour_bookings_rooms_cabins|show_cruise_options_rooms_cabins|show_tour_options_rooms_cabins|show_cruise_inquiries_rooms_cabins|show_tour_bookings_inquiries_cabins'], function () {

                            Route::get('delete/pax/{id}', ['uses' => 'ReservationPaxController@Delete', 'middleware' => 'ajax']);

                            Route::get('delete/placement/{id}', ['uses' => 'ReservationPlacementController@Delete', 'middleware' => 'ajax']);

                        });
                    });
                });
            });
        });

        Route::group(['middleware' => 'role:show_cruise_bookings|show_tour_bookings|show_cruise_bookings_canceled|show_tour_bookings_canceled'], function () {

            Route::get('booking/{item}', ['uses' => 'ReservationController@Info', 'as' => 'booking']);

            Route::post('booking/{item}', ['uses' => 'ReservationController@Info', 'as' => 'booking.post', 'middleware' => 'ajax']);

        });

        Route::group(['middleware' => 'role:show_cruise_options|show_tour_options|show_cruise_options_canceled|show_tour_options_canceled'], function () {

            Route::get('option/{item}', ['uses' => 'ReservationController@Info', 'as' => 'option']);

            Route::post('option/{item}', ['uses' => 'ReservationController@Info', 'as' => 'option.post', 'middleware' => 'ajax']);

        });

        Route::group(['middleware' => 'role:show_cruise_inquiries|show_tour_inquiries|show_cruise_inquiries_canceled|show_tour_inquiries_canceled'], function () {

            Route::get('inquiry/{item}', ['uses' => 'ReservationController@Info', 'as' => 'inquiry']);

            Route::post('inquiry/{item}', ['uses' => 'ReservationController@Info', 'as' => 'inquiry.post', 'middleware' => 'ajax']);

        });
    });
});

Route::group(['middleware' => 'role:show_notifications', 'prefix' => 'notifications'], function () {

    Route::get('actions', ['uses' => 'NotificationController@Actions', 'middleware' => ['role:show_notifications_actions']]);

    Route::get('updates', ['uses' => 'NotificationController@Updates', 'middleware' => ['role:show_notifications_updates']]);

    Route::group(['middleware' => 'role:show_notifications_reminders', 'prefix' => 'reminders'], function () {

        Route::get('/', ['uses' => 'NotificationController@Reminders', 'as' => 'reminders']);

        Route::get('completed/{id}', ['uses' => 'NotificationController@SetCompleted', 'middleware' => 'role:edit_notifications_reminders']);

    });

    Route::get('setnumber', ['uses' => 'NotificationController@SetNumber', 'middleware' => ['role:show_notifications_actions|show_notifications_updates|show_notifications_reminders']]);
});

Route::group(['middleware' => 'role:show_administration'], function () {

    Route::group(['middleware' => 'role:show_users'], function () {
        /*USERS*/
        Route::get('users', ['uses' => 'UserController@Index', 'as' => 'users_list']);

        Route::get('users/setnumber', ['uses' => 'UserController@SetNumber']);

        Route::get('user/{item}', ['middleware' => 'ajax', 'uses' => 'UserController@Info']);

        Route::post('save/user', ['middleware' => ['ajax', 'role:edit_users'], 'uses' => 'UserController@Save']);

        Route::get('delete/user/{id}', ['middleware' => ['ajax', 'role:edit_users'], 'uses' => 'UserController@Delete']);

        Route::get('activity/user/{id}', ['middleware' => ['ajax', 'role:edit_users'], 'uses' => 'UserController@ChangeActivity']);
    });

    Route::group(['middleware' => 'role:show_mailing', 'prefix' => 'mailing'], function () {

        Route::get('', ['uses' => 'MassTemplateController@Index', 'as' => 'mailing']);

        Route::get('mass/{item}', ['uses' => 'MassTemplateController@Info', 'as' => 'mass']);

        Route::post('notification/field', ['uses' => 'CustomTemplateController@NotificationField', 'middleware' => ['role:edit_mailing', 'role:edit_administration']]);

        Route::get('notification/{id}', ['uses' => 'CustomTemplateController@Info', 'as' => 'notification']);

        Route::post('save', ['uses' => 'CustomTemplateController@SaveMail', 'middleware' => ['role:edit_mailing', 'role:edit_administration'], 'as' => 'email_template.save']);

        Route::post('send', ['uses' => 'MassTemplateController@Send', 'middleware' => ['role:edit_mailing', 'role:edit_administration'], 'as' => 'email_template.send']);
    });

    Route::group(['middleware' => 'role:show_reports', 'prefix' => 'reports'], function () {

        Route::get('templates', ['uses' => 'CustomTemplateController@Index', 'as' => 'reports.templates']);

        Route::get('documents', ['uses' => 'DocumentController@Index', 'as' => 'reports.documents']);

        Route::get('documents/setnumber', ['uses' => 'DocumentController@SetNumber']);

        Route::get('template/{id}', ['uses' => 'CustomTemplateController@ReportInfo', 'as' => 'reports.template']);

        Route::get('document', ['uses' => 'DocumentController@NewDocument', 'as' => 'reports.document']);

        Route::group(['middleware' => 'role:edit_reports'], function () {

            Route::post('save/template', ['uses' => 'CustomTemplateController@SaveReport']);

            Route::post('save/document', ['uses' => 'DocumentController@Save']);

            Route::get('delete/document/{id}', ['uses' => 'DocumentController@Delete']);

        });

        Route::get('download/{id}', ['uses' => 'DocumentController@Download']);

    });
});