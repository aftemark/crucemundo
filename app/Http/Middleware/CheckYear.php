<?php namespace App\Http\Middleware;

use Closure;

class CheckYear
{
    /**
     * Check if has session variable catalog_year_id.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if($request->session()->has('catalog_year_id'))
            return $next($request);
        else
            return redirect()->route('years');
    }
}