<?php

namespace App\Http\Traits;

use Auth;

trait ReservationTrait
{
    public function reservationInfo($reservation)
    {
        $this_journey_placement_name = $reservation->journey->type == 'tour' ? 'room' : 'cabin';
        $services_array = [];
        $journey_services = [];
        $placements_data = [];
        $services_data = [];
        $placements_data["total"]["sub_total"] = 0;
        $placements_data["fines"] = [];
        $services_data["total"] = 0;
        $reservation_pax_count = 0;

        $placement_key = $reservation->journey->type == 'tour' ? 'room' : 'cabin';

        $client_user_percent = 0;
        if ($reservation->client->type == 'b2b') {
            if ($reservation->journey->type == 'cruise') {
                $percent = $reservation->client->percents()->where('ship_id', $reservation->journey->ship_id)->first();
                if ($percent !== NULL)
                    $client_user_percent = $percent->percent;
            } else if ($reservation->journey->type == 'tour') {
                $percent = $reservation->client->percents()->where('hotel_id', $reservation->journey->hotel_id)->first();
                if ($percent !== NULL)
                    $client_user_percent = $percent->percent;
            }
        } else if ($reservation->client->type == 'b2c') {
            $client_user_percent = $reservation->client->percent;
        }

        foreach ($reservation->journey->services as $journeyService) {
            if (isset($journeyService->service->language) && $journeyService->service->language_id != $reservation->language_id)
                continue;

            if ($journeyService->display && isset($journeyService->service)) {
                if ($journeyService->optional) {
                    $journey_services[$journeyService->id]["value"] = $journeyService->service->price;
                    $reservation_services["optional"][] = $journeyService->id;
                } else if ($journeyService->included) {
                    $journey_services[$journeyService->id]["value"] = 0;
                    $reservation_services["included"][] = $journeyService->id;
                }
                $journey_services[$journeyService->id]["name"] = $journeyService->service->name;

                if (!$journeyService->service->all_pax_cabin)
                    $journey_services[$journeyService->id]["pax"] = [];
                else {
                    $journey_services[$journeyService->id]["placement"] = [];
                    if ($journeyService->service->multiply)
                        $journey_services[$journeyService->id]["multiply"] = true;
                }
            } else if ($journeyService->display && isset($journeyService->package)) {
                if ($journeyService->optional) {
                    $journey_services[$journeyService->id]["value"] = $journeyService->package->price;
                    $reservation_services["optional"][] = $journeyService->id;
                } else if ($journeyService->included) {
                    $journey_services[$journeyService->id]["value"] = 0;
                    $reservation_services["included"][] = $journeyService->id;
                }
                $journey_services[$journeyService->id]["name"] = $journeyService->package->name;

                if (!$journeyService->package->all_pax_cabin)
                    $journey_services[$journeyService->id]["pax"] = [];
                else {
                    $journey_services[$journeyService->id]["placement"] = [];
                    if ($journeyService->package->multiply)
                        $journey_services[$journeyService->id]["multiply"] = true;
                }
            }
        }

        $supplement_groups = [];

        foreach ($reservation->placements as $placement) {
            $has_bed = 0;
            $has_bed_type = '';
            if ($reservation->journey->type == 'tour') {
                $this_type = $placement->placement[$this_journey_placement_name];
                $price = $reservation->reservation_type == 'group' ? $placement->placement->price_rec : $placement->placement->price_net;
            } else {
                $this_type = $placement->cruise_type;
                $price = $reservation->reservation_type == 'group' ? $this_type->price_rec : $this_type->price_net;
            }

            $pax_count = $placement->reservation_paxes->count();
            $reservation_pax_count += $pax_count;

            foreach ($placement->services as $placement_service)
                $journey_services[$placement_service->journey_service_id]["placement"][$placement_service->reservation_placement_id] = 1;

            $pax_types = [];
            $pax_types["adult"] = 0;
            $pax_types["children"] = 0;
            $pax_types["infant"] = 0;

            foreach ($placement->reservation_paxes as $placement_reservation_pax) {
                foreach ($placement_reservation_pax->services as $placement_reservation_pax_service)
                    $journey_services[$placement_reservation_pax_service->journey_service_id]["pax"][$placement->id][$placement_reservation_pax->id] = 1;

                if ($placement_reservation_pax->bed) {
                    $has_bed = 1;
                    $has_bed_type = $placement_reservation_pax->type;
                }

                $pax_types[$placement_reservation_pax->type]++;
            }

            $places_amount = $this_type->type->pax_amount +
                ($this_type->type->bed && $placement->bed ? 1 : 0) -
                $pax_types["adult"] -
                $pax_types["children"];

            if ($pax_count) {
                $add_percent = 100;
                if ($places_amount && isset($placement->placement->group)) {
                    if (!isset($supplement_groups[$placement->placement->group->id]))
                        $supplement_groups[$placement->placement->group->id] = $placement->placement->group->placements_amount;
                    if ($supplement_groups[$placement->placement->group->id]) {
                        $supplement_groups[$placement->placement->group->id]--;
                        $add_percent = $placement->placement->group->percent;
                    }
                }

                $pax_line = [];
                foreach ($pax_types as $pax_type => $pax_type_count)
                    if ($pax_type_count)
                        $pax_line[] = ($has_bed && $pax_type == $has_bed_type ? $pax_type_count - 1 : $pax_type_count) . ' ' . $pax_type;

                $placements_data["ids"][$placement->id]["description_first"] = $this_type->type->type->name . ' ' . $placement_key . ' for ' . implode(" + ", $pax_line) . ($has_bed ? ' + 1 additional bed for 1 ' . $has_bed_type : '');
                $placements_data["ids"][$placement->id]["room_total"] = number_format($pax_types["adult"] * $price + $pax_types["children"] * $price * 0.7 + ($places_amount * $price * $add_percent / 100), 2);
                $placements_data["ids"][$placement->id]["description_second"] = $pax_types["adult"] . ' adult * ' . $price . ($pax_types["children"] ? ' + ' . $pax_types["children"] . ' children * ' . $price . ' EUR*70%' : '') . ($places_amount ? ' + ' . $places_amount . ' * ' . $price . ' EUR*' . $add_percent . '%' : '') . ' = ' . $placements_data["ids"][$placement->id]["room_total"] . ' EUR';

                if ($reservation->client->type == 'b2b' && $client_user_percent > 0 && $reservation->reservation_type == 'fit') {
                    $placements_data["ids"][$placement->id]["commission_discount"] = 'Commission ' . $client_user_percent . '% = ' . $placements_data["ids"][$placement->id]["room_total"] . ' EUR * ' . $client_user_percent . '% = ' . ($commission = number_format(($placements_data["ids"][$placement->id]["room_total"] * $client_user_percent)/100, 2)) . ' EUR';
                    $placements_data["ids"][$placement->id]["to_pay"] = 'To pay: ' . $placements_data["ids"][$placement->id]["room_total"] . ' EUR - ' . $commission . ' EUR = ' . number_format($placements_data["ids"][$placement->id]["room_total"] - $commission, 2) . ' EUR';
                } else if ($reservation->client->type == 'b2c' && $reservation->client->percent > 0) {
                    $placements_data["ids"][$placement->id]["commission_discount"] = 'Discount ' . $reservation->client->percent . '% = ' . $placements_data["ids"][$placement->id]["room_total"] . ' EUR * ' . $reservation->client->percent . '% = ' . ($discount = number_format(($placements_data["ids"][$placement->id]["room_total"] * $reservation->client->percent)/100, 2)) . ' EUR';
                    $placements_data["ids"][$placement->id]["to_pay"] = 'To pay: ' . $placements_data["ids"][$placement->id]["room_total"] . ' EUR - ' . $discount . ' EUR = ' . number_format($placements_data["ids"][$placement->id]["room_total"] - $discount, 2) . ' EUR';
                } else if ($reservation->client->type == 'b2b') {
                    $placements_data["ids"][$placement->id]["commission_discount"] = 'Commission 0% = ' . $placements_data["ids"][$placement->id]["room_total"] . ' EUR';
                    $placements_data["ids"][$placement->id]["to_pay"] = 'To pay: ' . $placements_data["ids"][$placement->id]["room_total"] . ' EUR';
                } else if ($reservation->client->type == 'b2c') {
                    $placements_data["ids"][$placement->id]["commission_discount"] = 'Discount 0% = ' . $placements_data["ids"][$placement->id]["room_total"] . ' EUR';
                    $placements_data["ids"][$placement->id]["to_pay"] = 'To pay: ' . $placements_data["ids"][$placement->id]["room_total"] . ' EUR';
                }

                $placements_data["total"]["sub_total"] += $placements_data["ids"][$placement->id]["room_total"];

                foreach($journey_services as $journeyServiceID => $journeyService) {
                    if (isset($journeyService["placement"])) {
                        if(!empty($journeyService["placement"][$placement->id]) && isset($journeyService["multiply"])) {
                            $services_array["multiply"][$journeyServiceID][$placement->id]["count"] = $placement->reservation_paxes->count();
                            $services_array["multiply"][$journeyServiceID][$placement->id]["price"] = isset($journeyService["multiply"]) ? $journeyService["value"]/$pax_count : $journeyService["value"];
                        }
                        if (!empty($journeyService["placement"][$placement->id])) {
                            if(!isset($journeyService["multiply"])) $services_array["normal"][$journeyServiceID] = $pax_count;
                        }
                    } else if (isset($journeyService["pax"])) {
                        foreach ($placement->reservation_paxes as $placement_reservation_pax) {
                            if (!empty($journeyService["pax"][$placement->id][$placement_reservation_pax->id]))
                                $services_array["normal"][$journeyServiceID] = isset($services_array["normal"][$journeyServiceID]) ? $services_array["normal"][$journeyServiceID] + 1 : 1;
                        }
                    }
                }
            }
        }

        $fines = [];
        foreach ($reservation->fines as $fine)
            if ($fine->type != 'cancel')
                $fines[$fine->type] = isset($fines[$fine->type]) ? $fines[$fine->type] + $fine->amount : $fine->amount;

        foreach($fines as $fine_type => $fine)
            if ($fine_type == 'pax_number')
                $placements_data["fines"][] = 'Number of pax was updated, with fine - ' . $fine . ' EUR';
            else if ($fine_type == 'placements_number')
                $placements_data["fines"][] = 'Number of placements was updated, with fine - ' . $fine . ' EUR';
            else if ($fine_type == 'pax_types')
                $placements_data["fines"][] = 'Pax types were replaced, with fine - ' . $fine . ' EUR';
            else if ($fine_type == 'pax_details')
                $placements_data["fines"][] = 'Pax details were replaced, with fine - ' . $fine . ' EUR';


        if ($reservation->client->type == 'b2b' && $client_user_percent > 0 && $reservation->reservation_type == 'fit') {
            $placements_data["total"]["commission_discount"] = 'Commission ' . $client_user_percent . '% = ' . $placements_data["total"]["sub_total"] . ' EUR * ' . $client_user_percent . '% = ' . ($commission = number_format(($placements_data["total"]["sub_total"] * $client_user_percent)/100, 2)) . ' EUR';
            $placements_data["total"]["total"] = number_format($placements_data["total"]["sub_total"] - $commission, 2);
            $placements_data["total"]["to_pay"] = 'To pay: ' . $placements_data["total"]["sub_total"] . ' EUR - ' . $commission . ' EUR';
            foreach ($fines as $fine) {
                $placements_data["total"]["to_pay"] .= ' + ' . number_format($fine, 2) . ' EUR';
                $placements_data["total"]["total"] += $fine;
            }
            $placements_data["total"]["to_pay"] .= ' = ' . number_format($placements_data["total"]["total"], 2) . ' EUR';
        } else if ($reservation->client->type == 'b2c' && $reservation->client->percent > 0) {
            $placements_data["total"]["commission_discount"] = 'Discount ' . $reservation->client->percent . '% = ' . $placements_data["total"]["sub_total"] . ' EUR * ' . $reservation->client->percent . '% = ' . ($discount = number_format(($placements_data["total"]["sub_total"] * $reservation->client->percent)/100, 2)) . ' EUR';
            $placements_data["total"]["total"] = number_format($placements_data["total"]["sub_total"] - $discount, 2);
            $placements_data["total"]["to_pay"] = 'To pay: ' . $placements_data["total"]["sub_total"] . ' EUR - ' . $discount . ' EUR';
            foreach ($fines as $fine) {
                $placements_data["total"]["to_pay"] .= ' + ' . number_format($fine, 2) . ' EUR';
                $placements_data["total"]["total"] += $fine;
            }
            $placements_data["total"]["to_pay"] .= ' = ' . number_format($placements_data["total"]["total"], 2) . ' EUR';
        } else if ($reservation->client->type == 'b2b') {
            $placements_data["total"]["commission_discount"] = 'Commission 0% = ' . $placements_data["total"]["sub_total"] . ' EUR';
            $placements_data["total"]["to_pay"] = 'To pay: ' . $placements_data["total"]["sub_total"] . ' EUR';
            $placements_data["total"]["total"] = $placements_data["total"]["sub_total"];
            foreach ($fines as $fine) {
                $placements_data["total"]["to_pay"] .= ' + ' . number_format($fine, 2) . ' EUR';
                $placements_data["total"]["total"] += $fine;
            }
            if (!empty($fines))
                $placements_data["total"]["to_pay"] .= ' = ' . number_format($placements_data["total"]["total"], 2) . ' EUR';
        } else if ($reservation->client->type == 'b2c') {
            $placements_data["total"]["commission_discount"] = 'Discount 0% = ' . $placements_data["total"]["sub_total"] . ' EUR';
            $placements_data["total"]["to_pay"] = 'To pay: ' . $placements_data["total"]["sub_total"] . ' EUR';
            $placements_data["total"]["total"] = $placements_data["total"]["sub_total"];
            foreach ($fines as $fine) {
                $placements_data["total"]["to_pay"] .= ' + ' . number_format($fine, 2) . ' EUR';
                $placements_data["total"]["total"] += $fine;
            }
            if (!empty($fines))
                $placements_data["total"]["to_pay"] .= ' = ' . number_format($placements_data["total"]["total"], 2) . ' EUR';
        }

        /*$services_sum = 0;
        if (isset($reservation_services["optional"])) {
            foreach($reservation_services["optional"] as $optional_service_id) {
                if (isset($services_array["normal"][$optional_service_id]))
                    $services_sum += $journey_services[$optional_service_id]["value"] * $services_array["normal"][$optional_service_id];
                else if (isset($services_array["multiply"][$optional_service_id]))
                    foreach ($services_array["multiply"][$optional_service_id] as $multiply_element)
                        $services_sum += $multiply_element["price"] * $multiply_element["count"];
            }
        }*/
        if (isset($reservation_services["included"])) {
            foreach ($reservation_services["included"] as $included_service_id) {
                if (isset($services_array["normal"][$included_service_id])) {
                    $services_data["included"][] = $journey_services[$included_service_id]["name"] . ' * ' . $services_array["normal"][$included_service_id] . ' pax';
                } else if (isset($services_array["multiply"][$included_service_id])) {
                    foreach ($services_array["multiply"][$included_service_id] as $multiply_element) {
                        $services_data["included"][] = $journey_services[$included_service_id]["name"] . ' * ' . $multiply_element["count"] . ' pax';
                    }
                }
            }
        }

        if (isset($reservation_services["optional"])) {
            foreach($reservation_services["optional"] as $optional_service_id) {
                $sub_price = 0;
                if (isset($services_array["normal"][$optional_service_id])) {
                    $services_data["included"][] = $journey_services[$optional_service_id]["name"] . ' * ' . $services_array["normal"][$optional_service_id] . ' pax';
                    $services_data["included"][] = $journey_services[$optional_service_id]["value"] . ' EUR * ' . $services_array["normal"][$optional_service_id] . ' = ' . ($sub_price += $journey_services[$optional_service_id]["value"] * $services_array["normal"][$optional_service_id]) . ' EUR';
                } else if (isset($services_array["multiply"][$optional_service_id])) {
                    foreach($services_array["multiply"][$optional_service_id] as $multiply_element) {
                        $services_data["included"][] = $journey_services[$optional_service_id]["name"] . ' * ' . $multiply_element["count"] . ' pax';
                        $services_data["included"][] = $multiply_element["price"] . ' * ' . $multiply_element["count"] . ' = ' . ($sub_price += $multiply_element["price"] * $multiply_element["count"]) . ' EUR';
                    }
                }
                $services_data["total"] += $sub_price;
            }
        }

        return [$placements_data, $services_data, $journey_services, $reservation_pax_count];
    }

    public function reservationsList($model, $request = NULL)
    {
        $reservations = $model->reservations();

        if (isset($request)) {
            if ($request->has('reservation_type') && in_array($request->reservation_type, ['inquiry', 'option', 'booking'])) {
                $reservations = $reservations->where('type', $request->reservation_type);
            }
        }

        $reservations = $reservations->get();
        foreach ($reservations as $reservation) {
            if ($reservation->type == 'booking') {
                $reservation->role_name = 'bookings';
            } else if ($reservation->type == 'inquiry') {
                $reservation->role_name = 'inquiries';
            } else if ($reservation->type == 'option') {
                $reservation->role_name = 'options';
            }

            $reservation->pax_number = 0;
            foreach ($reservation->placements as $placement)
                $reservation->pax_number += $placement->adults + $placement->childrens + $placement->infants;
        }

        return $reservations;
    }

    public function placementPrice($placement)
    {
        $price = $placement->reservation->reservation_type == 'group' ? $placement->cruise_type->price_rec : $placement->cruise_type->price_net;

        $pax_count = $placement->reservation_paxes->count();
        $amount = 0;
        $services_names = [];
        foreach ($placement->services as $placement_service) {
            $this_service = isset($placement_service->service->service) ? $placement_service->service->service : $placement_service->service->package;
            $services_names[] = $this_service->name;
            if ($placement_service->service->optional)
                $amount += $this_service->multiply ? $this_service->price : $this_service->price * $pax_count;
        }

        foreach ($placement->reservation_paxes as $reservation_pax) {
            foreach ($reservation_pax->services as $pax_service) {
                $this_service = isset($pax_service->service->service) ? $pax_service->service->service : $pax_service->service->package;
                $services_names[$this_service->id] = $this_service->name;
                if ($pax_service->service->optional)
                    $amount += $this_service->price;
            }
        }
        $places_amount = $placement->cruise_type->type->pax_amount + ($placement->cruise_type->type->bed && $placement->bed ? 1 : 0) - $placement->adults - $placement->childrens;

        $add_percent = 100;
        if ($places_amount && isset($placement->placement->group)) {
            if (!isset($supplement_groups[$placement->placement->group->id]))
                $supplement_groups[$placement->placement->group->id] = $placement->placement->group->placements_amount;
            if ($supplement_groups[$placement->placement->group->id]) {
                $supplement_groups[$placement->placement->group->id]--;
                $add_percent = $placement->placement->group->percent;
            }
        }

        $placement->amount = $amount + number_format($placement->adults * $price + $placement->childrens * $price * 0.7 + ($places_amount * $price * $add_percent / 100), 2);;
        $placement->services_names = implode(', ', array_unique($services_names));
        return $placement;
    }

    public function reservationPercent($reservation)
    {
        $days_diff = floor((strtotime($reservation->journey->depart_date) - time()) / (60 * 60 * 24));
        $fee = $reservation->journey->fees()->where('from', '<=', $days_diff)->where('to', '>=', $days_diff)->first();

        return $fee !== NULL ? $fee->percent : 100;
    }


}