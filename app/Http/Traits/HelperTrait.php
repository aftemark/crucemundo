<?php

namespace App\Http\Traits;

use Mail;
use Mustache_Engine;
use App\Tags\ClientData;
use App\Notification;
use App\Fine;
use App\Journey;
use App\Reservation;
use App\CustomTemplate;
use App\User;

trait HelperTrait
{
    public function notify($user_id = NULL, $reservation_id = NULL, $type, $text, $journey_service_id = NULL, $reminder_type = NULL, $email_data = [])
    {
        if (!empty($email_data)) {
            $template = CustomTemplate::where('type', 'mail')->where('custom_type', $email_data['type'])->firstOrFail();
            if ($template->status) {
                $email_data += [
                    'subject' => $template->subject,
                    'emails' => []
                ];

                $reservation = $email_data['reservation'];
                if ($template->send_copy)
                    $email_data['emails'] = User::whereHas('roles', function($q) {
                        $q->where('name', 'edit_administration');
                    })->pluck('email');
                if ($reservation->send_email && $template->custom_type != 'overbooking') {
                    if ($reservation->client_id)
                        $email_data['emails'][] = $reservation->client->email;
                    if ($reservation->client_user_id)
                        $email_data['emails'][] = $reservation->client_user->email;
                }

                if (!empty($email_data['emails'])) {
                    $m = new Mustache_Engine;
                    $email_data['content'] = $m->render($template->text);
                    Mail::send('emails.mass', ['data' => $email_data], function ($m) use ($email_data) {
                        $mTo = $m->to($email_data['emails'][0]);
                        foreach ($email_data['emails'] as $email) {
                            if ($email_data['emails'][0] == $email)
                                continue;
                            $mTo->cc($email);
                        }
                        $mTo->subject($email_data['subject']);
                    });
                }
            } else
                $email_data = [];
        }

        if (empty($email_data) || !empty($email_data['emails'])) {
            $notification = new Notification;
            $notification->user_id = $user_id;
            $notification->reservation_id = $reservation_id;
            $notification->journey_service_id = $journey_service_id;
            $notification->type = $type;
            $notification->reminder_type = $reminder_type;
            $notification->text = $text;

            $notification->save();

            return $notification->id;
        }
    }

    public function fine($reservation_id, $notification_id = NULL, $type, $amount)
    {
        $notification = new Fine;
        $notification->reservation_id = $reservation_id;
        $notification->notification_id = $notification_id;
        $notification->type = $type;
        $notification->amount = $amount;

        $notification->save();
    }

    public static function deleteCheckModel($model, $relations)
    {
        $models = [];
        foreach ($relations as $relation => $relationName)
            if ($model[$relation]->count())
                $models[] = $relationName;

        return !empty($models) ? "Can't delete. There are many related entities (" . implode(', ', $models) . ")." : false;
    }

    public static function journeyCopy(Journey $journey, $fields = NULL, $newYearID = NULL, $oldServices = [])
    {
        $newJourney = new Journey;
        if (is_array($fields))
            foreach ($fields as $field)
                $newJourney[$field] = $journey[$field];
        else
            $newJourney = $journey->replicate();

        if ($newYearID !== NULL)
            $newJourney->year_id = $newYearID;

        $newJourney->save();

        $groups = [];
        $types = [];
        $services = [];

        foreach ($journey->fees as $fee) {
            $fee->journey_id = $newJourney->id;
            $newFee = $fee->replicate();
            $newFee->save();
        }

        foreach ($journey->groups as $group) {
            $group->journey_id = $newJourney->id;
            $newGroup = $group->replicate();
            $newGroup->save();
            $groups[$group->id] = $newGroup->id;
        }

        foreach ($journey->types as $type) {
            $type->journey_id = $newJourney->id;
            $newType = $type->replicate();
            $newType->save();
            $types[$type->id] = $newType->id;
        }

        foreach ($journey->services as $service) {
            if (!empty($newServices))
                $service->service_id = $newServices[$service->service_id ? 'services' : 'packages'][$service->service_id];
            $service->journey_id = $newJourney->id;
            $service->release_date = NULL;
            $service->remind = NULL;
            $newService = $service->replicate();
            $newService->save();
            $services[$service->id] = $newService->id;
        }

        foreach ($journey->nonDays as $nonDay) {
            $nonDay->journey_id = $newJourney->id;
            $newNonDay = $nonDay->replicate();
            $newNonDay->save();
            foreach ($nonDay->services as $nonService) {
                $nonService->non_compatible_id = $newNonDay->id;
                $nonService->journey_service_id = $services[$nonService->journey_service_id];
                $newNonService = $nonService->replicate();
                $newNonService->save();
            }
        }

        foreach ($journey->placements as $placement) {
            $placement->journey_id = $newJourney->id;
            if ($placement->supplement_group_id)
                $placement->supplement_group_id = $groups[$placement->supplement_group_id];
            if ($placement->cruise_type_id)
                $placement->cruise_type_id = $types[$placement->cruise_type_id];
            $placement->reservation_placement_id = NULL;
            $newPlacement = $placement->replicate();
            $newPlacement->save();
        }

        return $newJourney->id;
    }

    public static function reservationCopy(Reservation $reservation, $fields)
    {
        $newReservation = new Reservation;
        foreach ($fields as $field)
            $newReservation[$field] = $reservation[$field];

        $newReservation->save();

        foreach ($reservation->placements as $placement) {
            $placement->reservation_id = $newReservation->id;
            $newPlacement = $placement->replicate();
            $newPlacement->save();
            foreach ($placement->services as $nonService) {
                $nonService->non_compatible_id = $newNonDay->id;
                $nonService->journey_service_id = $services[$nonService->journey_service_id];
                $newNonService = $nonService->replicate();
                $newNonService->save();
            }
        }

        /*$groups = [];
        $types = [];
        $services = [];

        foreach ($reservation->fees as $fee) {
            $fee->journey_id = $newReservation->id;
            $newFee = $fee->replicate();
            $newFee->save();
        }

        foreach ($reservation->groups as $group) {
            $group->journey_id = $newReservation->id;
            $newGroup = $group->replicate();
            $newGroup->save();
            $groups[$group->id] = $newGroup->id;
        }

        foreach ($reservation->types as $type) {
            $type->journey_id = $newReservation->id;
            $newType = $type->replicate();
            $newType->save();
            $types[$type->id] = $newType->id;
        }

        foreach ($reservation->services as $service) {
            $service->journey_id = $newReservation->id;
            $service->release_date = NULL;
            $service->remind = NULL;
            $newService = $service->replicate();
            $newService->save();
            $services[$service->id] = $newService->id;
        }

        foreach ($reservation->nonDays as $nonDay) {
            $nonDay->journey_id = $newReservation->id;
            $newNonDay = $nonDay->replicate();
            $newNonDay->save();
            foreach ($nonDay->services as $nonService) {
                $nonService->non_compatible_id = $newNonDay->id;
                $nonService->journey_service_id = $services[$nonService->journey_service_id];
                $newNonService = $nonService->replicate();
                $newNonService->save();
            }
        }

        foreach ($reservation->placements as $placement) {
            $placement->journey_id = $newReservation->id;
            if ($placement->supplement_group_id)
                $placement->supplement_group_id = $groups[$placement->supplement_group_id];
            if ($placement->cruise_type_id)
                $placement->cruise_type_id = $types[$placement->cruise_type_id];
            $placement->reservation_placement_id = NULL;
            $newPlacement = $placement->replicate();
            $newPlacement->save();
        }*/

        return $newReservation->id;
    }

    public function checkReservationCopy(Reservation $reservation, $allow = true)
    {
        $journey_type = $reservation->journey->type == 'cruise' ? 'cruise_type_id' : 'placement_id';
        $placementsAmount = [];
        $journeyServicesCount = [];
        
        foreach ($reservation->journey->services as $journeyService)
            $journeyServicesCount[$journeyService->id] = $journeyService[$journeyService->service_id ? 'service' : 'package']->max_pax;

        foreach ($reservation->journey->placements as $journey_placement) {
            if ($reservation->journey->type == 'tour') {
                if (isset($placementsAmount[$journey_placement->id]))
                    $placementsAmount[$journey_placement->id] += $journey_placement->amount;
                else
                    $placementsAmount[$journey_placement->id] = $journey_placement->amount;
            } else if ($reservation->journey->type == 'cruise' && $journey_placement->display) {
                $placementsAmount[$journey_placement->cruise_type_id] = isset($placementsAmount[$journey_placement->cruise_type_id]) ? $placementsAmount[$journey_placement->cruise_type_id] + 1 : 1;
                /*if (!isset($placementsAmount[$journey_placement->cruise_type_id]))
                    $placementsAmount[$journey_placement->cruise_type_id] = $journey_placement->amount ? $journey_placement->amount : 1;
                else
                    $placementsAmount[$journey_placement->cruise_type_id] += $journey_placement->amount ? $journey_placement->amount : 1;*/
            }
        }

        if ($reservation->journey->type == 'cruise')
            foreach ($reservation->journey->types as $cruise_type)
                $placementsAmount[$cruise_type->id] += $cruise_type->overbooking;

        foreach ($reservation->journey->reservations as $other_reservation)
            if ($other_reservation->type != 'inquiry' && ($allow || $reservation->id != $other_reservation->id))
                foreach ($other_reservation->placements as $otherReservationPlacement) {
                    $pax_count = $otherReservationPlacement->reservation_paxes->count();
                    --$placementsAmount[$otherReservationPlacement[$journey_type]];
                    foreach ($otherReservationPlacement->services as $otherReservationPlacementService)
                        $journeyServicesCount[$otherReservationPlacementService->journey_service_id] -= $pax_count;
                    foreach ($otherReservationPlacement->reservation_paxes as $otherReservationPlacementPax)
                        foreach ($otherReservationPlacementPax->services as $otherReservationPlacementPaxService)
                            $journeyServicesCount[$otherReservationPlacementPaxService->journey_service_id]--;
                }

        return [$placementsAmount, $journeyServicesCount];
    }

    public function roleName($type)
    {
        if ($type == 'booking')
            return 'bookings';
        else if ($type == 'inquiry')
            return 'inquiries';
        else
            return 'options';
    }
}