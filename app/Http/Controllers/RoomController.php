<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Room;
use App\PlacementType;
use App\TypeCategory;
use App\Http\Requests;

class RoomController extends Controller
{
    public function Info ($item)
    {
        $types = PlacementType::all();

        if ($item != 'new') {
            $room = Room::find($item);
            $room->categories = array();
            $room->types = array();

            foreach($types as $type) {
                foreach ($type->categories as $category) {
                    if($category->typename == 'hotel') {
                        if($type->id == $room->type->type->id)
                            $room->types = [$type->id => $type->name] + $room->types;
                        else
                            $room->types = $room->types + [$type->id => $type->name];
                        if ($type->id == $room->type->type->id) {
                            if ($category->id == $room->type_category_id)
                                $room->categories = [$category->id => $category->name] + $room->categories;
                            else
                                $room->categories = $room->categories + [$category->id => $category->name];
                        }
                    }
                }
            }

        } else {
            $room = new Room;
            $roomTypes = array();
            //$roomCategories = array();

            //$firstCategories = array();
            $search = false;
            
            foreach($types as $type) {
                foreach ($type->categories as $category) {
                    if($category->typename == 'hotel') {
                        $roomTypes[$type->id] = $type->name;
                        if (!$search) {
                            $search = true;
                            $firstCategories = $type->categories;
                        }
                    }
                }
            }

            /*foreach ($firstCategories as $firstCategory)
                if($firstCategory->typename == 'hotel') {
                    $roomCategories = $roomCategories + [$firstCategory->id => $firstCategory->name];
                }*/

            $room->types = $roomTypes;
            //$room->categories =  $roomCategories;
            $room->categories =  array();

            $room->types = ['false' => 'Select category'] + $room->types;
            $room->categories =  ['false' => 'Select ID'] + $room->categories;
        }

        return view('rooms.form', array('data' => $room));
    }

    public function Save (Request $request)
    {
        $validate = [
            'area' => 'required|between:0,999.99',
            'type_category' => 'required|exists:type_categories,id',
            'description' => 'max:9999'
        ];
        if ($request->has('id')) {
            $validate = $validate + array('id' => 'required|exists:rooms,id');
            $room = Room::find($request->id);
        } else {
            $room = new Room;
        }
        $this->validate($request, $validate);

        $room->area = $request->area;
        $room->type_category_id = $request->type_category;
        $room->hotel_id = $request->hotel;
        $room->description = $request->description;
        $room->save();

        return $room->id;
    }

    public function Delete($id)
    {
        $room = Room::findOrFail($id);
        if ($return = \App\Http\Traits\HelperTrait::deleteCheckModel($room, ['placements' => 'Placement']))
            return response($return, 422);
        else
            $room->delete();
    }
}