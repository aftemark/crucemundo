<?php

namespace App\Http\Controllers;

use App\JourneyService;
use Illuminate\Http\Request;
use App\Reservation;
use App\Journey;
use App\Client;
use App\Language;
use App\ClientUser;
use App\Placement;
use App\ReservationPlacement;
use App\ReservationPax;
use App\ReservationPlacementService;
use App\ReservationPaxService;
use App\CruiseType;
use App\Http\Requests;
use Auth;
use Validator;
use Config;
use App\Http\Traits\ReservationTrait;
use App\Http\Traits\HelperTrait;

class ReservationController extends Controller
{
    use ReservationTrait;
    use HelperTrait;

    public function __construct(Request $request)
    {
        $this->year = $request->session()->get('catalog_year_id');
    }
    
    public function Index(Request $request)
    {
        $this->validate($request, [
            'journey' => 'in:cruise,tour',
            'reservation_type' => 'in:inquiry,option,booking',
            'field' => 'in:journey.code,journey.itinerary.name,client.type,language.name,release_date,reservation_type,all',
            'order' => 'in:asc,desc',
            'order_field' => 'in:journey.code,journey.itinerary.name,client.type,language.name,release_date,reservation_type',
        ]);
        $data = Reservation::where('year_id', $this->year);

        if ($request->input('canceled', NULL) === 'true')
            $data = $data->withTrashed()->where('deleted_at', '!=', NULL);

        if ($request->has('journey'))
            $data = $data->with('Journey')->whereHas('Journey', function ($q) use ($request) {
                $q->where('type', $request->journey);
            });

        if ($request->has('reservation_type'))
            $data = $data->where('type', $request->reservation_type);

        if ($request->has('search') && $request->has('field')) {
            if ($request->type == 'all')
                $data = $data->search($request->search);
            else
                $data = $data->search($request->search, [$request->field]);
        }

        if ($request->has('order') && $request->has('order_field')) {
            if ($request->order_field == 'journey.code')
                $data->select('reservations.*')->leftJoin('journeys', 'journey_id', '=', 'journeys.id')->orderBy('journeys.code', $request->order);
            else if ($request->order_field == 'journey.itinerary.name')
                $data->select('reservations.*')->leftJoin('journeys', 'journey_id', '=', 'journeys.id')->leftJoin('itineraries', 'journeys.itinerary_id', '=', 'itineraries.id')->orderBy('itineraries.name', $request->order);
            else if ($request->order_field == 'client.type')
                $data->select('reservations.*')->leftJoin('clients', 'client_id', '=', 'clients.id')->orderBy('clients.type', $request->order);
            else if ($request->order_field == 'language.name')
                $data->select('reservations.*')->leftJoin('languages', 'language_id', '=', 'languages.id')->orderBy('languages.name', $request->order);
            else
                $data->orderBy($request->order_field, $request->order);
        } else
            $data->orderBy('updated_at', 'desc');

        $data = $data->paginate($request->session()->get('reservations_number', 10));

        $services_amount = [];
        foreach ($data as $element) {
            if ($element->type == 'booking') {
                $element->role_name = 'bookings';
            } else if ($element->type == 'inquiry') {
                $element->role_name = 'inquiries';
            } else if ($element->type == 'option') {
                $element->role_name = 'options';
            }

            foreach ($element->placements as $element_placement) {
                foreach ($element_placement->services as $element_placement_service)
                    if ($element_placement_service->service->optional)
                        $services_amount[$element->id] = isset($services_amount[$element->id]) ? $services_amount[$element->id] + 1 : 1;

                foreach ($element_placement->reservation_paxes as $element_placement_pax)
                    foreach ($element_placement_pax->services as $element_placement_pax_service)
                        if ($element_placement_pax_service->service->optional)
                            $services_amount[$element->id] = isset($services_amount[$element->id]) ? $services_amount[$element->id] + 1 : 1;
            }
        }

        /*$data = $data->filter(function($element) {
            return !Auth::user()->hasRole('show_' . $element->journey->type . '_' . $element->role_name);
        });*/

        return view('reservations.list', array('data' => $data, 'services_amount' => $services_amount));
    }

    public function Info(Request $request, $item)
    {
        $reservation = $item == 'new' ? new Reservation : Reservation::withTrashed()->whereYearId($request->session()->get('catalog_year_id'))->findOrFail($item);

        $route_name = $request->route()->getName();

        if ($reservation->id && !in_array($route_name, [$reservation->type, $reservation->type . '.post']))
            return redirect()->back();

        $reservation_info = [];
        if ($route_name == 'booking' || $route_name == 'booking.post') {
            $reservation_info["name"] = 'Booking';
            $reservation_info["type_value"] = 'booking';
            $reservation_info["role_name"] = 'bookings';
        } else if ($route_name == 'inquiry' || $route_name == 'inquiry.post') {
            $reservation_info["name"] = 'Inquiry';
            $reservation_info["type_value"] = 'inquiry';
            $reservation_info["role_name"] = 'inquiries';
        } else {
            $reservation_info["name"] = 'Option';
            $reservation_info["type_value"] = 'option';
            $reservation_info["role_name"] = 'options';
        }

        if (isset($reservation->journey->type) && $reservation->journey->type == 'tour') {
            $reservation_info["journey_type"] = 'tour';
            $reservation_info["journey_accommodation"] = 'hotel';
            $reservation_info["placement_name"] = 'Room';
            $reservation_info["placement_key"] = 'room';
        } else if (isset($reservation->journey->type) && $reservation->journey->type == 'cruise') {
            $reservation_info["journey_type"] = 'cruise';
            $reservation_info["journey_accommodation"] = 'ship';
            $reservation_info["placement_name"] = 'Cabin';
            $reservation_info["placement_key"] = 'cabin';
        }
        $reservation_info["placements_number"] = $reservation->placements->count();
        $reservation_info["cancel_fine"] = NULL;
        $reservation_info["fines"] = [];
        
        if (($reservation->id && !Auth::user()->hasRole('show_' . $reservation_info["journey_type"] . '_' . $reservation_info["role_name"] . ($reservation->deleted_at ? '_canceled' : ''))) ||
            (!$reservation->id && (!Auth::user()->hasRole('show_cruise_' . $reservation_info["role_name"]) || !Auth::user()->hasRole('show_tour_' . $reservation_info["role_name"]))))
            return redirect()->back();

        $client_type = isset($reservation->client->type) ? $reservation->client->type : NULL ;
        $journey_type = isset($reservation->journey->type) ? $reservation->journey->type : NULL;
        $reservation_client_users = isset($reservation->client->users) ? $reservation->client->users() : NULL;

        $placements = 0;
        $max_pax_number = 0;
        $picked_categories = [];
        $reservation_categories = [];
        $cruise_types = [];
        $reservation_placements = [];
        $journey_services = [];
        $cabins_info = [];
        $validate = [];
        $reservation_services = [];
        $placements_data = [];
        $services_data = [];
        $debts = [];
        $client_user_percent = 0;

        $this_journey_placement_name = $journey_type == 'tour' ? 'room' : 'cabin';

        ini_set("log_errors", 1);
        ini_set("error_log", "log.log");

        if (isset($reservation->journey)) {
            if ($client_type) {
                if ($client_type == 'b2b') {
                    if ($reservation->journey->type == 'cruise') {
                        $percent = $reservation->client->percents()->where('ship_id', $reservation->journey->ship_id)->first();
                        if ($percent !== NULL)
                            $client_user_percent = $percent->percent;
                    } else if ($reservation->journey->type == 'tour') {
                        $percent = $reservation->client->percents()->where('hotel_id', $reservation->journey->hotel_id)->first();
                        if ($percent !== NULL)
                            $client_user_percent = $percent->percent;
                    }
                } else if ($reservation->client->type == 'b2c') {
                    $client_user_percent = $reservation->client->percent;
                }
            }

            foreach ($reservation->journey->placements as $journey_placement) {
                if ($journey_type == 'tour' || ($journey_type == 'cruise' && $journey_placement->display)) {
                    if ($journey_type == 'tour') {
                        $reservation_categories[$journey_placement[$this_journey_placement_name]->type->type->id]["category"] = $journey_placement[$this_journey_placement_name]->type->type;
                        if (!isset($reservation_categories[$journey_placement[$this_journey_placement_name]->type->type->id]["types"][$journey_placement[$this_journey_placement_name]->type->id][$journey_placement->id]))
                            $reservation_categories[$journey_placement[$this_journey_placement_name]->type->type->id]["types"][$journey_placement[$this_journey_placement_name]->type->id][$journey_placement->id] = $journey_placement;
                        else
                            $reservation_categories[$journey_placement[$this_journey_placement_name]->type->type->id]["types"][$journey_placement[$this_journey_placement_name]->type->id][$journey_placement->id]->amount += $journey_placement->amount;

                        $placements += $journey_placement->amount;
                    } else {
                        $reservation_categories[$journey_placement[$this_journey_placement_name]->type->type->id]["category"] = $journey_placement[$this_journey_placement_name]->type->type;
                        if (!isset($reservation_categories[$journey_placement[$this_journey_placement_name]->type->type->id]["types"][$journey_placement[$this_journey_placement_name]->type->id])) {
                            $reservation_categories[$journey_placement[$this_journey_placement_name]->type->type->id]["types"][$journey_placement[$this_journey_placement_name]->type->id]["name"] = $journey_placement[$this_journey_placement_name]->type->name;
                            $reservation_categories[$journey_placement[$this_journey_placement_name]->type->type->id]["types"][$journey_placement[$this_journey_placement_name]->type->id]["id"] = $journey_placement->cruise_type_id;
                            $reservation_categories[$journey_placement[$this_journey_placement_name]->type->type->id]["types"][$journey_placement[$this_journey_placement_name]->type->id]["amount"] = $journey_placement->amount ? $journey_placement->amount : 1;
                        } else {
                            $reservation_categories[$journey_placement[$this_journey_placement_name]->type->type->id]["types"][$journey_placement[$this_journey_placement_name]->type->id]["amount"] += $journey_placement->amount ? $journey_placement->amount : 1;
                        }
                        $placements++;

                        $cabins_info[$journey_placement[$this_journey_placement_name]->type->id]["numbers"][] = $journey_placement->cabin->num;

                        if (!isset($cabins_info[$journey_placement[$this_journey_placement_name]->type->id]["min_area"]) || $cabins_info[$journey_placement[$this_journey_placement_name]->type->id]["min_area"] > $journey_placement->cabin->area)
                            $cabins_info[$journey_placement[$this_journey_placement_name]->type->id]["min_area"] = $journey_placement->cabin->area;

                        if (!isset($cabins_info[$journey_placement[$this_journey_placement_name]->type->id]["max_area"]) || $cabins_info[$journey_placement[$this_journey_placement_name]->type->id]["max_area"] < $journey_placement->cabin->area)
                            $cabins_info[$journey_placement[$this_journey_placement_name]->type->id]["max_area"] = $journey_placement->cabin->area;
                    }

                    $max_pax_number += $journey_placement[$this_journey_placement_name]->type->bed ? 1 + $journey_placement[$this_journey_placement_name]->type->pax_amount : $journey_placement[$this_journey_placement_name]->type->pax_amount;
                }
            }

            if ($journey_type == 'cruise') {
                foreach ($reservation->journey->types as $cruise_type) {
                    $reservation_categories[$cruise_type->type->type->id]["types"][$cruise_type->type->id]["amount"] += $cruise_type->overbooking;
                    $cruise_types[$cruise_type->id] = $cruise_type;
                    $placements += $cruise_type->overbooking;
                }
            }

            foreach ($reservation->journey->reservations as $journey_reservation) {
                $reservation_check = false;
                if ($reservation->type == 'booking' || $reservation->type == 'option')
                    $reservation_check = $journey_reservation->type == 'booking' || $journey_reservation->type == 'option';

                if ($journey_reservation->id != $reservation->id && $reservation_check)
                    foreach ($journey_reservation->placements as $journey_reservation_placement) {
                        if ($journey_type == 'cruise')
                            $reservation_categories[$journey_reservation_placement->cruise_type->type->type->id]["types"][$journey_reservation_placement->cruise_type->type->id]["amount"]--;
                        else if ($journey_type == 'tour')
                            $reservation_categories[$journey_reservation_placement->placement->room->type->type->id]["types"][$journey_reservation_placement->placement->room->type->id][$journey_reservation_placement->placement->id]->amount--;
                    }
            }

            if ($reservation->reservation_type == 'fit') {
                $reservation_info["min_placements"] = $placements > 0 ? 1 : NULL;
                $reservation_info["max_placements"] = $placements > 5 ? 5 : $placements;
            } else if ($reservation->reservation_type == 'group') {
                $reservation_info["min_placements"] = $placements > 5 ? 5 : $placements;
                $reservation_info["max_placements"] = $placements > 20 ? 20 : $placements;
            }

            if (isset($request->placementsData)) {
                $request->pax = [];

                $this->validate($request, [
                    'pax_number' => 'integer|between:0,' . $max_pax_number,
                    'placements_number' => 'integer|between:' . $reservation_info["min_placements"] . ',' . $reservation_info["max_placements"],
                    'placements.*.category' => 'exists:placement_types,id',
                    'placements.*.placement' => 'exists:placements,id',
                    'placements.*.cruise_type' => 'exists:cruise_types,id',
                ]);

                for ($i = 1; $i <= $request->placements_number; $i++) {
                    $request_placement = $request->placements;

                    if (isset($request_placement[$i]["category"]) && $request_placement[$i]["category"] && (($journey_type == 'tour' && isset($request_placement[$i]["placement"]) && $request_placement[$i]["placement"]) || ($journey_type == 'cruise' && isset($request_placement[$i]["cruise_type"]) && $request_placement[$i]["cruise_type"]))) {
                        if (isset($request_placement[$i]["id"]))
                            $reservation_placements[$i] = ReservationPlacement::find($request_placement[$i]["id"]);
                        else
                            $reservation_placements[$i] = new ReservationPlacement;

                        if ($journey_type == 'tour') {
                            $this_model = $reservation->journey->placements()->where('id', $request_placement[$i]["placement"])->first();
                            $this_type = $this_model !== NULL ? $this_model->room->type : NULL;
                        } else {
                            $this_type = isset($cruise_types[$request_placement[$i]["cruise_type"]]) ? $cruise_types[$request_placement[$i]["cruise_type"]]->type : NULL;
                        }

                        if ($this_type !== NULL && $this_type->type->id == $request_placement[$i]["category"]) {
                            if ($journey_type == 'tour')
                                $reservation_placements[$i]->placement_id = $request_placement[$i]["placement"];
                            else
                                $reservation_placements[$i]->cruise_type_id = $request_placement[$i]["cruise_type"];
                            $reservation_placements[$i]->adults = isset($request_placement[$i]["adults"]) ? $request_placement[$i]["adults"] : 0;
                            $reservation_placements[$i]->childrens = isset($request_placement[$i]["childrens"]) ? $request_placement[$i]["childrens"] : 0;
                            $reservation_placements[$i]->infants = isset($request_placement[$i]["infants"]) ? $request_placement[$i]["infants"] : 0;
                            $request->pax[$i] = $reservation_placements[$i]->adults + $reservation_placements[$i]->childrens + $reservation_placements[$i]->infants;
                            if (isset($request_placement[$i]["bed"]))
                                $reservation_placements[$i]->bed = true;
                            else
                                $reservation_placements[$i]->bed = false;

                            $validate = $validate + ['pax.' . $i => 'between:1,' . ($reservation_placements[$i]->bed ? $this_type->pax_amount + 1 : $this_type->pax_amount)];
                            
                            if ($journey_type == 'tour')
                                $reservation_categories[$reservation_placements[$i]->placement[$this_journey_placement_name]->type->type->id]["types"][$reservation_placements[$i]->placement[$this_journey_placement_name]->type->id][$reservation_placements[$i]->placement->id]->amount -= 1;
                            else
                                $reservation_categories[$this_type->type->id]["types"][$this_type->id]["amount"] -= 1;
                        } else {
                            $reservation_placements[$i] = new ReservationPlacement;
                            $picked_categories[$i] = $request_placement[$i]["category"];
                        }
                    } else if (isset($request_placement[$i]["category"]) && $request_placement[$i]["category"]) {
                        $reservation_placements[$i] = new ReservationPlacement;
                        $picked_categories[$i] = $request_placement[$i]["category"];
                    } else
                        $reservation_placements[$i] = new ReservationPlacement;
                }

                $reservation->pax_number = $request->pax_number;

                $reservation->language_id = $request->input('language', $reservation->language_id);
            } else {
                $count = 0;
                foreach ($reservation->placements as $placement) {
                    if ($journey_type == 'tour')
                        $reservation_categories[$placement->placement[$this_journey_placement_name]->type->type->id]["types"][$placement->placement[$this_journey_placement_name]->type->id][$placement->placement->id]->amount -= 1;
                    else
                        $reservation_categories[$placement->cruise_type->type->type->id]["types"][$placement->cruise_type->type->id]["amount"] -= 1;
                    
                    $reservation_placements[++$count] = $placement;
                }
            }
            $this->validate($request, $validate);

            list($placements_data, $services_data, $journey_services, $reservation_info["pax_count"]) = $this->reservationInfo($reservation);
            $reservation_info["cancel_fine"] = $reservation->fines()->where('type', 'cancel')->first();

            $reservation_price = $reservation_info["cancel_fine"] === NULL ? $services_data["total"] + $placements_data["total"]["total"] : $reservation_info["cancel_fine"]->amount;

            foreach ($reservation->payments()->orderBy('created_at', 'asc')->get() as $payment)
                $debts[$payment->id] = $reservation_price -= $payment->amount;
        }

        if (isset($request->journey_type)) {
            $this->validate($request, ['journey_type' => 'in:cruise,tour']);
            $journey_type = $request->journey_type;
        } else if (isset($request->client_type)) {
            $this->validate($request, ['client_type' => 'in:b2b,b2c']);
            if (!isset($reservation->client) || $request->client_type != $reservation->client->type) {
                $client_type = $request->client_type;
                $reservation_client_users = [];
            }
        } else if (isset($request->client)) {
            if ($request->client == 'false') {
                $reservation_client_users = [];
                unset($reservation->client);
            } else {
                $this->validate($request, ['client' => 'exists:clients,id']);
                $reservation->client = Client::find($request->client);
                $reservation_client_users = $reservation->client->users();
            }
        } else if (isset($request->placements_number) && isset($reservation->journey)) {
            if ($request->placements_number == 'false') {
                $reservation_info["placements_number"] = NULL;
            } else {
                $this->validate($request, ['placements_number' => 'between:1,' . $placements]);
                $reservation_info["placements_number"] = $request->placements_number;
            }
        }

        $journeys = $journey_type ? Journey::where('type', $journey_type)->select(['id', 'code'])->get() : [];
        $clients = $client_type ? Client::where('type', $client_type)->select(['id', 'name'])->get() : [];
        $client_users = !empty($reservation_client_users) ? $reservation_client_users->select(['id', 'name'])->get() : [];
        
        return view('reservations.reservation', array(
            'reservation_info' => $reservation_info,
            'data' => $reservation,
            'journeys' => $journeys,
            'clients' => $clients,
            'client_users' => $client_users,
            'client_user_percent' => $client_user_percent,
            'placements' => $placements,
            'languages' => Language::select(['id', 'name'])->get(),
            'picked_categories' => $picked_categories,
            'reservation_categories' => $reservation_categories,
            'reservation_placements' => $reservation_placements,
            'cruise_types' => $cruise_types,
            'journey_services' => $journey_services,
            'cabins_info' => $cabins_info,
            'reservation_services' => $reservation_services,
            'placements_data' => $placements_data,
            'services_data' => $services_data,
            'debts' => $debts,
            'edit_role' => isset($reservation_info["journey_type"]) && Auth::user()->hasRole('edit_reservations') && Auth::user()->hasRole('edit_' . $reservation_info["journey_type"] . '_' . $reservation_info["role_name"] . ($reservation->deleted_at ? '_canceled' : '')),
            'create_role' => isset($reservation_info["journey_type"]) ? Auth::user()->hasRole('create_' . $reservation_info["journey_type"] . '_' . $reservation_info["role_name"]) : Auth::user()->hasRole('create_cruise_' . $reservation_info["role_name"]) || Auth::user()->hasRole('create_tour_' . $reservation_info["role_name"]),
            'show_rooms_cabins' => isset($reservation_info["journey_type"]) ? Auth::user()->hasRole('show_' . $reservation_info["journey_type"] . '_' . $reservation_info["role_name"] . ($reservation->deleted_at ? '_canceled' : '') . '_rooms_cabins') : Auth::user()->hasRole('show_cruise_' . $reservation_info["role_name"] . ($reservation->deleted_at ? '_canceled' : '') . '_rooms_cabins') || Auth::user()->hasRole('show_tour_' . $reservation_info["role_name"] . ($reservation->deleted_at ? '_canceled' : '') . '_rooms_cabins')
        ));
    }

    public function Save(Request $request)
    {
        $validate = array();

        if ($request->has('id')) {
            $this->validate($request, ['id' => 'required|exists:reservations,id']);
            $reservation = Reservation::find($request->id);

            $role_name = $this->roleName($reservation->type);
            if (!(Auth::user()->hasRole('create_' . $reservation->journey->type . '_' . $role_name) && (Auth::user()->hasRole('edit_' . $reservation->journey->type . '_' . $role_name) || !$reservation->services_tab || (Auth::user()->hasRole('show_' . $reservation->journey->type . '_' . $role_name . '_rooms_cabins') ? !$reservation->placements_tab : false))))
                return response('Forbidden.', 403);

            $journey_type = $reservation->journey->type;
        } else {
            $reservation = new Reservation;

            $role_name = $this->roleName($request->journey_type);
            if (!Auth::user()->hasRole('create_' . $request->journey_type . '_' . $role_name))
                return response('Forbidden.', 403);

            $journey_type = $request->journey_type;
        }

        $pageUrl = url('/' . $reservation->type . '/' . $reservation->id);
        $placementsNotification = NULL;

        if (!$reservation->id || ($reservation->id && $request->input('edit_tab', NULL) == 'general')) {
            $validate += [
                    'type' => 'required|in:option,inquiry,booking',
                    'journey_type' => 'required|in:cruise,tour',
                    'journey' => 'required|exists:journeys,id',
                    'reservation_type' => 'required|in:fit,group',
                    'client' => 'required|exists:clients,id'
                ];

            if ($request->input('type', 0) == 'option')
                $validate += ['till' => 'required|date'];
        }

        ini_set("log_errors", 1);
        ini_set("error_log", "log.log");
        if ($reservation->journey_id) {
            $journeyPlacement = $reservation->journey->type == 'tour' ? 'room' : 'cabin';

            if ($request->input('edit_tab') === 'placements' && $reservation->id) {
                $countPlacements = 0;
                $max_pax_number = 0;
                $journeyAmounts = [];
                $with_names = [];

                if ($reservation->placements_tab && $reservation->language_id != $request->input('language', $reservation->language_id))
                    $this->notify(Auth::user()->id, $reservation->id, 'action', 'Language was replaced - <a href="' . $pageUrl . '">' . $reservation->code . '</a>');
                $reservation->language_id = $request->input('language', $reservation->language_id);

                if ($request->has('placements')) {
                    foreach ($reservation->journey->placements as $placement) {
                        if ($journey_type == 'tour') {
                            $countPlacements += $placement->amount;
                            $journeyAmounts[$placement->id] = $placement->amount;
                        } else if ($journey_type == 'cruise' && $placement->display) {
                            $countPlacements++;
                            $journeyAmounts[$placement->cruise_type_id] = isset($journeyAmounts[$placement->cruise_type_id]) ? $journeyAmounts[$placement->cruise_type_id] + 1 : 1;
                        } else
                            continue;

                        $max_pax_number += $placement[$journeyPlacement]->type->bed ? 1 + $placement[$journeyPlacement]->type->pax_amount : $placement[$journeyPlacement]->type->pax_amount;
                    }

                    if ($journey_type != 'tour')
                        foreach ($reservation->journey->types as $cruise_type) {
                            $countPlacements += $cruise_type->overbooking;
                            $journeyAmounts[$cruise_type->id] = isset($journeyAmounts[$cruise_type->id]) ? $journeyAmounts[$cruise_type->id] + $cruise_type->overbooking : $cruise_type->overbooking;
                        }

                    if ($countPlacements > 1) {
                        if ($reservation->reservation_type == 'fit') {
                            $validate += ['placements_number' => 'integer|between:' . ($countPlacements > 0 ? 1 : NULL) . ',' . ($countPlacements > 5 ? 5 : $countPlacements)];
                        } else if ($reservation->reservation_type == 'group') {
                            $validate += ['placements_number' => 'integer|between:' . ($countPlacements > 5 ? 5 : $countPlacements) . ',' . ($countPlacements > 20 ? 20 : $countPlacements)];
                        }
                    }

                    $validate += [
                        'pax_number' => 'integer|between:0,' . $max_pax_number,
                        'language' => 'exists:languages,id',
                    ];

                    $placementIds = $reservation->placements()->pluck('id')->toArray();

                    $newCount = 0;
                    $clientPercent = 0;
                    $paxNotification = false;
                    $placementNotification = false;
                    $placementsInfo = [
                        'placements' => $reservation->placements->count(),
                        'paxFine' => 0,
                        'placementsFine' => 0
                    ];

                    if ($reservation->placements_tab && count($request->placements) != $placementsInfo['placements']) {
                        $this->notify(Auth::user()->id, $reservation->id, 'action', 'Number of placements was updated - <a href="' . $pageUrl . '">' . $reservation->code . '</a>');

                        if ($reservation->client->type == 'b2b') {
                            if ($reservation->journey->type == 'cruise') {
                                $percent = $reservation->client->percents()->where('ship_id', $reservation->journey->ship_id)->first();
                                if ($percent !== NULL)
                                    $clientPercent = $percent->percent / 100;
                            } else if ($reservation->journey->type == 'tour') {
                                $percent = $reservation->client->percents()->where('hotel_id', $reservation->journey->hotel_id)->first();
                                if ($percent !== NULL)
                                    $clientPercent = $percent->percent / 100;
                            }
                        } else if ($reservation->client->type == 'b2c' && $reservation->client->percent > 0)
                            $clientPercent = $reservation->client->percent / 100;
                    }
                    foreach ($request->placements as $placementsCount => $request_placement) {
                        if (($journey_type == 'tour' && !empty($request_placement["placement"]) && isset($journeyAmounts[$request_placement["placement"]]) && $journeyAmounts[$request_placement["placement"]]) ||
                            ($journey_type == 'cruise' && !empty($request_placement["cruise_type"]) && isset($journeyAmounts[$request_placement["cruise_type"]]) && $journeyAmounts[$request_placement["cruise_type"]])
                        ) {
                            if ($reservation->journey->type == 'tour') {
                                $placementValidate = ['placements.' . $placementsCount . '.placement' => 'required|exists:placements,id'];
                                $journeyAmounts[$request_placement["placement"]]--;
                            } else {
                                $placementValidate = ['placements.' . $placementsCount . '.cruise_type' => 'required|exists:cruise_types,id'];
                                $journeyAmounts[$request_placement["cruise_type"]]--;
                            }

                            $validator = Validator::make($request->all(), $placementValidate);
                            if ($validator->fails()) {
                                $validate += $placementValidate;
                                continue;
                            }

                            if ($countPlacements > $newCount) {
                                if (isset($request_placement["id"])) {
                                    if (in_array($request_placement["id"], $placementIds)) {
                                        $placement = ReservationPlacement::find($request_placement["id"]);
                                        $placementIds = array_diff($placementIds, array($request_placement["id"]));
                                    } else
                                        continue;
                                } else {
                                    $placement = new ReservationPlacement;
                                    $placement->reservation_id = $reservation->id;
                                }
                                if ($reservation->journey->type == 'tour') {
                                    $placement->placement_id = $request_placement["placement"];
                                    $placementID = Placement::find($request_placement["placement"])->room->type;
                                } else {
                                    $placement->cruise_type_id = $request_placement["cruise_type"];
                                    $placementID = CruiseType::find($request_placement["cruise_type"])->type;
                                }

                                if ($placementID->bed && isset($request_placement["bed"]) && $request_placement["bed"])
                                    $placement->bed = true;
                                else
                                    $placement->bed = false;

                                $paxAmount = $placementID->pax_amount + ($placementID->bed && $placement->bed ? 1 : 0);

                                $placementValidate = [
                                    'placements.' . $placementsCount . '.adults' => 'numeric|integer|between:0,' . $paxAmount,
                                    'placements.' . $placementsCount . '.childrens' => 'numeric|integer|between:0,' . $paxAmount,
                                    'placements.' . $placementsCount . '.infants' => 'numeric|integer|between:0,' . $paxAmount,
                                ];
                                $validator = Validator::make($request->all(), $placementValidate);

                                if ($validator->fails()) {
                                    $validate += $placementValidate;
                                    continue;
                                }

                                $placementValidate = ['placements.' . $placementsCount . '.adults' => 'numeric|integer|max:' . ($paxAmount)];
                                $max_childrens = $paxAmount - $request->input('placements.' . $placementsCount . '.adults', 0);
                                $placementValidate += [
                                    'placements.' . $placementsCount . '.childrens' => 'numeric|integer|max:' . ($max_childrens > $request->input('placements.' . $placementsCount . '.adults', 0) ? $request->input('placements.' . $placementsCount . '.adults', 0) : $max_childrens),
                                    'placements.' . $placementsCount . '.infants' => 'numeric|integer|max:' . ($request->input('placements.' . $placementsCount . '.adults', 0) - $request->input('placements.' . $placementsCount . '.childrens', 0))
                                ];

                                $validator = Validator::make($request->all(), $placementValidate);
                                if ($validator->fails()) {
                                    $validate += $placementValidate;
                                    continue;
                                }

                                $oldPaxCount = $placement->adults + $placement->childrens + $placement->infants;
                                $oldPaxCountArray = [
                                    'adult' => $placement->adults,
                                    'children' => $placement->childrens,
                                    'infant' => $placement->infants
                                ];

                                $placement->adults = $request->input('placements.' . $placementsCount . '.adults', 0);
                                $placement->childrens = $request->input('placements.' . $placementsCount . '.childrens', 0);
                                $placement->infants = $request->input('placements.' . $placementsCount . '.infants', 0);

                                $placement->save();
                                $newCount++;

                                $allPaxCount = $placement->adults + $placement->childrens + $placement->infants;

                                $paxCount = [
                                    'adult' => $placement->adults,
                                    'children' => $placement->childrens,
                                    'infant' => $placement->infants
                                ];

                                if ($reservation->type == 'booking')
                                    if ($oldPaxCount == $allPaxCount) {
                                        $changedPax = 0;
                                        foreach ($paxCount as $newPaxType => $newPaxAmount)
                                            if ($newPaxAmount > $oldPaxCountArray[$newPaxType])
                                                $changedPax += $newPaxAmount - $oldPaxCountArray[$newPaxType];
                                        if ($changedPax != 0)
                                            $this->fine($reservation->id, $this->notify(NULL, $reservation->id, 'update', 'Pax types were replaced, with fine - ' . number_format(Config::get('constants.fines.pax_type_change') * $changedPax, 2) . ' EUR - <a href="' . $pageUrl . '">' . $reservation->code . '</a>'), 'pax_types', Config::get('constants.fines.pax_type_change') * $changedPax);
                                    } else if ($allPaxCount != $oldPaxCount) {
                                        $paxNotification = true;
                                        if ($allPaxCount < $oldPaxCount) {
                                            //$this->notify(NULL, $reservation->id, 'update', 'Pax type was updated, with fine - ' . number_format(Config::get('constants.fines.pax_amount_change') * ($oldPaxCount - $allPaxCount), 2) . ' EUR - <a href="' . $pageUrl . '">' . $reservation->code . '</a>');
                                        }
                                    }

                                $count = 0;
                                $bed = $placement->bed;
                                $notInfantsCount = 0;
                                foreach ($placement->reservation_paxes as $pax) {
                                    $count++;
                                    if ($count > $allPaxCount || !$paxCount[$pax->type]) {
                                        if (!$paxCount[$pax->type])
                                            $count--;

                                        if ($reservation->type == 'booking') {
                                            $this_type = $reservation->journey->type == 'tour' ? $placement->placement->room : $placement->cruise_type;
                                            $paxFine = $reservation->reservation_type == 'group' ? $this_type->price_rec : $this_type->price_net;
                                            if ($pax->type == 'children')
                                                $paxFine *= 0.7;
                                            else if ($pax->type == 'infant')
                                                $paxFine = 0;

                                            $paxFine = $paxFine - $paxFine * $clientPercent;

                                            if ($reservation->journey->type == 'tour')
                                                foreach ($pax->services as $pax_service) {
                                                    $this_service = isset($pax_service->service->service) ? $pax_service->service->service : $pax_service->service->package;
                                                    if ($pax_service->service->optional)
                                                        $paxFine += $this_service->price;
                                                }

                                            $placementsInfo['paxFine'] += $paxFine;
                                        }

                                        $pax->delete();
                                        continue;
                                    }

                                    if ($paxCount[$pax->type])
                                        --$paxCount[$pax->type];

                                    if ($pax->type != 'infant')
                                        $notInfantsCount++;

                                    if ($bed && isset($request["placements"][$placementsCount]["pax_bed"]) && $request["placements"][$placementsCount]["pax_bed"] == $pax->id && $pax->type != 'infant') {
                                        $bed = 0;

                                        $pax->bed = true;
                                        $pax->save();
                                    } else if ($paxCount['adult'] < 1 && $paxCount['children'] < 1 && $notInfantsCount == $placement->adults + $placement->childrens && $bed) {
                                        $bed = 0;

                                        $pax->bed = true;
                                        $pax->save();
                                    } else {
                                        $pax->bed = false;
                                        $pax->save();
                                    }

                                    if (empty($pax->name) || empty($pax->surname) || empty($pax->birth))
                                        if (!in_array($placement->id, $with_names))
                                            $with_names[] = $placement->id;
                                }

                                for ($i = $count + 1; $i <= $allPaxCount; $i++) {
                                    $newPax = new ReservationPax;
                                    $newPax->reservation_placement_id = $placement->id;

                                    if ($paxCount['adult']-- > 0) {
                                        $newPax->type = 'adult';
                                        if ($bed && !$paxCount['adult'])
                                            $newPax->bed = true;
                                    } else if ($paxCount['children']-- > 0) {
                                        $newPax->type = 'children';
                                        if ($bed && !$paxCount['children'])
                                            $newPax->bed = true;
                                    } else if ($paxCount['infant']-- > 0)
                                        $newPax->type = 'infant';
                                    $newPax->name = 'New ' . $newPax->type;

                                    $newPax->save();

                                    if (empty($newPax->name) || empty($newPax->surname) || empty($newPax->sex) || empty($newPax->birth))
                                        if (!in_array($placement->id, $with_names))
                                            $with_names[] = $placement->id;
                                }
                            }
                        }
                    }

                    foreach ($reservation->placements as $reservationPlacement)
                        if (in_array($reservationPlacement->id, $placementIds)) {
                            $placementNotification = true;
                            if ($reservation->type == 'booking') {
                                $pax_count = 0;
                                foreach ($reservationPlacement->reservation_paxes as $reservationPlacementPax) {
                                    $pax_count++;
                                    $this_type = $reservation->journey->type == 'tour' ? $reservationPlacement->placement->room : $reservationPlacement->cruise_type;
                                    $paxFine = $reservation->reservation_type == 'group' ? $this_type->price_rec : $this_type->price_net;
                                    if ($reservationPlacementPax->type == 'children')
                                        $paxFine *= 0.7;
                                    else if ($reservationPlacementPax->type == 'infant')
                                        $paxFine = 0;

                                    $paxFine = $paxFine - $paxFine * $clientPercent;

                                    if ($reservation->journey->type == 'tour')
                                        foreach ($reservationPlacementPax->services as $pax_service) {
                                            $this_service = isset($pax_service->service->service) ? $pax_service->service->service : $pax_service->service->package;
                                            if ($pax_service->service->optional)
                                                $paxFine += $this_service->price;
                                        }

                                    $placementsInfo['paxFine'] += $paxFine;
                                    $placementsInfo['placementsFine'] += $paxFine;
                                }

                                if ($reservation->journey->type == 'tour')
                                    foreach ($reservationPlacement->services as $reservationPlacementService) {
                                        $this_service = isset($reservationPlacementService->service->service) ? $reservationPlacementService->service->service : $reservationPlacementService->service->package;
                                        if ($reservationPlacementService->service->optional)
                                            $placementsInfo['placementsFine'] += $this_service->multiply ? $this_service->price : $this_service->price * $pax_count;
                                    }
                            }
                            $reservationPlacement->delete();
                            if (($key = array_search($reservationPlacement->id, $with_names)) !== false)
                                unset($with_names[$key]);
                        }

                    if ($reservation->placements_tab && ($paxNotification || $placementNotification)) {
                        if ($paxNotification)
                            $this->notify(Auth::user()->id, $reservation->id, 'action', 'Number of pax was updated - <a href="' . $pageUrl . '">' . $reservation->code . '</a>');
                        if ($reservation->type == 'booking') {
                            $reservationPercent = $this->reservationPercent($reservation);
                            if ($paxNotification)
                                $this->fine($reservation->id, $this->notify(NULL, $reservation->id, 'update', 'Number of pax was updated, with fine - ' . number_format(($placementsInfo['paxFine'] * $reservationPercent) / 100, 2) . ' EUR - <a href="' . $pageUrl . '">' . $reservation->code . '</a>'), 'pax_number', ($placementsInfo['paxFine'] * $reservationPercent) / 100);
                            if ($placementNotification)
                                $this->fine($reservation->id, $this->notify(NULL, $reservation->id, 'update', 'Number of placements was updated, with fine - ' . number_format(($placementsInfo['placementsFine'] * $reservationPercent) / 100, 2) . ' EUR - <a href="' . $pageUrl . '">' . $reservation->code . '</a>'), 'placements_number', ($placementsInfo['placementsFine'] * $reservationPercent) / 100);
                        }
                    }
                    if ($reservation->type == 'booking' && empty($with_names)) {
                        $reservation->with_names = true;
                        $this->notify(Auth::user()->id, $reservation->id, 'action', 'Pax details were updated - <a href="' . $pageUrl . '">' . $reservation->code . '</a>');
                        $this->notify(NULL, $reservation->id, 'update', 'E-mail was sent (Pax details were updated) - <a href="' . $pageUrl . '">' . $reservation->code . '</a>', NULL, NULL, [
                            'type' => 'pax_details_update',
                            'reservation' => $reservation
                        ]);
                    } else
                        $reservation->with_names = false;
                }

                if (!$reservation->placements_tab) {
                    $reservation->placements_tab = true;
                    if ($reservation->services_tab && $reservation->type == 'booking')
                        $this->notify(NULL, $reservation->id, 'update', 'E-mail was sent (Invoice) - <a href="' . $pageUrl . '">' . $reservation->code . '</a>', NULL, NULL, [
                            'type' => $reservation->reservation_type == 'fit' ? 'invoice' : 'invoice_flexible',
                            'reservation' => $reservation
                        ]);
                }

                if ($reservation->placements->count() / $countPlacements >= 0.8 && $reservation->overbooking_status != '80') {
                    $placementsNotification = 'Overbooked';
                    $reservation->overbooking_status = '80';
                } else if ($reservation->placements->count() / $countPlacements >= 0.6 && $reservation->overbooking_status === NULL) {
                    $placementsNotification = 'Overbooked!';
                    $reservation->overbooking_status = '60';
                }
            } else if ($request->input('edit_tab') === 'services' && $reservation->id) {
                $nonCompatibles = [];

                foreach ($reservation->journey->nonDays as $nonDay) {
                    $nonIds = [];
                    foreach ($nonDay->services as $nonService)
                        $nonIds[] = $nonService->journey_service_id;

                    foreach ($nonDay->services as $nonService)
                        $nonCompatibles[$nonService->journey_service_id][] = $nonIds;
                }

                $journeyServicesCount = [];
                /*foreach ($reservation->journey->services as $journeyService) {
                    $service_type = $journeyService->service_id ? 'service' : 'package';
                    $journeyServices[$journeyService->id] = [
                        'min_pax' => $journeyService[$service_type]->min_pax,
                        'max_pax' => $journeyService[$service_type]->max_pax,
                        'count' => 0
                    ];
                }*/
                foreach ($reservation->journey->services as $journeyService) {
                    $service_type = $journeyService->service_id ? 'service' : 'package';
                    $journeyServicesCount[$journeyService->id] = [
                        'name' => $journeyService[$service_type]->name,
                        'min_pax' => $journeyService[$service_type]->min_pax,
                        'max_pax' => $journeyService[$service_type]->max_pax,
                        'count' => 0
                    ];
                }

                foreach ($reservation->journey->reservations as $otherReservation)
                    if ($otherReservation->id != $reservation->id)
                        foreach ($otherReservation->placements as $otherReservationPlacement) {
                            $pax_count = $otherReservationPlacement->reservation_paxes->count();
                            foreach ($otherReservationPlacement->services as $otherReservationPlacementService)
                                $journeyServicesCount[$otherReservationPlacementService->journey_service_id]['count'] += $pax_count;
                            foreach ($otherReservationPlacement->reservation_paxes as $otherReservationPlacementPax)
                                foreach ($otherReservationPlacementPax->services as $otherReservationPlacementPaxService)
                                    $journeyServicesCount[$otherReservationPlacementPaxService->journey_service_id]['count']++;
                        }

                foreach ($reservation->placements as $reservationPlacement) {
                    $pax_count = $reservationPlacement->reservation_paxes->count();
                    if (isset($request['service']['placement'][$reservationPlacement->id]))
                        foreach ($request['service']['placement'][$reservationPlacement->id] as $requestPlacementServiceID) {
                            if (isset($journeyServicesCount[$requestPlacementServiceID]))
                                $journeyServicesCount[$requestPlacementServiceID]['count'] += $pax_count;
                        }

                    foreach ($reservationPlacement->reservation_paxes as $reservationPax) {
                        if (isset($request['service']['pax'][$reservationPlacement->id][$reservationPax->id]))
                            foreach ($request['service']['pax'][$reservationPlacement->id][$reservationPax->id] as $requestPaxServiceID) {
                                if (isset($journeyServicesCount[$requestPaxServiceID]))
                                    $journeyServicesCount[$requestPaxServiceID]['count']++;
                            }
                    }
                }

                if ($reservation->services_tab)
                    foreach ($journeyServicesCount as $journeyServiceID => $journeyServiceCount) {
                        if ($journeyServiceCount['count'] >= $journeyServiceCount['min_pax'] && !$reservation->notifications()->where('journey_service_id', $journeyServiceID)->where('reminder_type', 'service_min')->exists())
                            $this->notify(NULL, $reservation->id, 'reminder', 'Reached the minimum number of pax in the service - <a href="' . $pageUrl . '">' . $reservation->journey->code . '</a> (' . $journeyServiceCount['name'] . ')', $journeyServiceID, 'service_min');

                        if ($journeyServiceCount['count'] == $journeyServiceCount['max_pax'] && !$reservation->notifications()->where('journey_service_id', $journeyServiceID)->where('reminder_type', 'service_max')->exists())
                            $this->notify(NULL, $reservation->id, 'reminder', 'Reached the maximum number of pax in the service - <a href="' . $pageUrl . '">' . $reservation->journey->code . '</a> (' . $journeyServiceCount['name'] . ')', $journeyServiceID, 'service_max');
                    }

                foreach ($reservation->placements as $reservation_placement) {
                    $reservation_placement->services()->delete();
                    $reservation_placement_services = [];
                    $serviceAttempts = [];

                    if (isset($request->service["placement"][$reservation_placement->id])) {
                        $validator = Validator::make($request->all(), ['service.placement.' . $reservation_placement->id . '.*' => 'exists:journey_services,id']);

                        if (!$validator->fails()) {
                            foreach ($request->service["placement"][$reservation_placement->id] as $reservation_placement_service_ID) {
                                $journey_service = $reservation->journey->services()->where('id', $reservation_placement_service_ID)->first();

                                if ($journey_service === NULL)
                                    continue;

                                $service_type = $journey_service->service_id ? 'service' : 'package';

                                if (isset($journey_service->service->language) && $journey_service->service->language_id != $reservation->language_id)
                                    continue;

                                if (/*$journey_service[$service_type]->min_pax > $journeyServicesCount[$reservation_placement_service_ID]['count'] || */$journey_service[$service_type]->max_pax < $journeyServicesCount[$reservation_placement_service_ID]['count']) {
                                    $serviceAttempts[$reservation_placement_service_ID] = $journey_service[$service_type]->name;
                                    continue;
                                }

                                $reservation_placement_services[] = $reservation_placement_service_ID;

                                $skip = false;
                                if (isset($nonCompatibles[$reservation_placement_service_ID]))
                                    foreach ($nonCompatibles[$reservation_placement_service_ID] as $nonCompatibleArray)
                                        foreach ($nonCompatibleArray as $nonCompatible)
                                            if ($reservation_placement_service_ID != $nonCompatible && in_array($nonCompatible, $reservation_placement_services)) {
                                                $skip = true;
                                                break;
                                            }

                                if ($skip)
                                    continue;

                                $reservation_placement_service = new ReservationPlacementService;
                                $reservation_placement_service->reservation_placement_id = $reservation_placement->id;
                                $reservation_placement_service->journey_service_id = $reservation_placement_service_ID;
                                $reservation_placement_service->save();
                            }
                        } else
                            $validate = $validate + ['service.placement.' . $reservation_placement->id . '.*' => 'exists:journey_services,id'];
                    }

                    foreach ($reservation_placement->reservation_paxes as $reservation_placement_pax) {
                        $reservation_placement_pax->services()->delete();
                        if (isset($request->service["pax"][$reservation_placement->id][$reservation_placement_pax->id])) {
                            $validator = Validator::make($request->all(), ['service.pax.' . $reservation_placement->id . '.' . $reservation_placement_pax->id . '.*' => 'exists:journey_services,id']);

                            if (!$validator->fails()) {
                                foreach ($request->service["pax"][$reservation_placement->id][$reservation_placement_pax->id] as $journeyServiceID) {
                                    $journey_service = $reservation->journey->services()->where('id', $journeyServiceID)->first();

                                    if ($journey_service === NULL)
                                        continue;

                                    $service_type = $journey_service->service_id ? 'service' : 'package';

                                    if (isset($journey_service->service->language) && $journey_service->service->language_id != $reservation->language_id)
                                        continue;

                                    if (/*$journey_service[$service_type]->min_pax > $journeyServicesCount[$journeyServiceID]['count'] || */$journey_service[$service_type]->max_pax < $journeyServicesCount[$journeyServiceID]['count']) {
                                        $serviceAttempts[$journeyServiceID] = $journey_service[$service_type]->name;
                                        continue;
                                    }

                                    $reservation_placement_services[] = $journeyServiceID;

                                    $skip = false;
                                    if (isset($nonCompatibles[$journeyServiceID]))
                                        foreach ($nonCompatibles[$journeyServiceID] as $nonCompatibleArray)
                                            foreach ($nonCompatibleArray as $nonCompatible)
                                                if ($journeyServiceID != $nonCompatible && in_array($nonCompatible, $reservation_placement_services)) {
                                                    $skip = true;
                                                    break;
                                                }

                                    if ($skip)
                                        continue;

                                    $reservation_pax_service = new ReservationPaxService;
                                    $reservation_pax_service->reservation_pax_id = $reservation_placement_pax->id;
                                    $reservation_pax_service->journey_service_id = $journeyServiceID;
                                    $reservation_pax_service->save();
                                }
                            } else
                                $validate = $validate + ['service.pax.' . $reservation_placement->id . '.' . $reservation_placement_pax->id . '.*' => 'exists:journey_services,id'];
                        }
                    }

                    foreach ($serviceAttempts as $serviceAttempt)
                        $this->notify(NULL, $reservation->id, 'reminder', 'Attempt to book service, ' . $serviceAttempt . ' - <a href="' . $pageUrl . '">' . $reservation->code . '</a>');
                }
                if (!$reservation->services_tab) {
                    $reservation->services_tab = true;
                    if ($reservation->placements_tab && $reservation->type == 'booking')
                        $this->notify(NULL, $reservation->id, 'update', 'E-mail was sent (Invoice flexible) - <a href="' . $pageUrl . '">' . $reservation->code . '</a>', NULL, NULL, [
                            'type' => $reservation->reservation_type == 'fit' ? 'invoice' : 'invoice_flexible',
                            'reservation' => $reservation
                        ]);
                } else
                    $this->notify(Auth::user()->id, $reservation->id, 'action', 'Services were updated - <a href="' . $pageUrl . '">' . $reservation->code . '</a>');
            }
        }

        $this->validate($request, $validate);

        if (!$reservation->id || ($reservation->id && $request->input('edit_tab', NULL) === 'general')) {
            $clientHasChanged = $reservation->client_id != $request->client;
            $reservation->client_id = $request->client;

            $newClientUserValue = NULL;
            if ($request->has('client_user')) {
                $validator = Validator::make($request->all(), ['client_user' => 'exists:client_users,id']);
                if (!$validator->fails()) {
                    $clientUser = ClientUser::find($request->client_user);
                    if ($clientUser->client_id == $request->client)
                        $newClientUserValue = $request->client_user;
                }
                if ($newClientUserValue != $reservation->client_user_id) {
                    $clientHasChanged = true;
                    $reservation->client_user_id = $newClientUserValue;
                }
            }

            if ($reservation->id && $clientHasChanged)
                $this->notify(Auth::user()->id, $reservation->id, 'action', 'Client was replaced - <a href="' . $pageUrl . '">' . $reservation->code . '</a>');

            $reservation->type = $request->type;
            $reservation->journey_id = $request->journey;

            if ($reservation->type == 'option')
                $reservation->till = $request->till;

            $reservation->reservation_type = $request->reservation_type;
            if (isset($request->send_email))
                $reservation->send_email = true;
            else
                $reservation->send_email = false;

            $reservation->year_id = $this->year;
        }

        $reservationIsNew = !$reservation->id;

        if (isset($reservation->journey) && $reservation->type != 'option')
            $reservation->till = date($reservation->journey->release_date, strtotime('+' . $reservation->journey->full_pay . ' days'));

        $reservation->save();

        $reservation->code = $reservation->id . '-' .
            ($reservation->placements->count() ? $reservation->placements->count() : 0) . '-' .
            (isset($reservation->client) ? ($reservation->client->type == 'b2b' ? $reservation->client->code : 'B2C' . $reservation->client->id) : 0) . '-' .
            (isset($reservation->journey->release_date) ? strtoupper(substr(date('M', strtotime($reservation->journey->release_date)),0 ,3)) . date('d', strtotime($reservation->journey->release_date)) : 0) . '-' .
            (isset($reservation->journey->ship->code) ? $reservation->journey->ship->code : (isset($reservation->journey->hotel->code) ? $reservation->journey->hotel->code : 0)) . '-' .
            substr($reservation->year->year, -2, 2);

        $reservation->save();

        if ($placementsNotification !== NULL) {
            $this->notify(NULL, NULL, 'reminder', $placementsNotification . ' - <a href="' . url('/' . $reservation->journey->type . '/' . $reservation->journey->id) . '">' . $reservation->journey->code . '</a>');
            $this->notify(NULL, $reservation->id, 'update', 'E-mail was sent (' . $placementsNotification . ') - <a href="' . url('/' . $reservation->journey->type . '/' . $reservation->journey->id) . '">' . $reservation->journey->code . '</a>', NULL, NULL, [
                'type' => 'overbooking',
                'reservation' => $reservation
            ]);
        }

        if ($reservationIsNew) {
            $this->notify(Auth::user()->id, $reservation->id, 'action', 'Added ' . ucfirst($reservation->type) . ' - <a href="' . url('/' . $reservation->type . '/' . $reservation->id) . '">' . $reservation->code . '</a>');
            if ($reservation->type != 'inquiry')
                $this->notify(NULL, $reservation->id, 'update', 'E-mail was sent (Added ' . $reservation->type . ') - <a href="' . url('/' . $reservation->type . '/' . $reservation->id) . '">' . $reservation->code . '</a>', NULL, NULL, [
                    'type' => 'new_' . $reservation->type,
                    'reservation' => $reservation
                ]);
        }
        
        return $reservation->id;
    }

    /*public function Delete(Request $request)
    {
        $this->validate($request, ['id' => 'exists:reservations,id']);

        $reservation = Reservation::find($request->id);

        $role_name = NULL;
        if ($reservation->type == 'booking') {
            $role_name = 'bookings';
        } else if ($reservation->type == 'inquiry') {
            $role_name = 'inquiries';
        } else if ($reservation->type == 'option') {
            $role_name = 'options';
        }

        if (!Auth::user()->hasRole('edit_' . $reservation->journey->type . '_' . $role_name))
            return redirect()->back();

        $reservation->delete();
        return redirect()->back();
    }*/

    public function Cancel($id)
    {
        $reservation = Reservation::findOrFail($id);

        $role_name = $this->roleName($reservation->type);
        if (!Auth::user()->hasRole('create_' . $reservation->journey->type . '_' . $role_name) || !Auth::user()->hasRole('edit_' . $reservation->journey->type . '_' . $role_name))
            return response('Forbidden.', 403);

        if ($reservation->type == 'booking') {
            list($placements_data, $services_data) = $this->reservationInfo($reservation);
            $reservation_price = $services_data["total"] + $placements_data["total"]["total"];
            foreach ($reservation->payments as $payment)
                $reservation_price -= $payment->amount;

            $reservation_price = ($reservation_price * ($this->reservationPercent($reservation))) / 100;

            $reservation->fines()->delete();
            $this->fine($reservation->id, $this->notify(NULL, $reservation->id, 'update', ucfirst($reservation->type) . ' (' . ucfirst($reservation->journey->type) . ') was canceled with fine - ' . number_format($reservation_price, 2) . ' EUR - <a href="' . url('/' . $reservation->type . '/' . $reservation->id) . '">' . $reservation->code . '</a>'), 'cancel', $reservation_price);
        }

        $this->notify(Auth::user()->id, $reservation->id, 'action', 'Canceled ' . ucfirst($reservation->type) . ' - <a href="' . url('/' . $reservation->type . '/' . $reservation->id) . '">' . $reservation->code . '</a>');
        if ($reservation->type != 'inquiry')
            $this->notify(NULL, $reservation->id, 'update', 'E-mail was sent (Canceled ' . ucfirst($reservation->type) . ') - <a href="' . url('/' . $reservation->type . '/' . $reservation->id) . '">' . $reservation->code . '</a>', NULL, NULL, [
                'type' => 'cancellation_' . $reservation->type,
                'reservation' => $reservation
            ]);

        $reservation->delete();
        return redirect()->route('reservations_list');
    }

    public function GetReservations(Request $request)
    {
        $this->validate($request, ['type' => 'required|in:cruise,tour,b2c,b2b,client_user', 'id' => 'required']);

        if ($request->type == 'cruise' || $request->type == 'tour') {
            $this->validate($request, [
                'id' => 'exists:journeys,id',
            ]);
            $model = Journey::find($request->id);
        } else if ($request->type == 'b2c' || $request->type == 'b2b') {
            $this->validate($request, [
                'id' => 'exists:clients,id',
            ]);
            $model = Client::find($request->id);
        } else if ($request->type == 'client_user') {
            $this->validate($request, [
                'id' => 'exists:client_users,id',
            ]);
            $model = ClientUser::find($request->id);
        }

        $reservations = $this->reservationsList($model, $request);

        return view('reservations.model_list', ['reservations' => $reservations]);
    }

    public function CabinsAssignment($journey)
    {
        if ($journey->type != 'cruise' || !$journey->reservations()->where('with_names', true)->where('type', 'booking')->exists())
            return redirect()->back();

        $journey_info = [
            "ship_name" => $journey->ship->name,
            "date" => date('Y.m.d', strtotime($journey->depart_date)) . ' - ' . date('Y.m.d', strtotime($journey->depart_date . ' + ' . ($journey->itinerary->nights) . ' days'))
        ];

        $decks = [];
        foreach ($journey->placements as $placement) {
            if (!isset($decks[$placement->cabin->deck_id]["name"]))
                $decks[$placement->cabin->deck_id]["name"] = $placement->cabin->deck->name;

            $paxes = [];
            if (isset($placement->reservation_placement))
                foreach ($placement->reservation_placement->reservation_paxes as $pax)
                    $paxes[] = $pax->name . ' ' . $pax->surname;

            $info = [
                "id" => $placement->id,
                "category" => $placement->cabin->type->type->name,
                "type" => $placement->cabin->type->name,
                "num" => $placement->cabin->num,
                "paxes" => implode(', ', $paxes),
                "status" => isset($placement->reservation_placement) ? 'Assigned' : 'Free'
            ];

            $decks[$placement->cabin->deck_id]["placements"][] = $info;
        }

        return view('assignment.list', [
            'journey_info' => $journey_info,
            'decks' => $decks
        ]);
    }

    public function PlacementAssignment($placement)
    {
        $reservations = $placement->journey->reservations()->where('with_names', true);

        if ($placement->journey->type != 'cruise' || !$reservations->exists())
            abort(500, 'Wrong placement.');
        
        $reservations_array = [];
        foreach ($reservations->get() as $reservation) {
            $placements = [];
            foreach ($reservation->placements as $other_placement)
                if (!isset($other_placement->assigned)) {
                    $placements[] = $this->placementPrice($other_placement);
                }

            if (!empty($placements)) {
                $reservations_array[$reservation->id]["reservation"] = [
                    'created_at' => $reservation->created_at,
                    'code' => $reservation->code
                ];
                $reservations_array[$reservation->id]["placements"] = $placements;
            }
        }
        if (isset($placement->reservation_placement))
            $placement->reservation_placement = $this->placementPrice($placement->reservation_placement);

        return view('assignment.form', [
            'placement' => $placement,
            'reservations' => $reservations_array,
        ]);
    }

    public function SaveAssignment(Request $request)
    {
        $this->validate($request, [
            'id' => 'required|exists:placements,id',
            'reservation_placement' => 'required|exists:reservation_placements,id',
            'note' => 'max:9999'
        ]);

        $reservation_placement = ReservationPlacement::find($request->reservation_placement);
        $placement = Placement::find($request->id);
        if (!$reservation_placement->reservation->with_names ||
            $reservation_placement->reservation->journey->type != 'cruise' ||
            $reservation_placement->assigned ||
            $reservation_placement->reservation->journey_id != $placement->journey_id)
            abort(500, 'Wrong reservation placement.');

        $placement->reservation_placement_id = $request->reservation_placement;
        $placement->note = $request->note;
        $placement->save();
    }

    public function ClearAssignment($placement)
    {
        if (!$placement->reservation_placement_id)
            abort(500, 'Already clear.');

        $placement->reservation_placement_id = NULL;
        $placement->save();
    }

    public function ConvertReservation(Request $request, $id)
    {
        $reservation = Reservation::findOrFail($id);

        $role_name = $this->roleName($reservation->type);
        if (!Auth::user()->hasRole('create_' . $reservation->journey->type . '_' . $role_name) || !Auth::user()->hasRole('edit_' . $reservation->journey->type . '_' . $role_name))
            return response('Forbidden.', 403);

        if (!($reservation->type == 'inquiry' || $reservation->type == 'option') || !isset($reservation->id))
            return redirect()->back();

        if ($reservation->type == 'option')
            $this->validate($request, ['to' => 'required|in:booking']);
        else
            $this->validate($request, ['to' => 'required|in:option,booking']);

        if ($reservation->type == 'inquiry') {
            $failed_entities = [];
            $placement_type = $reservation->journey->type == 'cruise' ? 'cruise_type' : 'placement';

            list($placementsAmount, $journeyServicesCount) = $this->checkReservationCopy($reservation, false);

            foreach ($reservation->journey->reservations as $other_reservation)
                if ($other_reservation->id != $reservation->id && $other_reservation->type != 'inquiry')
                    foreach ($other_reservation->placements as $placement) {
                        --$placementsAmount[$placement[$placement_type]->id];

                        if (!$placementsAmount[$placement[$placement_type]->id])
                            if ($reservation->journey->type == 'cruise')
                                $failed_entities['placements'][$placement[$placement_type]->id] = $placement->cruise_type->type->name;
                            else
                                $failed_entities['placements'][$placement[$placement_type]->id] = $placement->placement->room->type->name;
                        else {
                            foreach ($placement->services as $reservationService) {
                                if ($journeyServicesCount[$reservationService->journey_service_id] - $placement->reservation_paxes->count())
                                    $journeyServicesCount[$reservationService->journey_service_id] -= $placement->reservation_paxes->count();
                                else if (!isset($failed_entities['services'][$reservationService->journey_service_id]))
                                    $failed_entities['services'][$reservationService->journey_service_id] = $reservationService->service[$reservationService->service_id ? 'service' : 'package']->name;
                            }
                            foreach ($placement->reservation_paxes as $reservationPax) {
                                foreach ($reservationPax->services as $reservationPaxService) {
                                    if ($journeyServicesCount[$reservationPaxService->journey_service_id] - 1)
                                        $journeyServicesCount[$reservationPaxService->journey_service_id]--;
                                    else if (!isset($failed_copies['services'][$reservationPaxService->journey_service_id]))
                                        $failed_entities['services'][$reservationPaxService->journey_service_id] = $reservationPaxService->service[$reservationPaxService->service->service_id ? 'service' : 'package']->name;
                                }
                            }
                        }
                    }

            if (!empty($failed_entities)) {
                $return = '<p>Cannot convert this reservation because of these:</p>';
                if (!empty($failed_entities['placements']))
                    $return .= '<p>Placements - ' . implode(', ', $failed_entities['placements']) . '</p>';
                if (!empty($failed_entities['services']))
                    $return .= '<p>Services - ' . implode(', ', $failed_entities['services']) . '</p>';

                return ['failed_entities' => $return];
            }
        }

        $this->notify(Auth::user()->id, $reservation->id, 'action', ucfirst($reservation->type) . ' was converted to ' . ucfirst($request->to) . ' - <a href="' . url('/' . $request->to . '/' . $reservation->id) . '">' . $reservation->code . '</a>');
        $this->notify(NULL, $reservation->id, 'update', 'E-mail was sent (' . ucfirst($reservation->type) . ' was converted to ' . ucfirst($request->to) . ') - <a href="' . url('/' . $reservation->type . '/' . $reservation->id) . '">' . $reservation->code . '</a>', NULL, NULL, [
            'type' => 'convert_reservation',
            'reservation' => $reservation
        ]);

        $reservation->type = $request->to;

        $reservation->code = $reservation->id . '-' .
            ($reservation->placements->count() ? $reservation->placements->count() : 0) . '-' .
            (isset($reservation->client) ? ($reservation->client->type == 'b2b' ? $reservation->client->code : 'B2C' . $reservation->client->id) : 0) . '-' .
            (isset($reservation->journey->release_date) ? strtoupper(substr(date('M', strtotime($reservation->journey->release_date)),0 ,3)) . date('d', strtotime($reservation->journey->release_date)) : 0) . '-' .
            (isset($reservation->journey->ship->code) ? $reservation->journey->ship->code : (isset($reservation->journey->hotel->code) ? $reservation->journey->hotel->code : 0)) . '-' .
            substr($reservation->year->year, -2, 2);

        $reservation->save();

        return url('/' . $reservation->type . '/' . $reservation->id);
    }

    public function SaveReservationField(Request $request)
    {
        $this->validate($request, [
            'id' => 'required|exists:reservations,id',
            'field' => 'required|in:payments_status,paid'
        ]);

        if ($request->field == 'payments_status') {
            $this->validate($request, [
                'value' => 'required|in:deposit_invoiced,deposit_paid,total_invoiced,total_paid',
            ]);
            $reservation = Reservation::where('type', 'booking')->findOrFail($request->id);
            $value = !empty($request->value) ? $request->value : NULL;
            $role_name = $this->roleName($reservation->type);
            $rules = ['create_' . $reservation->journey->type . '_' . $role_name, 'edit_' . $reservation->journey->type . '_' . $role_name, 'show_' . $reservation->journey->type . '_' . $role_name];
        } else {
            $reservation = Reservation::withTrashed()->where('deleted_at', '!=', NULL)->where('type', 'booking')->findOrFail($request->id);
            $value = $request->has('value');
            $role_name = $this->roleName($reservation->type);
            $rules = ['edit_' . $reservation->journey->type . '_' . $role_name . '_canceled', 'show_' . $reservation->journey->type . '_' . $role_name . '_canceled'];
        }

        foreach ($rules as $rule)
            if (!Auth::user()->hasRole($rule))
                return response(403, 'Forbidden.');

        //if (!Auth::user()->hasRole('create_' . $reservation->journey->type . '_bookings') || !Auth::user()->hasRole('edit_' . $reservation->journey->type . '_bookings'))
        //    return response(403, 'Forbidden.');

        $role_name = $this->roleName($reservation->type);
        foreach ($reservation->deleted_at ? ['edit_' . $reservation->journey->type . '_' . $role_name . '_canceled', 'show_' . $reservation->journey->type . '_' . $role_name . '_canceled'] : ['create_' . $reservation->journey->type . '_' . $role_name, 'edit_' . $reservation->journey->type . '_' . $role_name, 'show_' . $reservation->journey->type . '_' . $role_name] as $rule)
            if (!Auth::user()->hasRole($rule))
                return response(403, 'Forbidden.');

        $reservation[$request->field] = $value;
        $reservation->save();
    }

    public function Copy(Request $request, $id)
    {
        $reservation = Reservation::withTrashed()->findOrFail($id);
        
        $role_name = $this->roleName($reservation->type);
        foreach ($reservation->deleted_at ? ['edit_' . $reservation->journey->type . '_' . $role_name . '_canceled', 'show_' . $reservation->journey->type . '_' . $role_name . '_canceled'] : ['create_' . $reservation->journey->type . '_' . $role_name, 'edit_' . $reservation->journey->type . '_' . $role_name, 'show_' . $reservation->journey->type . '_' . $role_name] as $rule)
            if (!Auth::user()->hasRole($rule))
                return response(403, 'Forbidden.');

        $copy = $request->has('copy');

        if ($copy) {
            $newReservation = new Reservation;
            foreach (['journey_id', 'client_id', 'client_user_id', 'type', 'year_id'] as $field)
                $newReservation[$field] = $reservation[$field];

            $newReservation->save();
        }

        $failed_entities = [];
        $placementsAmount = [];
        $journeyServicesCount = [];
        $placement_type = $reservation->journey->type == 'cruise' ? 'cruise_type' : 'placement';

        if ($reservation->type != 'inquiry')
            list($placementsAmount, $journeyServicesCount) = $this->checkReservationCopy($reservation);

        foreach ($reservation->placements as $placement) {
            if (!$placementsAmount[$placement[$placement_type]->id]) {
                if (!isset($failed_entities['placements'][$placement->id]))
                    if ($reservation->journey->type == 'cruise')
                        $failed_entities['placements'][$placement->id] = $placement->cruise_type->type->name;
                    else
                        $failed_entities['placements'][$placement->id] = $placement->placement->room->type->name;
            } else {
                $placementsAmount[$placement[$placement_type]->id]--;
                if ($copy) {
                    $placement->reservation_id = $newReservation->id;
                    $newPlacement = $placement->replicate();
                    $newPlacement->save();
                }
                foreach ($placement->services as $reservationService) {
                    if ($journeyServicesCount[$reservationService->journey_service_id] - $placement->reservation_paxes->count()) {
                        $journeyServicesCount[$reservationService->journey_service_id] -= $placement->reservation_paxes->count();
                        if ($copy) {
                            $reservationService->reservation_placement_id = $newPlacement->id;
                            $newReservationService = $reservationService->replicate();
                            $newReservationService->save();
                        }
                    } else if (!isset($failed_entities['services'][$reservationService->journey_service_id]))
                        $failed_entities['services'][$reservationService->journey_service_id] = $reservationService->service[$reservationService->service_id ? 'service' : 'package']->name;
                }
                foreach ($placement->reservation_paxes as $reservationPax) {
                    if ($copy) {
                        $reservationPax->reservation_placement_id = $newPlacement->id;
                        $newReservationPax = $reservationPax->replicate();
                        $newReservationPax->save();
                    }
                    foreach ($reservationPax->services as $reservationPaxService) {
                        if ($journeyServicesCount[$reservationPaxService->journey_service_id] - 1) {
                            $journeyServicesCount[$reservationPaxService->journey_service_id]--;
                            if ($copy) {
                                $reservationPaxService->reservation_pax_id = $newReservationPax->id;
                                $newReservationPaxService = $reservationPaxService->replicate();
                                $newReservationPaxService->save();
                            }
                        } else if (!isset($failed_entities['services'][$reservationPaxService->journey_service_id]))
                            $failed_entities['services'][$reservationPaxService->journey_service_id] = $reservationPaxService->service[$reservationPaxService->service_id ? 'service' : 'package']->name;
                    }
                }
            }
        }

        if ($copy) {
            if ($reservation->deleted_at)
                foreach ($reservation->notifications as $notification) {
                    $notification->reservation_id = $newReservation->id;
                    $newNotification = $notification->replicate();
                    $newNotification->save();
                }

            $newReservation->code = $reservation->id . '-' .
                ($newReservation->placements->count() ? $newReservation->placements->count() : 0) . '-' .
                (isset($newReservation->client) ? ($newReservation->client->type == 'b2b' ? $newReservation->client->code : 'B2C' . $newReservation->client->id) : 0) . '-' .
                (isset($newReservation->journey->release_date) ? strtoupper(substr(date('M', strtotime($newReservation->journey->release_date)),0 ,3)) . date('d', strtotime($newReservation->journey->release_date)) : 0) . '-' .
                (isset($newReservation->journey->ship->code) ? $newReservation->journey->ship->code : (isset($newReservation->journey->hotel->code) ? $newReservation->journey->hotel->code : 0)) . '-' .
                substr($newReservation->year->year, -2, 2);

            $newReservation->save();
            return url('/' . $newReservation->type . '/' . $newReservation->id);
        } else {
            $return = '';
            if (!empty($failed_entities['placements']))
                $return .= '<p>These placements will not copy: ' . implode(', ', $failed_entities['placements']) . '.</p>';
            if (!empty($failed_entities['services']))
                $return .= '<p>These services will not copy: ' . implode(', ', $failed_entities['services']) . '.</p>';
            if (empty($failed_entities['placements']) && empty($failed_entities['services']))
                $return .= '<p>Everything is ok.</p>';
            return ['failed_entities' => $return];
        }
    }

    public function SetNumber(Request $request)
    {
        $this->validate($request, ['number' => 'required|in:10,50,100']);
        $request->session()->put('reservations_number', $request->number);

        return redirect()->route('reservations_list');
    }
}
