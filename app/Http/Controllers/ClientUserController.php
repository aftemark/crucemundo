<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\ClientUser;
use App\Http\Requests;
use App\Http\Traits\ReservationTrait;

class ClientUserController extends Controller
{
    use ReservationTrait;

    public function Info(Request $request)
    {
        if ($request->item != 'new') {
            $this->validate($request, [
                'item' => ['required|exists:client_users,id']
            ]);
            $user = ClientUser::findOrFail($request->item);
        }
        else
            $user = new ClientUser;

        $reservations = $this->reservationsList($user);

        return view('clients.user', array('user' => $user, 'reservations' => $reservations));
    }
    
    public function Save(Request $request)
    {
        $validate = [
            'client_id' => 'required|exists:clients,id',
            'name' => 'required|max:255',
            'post' => 'max:255',
            'email' => 'required|email',
            'contact_info' => 'max:9999'
        ];

        if ($request->has('id')) {
            $validate = $validate + array('id' => 'required|exists:client_users,id');
            $user = ClientUser::find($request->id);
        } else {
            $user = new ClientUser;
        }
        $this->validate($request, $validate);

        $user->client_id = $request->client_id;
        $user->name = $request->name;
        $user->post = $request->post;
        $user->email = $request->email;
        $user->contact_info = $request->contact_info;
        $user->save();

        return $user->id;
    }

    public function Delete($id)
    {
        $client = ClientUser::findOrFail($id);
        if ($return = \App\Http\Traits\HelperTrait::deleteCheckModel($client, ['reservations' => 'Reservations']))
            return response($return, 422);
        else
            $client->delete();
    }
}
