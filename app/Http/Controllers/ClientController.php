<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Client;
use App\ClientPercent;
use App\Country;
use App\Ship;
use App\Hotel;
use App\Language;
use App\Http\Requests;
use App\Http\Traits\ReservationTrait;

class ClientController extends Controller
{
    use ReservationTrait;
    
    public function Index(Request $request)
    {
        if ($request->has('table')) {
            $this->validate($request, [
                'table' => 'in:b2c,b2b'
            ]);

            if ($request->table == 'b2c')
                $this->validate($request, [
                    'field' => 'in:name,country,language,percent,loyal_number,all',
                    'order_field' => 'in:name,country,language,percent,loyal_number',
                    'order' => 'in:asc,desc'
                ]);
            else if ($request->table == 'b2b')
                $this->validate($request, [
                    'field' => 'in:name,country,all',
                    'order_field' => 'in:name,country',
                    'order' => 'in:asc,desc'
                ]);
        }

        $number = $request->session()->get('clients_number', 10);

        $b2b = Client::where('type', 'b2b');
        $b2c = Client::where('type', 'b2c');
        $table = $request->table;
        if ($request->has('search') && $request->has('field') && $request->has('table')) {
            if ($request->field == 'all') {
                $$table->search($request->search);
            } else if ($request->field == 'country') {
                $$table->search($request->search, ['country.name']);
            } else {
                $$table->search($request->search, [$request->field]);
            }
        }

        if ($request->has('order') && $request->has('order_field') && $request->has('table')) {
            if ($request->order_field == 'country')
                $$table->select('clients.*')->leftJoin('countries', 'country_id', '=', 'countries.id')->orderBy('countries.name', $request->order);
            else
                $$table->orderBy($request->order_field, $request->order);
        }

        return view('clients.list', array('data' => [
            'b2b' => $b2b->paginate($number, ['*'], 'b2b_clients'),
            'b2c' => $b2c->paginate($number, ['*'], 'b2c_clients')
        ]));
    }

    public function SetNumber(Request $request)
    {
        $this->validate($request, ['number' => 'required|in:10,50,100']);
        $request->session()->put('clients_number', $request->number);

        return redirect()->route('clients_list');
    }

    public function B2CInfo(Request $request)
    {
        $client = Client::whereType('b2c')->findOrFail($request->id);

        $reminders = collect([]);
        foreach ($client->reservations as $reservation)
            $reminders = $reminders->merge($reservation->notifications()->whereType('reminder')->get());

        return view('clients.b2c', [
            'client' => $client,
            'countries' => Country::select(['id', 'name'])->get(),
            'languages' => Language::select('id', 'name')->get(),
            'reminders' => $reminders,
            'reservations' => $this->reservationsList($client)
        ]);
    }

    public function B2CNew()
    {
        return view('clients.b2c', array('client' => new Client, 'countries' => Country::select(['id', 'name'])->get(), 'languages' => Language::select('id', 'name')->get()));
    }

    public function SetClientUsersNumber(Request $request)
    {
        $this->validate($request, ['number' => 'required|in:10,50,100']);
        $request->session()->put('client_user_number', $request->number);

        return redirect()->back();
    }

    public function B2BInfo(Request $request)
    {
        $number = $request->session()->get('client_user_number', 10);

        $client = Client::whereType('b2b')->findOrFail($request->id);

        $commissions = [];
        $allShips = [];
        $allHotels = [];

        foreach (Ship::all() as $ship) {
            $allShips[$ship->id]['name'] = $ship->name;
            $allShips[$ship->id]['selected'] = false;
        }

        foreach (Hotel::all() as $hotel) {
            $allHotels[$hotel->id]['name'] = $hotel->name;
            $allHotels[$hotel->id]['selected'] = false;
        }

        foreach ($client->percents as $percent) {
            if ($percent->ship_id) {
                $commissions[$percent->percent]["ships"][$percent->ship_id] = $percent->ship->name;
                $allShips[$percent->ship_id]['selected'] = true;
            }

            if ($percent->hotel_id) {
                $commissions[$percent->percent]["hotels"][$percent->hotel_id] = $percent->hotel->name;
                $allHotels[$percent->hotel_id]['selected'] = true;
            }
        }

        if (isset($client->user))
            $notifications = $client->user->notifications()->where('type', 'action')->get();
        else
            $notifications = [];
        $notifications = collect($notifications);
        foreach ($client->users as $clientUser)
            if (isset($clientUser->user))
                $notifications = $notifications->merge($clientUser->user->notifications()->where('type', 'action')->get());
                //$notifications = $clientUser->user->notifications()->where('type', 'action')->get();

        $reminders = collect([]);
        foreach ($client->reservations as $reservation)
            $reminders = $reminders->merge($reservation->notifications()->whereType('reminder')->get());
        foreach ($client->users as $clientUser)
            foreach ($clientUser->reservations as $reservation)
                $reminders = $reminders->merge($reservation->notifications()->whereType('reminder')->get());

        return view('clients.b2b', [
            'client' => $client,
            'users' => $client->users()->paginate($number),
            'countries' => Country::select('id', 'name')->get(),
            'commissions' => $commissions,
            'allShips' => $allShips,
            'allHotels' => $allHotels,
            'reminders' => $reminders->reverse(),
            'notifications' => $notifications->reverse(),
            'reservations' => $this->reservationsList($client)
        ]);
    }

    public function B2BNew()
    {
        $allShips = array();
        $allHotels = array();

        foreach (Ship::all() as $ship) {
            $allShips[$ship->id]['name'] = $ship->name;
            $allShips[$ship->id]['selected'] = false;
        }

        foreach (Hotel::all() as $hotel) {
            $allHotels[$hotel->id]['name'] = $hotel->name;
            $allHotels[$hotel->id]['selected'] = false;
        }
        return view('clients.b2b', array('client' => new Client, 'allShips' => $allShips, 'allHotels' => $allHotels, 'countries' => Country::all(), 'languages' => Language::all()));
    }

    public function Save(Request $request)
    {
        if ($request->type == "b2c")
            $validate = [
                'name' => 'required|max:255',
                'second_name' => 'required|max:255',
                'type' => 'required|in:b2c,b2b',
                'country' => 'required|exists:countries,id',
                'language' => 'required|exists:languages,id',
                'percent' => 'integer|between:0,100',
                'loyal_number' => 'max:255',
                'email' => 'email|max:255',
                'contact_info' => 'max:9999'
            ];
        else
            $validate = [
                'code' => 'max:255',
                'name' => 'required|max:255',
                'type' => 'required|in:b2c,b2b',
                'country' => 'required|exists:countries,id',
                'email' => 'email|max:255',
                'contact_info' => 'max:9999',
                'pay_info' => 'max:9999',
                'commission.*' => 'integer|between:0,100',
                'ships.*.*' => 'exists:ships,id',
                'hotels.*.*' => 'exists:hotels,id'
            ];

        if ($request->has('id')) {
            $this->validate($request, $validate + ['id' => 'required|exists:clients,id']);
            $client = Client::find($request->id);
        } else {
            $this->validate($request, $validate);
            $client = new Client;
        }

        $client->name = $request->name;
        $client->type = $request->type;
        $client->country_id = $request->country;
        $client->email = $request->email;
        $client->contact_info = $request->contact_info;

        if ($request->type == "b2c") {
            $client->second_name = $request->second_name;
            $client->language_id = $request->language;
            $client->percent = $request->percent;
            $client->loyal_number = $request->loyal_number;
        } else {
            $client->code = $request->code;
            $client->pay_info = $request->pay_info;
        }

        $client->save();

        $client->percents()->delete();

        if (!empty($request->commission)) {
            if ($request->type == "b2b") {
                foreach ($request->commission as $percent) {
                    if (!empty($request['ships'][$percent]))
                        foreach ($request['ships'][$percent] as $shipID) {
                            $newpercent = new ClientPercent;
                            $newpercent->ship_id = $shipID;
                            $newpercent->client_id = $client->id;
                            $newpercent->percent = $percent;
                            $newpercent->save();
                        }

                    if (!empty($request['hotels'][$percent]))
                        foreach ($request['hotels'][$percent] as $hotelID) {
                            $newpercent = new ClientPercent;
                            $newpercent->hotel_id = $hotelID;
                            $newpercent->client_id = $client->id;
                            $newpercent->percent = $percent;
                            $newpercent->save();
                        }
                }
            }
        }

        return $client->id;
    }

    public function Users($id)
    {
        $client = Client::findOrFail($id);
        return $client->type == 'b2b' ? Client::findOrFail($id)->users()->pluck('name', 'id') : response('Wrong client.', 422);
    }

    public function Delete($id)
    {
        $client = Client::findOrFail($id);
        if ($return = \App\Http\Traits\HelperTrait::deleteCheckModel($client, ['users' => 'Users', 'reservations' => 'Reservations']))
            return response($return, 422);
        else
            $client->delete();

        return redirect()->route('clients_list');
    }
}
