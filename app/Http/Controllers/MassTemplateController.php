<?php

namespace App\Http\Controllers;

use App\CustomTemplate;
use App\MassTemplateReceiver;
use Illuminate\Http\Request;
use App\MassTemplate;
use App\Client;
use App\ClientUser;
use App\Http\Requests;
use Mustache_Engine;
use App\Tags\ClientData;
use Auth;
use Mail;

class MassTemplateController extends Controller
{
    public function Index()
    {
        return view('mailing.list', [
            'notifications' => CustomTemplate::where('type', 'mail')->get(),
            'mass_templates' => MassTemplate::orderBy('created_at', 'desc')->get(),
            'allow_edit' => Auth::user()->hasRole('edit_mailing') && Auth::user()->hasRole('edit_administration')
        ]);
    }

    public function Info($item)
    {
        $template = $item != 'new' ? MassTemplate::findOrFail($item) : new MassTemplate;

        $emails = [];
        $emailHolders = collect(Client::all())->merge(ClientUser::all());
        foreach ($emailHolders as $emailHolder)
            $emails[] = [
                'id' => $emailHolder->id,
                'type' => $type = get_class($emailHolder) == 'App\Client' ? 'client' : 'client_user',
                'name' => ($type && $emailHolder->type == 'b2b' ? $emailHolder->code : $emailHolder->name) . (isset($emailHolder->second_name) ? ' ' . $emailHolder->second_name : ''),
                'email' => $emailHolder->email
            ];

        $picked = ['client' => [], 'client_user' => []];
        foreach ($template->receivers as $receiver)
            $picked[$receiver->client_id ? 'client' : 'client_user'][] = $receiver->client_id ? $receiver->client_id : $receiver->client_user_id;

        return view('mailing.mass', [
            'template' => $template,
            'emails' => $emails,
            'picked' => $picked
        ]);
    }

    public function Send(Request $request)
    {
        $this->validate($request, [
            'id' => 'required|exists:mail_templates,id',
            'subject' => 'required|max:255',
            'text' => 'required',
            'client.*' => 'exists:clients,id',
            'client_user.*' => 'exists:client_users,id'
        ]);

        $template = MassTemplate::findOrFail($request->id);
        $template->subject = $request->subject;
        $template->text = $request->text;

        $template = $template->replicate();
        $template->save();

        foreach (($request->has('client') ? array_unique($request->client) : []) as $client) {
            $receiver = new MassTemplateReceiver;
            $receiver->mail_template_id = $template->id;
            $receiver->client_id = $client;
            $receiver->save();
        }

        foreach (($request->has('client_user') ? array_unique($request->client_user) : []) as $client_user) {
            $receiver = new MassTemplateReceiver;
            $receiver->mail_template_id = $template->id;
            $receiver->client_user_id = $client_user;
            $receiver->save();
        }

        $data = ['subject' => $template->subject];
        $count = 0;
        foreach ($template->receivers as $receiver) {
            $m = new Mustache_Engine;
            $data += [
                'email' => $receiver->client->email,
                'content' => $m->render($template->text, new ClientData($receiver->client))
            ];
            Mail::send('emails.mass', ['data' => $data], function ($m) use ($data) {
                $m->to($data['email'])->subject($data['subject']);
            });
            $count++;
        }

        return redirect()->route('mailing')->with(['message' => $count . ' e-mails were sent.']);
    }
}
