<?php

namespace App\Http\Controllers;

use App\CruiseType;
use Illuminate\Http\Request;
use App\Journey;
use App\JourneyService;
use App\Ship;
use App\Service;
use App\Itinerary;
use App\NonCompatible;
use App\NonCompatibleService;
use App\Placement;
use App\SupplementGroup;
use App\JourneyFee;
use App\Special;
use App\SpecialRelation;
use App\ServicePackage;
use App\ItineraryCity;
use App\Http\Requests;
use App\Http\Traits\ReservationTrait;

class CruiseController extends Controller
{
    use ReservationTrait;

    public function __construct(Request $request)
    {
        $this->year = $request->session()->get('catalog_year_id');
    }

    public function Index(Request $request)
    {
        $this->validate($request, [
            'field' => 'in:depart_date,ship.name,itinerary.name,itinerary.from_to,itinerary.nights,season',
            'order' => 'in:asc,desc',
            'order_field' => 'in:depart_date,ship.name,itinerary.name,itinerary.from_to,itinerary.nights,season'
        ]);

        $data = Journey::where('type', 'cruise')->where('year_id', $this->year);

        if ($request->has('search') && $request->has('field')) {
            if ($request->type == 'all')
                $data->search($request->search);
            else
                $data->search($request->search, [$request->field]);
        }

        if ($request->has('order') && $request->has('order_field')) {
            if ($request->order_field == 'ship.name')
                $data->select('journeys.*')->leftJoin('ships', 'ship_id', '=', 'ships.id')->orderBy('ships.name', $request->order);
            else if ($request->order_field == 'itinerary.name')
                $data->select('journeys.*')->leftJoin('itineraries', 'itinerary_id', '=', 'itineraries.id')->orderBy('itineraries.name', $request->order);
            else if ($request->order_field == 'itinerary.from_to')
                $data->select('journeys.*')->leftJoin('itineraries', 'itinerary_id', '=', 'itineraries.id')->orderBy('itineraries.from_to', $request->order);
            else if ($request->order_field == 'itinerary.nights')
                $data->select('journeys.*')->leftJoin('itineraries', 'itinerary_id', '=', 'itineraries.id')->orderBy('itineraries.nights', $request->order);
            else
                $data->orderBy($request->order_field, $request->order);
        } else
            $data->orderBy('updated_at', 'desc');

        $data = $data->paginate($request->session()->get('cruises_number', 10));
        foreach ($data as $cruise) {
            $beds = $cruise->ship->add_bed;
            if (isset($cruise->ship)) {
                foreach ($cruise->placements as $placement) {
                    if ($placement->display)
                        $cruise->places += $placement->cabin->type->pax_amount;
                    if ($beds > 1 && $placement->cabin->type->bed) {
                        --$beds;
                        $cruise->places += 1;
                    }
                }
            }
        }

        return view('cruises.list', array('data' => $data));
    }

    public function Info($item)
    {
        if ($item != 'new')
            $cruise = Journey::where('type', 'cruise')->findOrFail($item);
        else
            $cruise = new Journey;

        if(isset($cruise->depart_date) && isset($cruise->itinerary))
            $cruise->ending_date = date('Y-m-d', strtotime($cruise->depart_date . ' + ' . ($cruise->itinerary->nights) . ' days'));

        $services = array();
        if (isset($cruise->services)) {
            foreach ($cruise->services as $service) {
                if ($service->itinerary_city_id !== NULL) {
                    if (isset($service->service_id))
                        $services[$service->itinerary_city_id]["service"][$service->service_id] = $service;
                    else if (isset($service->service_package_id))
                        $services[$service->itinerary_city_id]["package"][$service->service_package_id] = $service;
                }
                else {
                    if (isset($service->service_id))
                        $services["other"]["service"][$service->service_id] = $service;
                    else if (isset($service->service_package_id))
                    $services["other"]["package"][$service->service_package_id] = $service;
                }
            }
        }

        $otherServices = array();
        if (isset($cruise->ship))
            $otherServices = collect($cruise->ship->services()->where('year_id', $this->year)->get())
                ->merge(collect(Service::where('service_type', 'other')->where('year_id', $this->year)->get()))
                ->merge(collect(Service::where('service_type', 'catering')->where('other', '!=', NULL)->where('year_id', $this->year)->get()))->unique();

        $cities = array();
        $days = array();
        $nonServices = array();
        if (isset($cruise->itinerary)) {
            foreach ($cruise->itinerary->relations as $itinerary) {
                $days[$itinerary->day][$itinerary->id] = $itinerary->city;
                $cities[] = $itinerary->id;
            }

            $nonServices = array();
            $counter = 0;
            if (isset($cruise->nonDays)) {
                foreach ($cruise->nonDays as $nonDay) {
                    if (in_array($nonDay->itinerary_city_id, $cities) || $nonDay->itinerary_city_id === NULL) {
                        ++$counter;
                        foreach ($nonDay->services as $nonService) {
                            $nonService->counter = $counter;
                            if ($nonDay->itinerary_city_id)
                                $nonServices[$nonDay->itinerary_city_id][$nonDay->id][$nonService->journey_service_id] = $nonService;
                            else
                                $nonServices["other"][$nonDay->id][$nonService->journey_service_id] = $nonService;
                        }
                    }
                }
            }
        }
        ksort($days);

        $selectedCabins = array();
        if (isset($cruise->groups))
            foreach ($cruise->groups as $group)
                foreach ($group->placements as $placement)
                    $selectedCabins[] = $placement->cabin_id;

        $reservations = $this->reservationsList($cruise);

        $types_info = [];
        if ($cruise->placements->count()) {
            $type_fields = [
                'factual_cabins' => 0,
                'factual_pax' => 0,
                'overbooked_cabins' => 0,
                'overbooked_pax' => 0,
                'sell_cabins' => 0,
                'sell_pax' => 0,
                'names_cabins' => 0,
                'names_pax' => 0,
                'no_names_cabins' => 0,
                'no_names_pax' => 0,
                'option_cabins' => 0,
                'option_pax' => 0,
                'booked_options_cabins' => 0,
                'booked_options_pax' => 0,
                'available_cabins' => 0,
                'available_pax' => 0,
                'inquiry_cabins' => 0,
                'inquiry_pax' => 0
            ];
            $types_info["overall"] = $type_fields;

            foreach ($cruise->types as $cruise_type) {
                $types_info["types"][$cruise_type->id] = $type_fields;
                $types_info["types"][$cruise_type->id]['overbooked_cabins'] = $cruise_type->overbooking;
                $types_info["types"][$cruise_type->id]['overbooked_pax'] = $cruise_type->overbooking * ($cruise_type->type->bed ? $cruise_type->type->pax_amount + 1 : $cruise_type->type->pax_amount);
                $types_info["types"][$cruise_type->id]['available_cabins'] = $cruise_type->placements->count() + $cruise_type->overbooking;
                $types_info["types"][$cruise_type->id]['available_pax'] = $cruise_type->overbooking + $cruise_type->placements->count() * ($cruise_type->type->bed ? $cruise_type->type->pax_amount + 1 : $cruise_type->type->pax_amount);
            }

            foreach ($cruise->placements as $placement) {
                if (isset($placement->reservation_placement)) {
                    $types_info["types"][$placement->cruise_type_id]["factual_cabins"]++;
                    $types_info["types"][$placement->cruise_type_id]["factual_pax"] += $placement->reservation_placement->reservation_paxes->count();
                    $types_info["types"][$placement->cruise_type_id]["available_cabins"]--;
                    $types_info["types"][$placement->cruise_type_id]["available_pax"] -= $placement->reservation_placement->reservation_paxes->count();
                } else {
                    $types_info["types"][$placement->cruise_type_id]["sell_cabins"]++;
                    $types_info["types"][$placement->cruise_type_id]["sell_pax"] += $placement->cruise_type->type->bed ? $placement->cruise_type->type->pax_amount + 1 : $placement->cruise_type->type->pax_amount;
                }
            }

            foreach ($cruise->reservations as $reservation) {
                foreach ($reservation->placements as $placement) {
                    if ($reservation->type == 'booking') {
                        if ($reservation->with_names) {
                            $types_info["types"][$placement->cruise_type_id]["names_cabins"]++;
                            $types_info["types"][$placement->cruise_type_id]["names_pax"] += $placement->reservation_paxes->count();
                        } else {
                            $types_info["types"][$placement->cruise_type_id]["no_names_cabins"]++;
                            $types_info["types"][$placement->cruise_type_id]["no_names_pax"] += $placement->cruise_type->type->bed ? $placement->cruise_type->type->pax_amount + 1 : $placement->cruise_type->type->pax_amount;
                            $types_info["types"][$placement->cruise_type_id]["available_cabins"]--;
                            $types_info["types"][$placement->cruise_type_id]["available_pax"] -= $placement->reservation_paxes->count();
                            $types_info["types"][$placement->cruise_type_id]["booked_options_cabins"]++;
                            $types_info["types"][$placement->cruise_type_id]["booked_options_pax"] += $placement->reservation_paxes->count();
                        }
                    } else if ($reservation->type == 'option') {
                        $types_info["types"][$placement->cruise_type_id]["option_cabins"]++;
                        $types_info["types"][$placement->cruise_type_id]["option_pax"] += $placement->reservation_paxes->count();
                        $types_info["types"][$placement->cruise_type_id]["available_cabins"]--;
                        $types_info["types"][$placement->cruise_type_id]["available_pax"] -= $placement->reservation_paxes->count();
                        $types_info["types"][$placement->cruise_type_id]["booked_options_cabins"]++;
                        $types_info["types"][$placement->cruise_type_id]["booked_options_pax"] += $placement->reservation_paxes->count();
                    } else if ($reservation->type == 'inquiry') {
                        $types_info["types"][$placement->cruise_type_id]["inquiry_cabins"]++;
                        $types_info["types"][$placement->cruise_type_id]["inquiry_pax"] += $placement->reservation_paxes->count();
                    }
                }
            }

            foreach ($types_info["types"] as $type_fields)
                foreach ($type_fields as $field_key => $field_value)
                    $types_info["overall"][$field_key] += $field_value;
        }

        return view('cruises.form', array(
            'data' => $cruise,
            'otherServices' => $otherServices,
            'ships' => Ship::select(['id', 'name'])->get(),
            'cruises' => Journey::where('type', 'cruise')->select(['id', 'code'])->get(),
            'itineraries' => Itinerary::select(['id', 'name', 'from_to'])->get(),
            'services' => $services,
            'nonServices' => $nonServices,
            'days' => $days,
            'selectedCabins' => $selectedCabins,
            'reservations' => $reservations,
            'types_info' => $types_info
        ));
    }

    public function Save(Request $request)
    {
        $validate = [
            'code' => 'required|max:255',
            'ship' => 'required|exists:ships,id',
            'season' => 'required|in:middle,high,low',
            'depart_date' => 'required|date|after:journey_release_date',
            'journey_release_date' => 'required|date',
            'journey_remind' => 'required|integer',
            'description' => 'max:9999',
            'booking_percent' => 'integer',
            'full_pay' => 'integer',
            'itinerary' => 'exists:itineraries,id'
        ];
        if ($request->has('id')) {
            $cruise = Journey::findOrFail($request->id);
            $validate['itinerary'] = 'required|exists:itineraries,id';
        } else
            $cruise = new Journey;

        if (isset($cruise->itinerary)) {
            $validate = $validate + [
                    'radio_service.*.*' => 'in:optional,included',
                    'display_service.*.*' => 'in:true',
                    'release_date_service.*.*' => 'date',
                    'remind_service.*.*' => 'integer',
                    'radio_package.*.*' => 'in:optional,included',
                    'display_package.*.*' => 'in:true',
                    'release_date_package.*.*' => 'date',
                    'remind_package.*.*' => 'integer'
                ];
            if (!empty($request["nonCompatibles"]))
                $validate = $validate + [
                        'supplement_rate.*' => 'numeric|between:0,99.99',
                        'supplement_group.*.*' => 'exists:cabins,id'
                    ];
        }
        if (isset($cruise->ship)) {
            $validate = $validate + [
                    'netPrice.*' => 'numeric|between:0,9999999.99',
                    'recPrice.*' => 'numeric|between:0,9999999.99'
                ];
            if (isset($request["supplement_rate"]))
                $validate = $validate + [
                        'supplement_rate.*' => 'numeric|between:0,99.99',
                        'supplement_group.*.*' => 'exists:cabins,id',
                        'placements_amount.*' => 'numeric|integer',
                        'deck_id.*' => 'required_with:supplement_rate.*|required_with:supplement_group.*|required_with:placements_amount.*|exists:decks,id'
                    ];
        }
        $validate = $validate + [
                'fee_percent.*' => 'numeric|between:0,99.99',
                'fee_from.*' => 'integer',
                'fee_to.*' => 'integer'
            ];

        $this->validate($request, $validate);

        $cruise->type = 'cruise';
        $cruise->code = $request->code;
        $cruise->ship_id = $request->ship;
        $cruise->description = $request->description;
        $cruise->release_date = $request->journey_release_date;
        $cruise->depart_date = $request->depart_date;
        $cruise->remind = $request->journey_remind;
        if (isset($request->booking_percent))
            $cruise->booking_percent = $request->booking_percent;
        if (isset($request->full_pay))
            $cruise->full_pay = $request->full_pay;

        $cruise->fees()->delete();
        $cruise->groups()->delete();
        foreach ($cruise->nonDays as $nonDay) {
            if (isset($nonDay->services))
                foreach ($nonDay->services as $nonDayService) {
                    $nonDayService->delete();
                }
            $nonDay->delete();
        }
        $cruiseItinerary = $cruise->itinerary;

        if (isset($request->itinerary) && $cruise->itinerary_id != $request->itinerary) {
            foreach ($cruise->specials as $special)
                foreach ($special->relations as $relation) {
                    $relation->placement_id = NULL;
                    $relation->journey_service_id = NULL;
                    $relation->save();
                }

            $cruise->services()->delete();
            $cruise->itinerary_id = $request->itinerary;

            $cruiseItinerary = Itinerary::find($request->itinerary);
        }

        if (isset($request->ship) && $cruise->ship_id != $request->ship) {
            $cruise->placements()->delete();
        }

        $cruise->year_id = $this->year;
        $cruise->save();

        if (isset($request["fee_percent"]))
            foreach ($request["fee_percent"] as $feeNum => $fee_percent) {
                if ($fee_percent > 0) {
                    $journeyFee = new JourneyFee;
                    $journeyFee->journey_id = $cruise->id;
                    $journeyFee->percent = $fee_percent;
                    $journeyFee->from = $request["fee_from"][$feeNum];
                    if (isset($request["fee_to"][$feeNum]))
                        $journeyFee->to = $request["fee_to"][$feeNum];
                    $journeyFee->save();
                }
            }

        if (isset($cruise->itinerary))
            foreach ($cruiseItinerary->relations as $itinerary) {
                $usedPackages = array();
                foreach ($itinerary->city->services->where('year_id', $this->year) as $service) {
                    $findService = $cruise->services->where('service_id', $service->id)->where('itinerary_city_id', $itinerary->id)->first();
                    if ($findService === NULL) {
                        $newService = new JourneyService;
                        $newService->journey_id = $cruise->id;
                        $newService->itinerary_city_id = $itinerary->id;
                        $newService->service_id = $service->id;
                        $newService->save();
                    } else {
                        $newService = $findService;
                    }
                    if (isset($request["radio_service"][$itinerary->id][$service->id]) &&
                        ($request["radio_service"][$itinerary->id][$service->id] == 'included' || $request["radio_service"][$itinerary->id][$service->id] == 'optional')
                    ) {
                        $newService->journey_id = $cruise->id;
                        $newService->itinerary_city_id = $itinerary->id;
                        $newService->service_id = $service->id;
                        $newService->included = $request["radio_service"][$itinerary->id][$service->id] == 'included' ? true : false;
                        $newService->optional = $request["radio_service"][$itinerary->id][$service->id] == 'optional' ? true : false;
                        $newService->display = isset($request["display_service"][$itinerary->id][$service->id]) && $request["display_service"][$itinerary->id][$service->id] == "true" ? true : false;
                        $newService->release_date = $request["release_date_service"][$itinerary->id][$service->id] ? $request["release_date_service"][$itinerary->id][$service->id] : NULL;
                        $newService->remind = $request["remind_service"][$itinerary->id][$service->id] ? $request["remind_service"][$itinerary->id][$service->id] : NULL;
                        $newService->save();
                    }
                    foreach ($service->packages as $package) {
                        $findPackage = $cruise->services()->where('service_package_id', $package->service_package_id)->where('itinerary_city_id', $itinerary->id)->first();
                        if ($findPackage === NULL) {
                            $newPackage = new JourneyService;
                            $newPackage->journey_id = $cruise->id;
                            $newPackage->itinerary_city_id = $itinerary->id;
                            $newPackage->service_package_id = $package->service_package_id;
                            $newPackage->save();
                        } else {
                            $newPackage = $findPackage;
                        }
                        if (!isset($usedPackages[$itinerary->id][$package->service_package_id]) && isset($request["radio_package"][$itinerary->id][$package->service_package_id]) &&
                            ($request["radio_package"][$itinerary->id][$package->service_package_id] == 'included' || $request["radio_package"][$itinerary->id][$package->service_package_id] == 'optional')
                        ) {
                            $usedPackages[$itinerary->id][$package->service_package_id] = true;
                            $newPackage->journey_id = $cruise->id;
                            $newPackage->itinerary_city_id = $itinerary->id;
                            $newPackage->service_package_id = $package->service_package_id;
                            $newPackage->included = $request["radio_package"][$itinerary->id][$package->service_package_id] == 'included' ? true : false;
                            $newPackage->optional = $request["radio_package"][$itinerary->id][$package->service_package_id] == 'optional' ? true : false;
                            $newPackage->display = isset($request["display_package"][$itinerary->id][$package->service_package_id]) && $request["display_package"][$itinerary->id][$package->service_package_id] == "true" ? true : false;
                            $newPackage->release_date = $request["release_date_package"][$itinerary->id][$package->service_package_id] ? $request["release_date_package"][$itinerary->id][$package->service_package_id] : NULL;
                            $newPackage->remind = $request["remind_package"][$itinerary->id][$package->service_package_id] ? $request["remind_package"][$itinerary->id][$package->service_package_id] : NULL;
                            $newPackage->save();
                        }
                    }
                }
            }

        if (isset($cruise->ship)) {
            $this->validate($request, [
                'netPrice.*' => 'numeric|between:0,9999999.99',
                'recPrice.*' => 'numeric|between:0,9999999.99',
                'overbooking.*' => 'numeric|integer'
            ]);

            $usedPackages = array();
            $otherServices = collect($cruise->ship->services()->where('year_id', $this->year)->get())
                ->merge(collect(Service::where('service_type', 'other')->where('year_id', $this->year)->get()))
                ->merge(collect(Service::where('service_type', 'catering')->where('other', '!=', NULL)->where('year_id', $this->year)->get()))->unique();

            foreach ($otherServices as $otherService) {
                $findService = $cruise->services->where('service_id', $otherService->id)->where('itinerary_city_id', NULL)->first();
                if ($findService === NULL) {
                    $newService = new JourneyService;
                    $newService->journey_id = $cruise->id;
                    $newService->service_id = $otherService->id;
                    $newService->save();
                } else {
                    $newService = $findService;
                }
                if (isset($request["radio_service"]["other"][$otherService->id]) &&
                    ($request["radio_service"]["other"][$otherService->id] == 'included' || $request["radio"]["other"][$otherService->id] == 'optional')
                ) {
                    $newService->journey_id = $cruise->id;
                    $newService->itinerary_city_id = NULL;
                    $newService->service_id = $otherService->id;
                    $newService->included = $request["radio_service"]["other"][$otherService->id] == 'included' ? true : false;
                    $newService->optional = $request["radio_service"]["other"][$otherService->id] == 'optional' ? true : false;
                    $newService->display = isset($request["display_service"]["other"][$otherService->id]) && $request["display_service"]["other"][$otherService->id] == "true" ? true : false;
                    $newService->release_date = $request["release_date_service"]["other"][$otherService->id] ? $request["release_date_service"]["other"][$otherService->id] : NULL;
                    $newService->remind = $request["remind_service"]["other"][$otherService->id] ? $request["remind_service"]["other"][$otherService->id] : NULL;
                    $newService->save();
                }
                foreach ($otherService->packages as $otherPackage) {
                    $findPackage = $cruise->services->where('service_package_id', $otherPackage->service_package_id)->where('itinerary_city_id', NULL)->first();
                    if ($findPackage === NULL) {
                        $newPackage = new JourneyService;
                        $newPackage->journey_id = $cruise->id;
                        $newPackage->service_package_id = $otherPackage->service_package_id;
                        $newPackage->save();
                    } else {
                        $newPackage = $findPackage;
                    }
                    if (!isset($usedPackages[$otherPackage->servicePackage->id]) && isset($request["radio_package"]["other"][$otherPackage->servicePackage->id]) &&
                        ($request["radio_package"]["other"][$otherPackage->servicePackage->id] == 'included' || $request["radio_package"]["other"][$otherPackage->servicePackage->id] == 'optional')
                    ) {
                        $usedPackages[$otherPackage->servicePackage->id] = true;
                        $newPackage->journey_id = $cruise->id;
                        $newPackage->itinerary_city_id = NULL;
                        $newPackage->service_package_id = $otherPackage->servicePackage->id;
                        $newPackage->included = $request["radio_package"]["other"][$otherPackage->servicePackage->id] == 'included' ? true : false;
                        $newPackage->optional = $request["radio_package"]["other"][$otherPackage->servicePackage->id] == 'optional' ? true : false;
                        $newPackage->display = isset($request["display_package"]["other"][$otherPackage->servicePackage->id]) && $request["display_package"]["other"][$otherPackage->servicePackage->id] == "true" ? true : false;
                        $newPackage->release_date = $request["release_date_package"]["other"][$otherPackage->servicePackage->id] ? $request["release_date_package"]["other"][$otherPackage->servicePackage->id] : NULL;
                        $newPackage->remind = $request["remind_package"]["other"][$otherPackage->servicePackage->id] ? $request["remind_package"]["other"][$otherPackage->servicePackage->id] : NULL;
                        $newPackage->save();
                    }
                }
            }

            foreach ($cruise->ship->alldecks as $deck) {
                foreach ($deck->cabins as $cabin) {
                    $findPlacement = $cruise->placements->where('cabin_id', $cabin->id)->first();
                    if ($findPlacement === NULL) {
                        $placement = new Placement;
                        $placement->journey_id = $cruise->id;
                        $placement->cabin_id = $cabin->id;
                        $placement->save();

                        $cruise_type = $placement->cabin->deck->types()->where('journey_id', $cruise->id)->where('type_category_id', $placement->cabin->type_category_id)->first();
                        if ($cruise_type === NULL) {
                            $cruise_type = new CruiseType;
                            $cruise_type->journey_id = $placement->journey_id;
                            $cruise_type->type_category_id = $placement->cabin->type_category_id;
                            $cruise_type->deck_id = $placement->cabin->deck_id;
                            $cruise_type->save();
                        }

                        $placement->cruise_type_id = $cruise_type->id;
                    } else
                        $placement = $findPlacement;

                    $placement->display = isset($request["cabinInclude"][$cabin->id]);
                    $placement->save();
                }
            }

            foreach ($cruise->types as $cruise_type) {
                if ($cruise_type->price_net != $request->input('netPrice.' . $cruise_type->id, 0))
                    $cruise_type->price_net = $request->input('netPrice.' . $cruise_type->id, 0);
                if ($cruise_type->price_rec != $request->input('recPrice.' . $cruise_type->id, 0))
                    $cruise_type->price_rec = $request->input('recPrice.' . $cruise_type->id, 0);
                if ($cruise_type->overbooking != $request->input('overbooking.' . $cruise_type->id, 0))
                    $cruise_type->overbooking = $request->input('overbooking.' . $cruise_type->id, 0);
                $cruise_type->save();
            }

            if (isset($request["supplement_rate"])) {
                foreach ($request["supplement_rate"] as $num => $rate) {
                    if (isset($request["supplement_group"][$num]) && !empty($request["supplement_group"][$num])) {
                        $supplement_group = new SupplementGroup;
                        $supplement_group->journey_id = $cruise->id;
                        $supplement_group->percent = $rate;
                        $supplement_group->placements_amount = $request->input('placements_amount.' . $num, 0);
                        $supplement_group->deck_id = $request->input('deck_id.' . $num, 0);
                        $supplement_group->save();
                        foreach ($request["supplement_group"][$num] as $requestPlacement) {
                            $groupPlacement = $cruise->placements()->where('cabin_id', $requestPlacement)->first();
                            $groupPlacement->supplement_group_id = $supplement_group->id;
                            $groupPlacement->save();
                        }
                    }
                }
            }
        }

        if (isset($cruise->itinerary)) {
            foreach ($cruiseItinerary->relations as $relation) {
                if (!empty($request["nonCompatibles"][$relation->id])) {
                    foreach ($request["nonCompatibles"][$relation->id] as $nonPackageID) {
                        if (count($nonPackageID) > 1) {
                            $newNon = new NonCompatible;
                            $newNon->journey_id = $cruise->id;
                            $newNon->itinerary_city_id = $relation->id;
                            $newNon->save();

                            foreach ($nonPackageID as $nonServiceID) {
                                if (JourneyService::find($nonServiceID) !== NULL) {
                                    $nonService = new NonCompatibleService;
                                    $nonService->non_compatible_id = $newNon->id;
                                    $nonService->journey_service_id = $nonServiceID;
                                    $nonService->save();
                                }
                            }
                        }
                    }
                }
            }
            if (!empty($request["nonCompatibles"]["other"])) {
                foreach ($request["nonCompatibles"]["other"] as $nonPackageID) {
                    if (count($nonPackageID) > 1) {
                        $newNon = new NonCompatible;
                        $newNon->journey_id = $cruise->id;
                        $newNon->save();

                        foreach ($nonPackageID as $nonServiceID) {
                            if (JourneyService::find($nonServiceID) !== NULL) {
                                $nonService = new NonCompatibleService;
                                $nonService->non_compatible_id = $newNon->id;
                                $nonService->journey_service_id = $nonServiceID;
                                $nonService->save();
                            }
                        }
                    }
                }
            }
        }

        return $cruise->id;
    }

    public function SetNumber(Request $request)
    {
        $this->validate($request, ['number' => 'required|in:10,50,100']);
        $request->session()->put('cruises_number', $request->number);

        return redirect()->route('cruises_list');
    }

    public function Delete($id)
    {
        $journey = Journey::findOrFail($id);

        if ($journey->type != 'cruise')
            abort(404);

        if ($return = \App\Http\Traits\HelperTrait::deleteCheckModel($journey, ['reservations' => 'Reservation']))
            return response($return, 422);
        else
            $journey->delete();
        return redirect()->back();
    }

    public function Copy($id)
    {
        $journey = Journey::findOrFail($id);

        if ($journey->type != 'cruise')
            return redirect()->back();

        $journeyID = \App\Http\Traits\HelperTrait::journeyCopy($journey, ['ship_id', 'season', 'itinerary_id', 'booking_percent', 'full_pay', 'type', 'year_id']);

        return redirect('/cruise/' . $journeyID);
    }
}
