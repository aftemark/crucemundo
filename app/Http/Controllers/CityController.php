<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Country;
use App\City;
use App\Http\Requests;

class CityController extends Controller
{
    public function Index(Request $request)
    {
        $this->validate($request, [
            'field' => 'in:country,city,all',
            'order' => 'in:asc,desc',
            'order_field' => 'in:country,city'
        ]);

        $data = City::select('*');
        if ($request->has('search') && $request->has('field'))
            if ($request->field == 'country') {
                $data->search($request->search, ['country.name']);
            } else if ($request->field == 'city') {
                $data->search($request->search, ['name']);
            } else if ($request->field == 'all') {
                $data->search($request->search, ['id', 'name', 'description', 'country.name']);
            }

        if ($request->has('order') && $request->has('order_field')) {
            if ($request->order_field == 'country')
                $data->select('cities.*')->leftJoin('countries', 'country_id', '=', 'countries.id')->orderBy('countries.name', $request->order);
            else if ($request->order_field == 'city')
                $data->orderBy('name', $request->order);
        } else
            $data->orderBy('updated_at', 'desc');

        return view('cities.list', array('data' => $data->paginate($request->session()->get('cities_number', 10), ['*'], 'cities_number')));
    }

    public function Info($item)
    {
        $city = $item == 'new' ? new City : City::find($item);

        return view('cities.form', [
            'data' => $city,
            'countries' => Country::pluck('name', 'id')
        ]);
    }

    public function Save(Request $request)
    {
        if ($request->has('id')) {
            $this->validate($request, [
                'id' => 'required|exists:cities,id',
                'city' => 'required|max:255',
                'country' => 'required|exists:countries,id',
                'description' => 'max:9999']);
            $country = City::find($request->id);
        } else {
            $this->validate($request, [
                'city' => 'required|max:255',
                'country' => 'required|exists:countries,id',
                'description' => 'max:9999'
            ]);
            $country = new City;
        }
        $country->name = $request->city;
        $country->country_id = $request->country;
        $country->description = $request->description;
        $country->save();
    }

    public function SetNumber(Request $request)
    {
        $this->validate($request, ['number' => 'required|in:10,50,100']);
        $request->session()->put('cities_number', $request->number);

        return redirect()->route('cities_list');
    }

    public function Delete($id)
    {
        $city = City::findOrFail($id);
        if ($return = \App\Http\Traits\HelperTrait::deleteCheckModel($city, ['airports' => 'Airports', 'hotels' => 'Hotels', 'services' => 'Services', 'itinerary_cities' => 'Itineraries']))
            return response($return, 422);
        else
            $city->delete();
    }
}
