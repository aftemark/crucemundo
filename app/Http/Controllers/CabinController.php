<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Cabin;
use App\PlacementType;
use App\Http\Requests;

class CabinController extends Controller
{
    public function Info ($item)
    {
        $types = PlacementType::all();

        if ($item != 'new') {
            $cabin = Cabin::find($item);
            $cabin->categories = array();
            $cabin->types = array();
            
            foreach($types as $type) {
                foreach ($type->categories as $category) {
                    if($category->typename == 'ship') {
                        if($type->id == $cabin->type->type->id)
                            $cabin->types = [$type->id => $type->name] + $cabin->types;
                        else
                            $cabin->types = $cabin->types + [$type->id => $type->name];
                        if ($type->id == $cabin->type->type->id) {
                            if ($category->id == $cabin->type_category_id)
                                $cabin->categories = [$category->id => $category->name] + $cabin->categories;
                            else
                                $cabin->categories = $cabin->categories + [$category->id => $category->name];
                        }
                    }
                }
            }

        } else {
            $cabin = new Cabin;
            $roomTypes = array();
            //$roomCategories = array();

            //$firstCategories = array();
            $search = false;

            foreach($types as $type) {
                foreach ($type->categories as $category) {
                    if($category->typename == 'ship') {
                        $roomTypes[$type->id] = $type->name;
                        if (!$search) {
                            $search = true;
                            $firstCategories = $type->categories;
                        }
                    }
                }
            }

            /*foreach ($firstCategories as $firstCategory)
                if($firstCategory->typename == 'ship') {
                    $roomCategories = $roomCategories + [$firstCategory->id => $firstCategory->name];
                }*/

            $cabin->types = $roomTypes;
            //$cabin->categories =  $roomCategories;
            $cabin->categories =  array();

            $cabin->types = ['false' => 'Select category'] + $cabin->types;
            $cabin->categories =  ['false' => 'Select ID'] + $cabin->categories;
        }

        return view('cabins.form', array('data' => $cabin));
    }

    public function Save (Request $request)
    {
        $validate = [
            'area' => 'required|between:0,999.99',
            'num' => 'required|integer',
            'type_category' => 'required|exists:type_categories,id',
            'deck' => 'required|exists:decks,id',
            'description' => 'max:9999'
        ];
        if ($request->has('id')) {
            $validate = $validate + array('id' => 'required|exists:cabins,id');
            $cabin = Cabin::find($request->id);
        } else {
            $cabin = new Cabin;
        }
        $this->validate($request, $validate);

        $cabin->num = $request->num;
        $cabin->area = $request->area;
        $cabin->type_category_id = $request->type_category;
        $cabin->deck_id = $request->deck;
        $cabin->description = $request->description;
        $cabin->save();

        return $cabin->id;
    }

    public function Delete($id)
    {
        $cabin = Cabin::findOrFail($id);
        if ($return = \App\Http\Traits\HelperTrait::deleteCheckModel($cabin, ['placements' => 'Placements']))
            return response($return, 422);
        else
            $cabin->delete();
    }
}
