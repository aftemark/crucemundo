<?php

namespace App\Http\Controllers;

use Auth;
use Config;

use Illuminate\Http\Request;
use App\User;
use App\Role;
use App\Client;
use App\ClientUser;
use App\Http\Requests;

class UserController extends Controller
{
    public function SignIn(Request $request)
    {
        $this->validate($request, [
            'email' => 'required|email',
            'password' => 'required',
            'g-recaptcha-response' => 'required'
        ]);

        if (Auth::attempt(['email' => $request['email'], 'password' => $request['password']], isset($request->remember)))
            return redirect()->route('years');

        return redirect()->back();
    }

    public function Index(Request $request)
    {
        $this->validate($request, [
            'field' => 'in:all,name,status',
            'order' => 'in:asc,desc',
            'order_field' => 'in:name,status'
        ]);

        $data = User::select('*');
        if ($request->has('search')) {
            if ($request->field == 'all')
                $data->search($request->search);
            else
                $data->search($request->search, [$request->field]);
        }

        if ($request->has('order') && $request->has('order_field')) {
            $data->orderBy($request->order_field, $request->order);
        } else
            $data->orderBy('updated_at', 'desc');

        return view('users.list', ['data' => $data->paginate($request->session()->get('users_number', 10))]);
    }

    public function Info($item)
    {
        if ($item != 'new') {
            $user = User::findOrFail($item);
        } else {
            $user = new User;
        }

        $user->allroles = array();

        $roles = Role::all();
        foreach ($roles as $role) {
            if (count($user->roles) && $role->id == $user->roles[0]->id)
                $user->allroles = array($role->id => $role->display_name) + $user->allroles;
            else
                $user->allroles = $user->allroles + array($role->id => $role->display_name);
        }

        $user->allroles = array('false' => 'Select role') + $user->allroles;

        return view('users.form', [
            'data' => $user,
            'clients' => Client::where('type', 'b2b')->get(),
            'modules'=> Config::get('constants.modules')
        ]);
    }

    public function Save(Request $request)
    {
        $validate = [
            'position' => 'max:255',
            'office' => 'max:255',
            'client_user' => 'exists:client_users,id'
        ];
        if ($request->has('id')) {
            $this->validate($request, $validate + array('id' => 'required|exists:users,id',
                'password' => 'min:6|max:255|confirmed',
                'password_confirmation' => 'min:6|max:255'
            ));
            $user = User::findOrFail($request->id);
        } else {
            $this->validate($request, $validate + array(
                'login' => 'required|email|max:255',
                'password' => 'required|min:6|max:255|confirmed',
                'password_confirmation' => 'required|min:6|max:255'
            ));
            $user = new User;
        }
        $user->name = $request->name;
        $user->position = $request->position;
        $user->office = $request->office;

        if (!$request->has('id')) {
            $user->email = $request->login;
        }
        if ($request->has('password')) {
            $user->password = bcrypt($request->password);
        }
        if (isset($user->client_user)) {
            $clientUser = $user->client_user;
            $clientUser->user_id = NULL;
            $clientUser->save();
        }

        $user->save();

        if ($request->has('client_user')) {
            $clientUser = ClientUser::find($request->client_user);
            if (!isset($clientUser->user)) {
                $clientUser->user_id = $user->id;
                $clientUser->save();
            }
        }

        $user->detachAllRoles();

        $modules = Config::get('constants.modules');
        foreach ($modules as $moduleName => $module) {
            if ($request->has('edit_' . $moduleName))
                $user->attachRole(Role::where('name', '=', 'edit_' . $moduleName)->first());

            if ($request->has('show_' . $moduleName))
                $user->attachRole(Role::where('name', '=', 'show_' . $moduleName)->first());

            foreach ($module["children"] as $childName => $children) {
                $create_edit = isset($children['create']) && $children['create'] ? 'create' : 'edit';
                if ($request->has($create_edit . '_' . $childName))
                    $user->attachRole(Role::where('name', '=', $create_edit . '_' . $childName)->first());

                if ($request->has('show_' . $childName))
                    $user->attachRole(Role::where('name', '=', 'show_' . $childName)->first());

                if (is_array($children))
                    foreach($children['children'] as $children_children_name => $children_children) {
                        if ($children_children['type'] == 'edit')
                            if ($request->has($children_children_name . '_' . $childName))
                                $user->attachRole(Role::where('name', '=', $children_children_name . '_' . $childName)->first());

                        if ($children_children['type'] == 'show')
                            if ($request->has('show_' . $childName . '_' . $children_children_name))
                                $user->attachRole(Role::where('name', '=', 'show_' . $childName . '_' . $children_children_name)->first());
                    }
            }
        }

        return $user->id;
    }

    public function ChangeActivity($id)
    {
        $user = User::findOrFail($id);
        if($user->active)
            $user->active = false;
        else
            $user->active = true;

        $user->save();
    }

    public function SetNumber(Request $request)
    {
        $this->validate($request, ['number' => 'required|in:10,50,100']);
        $request->session()->put('users_number', $request->number);

        return redirect()->route('users_list');
    }

    public function Delete($id)
    {
        $user = User::findOrFail($id);
        if ($return = \App\Http\Traits\HelperTrait::deleteCheckModel($user, ['client_users' => 'Users']))
            return response($return, 422);
        else
            $user->delete();
    }
}
