<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Ship;
use App\Hotel;
use App\Country;
use App\City;
use App\Http\Requests;
use Searchy;

class ShipController extends Controller
{
    public function Index(Request $request)
    {
        if ($request->has('table')) {
            $this->validate($request, [
                'table' => 'in:ships,hotels'
            ]);

            if ($request->table == 'ships')
                $this->validate($request, [
                    'field' => 'in:code,name,rating,all',
                    'order_field' => 'in:code,name,rating',
                    'order' => 'in:asc,desc'
                ]);
            else if ($request->table == 'hotels')
                $this->validate($request, [
                    'field' => 'in:code,name,rating,country,city,all',
                    'order_field' => 'in:code,name,rating,country,city',
                    'order' => 'in:asc,desc'
                ]);
        }

        $number = $request->session()->get('accommodations_number', 10);

        $ships = Ship::select('*');
        $hotels = Hotel::select('*');
        $table = $request->table;
        if ($request->has('search') && $request->has('field') && $request->has('table')) {
            if ($request->field == 'all') {
                $$table->search($request->search);
            } else if ($request->field == 'city') {
                $hotels->search($request->search, ['city.name']);
            } else if ($request->field == 'country') {
                $hotels->search($request->search, ['city.country.name']);
            } else {
                $$table->search($request->search, [$request->field]);
            }
        }

        if ($request->has('order') && $request->has('order_field') && $request->has('table')) {
            if ($request->order_field == 'country')
                $$table->select($table . '.*')->leftJoin('cities', 'city_id', '=', 'cities.id')->leftJoin('countries', 'cities.country_id', '=', 'countries.id')->orderBy('countries.name', $request->order);
            else if ($request->order_field == 'city')
                $$table->select($table . '.*')->leftJoin('cities', 'city_id', '=', 'cities.id')->orderBy('cities.name', $request->order);
            else
                $$table->orderBy($request->order_field, $request->order);
        }

        if ($request->input('table', NULL) != 'ships')
            $ships->orderBy('updated_at', 'desc');
        if ($request->input('table', NULL) != 'hotels')
            $hotels->orderBy('updated_at', 'desc');

        $data = [
            'hotels' => $hotels->paginate($number, ['*'], 'hotels_services'),
            'ships' => $ships->paginate($number, ['*'], 'ships_services')
        ];
        foreach ($data['ships'] as $ship) {
            $ship->max_capacity = $ship->add_bed;
            foreach ($ship->alldecks as $deck)
                foreach ($deck->cabins as $cabin)
                    $ship->max_capacity += $cabin->type->pax_amount;
        }

        return view('accommodations.list', array('data' => $data));
    }

    public function SetNumber(Request $request)
    {
        $this->validate($request, ['number' => 'required|in:10,50,100']);
        $request->session()->put('accommodations_number', $request->number);

        return redirect()->route('accommodations_list');
    }

    public function CabinsIndex($ship)
    {
        $data = array();
        foreach ($ship->alldecks as $deck) {
            $data[$deck->name]["deck_id"] = $deck->id;
            foreach ($deck->cabins as $cabin) {
                $data[$deck->name]["data"][$cabin->type->type->name][$cabin->type->name][] = $cabin;
            }
        }
        return view('cabins.list', array('data' => $data, 'add_bed' => $ship->add_bed));
    }

    public function Info($ship)
    {
        return view('ships.form', array('data' => $ship));
    }

    public function Save(Request $request)
    {
        $validate = [
            'code' => 'required|max:255',
            'name' => 'required|max:255',
            'rating' => 'required|integer',
            'year_built' => 'integer',
            'year_last' => 'integer',
            'width' => 'between:0,9999.99',
            'length' => 'between:0,9999.99',
            'draft' => 'between:0,9999.99',
            'services_desc' => 'max:9999',
            'add_desc' => 'max:9999',
            'description' => 'max:9999',
            'add_bed' => 'integer'
        ];

        if ($request->item == 'new') {
            $validate += $validate + array('decks' => 'required|integer');
            $ship = new Ship;
            $ship->decks = $request->decks;
        } else {
            $this->validate($request, ['item' => 'exists:ships,id']);
            $ship = Ship::find($request->item);
        }
        $this->validate($request, $validate);

        $ship->code = $request->code;
        $ship->name = $request->name;
        $ship->rating = $request->rating;
        $ship->year_built = $request->year_built;
        $ship->year_last = $request->year_last;
        $ship->width = $request->width;
        $ship->length = $request->length;
        $ship->draft = $request->draft;
        $ship->services_desc = $request->services_desc;
        $ship->add_desc = $request->add_desc;
        $ship->description = $request->description;
        $ship->add_bed = $request->add_bed;

        $ship->save();

        return $ship->id;
    }

    public function ShipData($ship)
    {
        $data = array();
        $data["cabins"] = 0;
        $data["pax"] = 0;

        foreach ($ship->alldecks as $deck) {
            $data["decks"][$deck->id] = count($deck->cabins);
            $data["cabins"] += count($deck->cabins);
            foreach ($deck->cabins as $cabin) {
                $data["pax"] += $cabin->type->pax_amount;
            }
        }

        return $data;
    }

    public function Delete($id)
    {
        $ship = Ship::findOrFail($id);
        if ($return = \App\Http\Traits\HelperTrait::deleteCheckModel($ship, ['services' => 'Services', 'journeys' => 'Cruises']))
            return response($return, 422);
        else
            $ship->delete();
        return redirect()->back();
    }
}
