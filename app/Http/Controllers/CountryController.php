<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Country;
use App\AllCountry;
use App\Http\Requests;

class CountryController extends Controller
{
    public function Index (Request $request)
    {
        if ($request->has('search'))
            return view('countries.list', array('data' => Country::search($request->search)->get()));
        else
            return view('countries.list', array('data' => Country::all()));
    }

    public function Info ($item)
    {
        if ($item != 'new')
            $data = Country::find($item);
        else
            $data = new Country;

        return view('countries.form', array('data' => $data));
    }

    public function Save (Request $request)
    {
        if ($request->has('id')) {
            $this->validate($request, [
                'id' => 'exists:countries,id',
                'name' => 'max:255|required']);
            $country = Country::find($request->id);
        } else {
            $this->validate($request, ['name' => 'max:255|required']);
            $country = new Country;
        }
            $country->name = $request->name;
            $country->save();

        return $country->id;
    }

    public function Delete($id)
    {
        $country = Country::findOrFail($id);
        if ($return = \App\Http\Traits\HelperTrait::deleteCheckModel($country, ['cities' => 'Cities', 'pax' => 'Reservation Pax', 'clients' => 'Clients']))
            return response($return, 422);
        else
            $country->delete();
    }
}