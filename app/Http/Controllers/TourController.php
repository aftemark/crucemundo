<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Journey;
use App\JourneyService;
use App\Hotel;
use App\Service;
use App\Itinerary;
use App\NonCompatible;
use App\NonCompatibleService;
use App\Placement;
use App\SupplementGroup;
use App\JourneyFee;
use App\Special;
use App\SpecialRelation;
use App\ServicePackage;
use App\ItineraryCity;
use App\Http\Requests;
use App\Http\Traits\ReservationTrait;

class TourController extends Controller
{
    use ReservationTrait;
    
    public function __construct(Request $request)
    {
        $this->year = $request->session()->get('catalog_year_id');
    }

    public function Index(Request $request)
    {
        $this->validate($request, [
            'field' => 'in:depart_date,hotel.name,itinerary.name,itinerary.from_to,itinerary.nights,season',
            'order' => 'in:asc,desc',
            'order_field' => 'in:depart_date,hotel.name,itinerary.name,itinerary.from_to,itinerary.nights,season'
        ]);

        $data = Journey::where('type', 'tour')->where('year_id', $this->year);

        if ($request->has('search') && $request->has('field')) {
            if ($request->type == 'all')
                $data = $data->search($request->search);
            else
                $data = $data->search($request->search, [$request->field]);
        }

        if ($request->has('order') && $request->has('order_field')) {
            if ($request->order_field == 'hotel.name')
                $data->select('journeys.*')->leftJoin('hotels', 'hotel_id', '=', 'hotels.id')->orderBy('hotels.name', $request->order);
            else if ($request->order_field == 'itinerary.name')
                $data->select('journeys.*')->leftJoin('itineraries', 'itinerary_id', '=', 'itineraries.id')->orderBy('itineraries.name', $request->order);
            else if ($request->order_field == 'itinerary.from_to')
                $data->select('journeys.*')->leftJoin('itineraries', 'itinerary_id', '=', 'itineraries.id')->orderBy('itineraries.from_to', $request->order);
            else if ($request->order_field == 'itinerary.nights')
                $data->select('journeys.*')->leftJoin('itineraries', 'itinerary_id', '=', 'itineraries.id')->orderBy('itineraries.nights', $request->order);
            else
                $data->orderBy($request->order_field, $request->order);
        } else
            $data->orderBy('updated_at', 'desc');

        $data = $data->paginate($request->session()->get('cruises_number', 10));
        foreach ($data as $tour) {
            if (isset($tour->hotel)) {
                $tour->places = 0;
                foreach ($tour->placements as $placement) {
                    $amount = $placement->room->type->pax_amount;
                    if ($placement->room->type->bed)
                        $amount += 1;
                    $amount = $amount * $placement->amount;
                    $tour->places += $amount;
                }
            }
        }

        return view('tours.list', array('data' => $data));
    }

    public function Info($item)
    {
        if ($item != 'new') {
            $tour = Journey::where('type', 'tour')->findOrFail($item);
        } else {
            $tour = new Journey;
        }

        if (isset($tour->depart_date) && isset($tour->itinerary))
            $tour->ending_date = date('Y-m-d', strtotime($tour->depart_date . ' + ' . ($tour->itinerary->nights) . ' days'));

        $services = array();
        if (isset($tour->services)) {
            foreach ($tour->services as $service) {
                if ($service->itinerary_city_id !== NULL) {
                    if (isset($service->service_id))
                        $services[$service->itinerary_city_id]["service"][$service->service_id] = $service;
                    else if (isset($service->service_package_id))
                        $services[$service->itinerary_city_id]["package"][$service->service_package_id] = $service;
                } else {
                    if (isset($service->service_id))
                        $services["other"]["service"][$service->service_id] = $service;
                    else if (isset($service->service_package_id))
                        $services["other"]["package"][$service->service_package_id] = $service;
                }
            }
        }

        $otherServices = array();
        $roomCategories = array();
        $nonServices = array();
        if (isset($tour->hotel)) {
            $otherServices = collect($tour->hotel->services()->where('year_id', $this->year)->get())
                ->merge(collect(Service::where('service_type', 'other')->where('year_id', $this->year)->get()))
                ->merge(collect(Service::where('service_type', 'catering')->where('other', '!=', NULL)->where('year_id', $this->year)->get()))->unique();

            /*foreach ($otherServices as $otherService)
                $otherServices = collect($otherServices)->merge(collect($otherService->packages()))->unique();*/

            foreach ($tour->hotel->rooms as $room)
                $roomCategories[$room->id] = $room->type->name;
        }

        $cities = array();
        $days = array();
        if (isset($tour->itinerary)) {
            foreach ($tour->itinerary->relations as $itinerary) {
                $days[$itinerary->day][$itinerary->id] = $itinerary->city;
                $cities[] = $itinerary->id;
            }

            $counter = 0;
            if (isset($tour->nonDays)) {
                foreach ($tour->nonDays as $nonDay) {
                    if (in_array($nonDay->itinerary_city_id, $cities) || $nonDay->itinerary_city_id === NULL) {
                        ++$counter;
                        foreach ($nonDay->services as $nonService) {
                            $nonService->counter = $counter;
                            if ($nonDay->itinerary_city_id)
                                $nonServices[$nonDay->itinerary_city_id][$nonDay->id][$nonService->journey_service_id] = $nonService;
                            else
                                $nonServices["other"][$nonDay->id][$nonService->journey_service_id] = $nonService;
                        }
                    }
                }
            }
        }

        $selectedRooms = array();
        if (isset($tour->groups))
            foreach ($tour->groups as $group)
                foreach ($group->placements as $placement)
                    $selectedRooms[] = $placement->room_id;

        $reservations = $this->reservationsList($tour);

        $types_info = [];
        if ($tour->placements->count()) {
            $type_fields = [
                'name' => '',
                'factual_rooms' => 0,
                'factual_pax' => 0,
                'sell_rooms' => 0,
                'sell_pax' => 0,
                'names_rooms' => 0,
                'names_pax' => 0,
                'no_names_rooms' => 0,
                'no_names_pax' => 0,
                'option_rooms' => 0,
                'option_pax' => 0,
                'booked_options_rooms' => 0,
                'booked_options_pax' => 0,
                'available_rooms' => 0,
                'available_pax' => 0,
                'inquiry_rooms' => 0,
                'inquiry_pax' => 0
            ];
            $types_info["overall"] = $type_fields;

            foreach ($tour->placements as $placement) {
                $type_id = $placement->room->type->id;
                if (!isset($types_info["types"][$type_id]))
                    $types_info["types"][$type_id] = $type_fields;
                $types_info["types"][$type_id]["name"] = $placement->room->type->name;
                $types_info["types"][$type_id]["available_rooms"] = $placement->amount;
                $types_info["types"][$type_id]["available_pax"] = $placement->amount * ($placement->room->type->bed ? $placement->room->type->pax_amount + 1 : $placement->room->type->pax_amount);
                $types_info["types"][$type_id]["sell_rooms"] = $placement->amount;
                $types_info["types"][$type_id]["sell_pax"] = $placement->amount * ($placement->room->type->bed ? $placement->room->type->pax_amount + 1 : $placement->room->type->pax_amount);
            }

            foreach ($tour->reservations as $reservation) {
                foreach ($reservation->placements as $placement) {
                    $type_id = $placement->placement->room->type->id;
                    if ($reservation->type == 'booking') {
                        if ($reservation->with_names) {
                            $types_info["types"][$type_id]["factual_rooms"]++;
                            $types_info["types"][$type_id]["factual_pax"] += $placement->reservation_paxes->count();
                            $types_info["types"][$type_id]["names_rooms"]++;
                            $types_info["types"][$type_id]["names_pax"] += $placement->reservation_paxes->count();
                            $types_info["types"][$type_id]["sell_rooms"]--;
                            $types_info["types"][$type_id]["sell_pax"] -= $placement->reservation_paxes->count();
                        } else {
                            $types_info["types"][$type_id]["no_names_rooms"]++;
                            $types_info["types"][$type_id]["no_names_pax"] += $placement->placement->room->type->bed ? $placement->placement->room->type->pax_amount + 1 : $placement->placement->room->type->pax_amount;
                            $types_info["types"][$type_id]["available_rooms"]--;
                            $types_info["types"][$type_id]["available_pax"] -= $placement->reservation_paxes->count();
                            $types_info["types"][$type_id]["booked_options_rooms"]++;
                            $types_info["types"][$type_id]["booked_options_pax"] += $placement->reservation_paxes->count();
                        }
                    } else if ($reservation->type == 'option') {
                        $types_info["types"][$type_id]["option_rooms"]++;
                        $types_info["types"][$type_id]["option_pax"] += $placement->reservation_paxes->count();
                        $types_info["types"][$type_id]["available_rooms"]--;
                        $types_info["types"][$type_id]["available_pax"] -= $placement->reservation_paxes->count();
                        $types_info["types"][$type_id]["booked_options_rooms"]++;
                        $types_info["types"][$type_id]["booked_options_pax"] += $placement->reservation_paxes->count();
                    } else if ($reservation->type == 'inquiry') {
                        $types_info["types"][$type_id]["inquiry_rooms"]++;
                        $types_info["types"][$type_id]["inquiry_pax"] += $placement->reservation_paxes->count();
                    }
                }
            }

            foreach ($types_info["types"] as $type_fields)
                foreach ($type_fields as $field_key => $field_value)
                    $types_info["overall"][$field_key] += $field_value;
        }

        return view('tours.form', array(
            'data' => $tour,
            'otherServices' => $otherServices,
            'hotels' => Hotel::select(['id', 'name'])->get(),
            'cruises' => Journey::where('type', 'cruise')->select(['id', 'code'])->get(),
            'itineraries' => Itinerary::select(['id', 'name', 'from_to'])->get(),
            'services' => $services,
            'nonServices' => $nonServices,
            'days' => $days,
            'roomCategories' => $roomCategories,
            'selectedRooms' => $selectedRooms,
            'reservations' => $reservations,
            'types_info' => $types_info
        ));
    }

    public function Save(Request $request)
    {
        $validate = array();
        if ($request->has('id')) {
            $tour = Journey::where('type', 'tour')->findOrFail($request->id);
        } else
            $tour = new Journey;

        $validate = $validate + [
                'code' => 'max:255',
                'hotel' => 'exists:hotels,id',
                'season' => 'in:middle,high,low',
                'depart_date' => 'date',
                'journey_release_date' => 'date',
                'journey_remind' => 'integer',
                'description' => 'max:9999',
                'booking_percent' => 'integer',
                'full_pay' => 'integer',
                'itinerary' => 'exists:itineraries,id',
                'cruise' => 'exists:journeys,id'
            ];
        if (isset($tour->itinerary)) {
            $validate = $validate + [
                    'radio_service.*.*' => 'in:optional,included',
                    'display_service.*.*' => 'in:true',
                    'release_date_service.*.*' => 'date',
                    'remind_service.*.*' => 'integer',
                    'radio_package.*.*' => 'in:optional,included',
                    'display_package.*.*' => 'in:true',
                    'release_date_package.*.*' => 'date',
                    'remind_package.*.*' => 'integer'
                ];
            if (!empty($request["nonCompatibles"]))
                $validate = $validate + [
                        'supplement_rate.*' => 'numeric|between:0,99.99',
                        'supplement_group.*.*' => 'exists:rooms,id'
                    ];
        }
        if (isset($tour->hotel)) {
            $validate = $validate + [
                    'netPrice.*' => 'numeric|between:0,9999999.99',
                    'recPrice.*' => 'numeric|between:0,9999999.99',
                    'roomsAmount.*' => 'integer'
                ];
            if (isset($request["supplement_rate"]))
                $validate = $validate + [
                        'supplement_rate.*' => 'numeric|between:0,99.99',
                        'supplement_group.*.*' => 'exists:rooms,id'
                    ];
        }
        $validate = $validate + [
                'fee_percent.*' => 'numeric|between:0,99.99',
                'fee_from.*' => 'integer',
                'fee_to.*' => 'integer'
            ];

        if (isset($request->cruise))
            $validate = $validate + [
                    'extension_type' => 'required|in:pre,post'
                ];

        $this->validate($request, $validate);

        $tour->type = 'tour';
        $tour->code = $request->code;
        $tour->hotel_id = $request->hotel;
        $tour->description = $request->description;
        $tour->release_date = $request->journey_release_date;
        $tour->depart_date = $request->depart_date;
        $tour->remind = $request->journey_remind;
        if (isset($request->booking_percent))
            $tour->booking_percent = $request->booking_percent;
        if (isset($request->full_pay))
            $tour->full_pay = $request->full_pay;
        if (isset($request->cruise)) {
            $tour->journey_id = $request->cruise;
            $tour->extension_type = $request->extension_type;
        } else {
            $tour->journey_id = NULL;
            $tour->extension_type = false;
        }

        $tour->fees()->delete();
        $tour->groups()->delete();
        foreach ($tour->nonDays as $nonDay) {
            if (isset($nonDay->services))
                foreach ($nonDay->services as $nonDayService) {
                    $nonDayService->delete();
                }
            $nonDay->delete();
        }
        $tourItinerary = $tour->itinerary;

        if (isset($request->itinerary) && $tour->itinerary_id != $request->itinerary) {
            foreach ($tour->specials as $special)
                foreach ($special->relations as $relation) {
                    $relation->placement_id = NULL;
                    $relation->journey_service_id = NULL;
                    $relation->save();
                }

            $tour->services()->delete();
            $tour->itinerary_id = $request->itinerary;

            $tourItinerary = Itinerary::find($request->itinerary);
        }

        if (isset($request->hotel) && $tour->hotel_id != $request->hotel) {
            $tour->placements()->delete();
        }

        $tour->year_id = $this->year;
        $tour->save();

        if (isset($request["fee_percent"]))
            foreach ($request["fee_percent"] as $feeNum => $fee_percent) {
                if ($fee_percent > 0) {
                    $journeyFee = new JourneyFee;
                    $journeyFee->journey_id = $tour->id;
                    $journeyFee->percent = $fee_percent;
                    $journeyFee->from = $request["fee_from"][$feeNum];
                    if (isset($request["fee_to"][$feeNum]))
                        $journeyFee->to = $request["fee_to"][$feeNum];
                    $journeyFee->save();
                }
            }

        if (isset($tour->itinerary)) {
            foreach ($tourItinerary->relations as $itinerary) {
                $usedPackages = array();
                foreach ($itinerary->city->services->where('year_id', $this->year) as $service) {
                    $findService = $tour->services->where('service_id', $service->id)->where('itinerary_city_id', $itinerary->id)->first();
                    if ($findService === NULL) {
                        $newService = new JourneyService;
                        $newService->journey_id = $tour->id;
                        $newService->itinerary_city_id = $itinerary->id;
                        $newService->service_id = $service->id;
                        $newService->save();
                    } else {
                        $newService = $findService;
                    }
                    if (isset($request["radio_service"][$itinerary->id][$service->id]) &&
                        ($request["radio_service"][$itinerary->id][$service->id] == 'included' || $request["radio_service"][$itinerary->id][$service->id] == 'optional')
                    ) {
                        $newService->journey_id = $tour->id;
                        $newService->itinerary_city_id = $itinerary->id;
                        $newService->service_id = $service->id;
                        $newService->included = $request["radio_service"][$itinerary->id][$service->id] == 'included' ? true : false;
                        $newService->optional = $request["radio_service"][$itinerary->id][$service->id] == 'optional' ? true : false;
                        $newService->display = isset($request["display_service"][$itinerary->id][$service->id]) && $request["display_service"][$itinerary->id][$service->id] == "true" ? true : false;
                        $newService->release_date = $request["release_date_service"][$itinerary->id][$service->id] ? $request["release_date_service"][$itinerary->id][$service->id] : NULL;
                        $newService->remind = $request["remind_service"][$itinerary->id][$service->id] ? $request["remind_service"][$itinerary->id][$service->id] : NULL;
                        $newService->save();
                    }
                    foreach ($service->packages as $package) {
                        $findPackage = $tour->services()->where('service_package_id', $package->service_package_id)->where('itinerary_city_id', $itinerary->id)->first();
                        if ($findPackage === NULL) {
                            $newPackage = new JourneyService;
                            $newPackage->journey_id = $tour->id;
                            $newPackage->itinerary_city_id = $itinerary->id;
                            $newPackage->service_package_id = $package->service_package_id;
                            $newPackage->save();
                        } else {
                            $newPackage = $findPackage;
                        }
                        if (!isset($usedPackages[$itinerary->id][$package->service_package_id]) && isset($request["radio_package"][$itinerary->id][$package->service_package_id]) &&
                            ($request["radio_package"][$itinerary->id][$package->service_package_id] == 'included' || $request["radio_package"][$itinerary->id][$package->service_package_id] == 'optional')
                        ) {
                            $usedPackages[$itinerary->id][$package->service_package_id] = true;
                            $newPackage->journey_id = $tour->id;
                            $newPackage->itinerary_city_id = $itinerary->id;
                            $newPackage->service_package_id = $package->service_package_id;
                            $newPackage->included = $request["radio_package"][$itinerary->id][$package->service_package_id] == 'included' ? true : false;
                            $newPackage->optional = $request["radio_package"][$itinerary->id][$package->service_package_id] == 'optional' ? true : false;
                            $newPackage->display = isset($request["display_package"][$itinerary->id][$package->service_package_id]) && $request["display_package"][$itinerary->id][$package->service_package_id] == "true" ? true : false;
                            $newPackage->release_date = $request["release_date_package"][$itinerary->id][$package->service_package_id] ? $request["release_date_package"][$itinerary->id][$package->service_package_id] : NULL;
                            $newPackage->remind = $request["remind_package"][$itinerary->id][$package->service_package_id] ? $request["remind_package"][$itinerary->id][$package->service_package_id] : NULL;
                            $newPackage->save();
                        }
                    }
                }
            }
        }

        if (isset($tour->hotel)) {
            $usedPackages = array();
            $otherServices = collect($tour->hotel->services()->where('year_id', $this->year)->get())
                ->merge(collect(Service::where('service_type', 'other')->where('year_id', $this->year)->get()))
                ->merge(collect(Service::where('service_type', 'catering')->where('other', '!=', NULL)->where('year_id', $this->year)->get()))->unique();

            foreach ($otherServices as $otherService) {
                $findService = $tour->services->where('service_id', $otherService->id)->where('itinerary_city_id', NULL)->first();
                if ($findService === NULL) {
                    $newService = new JourneyService;
                    $newService->journey_id = $tour->id;
                    $newService->service_id = $otherService->id;
                    $newService->save();
                } else {
                    $newService = $findService;
                }
                if (isset($request["radio_service"]["other"][$otherService->id]) &&
                    ($request["radio_service"]["other"][$otherService->id] == 'included' || $request["radio"]["other"][$otherService->id] == 'optional')
                ) {
                    $newService->journey_id = $tour->id;
                    $newService->itinerary_city_id = NULL;
                    $newService->service_id = $otherService->id;
                    $newService->included = $request["radio_service"]["other"][$otherService->id] == 'included' ? true : false;
                    $newService->optional = $request["radio_service"]["other"][$otherService->id] == 'optional' ? true : false;
                    $newService->display = isset($request["display_service"]["other"][$otherService->id]) && $request["display_service"]["other"][$otherService->id] == "true" ? true : false;
                    $newService->release_date = $request["release_date_service"]["other"][$otherService->id] ? $request["release_date_service"]["other"][$otherService->id] : NULL;
                    $newService->remind = $request["remind_service"]["other"][$otherService->id] ? $request["remind_service"]["other"][$otherService->id] : NULL;
                    $newService->save();
                }
                foreach ($otherService->packages as $otherPackage) {
                    $findPackage = $tour->services->where('service_package_id', $otherPackage->service_package_id)->where('itinerary_city_id', NULL)->first();
                    if ($findPackage === NULL) {
                        $newPackage = new JourneyService;
                        $newPackage->journey_id = $tour->id;
                        $newPackage->service_package_id = $otherPackage->service_package_id;
                        $newPackage->save();
                    } else {
                        $newPackage = $findPackage;
                    }
                    if (!isset($usedPackages[$otherPackage->servicePackage->id]) && isset($request["radio_package"]["other"][$otherPackage->servicePackage->id]) &&
                        ($request["radio_package"]["other"][$otherPackage->servicePackage->id] == 'included' || $request["radio_package"]["other"][$otherPackage->servicePackage->id] == 'optional')
                    ) {
                        $usedPackages[$otherPackage->servicePackage->id] = true;
                        $newPackage->journey_id = $tour->id;
                        $newPackage->itinerary_city_id = NULL;
                        $newPackage->service_package_id = $otherPackage->servicePackage->id;
                        $newPackage->included = $request["radio_package"]["other"][$otherPackage->servicePackage->id] == 'included' ? true : false;
                        $newPackage->optional = $request["radio_package"]["other"][$otherPackage->servicePackage->id] == 'optional' ? true : false;
                        $newPackage->display = isset($request["display_package"]["other"][$otherPackage->servicePackage->id]) && $request["display_package"]["other"][$otherPackage->servicePackage->id] == "true" ? true : false;
                        $newPackage->release_date = $request["release_date_package"]["other"][$otherPackage->servicePackage->id] ? $request["release_date_package"]["other"][$otherPackage->servicePackage->id] : NULL;
                        $newPackage->remind = $request["remind_package"]["other"][$otherPackage->servicePackage->id] ? $request["remind_package"]["other"][$otherPackage->servicePackage->id] : NULL;
                        $newPackage->save();
                    }
                }
            }

            foreach ($tour->hotel->rooms as $room) {
                $findPlacement = $tour->placements->where('room_id', $room->id)->first();
                if ($findPlacement === NULL) {
                    $placement = new Placement;
                    $placement->journey_id = $tour->id;
                    $placement->room_id = $room->id;
                } else
                    $placement = $findPlacement;

                if (isset($request["netPrice"][$room->id]))
                    $placement->price_net = $request["netPrice"][$room->id];
                if (isset($request["recPrice"][$room->id]))
                    $placement->price_rec = $request["recPrice"][$room->id];
                if (isset($request["roomsAmount"][$room->id]))
                    $placement->amount = $request["roomsAmount"][$room->id];
                $placement->save();
            }
            ini_set("log_errors", 1);
            ini_set("error_log", "log.log");
            if (isset($request["supplement_rate"])) {
                foreach ($request["supplement_rate"] as $num => $rate) {
                    if (isset($request["supplement_group"][$num]) && !empty($request["supplement_group"][$num])) {
                        $supplement_group = new SupplementGroup;
                        $supplement_group->journey_id = $tour->id;
                        $supplement_group->percent = $rate;
                        $supplement_group->save();
                        error_log(!empty($request["supplement_group"][$num]));
                        foreach ($request["supplement_group"][$num] as $requestPlacement) {
                            $groupPlacement = $tour->placements()->where('room_id', $requestPlacement)->first();
                            $groupPlacement->supplement_group_id = $supplement_group->id;
                            $groupPlacement->save();
                        }
                    }
                }
            }
        }

        if (isset($tour->itinerary)) {
            foreach ($tourItinerary->relations as $relation) {
                if (!empty($request["nonCompatibles"][$relation->id])) {
                    foreach ($request["nonCompatibles"][$relation->id] as $nonPackageID) {
                        if (count($nonPackageID) > 1) {
                            $newNon = new NonCompatible;
                            $newNon->journey_id = $tour->id;
                            $newNon->itinerary_city_id = $relation->id;
                            $newNon->save();

                            foreach ($nonPackageID as $nonServiceID) {
                                if (JourneyService::find($nonServiceID) !== NULL) {
                                    $nonService = new NonCompatibleService;
                                    $nonService->non_compatible_id = $newNon->id;
                                    $nonService->journey_service_id = $nonServiceID;
                                    $nonService->save();
                                }
                            }
                        }
                    }
                }
            }
            if (!empty($request["nonCompatibles"]["other"])) {
                foreach ($request["nonCompatibles"]["other"] as $nonPackageID) {
                    if (count($nonPackageID) > 1) {
                        $newNon = new NonCompatible;
                        $newNon->journey_id = $tour->id;
                        $newNon->save();

                        foreach ($nonPackageID as $nonServiceID) {
                            if (JourneyService::find($nonServiceID) !== NULL) {
                                $nonService = new NonCompatibleService;
                                $nonService->non_compatible_id = $newNon->id;
                                $nonService->journey_service_id = $nonServiceID;
                                $nonService->save();
                            }
                        }
                    }
                }
            }
        }

        return $tour->id;
    }

    public function SetNumber(Request $request)
    {
        $this->validate($request, ['number' => 'required|in:10,50,100']);
        $request->session()->put('tours_number', $request->number);

        return redirect()->route('tours_list');
    }

    public function Delete($id)
    {
        $journey = Journey::findOrFail($id);

        if ($journey->type != 'tour')
            abort(404);

        if ($return = \App\Http\Traits\HelperTrait::deleteCheckModel($journey, ['reservations' => 'Reservation']))
            return response($return, 422);
        else
            $journey->delete();
        return redirect()->back();
    }

    public function Copy($id)
    {
        $journey = Journey::findOrFail($id);

        if ($journey->type != 'tour')
            return redirect()->back();

        $journeyID = \App\Http\Traits\HelperTrait::journeyCopy($journey, ['hotel_id', 'season', 'itinerary_id', 'booking_percent', 'full_pay', 'extension_type', 'type', 'year_id']);

        return redirect('/tour/' . $journeyID);
    }
}
