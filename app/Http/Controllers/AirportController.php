<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Airport;
use App\City;
use App\Http\Requests;

class AirportController extends Controller
{
    public function Index (Request $request)
    {
        if ($request->has('search')) {
            return view('airports.list', array('data' => Airport::search($request->search, ['id', 'name', 'city.name'])->get()));
        }

        return view('airports.list', array('data' => Airport::all()));
    }

    public function Info ($item)
    {
        $cities = City::all();

        if ($item != 'new') {
            $airport = Airport::find($item);
            $airport->cities = array();

            foreach ($cities as $each)
                if ($each->id == $airport->airport_id)
                    $airport->cities = array($each->id => $each->name) + $airport->cities;
                else
                    $airport->cities = $airport->cities + array($each->id => $each->name);
        } else {
            $airport = new Airport;
            $airport->cities = array();

            foreach ($cities as $each)
                $airport->cities = [$each->id => $each->name] + $airport->cities;
        }

        return view('airports.form', array('data' => $airport));
    }

    public function Save(Request $request)
    {
        if ($request->has('id')) {
            $this->validate($request, [
                'id' => 'required|exists:airports,id',
                'city' => 'required|exists:cities,id',
                'name' => 'required|max:250'
            ]);
            $category = Airport::find($request->id);
        } else {
            $this->validate($request, [
                'city' => 'required|exists:cities,id',
                'name' => 'required|max:250'
            ]);
            $category = new Airport;
        }
        $category->city_id = $request->city;
        $category->name = $request->name;

        $category->save();

        return $category->id;
    }

    public function Delete($id)
    {
        $airport = Airport::findOrFail($id);
        if ($return = \App\Http\Traits\HelperTrait::deleteCheckModel($airport, ['pax' => 'Reservation Pax']))
            return response($return, 422);
        else
            $airport->delete();
    }
}
