<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Language;
use App\Http\Requests;

class LanguageController extends Controller
{
    public function Index(Request $request)
    {
        if ($request->has('search'))
            return view('languages.list', array('data' => Language::search($request->search)->get()));
        else
            return view('languages.list', array('data' => Language::all()));
    }

    public function Info($item)
    {
        if ($item != 'new') {
            $language = Language::findOrFail($item);
        } else {
            $language = new Language;
        }

        return view('languages.form', array('data' => $language));
    }

    public function Save(Request $request)
    {
        if ($request->has('id')) {
            $this->validate($request, ['id' => 'required|exists:languages,id']);
            $language = Language::find($request->id);
        } else {
            $language = new Language;
        }
        $language->name = $request->name;
        $language->save();

        return $language->id;
    }

    public function Delete($id)
    {
        $language = Language::findOrFail($id);
        if ($return = \App\Http\Traits\HelperTrait::deleteCheckModel($language, ['clients' => 'Clients', 'services' => 'Services', 'reservations' => 'Reservations']))
            return response($return, 422);
        else
            $language->delete();
    }
}