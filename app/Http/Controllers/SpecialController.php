<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Journey;
use App\SpecialRelation;
use App\Special;
use App\Http\Requests;

class SpecialController extends Controller
{
    public function __construct(Request $request)
    {
        $this->year = $request->session()->get('catalog_year_id');
    }

    public function Info(Request $request = NULL, $item, $id = NULL)
    {
        if ($item != 'new') {
            $special = Special::find($item);
        } else {
            $special = new Special;
            if ($id)
                $special->journey_id = $id;
            else
                abort(500, 'Wrong journey id.');
        }

        if (isset($request->type))
            $special->type = $request->type;

        $specialPlacements = array();
        $specialServices = array();
        $special_info = [];
        $special_info["placement_name"] = $special->journey->type == 'tour' ? 'room' : 'cabin' ;

        foreach ($special->relations as $relation) {
            if ($relation->placement_id !== NULL)
                $specialPlacements[] = $relation->placement_id;
            else if ($special->type == 'free' && ($special->offer_type == 'free_services' || $special->offer_type == 'free_packages') && $relation->journey_service_id !== NULL)
                $specialServices[] = $relation->journey_service_id;
            else if ($special->type == 'free' && $special->offer_type == 'free_extensions' && $relation->journey_id !== NULL)
                $specialServices[] = $relation->journey_id;
        }

        $journeyServices = array();
        $journeyPackages = array();

        foreach ($special->journey->services->where('display', 1) as $service)
            if (isset($service->service_id))
                $journeyServices[$service->id] = $service->service->name;
            else if (isset($service->service_package_id))
                $journeyPackages[$service->id] = $service->package->name;

        return view('specials.form', array(
            'special' => $special,
            'specialPlacements' => $specialPlacements,
            'specialServices' => $specialServices,
            'journeyServices' => $journeyServices,
            'journeyPackages' => $journeyPackages,
            'tours' => Journey::where('type', 'tour')->where('year_id', $this->year)->select('id', 'code')->get(),
            'special_info' => $special_info
        ));
    }

    public function Save(Request $request)
    {
        $validate = array();
        if ($request->has('id')) {
            $validate = ['id' => 'required|exists:specials,id'];
            $special = Special::find($request->id);
        } else
            $special = new Special;

        $this->validate($request, ['journey' => 'exists:journeys,id']);
        $journey = Journey::find($request->journey);

        $validate = $validate + [
                'type' => 'in:discount,free,exception',
                'date_start' => 'date',
                'date_end' => 'date',
                'cabin_types.*' => 'exists:placements,id',
                'condition' => 'in:group,fit,both',
                'previous' => 'in:true'
            ];
        if (isset($request->type) && isset($request->offer_type)) {
            if ($request->type == 'discount')
                $validate = $validate + ['offer_type' => 'in:percent,eur,new'];
            else if ($request->type == 'free') {
                if ($journey->type == 'tour')
                    $validate = $validate + ['offer_type' => 'in:free_services,free_packages', 'services.*' => 'exists:journey_services,id'];
                else if ($journey->type == 'cruise') {
                    $validate = $validate + ['offer_type' => 'in:free_services,free_packages,free_extensions'];
                    if ($request->offer_type == 'free_extensions')
                        $validate = $validate + ['services.*' => 'exists:journeys,id'];
                    else if ($request->offer_type == 'free_services' || $request->offer_type == 'free_packages')
                        $validate = $validate + ['services.*' => 'exists:journey_services,id'];
                }
            }
            else if ($request->type == 'exception') {
                if ($journey->type == 'tour')
                    $validate = $validate + ['offer_type' => 'in:no_single'];
                else if ($journey->type == 'cruise')
                    $validate = $validate + ['offer_type' => 'in:no_single,no_port'];
            }

            if ($request->offer_type == 'percent')
                $validate = $validate + ['offer_amount' => 'numeric|between:0,100.00'];
            else if ($request->offer_type == 'new' || $request->offer_type == 'eur')
                $validate = $validate + ['offer_amount' => 'numeric|between:0,9999999.99'];
        }

        $this->validate($request, $validate);

        $special->journey_id = $request->journey;
        $special->date_start = $request->date_start;
        $special->date_end = $request->date_end;
        $special->type = $request->type;
        $special->offer_type = $request->offer_type;
        $special->offer_amount = $request->offer_amount;
        $special->special_condition = $request->condition;
        if (isset($request->previous))
            $special->previous = true;
        $special->save();

        $special->relations()->delete();

        if (isset($request->cabin_types))
            foreach($request->cabin_types as $cabin_type) {
                $placement = new SpecialRelation;
                $placement->special_id = $special->id;
                $placement->placement_id = $cabin_type;
                $placement->save();
            }

        if ($request->type == 'free' && isset($request->services))
            foreach($request->services as $service) {
                $placement = new SpecialRelation;
                $placement->special_id = $special->id;
                if ($special->offer_type == 'free_extensions')
                    $placement->journey_id = $service;
                else
                    $placement->journey_service_id = $service;
                $placement->save();
            }

        $discount = NULL;
        $condition = NULL;
        $specialPlacements = array();
        $specialServices = array();
        $specialPackages = array();
        $specialExtensions = array();
        $supplement = NULL;

        foreach ($special->relations as $specialRelation) {
            if ($specialRelation->journey_service_id !== NULL) {
                if ($specialRelation->service->service_id !== NULL)
                    $specialServices[] = $specialRelation->service->service->name;
                else if ($specialRelation->service->service_package_id !== NULL)
                    $specialPackages[] = $specialRelation->service->package->name;
            } else if ($specialRelation->journey_id !== NULL) {
                $specialExtensions[] = $specialRelation->journey->code;
            } else if ($specialRelation->placement_id !== NULL) {
                if (isset($specialRelation->placement)) {
                    if ($journey->type == 'tour')
                        $specialPlacements[] = $specialRelation->placement->room->type->name;
                    else if ($journey->type == 'cruise')
                        $specialPlacements[] = $specialRelation->placement->cabin->type->name;
                }
            }
        }

        if ($special->special_condition == 'group')
            $condition = 'Group';
        else if ($special->special_condition == 'fit')
            $condition = 'Fit';
        else if ($special->special_condition == 'both')
            $condition = 'Group&Fit';

        if ($special->type == 'discount') {
            if ($special->offer_type == 'percent')
                $discount = 'Discount ' . $special->offer_amount . '%';
            else if ($special->offer_type == 'eur')
                $discount = 'Discount ' . $special->offer_amount . ' EUR';
            else if ($special->offer_type == 'new')
                $discount = 'New price ' . $special->offer_amount . ' EUR';

            $special->description = $discount . ' (' . implode(', ', $specialPlacements) .  ') - ' . $condition;
        } else if ($special->type == 'free') {
            if ($special->offer_type == 'free_services')
                $special->description = 'Free services: ' . implode(', ', $specialServices) . ' (' . implode(', ', $specialPlacements) .  ') - ' . $condition;
            else if ($special->offer_type == 'free_packages')
                $special->description = 'Free excursion packages: ' . implode(', ', $specialPackages) . ' (' . implode(', ', $specialPlacements) .  ') - ' . $condition;
            else if ($special->offer_type == 'free_extensions')
                $special->description = 'Free extensions: ' . implode(', ', $specialExtensions) . ' (' . implode(', ', $specialPlacements) .  ') - ' . $condition;
        } else if ($special->type == 'exception') {
            if ($special->offer_type == 'no_single')
                $supplement = 'No Single Supplement';
            else if ($special->offer_type == 'no_port' && $special->journey->type == 'cruise')
                $supplement = 'No Port Tax';

            $special->description = $supplement . ' (' . implode(', ', $specialPlacements) .  ')';
        }

        $special->save();
    }

    public function Delete($id)
    {
        Special::findOrFail($id)->delete();
    }
}
