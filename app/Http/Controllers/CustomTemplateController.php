<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\CustomTemplate;

class CustomTemplateController extends Controller
{
    public function Index()
    {
        return view('reports.templates', [
            'templates' => CustomTemplate::where('type', 'report')->get()
        ]);
    }

    public function MailInfo($id)
    {
        return view('mailing.notification', [
            'template' => CustomTemplate::where('type', 'mail')->findOrFail($id)
        ]);
    }

    public function SaveMail(Request $request)
    {
        $this->validate($request, [
            'id' => 'required|exists:custom_templates,id',
            'subject' => 'required|max:255',
            'name' => 'required|max:255',
            'text' => 'required'
        ]);

        $template = CustomTemplate::where('type', 'mail')->findOrFail($request->id);
        $template->name = $request->name;
        $template->subject = $request->subject;
        $template->text = $request->text;

        $template->save();

        return redirect()->route('mailing');
    }

    public function NotificationField(Request $request)
    {
        $this->validate($request, [
            'id' => 'required|exists:custom_templates,id',
            'field' => 'required|in:status,send_copy',
        ]);

        $notification = CustomTemplate::where('type', 'mail')->findOrFail($request->id);
        $notification[$request->field] = $request->has('value');

        $notification->save();
    }

    public function ReportInfo($id)
    {
        return view('reports.template', [
            'template' => CustomTemplate::where('type', 'report')->findOrFail($id)
        ]);
    }

    public function SaveReport(Request $request)
    {
        $this->validate($request, [
            'id' => 'required|exists:custom_templates,id',
            'text' => 'required'
        ]);

        $template = CustomTemplate::where('type', 'report')->findOrFail($request->id);
        $template->text = $request->text;

        $template->save();

        return redirect()->route('reports.templates');
    }
}
