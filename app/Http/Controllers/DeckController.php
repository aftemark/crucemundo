<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Deck;
use App\Http\Requests;

class DeckController extends Controller
{
    public function Save (Request $request, $id)
    {
        $validate = [
            'code' => 'required|max:255',
            'name' => 'required|max:255',
            'description' => 'max:9999',
        ];

        if ($id == 'new') {
            $validate = $validate + array('ship_id' => 'exists:ships,id');
            $deck = new Deck;
        } else {
            $deck = Deck::find($id);
        }
        $this->validate($request, $validate);


        $deck->code = $request->code;
        $deck->name = $request->name;
        if (isset($request->description))
            $deck->description = $request->description;

        if ($id == 'new')
            $deck->ship_id = $request->ship_id;

        $deck->save();

        return $deck->id;
    }
}
