<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Itinerary;
use App\ItineraryCity;
use App\City;
use App\Http\Requests;

class ItineraryController extends Controller
{
    public function Index(Request $request)
    {
        $this->validate($request, [
            'field' => 'in:all,name,nights,from_to',
            'order' => 'in:asc,desc',
            'order_field' => 'in:name,nights,from_to'
        ]);

        $data = Itinerary::select('*');
        if ($request->has('search')) {
            if ($request->type == 'all')
                $data->search($request->search);
            else
                $data->search($request->search, $request->type);
        }

        if ($request->has('order') && $request->has('order_field'))
            $data->orderBy($request->order_field, $request->order);
        else
            $data->orderBy('updated_at', 'desc');

        return view('itineraries.list', array('data' => $data->paginate($request->session()->get('itineraries_number', 10))));
    }

    public function Info($item)
    {
        if ($item != 'new') {
            $itinerary = Itinerary::findOrFail($item);
        } else {
            $itinerary = new Itinerary;
        }
        
        $allCities = City::all();
        $itinerary->allcities = array();
        $relations = $itinerary->relations;
        $newCities = array();

        foreach ($allCities as $each)
            $itinerary->allcities = [$each->id => $each->name] + $itinerary->allcities;

        if (!empty($relations))
            foreach ($relations as $relation)
                $newCities[$relation->day][] = $relation->city->id;

        $itinerary->itinerarycities = $newCities;

        return $itinerary;
    }

    public function Save(Request $request)
    {
        $validate = [
            'name' => 'required|max:255',
            'nights' => 'required|integer|numeric|between:1,999',
        ];

        if ($request->has('id')) {
            $validate = $validate + ['id' => 'required|exists:itineraries,id'];
            $itinerary = Itinerary::findOrFail($request->id);
        } else {
            $itinerary = new Itinerary;
        }

        for ($i = 1; $i <= $request->nights; $i++) {
            $validate['itinerary_cities.' . $i . '.*'] = 'exists:cities,id';
        }
        $this->validate($request, $validate);

        $cities = array();

        $itinerary->name = $request->name;
        $itinerary->nights = $request->nights;

        $itinerary->save();

        $itinerary->relations()->delete();

        foreach ($request->itinerary_cities as $key => $day) {
            foreach ($day as $cityID) {
                $city = new ItineraryCity;
                $city->itinerary_id = $itinerary->id;
                $city->city_id = $cityID;
                $city->day = $key;
                $city->save();

                $cities[] = $cityID;
            }
        }

        $saved = Itinerary::find($itinerary->id);
        $saved->from_to = City::find($cities[0])->name . ' - ' . City::find(end($cities))->name;
        $saved->save();
    }

    public function SetNumber(Request $request)
    {
        $this->validate($request, ['number' => 'required|in:10,50,100']);
        $request->session()->put('itineraries_number', $request->number);

        return redirect()->route('itineraries_list');
    }

    public function Delete($id)
    {
        $itinerary = Itinerary::findOrFail($id);
        if ($return = \App\Http\Traits\HelperTrait::deleteCheckModel($itinerary, ['journeys' => 'Journeys']))
            return response($return, 422);
        else
            $itinerary->delete();
    }
}
