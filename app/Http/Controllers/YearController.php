<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Year;
use App\Http\Requests;
use Session;
use Validator;
use Redirect;

class YearController extends Controller
{
    public function YearIndex()
    {
        $years['active'] = Year::where('status', 'active')->get();
        $years['archive'] = Year::where('status', 'archive')->get();

        return view('year', array('years' => $years));
    }

    public function YearSelect($year)
    {
        Session::put('catalog_year_id', $year->id);

        Session::put('catalog_year', $year->year);

        return redirect()->route('statistics', array('year' => Session::get('catalog_year')));
    }

    public function YearCopy(Request $request)
    {
        $this->validate($request, [
            'copy' => 'required|exists:years,id',
            'year' => 'required|numeric|integer'
        ]);

        $year = new Year;
        $year->year = $request->year;
        $year->status = 'active';
        $year->save();

        $copy = Year::find($request->copy);

        $newServices = ['services' => [], 'packages' => []];

        foreach ($copy->services as $service) {
            $newService = $service->replicate();
            $newService->year_id = $year->id;
            $newService->save();
            $newServices['services'][$service->id] = $newService->id;
        }

        foreach ($copy->packages as $package) {
            $newPackage = $package->replicate();
            $newPackage->year_id = $year->id;
            $newPackage->save();
            $newServices['packages'][$package->id] = $newPackage->id;
            foreach($package->services as $packageService) {
                $newPackageService = $packageService->replicate();
                $newPackageService->service_package_id = $newPackage->id;
                $newPackageService->service_id = $newServices['services'][$packageService->service_id];
                $newPackageService->save();
            }
        }

        foreach ($copy->journeys as $journey)
            \App\Http\Traits\HelperTrait::journeyCopy($journey, NULL, $year->id, $newServices);

        return redirect()->back();
    }

    public function ArchiveYear(Request $request)
    {
        $this->validate($request, [
            'year' => 'required|exists:years,id'
        ]);

        $year = Year::find($request->year);
        $year->status = 'archive';
        $year->save();

        return redirect()->back();
    }

    public function Statistics($year)
    {
        return view('statistics', array('year' => $year->year));
    }
}
