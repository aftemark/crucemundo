<?php

namespace App\Http\Controllers;

use Auth;
use Session;

use App\Reservation;
use Illuminate\Http\Request;
use App\ReservationPlacement;
use App\Http\Requests;
use App\Http\Traits\HelperTrait;

class ReservationPlacementController extends Controller
{
    use HelperTrait;

    public function Delete(Request $request)
    {
        $this->validate($request, ['id' => 'exists:reservation_placements,id']);

        $placement = ReservationPlacement::find($request->id);

        $reservation = $placement->reservation;
        $role_name = $this->roleName($reservation->type);
        if (!Auth::user()->hasRole('create_' . $reservation->journey->type . '_' . $role_name) || !Auth::user()->hasRole('edit_' . $reservation->journey->type . '_' . $role_name) || !Auth::user()->hasRole('show_' . $reservation->journey->type . '_' . $role_name . '_rooms_cabins'))
            return response('Forbidden.', 403);

        if ($return = $this->deleteCheckModel($placement, ['assigned' => 'Placement is assigned']))
            return response($return, 422);
        else
            $placement->delete();
    }

    public function NonCompatibles($id)
    {
        $reservation = Reservation::withTrashed()->whereYearId(Session::get('catalog_year_id'))->findOrFail($id);

        $role_name = $this->roleName($reservation->type);

        if (!Auth::user()->hasRole('show_' . $reservation->journey->type . '_' . $role_name))
            return response()->json('Forbidden', 403);
        /*$this->validate($request, [
            'placement_id' => 'exists:reservation_placements,id'
        ]);

        $reservation_placement = ReservationPlacement::find($request->placement_id);

        $this->validate($request, [
            'service.placement.' . $reservation_placement->id . '.*' => 'exists:journey_services,id'
        ]);*/

        $nonCompatibles = [];
        foreach($reservation->journey->nonDays as $nonDay) {
            /*$nonIds = [];
            foreach($nonDay->services as $nonService)
                $nonIds[] = $nonService->journey_service_id;

            foreach($nonDay->services as $nonService)
                $nonCompatibles[$nonService->journey_service_id][] = $nonIds;*/

            foreach($nonDay->services as $nonServiceOuter) {
                if (!isset($nonCompatibles[$nonServiceOuter->journey_service_id]))
                    $nonCompatibles[$nonServiceOuter->journey_service_id] = [];
                foreach ($nonDay->services as $nonServiceInner)
                    if (!in_array($nonServiceInner->journey_service_id, $nonCompatibles[$nonServiceOuter->journey_service_id]) && $nonServiceOuter->journey_service_id != $nonServiceInner->journey_service_id)
                        $nonCompatibles[$nonServiceOuter->journey_service_id][] = $nonServiceInner->journey_service_id;
            }
        }

        /*$reservation_placement_services = [];
        $disabled = [];

        if (isset($request->service["placement"][$reservation_placement->id])) {
            foreach ($request->service["placement"][$reservation_placement->id] as $reservation_placement_service_ID) {
                $journey_service = $reservation->journey->services->where('id', $reservation_placement_service_ID)->first();
                if (isset($journey_service->service->language) && $journey_service->service->language_id != $reservation->language_id)
                    continue;

                $reservation_placement_services[] = $reservation_placement_service_ID;

                $skip = false;
                if (isset($nonCompatibles[$reservation_placement_service_ID]))
                    foreach ($nonCompatibles[$reservation_placement_service_ID] as $nonCompatibleArray)
                        foreach ($nonCompatibleArray as $nonCompatible)
                            if ($reservation_placement_service_ID != $nonCompatible && in_array($nonCompatible, $reservation_placement_services)) {
                                $skip = true;
                                break;
                            }

                if ($skip)
                    continue;
            }
        }

        foreach ($reservation_placement->reservation_paxes as $reservation_placement_pax) {
            if (isset($request->service["pax"][$reservation_placement->id][$reservation_placement_pax->id])) {
                $validator = Validator::make($request->all(), ['service.pax.' . $reservation_placement->id . '.' . $reservation_placement_pax->id . '.*' => 'exists:journey_services,id']);

                if (!$validator->fails()) {
                    foreach ($request->service["pax"][$reservation_placement->id][$reservation_placement_pax->id] as $journeyServiceID) {
                        $journey_service = $reservation->journey->services->where('id', $journeyServiceID)->first();
                        if (isset($journey_service->service->language) && $journey_service->service->language_id != $reservation->language_id)
                            continue;

                        $reservation_placement_services[] = $journeyServiceID;

                        $skip = false;
                        if (isset($nonCompatibles[$journeyServiceID]))
                            foreach ($nonCompatibles[$journeyServiceID] as $nonCompatibleArray)
                                foreach ($nonCompatibleArray as $nonCompatible)
                                    if ($journeyServiceID != $nonCompatible && in_array($nonCompatible, $reservation_placement_services)) {
                                        $skip = true;
                                        break;
                                    }

                        if ($skip)
                            continue;
                    }
                }
            }
        }*/

        return $nonCompatibles;
    }
}
