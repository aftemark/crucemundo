<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests;
use Carbon\Carbon;
use PDF;
use App\Document;
use App\CustomTemplate;

class DocumentController extends Controller
{
    public function Index(Request $request)
    {
        return view('reports.documents', [
            'documents' => Document::paginate($request->session()->get('documents_number', 10))
        ]);
    }

    public function NewDocument()
    {
        return view('reports.document', [
            'document' => new Document,
            'templates' => CustomTemplate::where('type', 'report')->get()
        ]);
    }

    public function Save(Request $request)
    {
        $this->validate($request, [
            'template' => 'required|exists:custom_templates,id',
            'date' => 'required|date_format:"d.m.Y"',
            'number' => 'required|numeric|integer'
        ]);

        $template = CustomTemplate::where('type', 'report')->findOrFail($request->template);

        $document = new Document;
        $document->name = $template->name . ' ';
        $document->number = $request->number;
        $document->date = Carbon::createFromFormat('d.m.Y', $request->date)->toDateTimeString();
        $document->text = $template->text;

        $document->save();

        return redirect()->route('reports.documents');
    }

    public function Delete($id)
    {
        Document::findOrFail($id)->delete();

        return redirect()->route('reports.documents');
    }

    public function Download($id)
    {
        $document = Document::findOrFail($id);
        $pdf = PDF::loadHTML($document->text);

        return $pdf->download($document->name . '.pdf');
    }

    public function SetNumber(Request $request)
    {
        $this->validate($request, ['number' => 'required|in:10,50,100']);
        $request->session()->put('documents_number', $request->number);

        return redirect()->route('reports.documents');
    }
}
