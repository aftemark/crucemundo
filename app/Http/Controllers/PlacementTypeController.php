<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\PlacementType;
use App\Http\Requests;

class PlacementTypeController extends Controller
{
    public function Index(Request $request)
    {
        if ($request->has('search'))
            return view('placement_types.list', array('data' => PlacementType::search($request->search)->get()));
        else
            return view('placement_types.list', array('data' => PlacementType::all()));
    }

    public function Info($item)
    {
        if ($item != 'new') {
            $language = PlacementType::find($item);
        } else {
            $language = new PlacementType;
        }

        return view('placement_types.form', array('data' => $language));
    }

    public function Save(Request $request)
    {
        if ($request->has('id')) {
            $this->validate($request, ['id' => 'required|exists:placement_types,id']);
            $type = PlacementType::find($request->id);
        } else {
            $type = new PlacementType;
        }
        $type->name = $request->name;
        $type->save();

        return $type->id;
    }

    public function AjaxInfoShip($item)
    {
        $data = array();
        $type = PlacementType::find($item);

        foreach ($type->categories as $category) {
            if($category->typename == 'ship')
                $data[$category->id] = $category->name;
        }

        return $data;
    }

    public function AjaxInfoHotel($item)
    {
        $data = array();
        $type = PlacementType::find($item);

        foreach ($type->categories as $category) {
            if($category->typename == 'hotel')
                $data[$category->id] = $category->name;
        }

        return $data;
    }

    public function Delete($id)
    {
        $type = PlacementType::findOrFail($id);
        if ($return = \App\Http\Traits\HelperTrait::deleteCheckModel($type, ['categories' => 'Cabin or Room IDs']))
            return response($return, 422);
        else
            $type->delete();
    }
}
