<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\TypeCategory;
use App\PlacementType;
use App\Http\Requests;

class TypeCategoryController extends Controller
{
    public function HotelIndex(Request $request)
    {
        if ($request->has('search'))
            return view('type_categories.list', array('data' => TypeCategory::where('typename', 'hotel')->search($request->search)->get(), 'type' => 'hotel'));

        return view('type_categories.list', array('data' => TypeCategory::where('typename', 'hotel')->get(), 'type' => 'hotel'));
    }

    public function ShipIndex(Request $request)
    {
        if ($request->has('search'))
            return view('type_categories.list', array('data' => TypeCategory::where('typename', 'ship')->search($request->search)->get(), 'type' => 'ship'));

        return view('type_categories.list', array('data' => TypeCategory::where('typename', 'ship')->get(), 'type' => 'ship'));
    }

    public function HotelInfo($item)
    {
        $types = PlacementType::all();

        if ($item != 'new') {
            $category = TypeCategory::findOrFail($item);
            $category->types = array();

            foreach ($types as $each)
                if ($each->id == $category->placement_type_id)
                    $category->types = array($each->id => $each->name) + $category->types;
                else
                    $category->types = $category->types + array($each->id => $each->name);
        } else {
            $category = new TypeCategory;
            $category->types = array();

            foreach ($types as $each)
                $category->types = [$each->id => $each->name] + $category->types;
        }

        $category->types = ['false' => 'Select category'] + $category->types;

        return view('type_categories.form', array('data' => $category, 'type' => 'hotel'));
    }

    public function ShipInfo($item)
    {
        $types = PlacementType::all();

        if ($item != 'new') {
            $category = TypeCategory::findOrFail($item);
            $category->types = array();

            foreach ($types as $each)
                if ($each->id == $category->placement_type_id)
                    $category->types = array($each->id => $each->name) + $category->types;
                else
                    $category->types = $category->types + array($each->id => $each->name);
        } else {
            $category = new TypeCategory;
            $category->types = array();

            foreach ($types as $each)
                $category->types = [$each->id => $each->name] + $category->types;
            $category->types = ['false' => 'Select category'] + $category->types;
        }
        
        return view('type_categories.form', array('data' => $category, 'type' => 'ship'));
    }

    public function Save(Request $request)
    {
        if ($request->has('id')) {
            $this->validate($request, [
                'id' => 'required|exists:type_categories,id',
                'category' => 'required|exists:placement_types,id',
                'pax_amount' => 'required|integer',
                'typename' => 'required|in:ship,hotel',
                'name' => 'required|max:250'
            ]);
            $category = TypeCategory::find($request->id);
        } else {
            $this->validate($request, [
                'category' => 'required|exists:placement_types,id',
                'pax_amount' => 'required|integer',
                'typename' => 'required|in:ship,hotel',
                'name' => 'required|max:250'
            ]);
            $category = new TypeCategory;
        }
        $category->name = $request->name;
        $category->placement_type_id = $request->category;
        $category->typename = $request->typename;
        $category->pax_amount = $request->pax_amount;
        if($request->balcony)
            $category->balcony = true;
        else
            $category->balcony = false;
        if($request->bed)
            $category->bed = true;
        else
            $category->bed = false;

        $category->save();

        return $category->id;
    }

    public function AjaxInfo($item)
    {
        $data = array();
        $type = TypeCategory::findOrFail($item);
        
        $data["Bed"] = $type->bed;
        $data["Balcony"] = $type->balcony;
        $data["PaxAmount"] = $type->pax_amount;

        return $data;
    }

    public function Delete($id)
    {
        $typeCategory = TypeCategory::findOrFail($id);
        if ($return = \App\Http\Traits\HelperTrait::deleteCheckModel($typeCategory, ['cabins' => 'Cabins', 'rooms' => 'Rooms', 'cruise_types' => 'Cruise Types']))
            return response($return, 422);
        else
            $typeCategory->delete();
    }
}
