<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Notification;
use App\Http\Requests;
use Carbon\Carbon;

class NotificationController extends Controller
{
    public function Actions(Request $request)
    {
        $this->validate($request, [
            'order' => 'in:asc,desc',
            'order_field' => 'in:created_at,user.name,text',
        ]);
        $notifications = Notification::where('type', 'action');

        if ($request->has('date'))
            if ($request->date == 'today')
                $notifications->whereDay('created_at', '=', date('d'));
            else if ($request->date == 'yesterday')
                $notifications->whereDay('created_at', '=', date('d', strtotime('-1 days')));
            else if ($request->date == 'week')
                $notifications->whereDate('created_at', '>=', Carbon::now()->subDays(7)->toDateString());

        if ($request->has('search'))
            $notifications->search($request->search);

        if ($request->has('order') && $request->has('order_field')) {
            if ($request->order_field == 'user.name')
                $notifications->select('notifications.*')->leftJoin('users', 'user_id', '=', 'users.id')->orderBy('users.name', $request->order);
            else
                $notifications->orderBy($request->order_field, $request->order);
        } else
            $notifications->orderBy('created_at', 'desc');

        return view('users_actions.list', array('notifications' => $notifications->paginate($request->session()->get('notifications_actions', 10))));
    }

    public function Updates(Request $request)
    {
        $this->validate($request, [
            'order' => 'in:asc,desc',
            'order_field' => 'in:created_at,text',
        ]);
        $notifications = Notification::where('type', 'update');

        if ($request->has('date'))
            if ($request->date == 'today')
                $notifications->whereDay('created_at', '=', date('d'));
            else if ($request->date == 'yesterday')
                $notifications->whereDay('created_at', '=', date('d', strtotime('-1 days')));
            else if ($request->date == 'week')
                $notifications->whereDate('created_at', '>=', Carbon::now()->subDays(7)->toDateString());

        if ($request->has('search'))
            $notifications->search($request->search);

        if ($request->has('order') && $request->has('order_field')) {
            $notifications->orderBy($request->order_field, $request->order);
        } else
            $notifications->orderBy('created_at', 'desc');

        return view('program_updates.list', array('notifications' => $notifications->paginate($request->session()->get('notifications_updates', 10))));
    }

    public function Reminders(Request $request)
    {
        $this->validate($request, [
            'table' => 'in:acting,overdue,completed',
            'order' => 'in:asc,desc',
            'order_field' => 'in:created_at,text'
        ]);
        $number = $request->session()->get('notifications_reminders', 10);

        $notifications = [
            'acting' => Notification::where('type', 'reminder')->where('completed', false)->whereDate('created_at', '>=', Carbon::now()->subDay()),
            'overdue' => Notification::where('type', 'reminder')->where('completed', false)->whereDate('created_at', '<=', Carbon::now()->subDay()),
            'completed' => Notification::where('type', 'reminder')->where('completed', true)
        ];

        foreach ($notifications as $notification_type => $notifications_collection) {
            if ($request->has($notification_type . '_date'))
                if ($request[$notification_type . '_date'] == 'today')
                    $notifications_collection = $notifications_collection->whereDay('created_at', date('d'));
                else if ($request[$notification_type . '_date'] == 'yesterday')
                    $notifications_collection = $notifications_collection->whereDay('created_at', date('d', strtotime('-1 days')));
                else if ($request[$notification_type . '_date'] == 'week')
                    $notifications_collection = $notifications_collection->whereDate('created_at', '>=', Carbon::now()->subDays(7)->toDateString());
            if ($request->input('table', NULL) == $notification_type) {
                if ($request->has('search'))
                    $notifications->search($request->search);
                if ($request->has('order') && $request->has('order_field'))
                    $notifications->orderBy($request->order_field, $request->order);
            } else
                $notifications[$notification_type] = $notifications_collection->orderBy('created_at', 'desc');
            $notifications[$notification_type] = $notifications_collection->paginate($number);
        }

        return view('reminders.list', array('notifications' => $notifications));
    }

    public function SetCompleted($id)
    {
        $reminder = Notification::where('type', 'reminder')->where('completed', false)->findOrFail($id);
        $reminder->completed = true;
        $reminder->save();

        return redirect()->route('reminders');
    }

    public function SetNumber(Request $request)
    {
        $this->validate($request, [
            'page' => 'required|in:actions,updates,reminders',
            'number' => 'required|in:10,50,100'
        ]);

        if (!Auth::user()->hasRole('show_notifications_' . $request->page))
            return redirect()->back();

        $request->session()->put('notifications_' . $request->page, $request->number);

        return redirect()->back();
    }
}
