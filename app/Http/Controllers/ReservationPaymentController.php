<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\ReservationPayment;
use App\Reservation;
use App\Http\Requests;
use Auth;
use App\Http\Traits\ReservationTrait;

class ReservationPaymentController extends Controller
{
    use ReservationTrait;

    public function Info(Request $request)
    {
        if ($request->item == 'new')
            $payment = new ReservationPayment;
        else {
            $payment = ReservationPayment::findOrFail($request->item);

            if (!Auth::user()->hasRole('edit_' . $payment->reservation->journey->type . '_bookings'))
                return response()->json('Forbidden', 403);
        }

        return response()->json($payment);
    }

    public function Save(Request $request)
    {
        $this->validate($request, ['reservation_id' => 'required|exists:reservations,id']);

        $reservation = Reservation::findOrFail($request->reservation_id);

        if (!Auth::user()->hasRole('edit_' . $reservation->journey->type . '_bookings') || !Auth::user()->hasRole('create_' . $reservation->journey->type . '_bookings'))
            return response('Forbidden.', 403);

        if ($request->has('id') && $request->id) {
            $this->validate($request, ['id' => 'required|exists:reservation_payments,id']);
            $payment = ReservationPayment::findOrFail($request->id);

            $payment->reservation_id = $request->reservation_id;
        } else {
            $payment = new ReservationPayment;

            $payment->reservation_id = $request->reservation_id;
        }

        list($placements_data, $services_data) = $this->reservationInfo($reservation);
        $reservation_price = $services_data["total"] + $placements_data["total"]["total"];
        
        foreach ($reservation->payments as $reservation_payment)
            if ($reservation_payment->id != $payment->id)
                $reservation_price -= $reservation_payment->amount;

        $this->validate($request, [
            'date' => 'required|date',
            'type' => 'required|in:deposit,full,100',
            'amount' => 'required|numeric|between:0.01,' . number_format($reservation_price, 2)
        ]);

        $payment->date = $request->date;
        $payment->type = $request->type;
        $payment->amount = $request->amount;
        if ($request->has('email') && $request->email)
            $payment->email = true;
        else
            $payment->email = false;

        $payment->save();
    }

    public function Delete(Request $request)
    {
        $this->validate($request, ['id' => 'exists:reservation_payments,id']);

        $payment = ReservationPayment::findOrFail($request->id);

        if (!Auth::user()->hasRole('edit_' . $payment->reservation->journey->type . '_bookings') || !Auth::user()->hasRole('create_' . $payment->reservation->journey->type . '_bookings'))
            return response('Forbidden.', 403);

        $payment->delete();
    }
}
