<?php

namespace App\Http\Controllers;

use Auth;
use Validator;
use Carbon\Carbon;
use Config;

use Illuminate\Http\Request;
use App\ReservationPax;
use App\ReservationPlacement;
use App\Country;
use App\Airport;
use App\Http\Requests;
use App\Http\Traits\ReservationTrait;
use App\Http\Traits\HelperTrait;

class ReservationPaxController extends Controller
{
    use HelperTrait;
    use ReservationTrait;

    public function PaxList(Request $request)
    {
        $this->validate($request, ['id' => 'exists:reservation_placements,id']);

        $placement = ReservationPlacement::find($request->id);

        $role_name = $this->roleName($placement->reservation->type);
        if (!Auth::user()->hasRole('show_' . $placement->reservation->journey->type . '_' . $role_name))
            return response('Forbidden.', 403);

        $placements_number = $placement->adults + $placement->childrens + $placement->infants;

        $paxes = [];

        $count = 0;

        foreach ($placement->reservation_paxes as $pax) {
            $paxes[++$count] = $pax;
        }

        if ($count > $placements_number)
            for ($i = $count + 1; $i <= $placements_number; $i++) {
                $newPax = new ReservationPax;
                $newPax->reservation_placement_id = $request->id;
                $paxes[$i] = $newPax;
            }

        return view('reservations.pax', array(
            'paxes' => $paxes,
            'placementID' => $placement->id,
            'countries' => Country::all(),
            'airports' => Airport::all()
        ));
    }

    public function SavePax(Request $request)
    {
        if (!$request->has('pax'))
            return response('No Request.', 422);

        $placement = ReservationPlacement::findOrFail($request->id);

        $role_name = $this->roleName($placement->reservation->type);
        if (!Auth::user()->hasRole('create_' . $placement->reservation->journey->type . '_' . $role_name) || !Auth::user()->hasRole('edit_' . $placement->reservation->journey->type . '_' . $role_name))
            return response('Forbidden.', 403);

        $reservation_type = $placement->reservation->type;

        $validate = [];
        $paxFines = 0;
        for ($i = 1; $i <= $placement->adults + $placement->childrens + $placement->infants; $i++) {
            if (isset($request["pax"][$i])) {
                $requestPax = $request["pax"][$i];
                $pax = ReservationPax::findOrFail($requestPax["id"]);

                $paxValidate = [
                    'pax.' . $i . '.name' => 'max:255',
                    'pax.' . $i . '.sex' => 'in:0,1',
                    'pax.' . $i . '.surname' => 'max:255',
                    'pax.' . $i . '.document_type' => 'max:255',
                    'pax.' . $i . '.document_number' => 'max:255',
                    'pax.' . $i . '.country_id' => 'exists:countries,id',
                    'pax.' . $i . '.validity' => 'date',
                    'pax.' . $i . '.arrival' => 'date',
                    'pax.' . $i . '.departure' => 'date',
                    'pax.' . $i . '.arrival_airport_id' => 'exists:airports,id',
                    'pax.' . $i . '.departure_airport_id' => 'exists:airports,id',
                    'pax.' . $i . '.arrival_number' => 'max:255',
                    'pax.' . $i . '.departure_number' => 'max:255',
                ];

                $validate_date = NULL;
                $first_date = Carbon::parse($placement->reservation->journey->depart_date);
                $second_date = $first_date->toDateTimeString();

                if ($pax->type == 'infant')
                    $validate_date = '|after:' . $first_date->subYears(3)->toDateTimeString() . '|before:' . $second_date;
                else if ($pax->type == 'children')
                    $validate_date = '|after:' . $first_date->subYears(13)->addDay()->toDateTimeString() . '|before:' . $second_date;
                else if ($pax->type == 'adult')
                    if ($placement->childrens + $placement->infants)
                        $validate_date = '|before:' . $first_date->subYears(21)->toDateTimeString();
                    else
                        $validate_date = '|before:' . $first_date->subYears(13)->toDateTimeString();

                $paxValidate += [
                    'pax.' . $i . '.birth' => 'date_format:"d.m.Y"' . $validate_date
                ];

                if (!empty($requestPax["arrival"]) || !empty($requestPax["arrival_airport_id"]) || !empty($requestPax["arrival_number"])) {
                    $paxValidate['pax.' . $i . '.arrival'] = 'required|date_format:"d.m.Y"';
                    $paxValidate['pax.' . $i . '.arrival_airport_id'] = 'required|exists:airports,id';
                    $paxValidate['pax.' . $i . '.arrival_number'] = 'required|max:255';
                }

                if (!empty($requestPax["departure"]) || !empty($requestPax["departure_airport_id"]) || !empty($requestPax["departure_number"])) {
                    $paxValidate['pax.' . $i . '.departure'] = 'required|date_format:"d.m.Y"|after:pax.' . $i . '.arrival';
                    $paxValidate['pax.' . $i . '.departure_airport_id'] = 'required|exists:airports,id';
                    $paxValidate['pax.' . $i . '.departure_number'] = 'required|max:255';
                }

                $validator = Validator::make($request->all(), $paxValidate);

                if (!$validator->fails()) {
                    if ($pax->filled && ($pax->name != $request->input("pax." . $i . ".name") || $pax->surname != $request->input("pax." . $i . ".surname") || $pax->sex != $request->input("pax." . $i . ".sex") ||
                        $pax->birth != (!empty($requestPax["birth"]) ? Carbon::createFromFormat('d.m.Y', $requestPax["birth"])->toDateTimeString() : 0) || $pax->document_type != $request->input("pax." . $i . ".document_type") ||
                        $pax->document_number != $request->input("pax." . $i . ".document_number") || $pax->validity != (!empty($requestPax["birth"]) ? Carbon::createFromFormat('d.m.Y', $requestPax["birth"])->toDateTimeString() : 0))) {
                        $this->notify(Auth::user()->id, $placement->reservation->id, 'action', 'Pax details were replaced - <a href="' . url('/' . $placement->reservation->type . '/' . $placement->reservation->id) . '">' . $placement->reservation->code . '</a>');
                        if ($reservation_type == 'booking')
                            $paxFines++;
                    }

                    $pax->reservation_placement_id = $placement->id;
                    $pax->name = $request->input("pax." . $i . ".name", NULL);
                    $pax->surname = $request->input("pax." . $i . ".surname", NULL);
                    $pax->sex = isset($requestPax["sex"]) && ($requestPax["sex"] === '0' || $requestPax["sex"] === '1') ? $requestPax["sex"] : NULL;
                    $pax->birth = !empty($requestPax["birth"]) ? Carbon::createFromFormat('d.m.Y', $requestPax["birth"])->toDateTimeString() : NULL;
                    $pax->document_type = $request->input("pax." . $i . ".document_type", NULL);
                    $pax->document_number = $request->input("pax." . $i . ".document_number", NULL);
                    $pax->country_id = $request->input("pax." . $i . ".country_id", NULL);
                    $pax->validity = !empty($requestPax["validity"]) ? Carbon::createFromFormat('d.m.Y', $requestPax["validity"])->toDateTimeString() : NULL;
                    $pax->arrival = !empty($requestPax["arrival"]) ? Carbon::createFromFormat('d.m.Y', $requestPax["arrival"])->toDateTimeString() : NULL;
                    $pax->departure = !empty($requestPax["departure"]) ? Carbon::createFromFormat('d.m.Y', $requestPax["departure"])->toDateTimeString() : NULL;
                    $pax->arrival_airport_id = $request->input("pax." . $i . ".arrival_airport_id", NULL);
                    $pax->departure_airport_id = $request->input("pax." . $i . ".departure_airport_id", NULL);
                    $pax->arrival_number = $request->input("pax." . $i . ".arrival_number", NULL);
                    $pax->departure_number = $request->input("pax." . $i . ".departure_number", NULL);

                    if (!$pax->filled && !empty($pax->name) && !empty($pax->surname) && isset($pax->sex) && !empty($pax->birth) &&
                        !empty($pax->document_type) && !empty($pax->document_number) && !empty($pax->validity)) {
                         $pax->filled = true;
                         //$this->notify(Auth::user()->id, $placement->reservation->id, 'action', 'Pax details were updated - <a href="' . url('/' . $placement->reservation->type . '/' . $placement->reservation->id) . '">' . $placement->reservation->code . '</a>');
                    }

                    $pax->save();
                } else
                    $validate += $paxValidate;
            }
        }

        if ($paxFines)
            $this->fine($placement->reservation->id, $this->notify(Auth::user()->id, $placement->reservation->id, 'action', 'Pax details were replaced, with fine - ' . number_format($paxFines * Config::get('constants.fines.pax_details_replacement'), 2) . ' EUR - <a href="' . url('/' . $placement->reservation->type . '/' . $placement->reservation->id) . '">' . $placement->reservation->code . '</a>'), 'pax_details', $paxFines * Config::get('constants.fines.pax_details_replacement'));

        $this->validate($request, $validate);
    }

    public function Delete(Request $request)
    {
        $pax = ReservationPax::findOrFail($request->id);

        $reservation = $pax->placement->reservation;
        $role_name = $this->roleName($reservation->type);
        if (!Auth::user()->hasRole('create_' . $reservation->journey->type . '_' . $role_name) || !Auth::user()->hasRole('edit_' . $reservation->journey->type . '_' . $role_name) || !Auth::user()->hasRole('show_' . $reservation->journey->type . '_' . $role_name . '_rooms_cabins'))
            return response('Forbidden.', 403);

        $placement = $pax->placement;

        $newTypeAmount = $placement[$pax->type . 's'] - 1;
        if ($pax->type == 'adult' && $newTypeAmount >= $placement->childrens + $placement->infants) {
            $placement[$pax->type . 's'] = $newTypeAmount;
            $placement->save();
        } else if ($pax->type == 'adult')
            return false;
        else {
            $placement[$pax->type . 's'] = $newTypeAmount;
            $placement->save();
        }

        if ($placement->reservation->placements_tab && $placement->reservation->type == 'booking') {
            $reservationPercent = $this->reservationPercent($placement->reservation);
            $clientPercent = 0;

            if ($placement->reservation->client->type == 'b2b') {
                if ($placement->reservation->journey->type == 'cruise') {
                    $percent = $placement->reservation->client->percents()->where('ship_id', $placement->reservation->journey->ship_id)->first();
                    if ($percent !== NULL)
                        $clientPercent = $percent->percent / 100;
                } else if ($placement->reservation->journey->type == 'tour') {
                    $percent = $placement->reservation->client->percents()->where('hotel_id', $placement->reservation->journey->hotel_id)->first();
                    if ($percent !== NULL)
                        $clientPercent = $percent->percent / 100;
                }
            } else if ($placement->reservation->client->type == 'b2c' && $placement->reservation->client->percent > 0)
                $clientPercent = $placement->reservation->client->percent / 100;

            $this_type = $placement->reservation->journey->type == 'tour' ? $placement->placement->room : $placement->cruise_type;
            $paxFine = $placement->reservation->reservation_type == 'group' ? $this_type->price_rec : $this_type->price_net;
            if ($pax->type == 'children')
                $paxFine *= 0.7;
            else if ($pax->type == 'infant')
                $paxFine = 0;

            $paxFine = $paxFine - $paxFine * $clientPercent;

            if ($placement->reservation->journey->type == 'tour')
                foreach ($pax->services as $pax_service) {
                    $this_service = isset($pax_service->service->service) ? $pax_service->service->service : $pax_service->service->package;
                    if ($pax_service->service->optional)
                        $paxFine += $this_service->price;
                }

            $this->notify(Auth::user()->id, $placement->reservation->id, 'action', 'Number of pax was updated - <a href="' . url('/' . $placement->reservation->type . '/' . $placement->reservation->id) . '">' . $placement->reservation->code . '</a>');
            $this->fine($placement->reservation->id, $this->notify(NULL, $placement->reservation->id, 'update', 'Number of pax was updated, with fine - ' . number_format(($paxFine * $reservationPercent) / 100, 2) . ' EUR - <a href="' . url('/' . $placement->reservation->type . '/' . $placement->reservation->id) . '">' . $placement->reservation->code . '</a>'), 'pax_number', ($paxFine * $reservationPercent) / 100);
        }

        $pax->delete();
    }
}
