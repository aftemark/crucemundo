<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Service;
use App\Http\Requests;

class ServiceController extends Controller
{
    public function __construct(Request $request)
    {
        $this->year = $request->session()->get('catalog_year_id');
    }

    public function Index(Request $request)
    {
        $types = ['all', 'excursion', 'transport', 'catering', 'other'];

        $number = $request->session()->get('services_number', 10);

        $data = [];
        foreach ($types as $type) {
            $data[$type] = Service::where('year_id', $this->year);
            if ($type != 'all')
                $data[$type] = $data[$type]->where('service_type', $type);
        }

        if ($request->has('search') && $request->has('field') && $request->has('table')) {
            foreach ($types as $type) {
                if ($type == $request->table) {
                    if ($request->table == 'excursion' || $request->table == 'transport')
                        $validate = array('field' => 'required|in:all,name');
                    else if ($request->table == 'all')
                        $validate = array('field' => 'required|in:all,service_type,name,city,ship,hotel');
                    else
                        $validate = array('field' => 'required|in:all,name,city,ship,hotel');

                    $this->validate($request, $validate + [
                            'search' => 'max:255',
                            'table' => 'required|in:' . implode(',', $types),
                            'order_field' => 'in:service_type,name',
                            'order' => 'in:asc,desc'
                        ]);

                    if ($request->field == 'all') {
                        $data[$request->table] = $data[$request->table]->search($request->search, ['service_type', 'name', 'city.name', 'ship.name', 'hotel.name']);
                    } else if ($request->field == 'service_type' || $request->field == 'name') {
                        if ($request->table != 'all')
                            $data[$request->table] = $data[$request->table]->search($request->search, [$request->field]);
                        else
                            $data[$request->table] = $data[$request->table]->search($request->search, [$request->field]);

                    } else {
                        $data[$request->table] = $data[$request->table]->search($request->search, [$request->field . '.name']);
                    }
                }
            }
        }

        if ($request->has('order') && $request->has('order_field') && $request->has('table'))
            $data[$request->table] = $data[$request->table]->orderBy($request->order_field, $request->order);

        foreach ($types as $type) {
            if ($request->input('table', NULL) !== $type)
                $data[$type] = $data[$type]->orderBy('updated_at', 'desc');
            $data[$type] = $data[$type]->paginate($number, ['*'], $type . '_services');
        }

        return view('services.list', array('data' => $data));
    }

    public function SetNumber(Request $request)
    {
        $this->validate($request, ['number' => 'required|in:10,50,100']);
        $request->session()->put('services_number', $request->number);

        return redirect()->route('services_list');
    }

    public function Info(Request $request, $item)
    {
        if ($item != 'new') {
            $service = Service::find($item);
            $service_type = $service->service_type;
        } else {
            $this->validate($request, ['service_type' => 'required|in:excursion,transport,catering,other']);
            $service = new Service;
            $service_type = $request->service_type;
        }

        $service->venue_types = array();
        $service->venues = array();

        $venue_types = array('ships' => 'App\Ship', 'hotels' => 'App\Hotel', 'cities' => 'App\City', 'other' => 'Other');
        $venue_types_names = array('App\Ship' => 'Ship', 'App\Hotel' => 'Hotel', 'App\City' => 'City', 'Other' => 'Other');
        $venue_types_fields = array('ships' => 'ship_id', 'hotels' => 'hotel_id', 'cities' => 'city_id', 'other' => 'other');

        if ($service_type == 'other' || $service_type == 'catering') {
            foreach ($venue_types as $field => $venue_type) {
                if (!empty($service[$venue_types_fields[$field]])) {
                    $service->venue_types = array($field => $venue_types_names[$venue_type]) + $service->venue_types;
                    if ($field == 'other') {
                        $service->venues = $service->other;
                    } else {
                        $venue = $venue_type::all();
                        foreach ($venue as $entity) {
                            if($entity->id == $service[$field])
                                $service->venues = array($entity->id => $entity->name) + $service->venues;
                            else
                                $service->venues = $service->venues + array($entity->id => $entity->name);
                        }
                    }
                } else
                    $service->venue_types = $service->venue_types + array($field => $venue_types_names[$venue_type]);
            }
            if (!$service->other) {
                if (empty($service[reset($venue_types_fields)])) {
                    $firstVenue = reset($venue_types);
                    $venue = $firstVenue::all();
                    foreach ($venue as $entity) {
                        $service->venues = $service->venues + array($entity->id => $entity->name);
                    }
                }
            }
        } else if ($service_type == 'excursion' || $service_type == 'transport') {

            if($service_type == 'excursion') {
                $service->languages = $service->language_id ? array() : array('false' => 'Select a language');
                $langModel = 'App\Language';
                $languages = $langModel::all();

                foreach ($languages as $language) {
                    if($language->id == $service->language_id)
                        $service->languages = array($language->id => $language->name) + $service->languages;
                    else
                        $service->languages = $service->languages + array($language->id => $language->name);
                }
            }

            $service->venues = $service->city_id ? array() : array('false' => 'Select a city');
            $cityModel = 'App\City';
            $cities = $cityModel::all();

            foreach ($cities as $city) {
                if($city->id == $service->city_id)
                    $service->venues = array($city->id => $city->name) + $service->venues;
                else
                    $service->venues = $service->venues + array($city->id => $city->name);
            }
        }

        return view('services.form', array('data' => $service, 'service_type' => $service_type));
    }

    public function Save(Request $request)
    {
        $venue_types = array('ships' => 'ship_id', 'hotels' => 'hotel_id', 'cities' => 'city_id', 'other' => 'other');
        $validate = [
            'name' => 'required|min:2|max:255',
            'venue_type' => 'required|in:' . implode(',', array_keys($venue_types)),
            'min_pax' => 'required|integer',
            'max_pax' => 'required|integer',
            'price' => 'required|between:0,9999999.99',
            'available' => 'in:true',
            'board_price' => 'between:0,9999999.99',
            'description' => 'max:9999',
            'condition_desc' => 'max:9999',
            'service_type' => 'required|in:excursion,transport,catering,other'
        ];

        if ($request->service_type == 'excursion')
            $validate = $validate + array('language' => 'required|exists:languages,id');

        if ($request->venue_type == 'other')
            $validate = $validate + array('venue' => 'required|max:255');
        else
            $validate = $validate + array('venue' => 'required|exists:' . $request->venue_type . ',id');

        if ($request->has('id')) {
            $this->validate($request, $validate + array('id' => 'required|exists:services,id'));
            $service = Service::find($request->id);
        } else {
            $this->validate($request, $validate);
            $service = new Service;
            $service->service_type = $request->service_type;
        }

        $service->name = $request->name;
        $service->min_pax = $request->min_pax;
        $service->max_pax = $request->max_pax;
        
        $service->price = $request->price;

        if ($request->venue_type == 'other')
            $service->other = $request->venue;
        else
            $service[$venue_types[$request->venue_type]] = $request->venue;

        if ($request->service_type == 'excursion')
            $service->language_id = $request->language;

        if ($service->service_type != 'excursion') {
            if ($request->all_pax_cabin == 'true') {
                $service->all_pax_cabin = true;
                if ($request->multiply == 'true')
                    $service->multiply = true;
                else
                    $service->multiply = false;
            } else {
                $service->all_pax_cabin = false;
                $service->multiply = false;
            }
        }

        $service->description = $request->description;
        $service->condition_desc = $request->condition_desc;
        $service->year_id = $this->year;
        $service->save();

        return $service->id;
    }

    public function GetVenues(Request $request)
    {
        $venue_types = array('ships' => 'App\Ship', 'hotels' => 'App\Hotel', 'cities' => 'App\City');
        $this->validate($request, ['type' => 'required|in:' . implode(',', array_keys($venue_types))]);

        $data = array();

        $venue = $venue_types[$request->type];
        $venue = $venue::all();
        foreach ($venue as $entity) {
            $data = $data + array($entity->id => $entity->name);
        }

        return $data;
    }

    public function Delete($id)
    {
        $service = Service::findOrFail($id);
        if ($return = \App\Http\Traits\HelperTrait::deleteCheckModel($service, ['packages' => 'Packages', 'journey_services' => 'Journey Services']))
            return response($return, 422);
        else
            $service->delete();
    }
}
