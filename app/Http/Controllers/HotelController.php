<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Hotel;
use App\City;
use App\Http\Requests;

class HotelController extends Controller
{
    public function Info($item)
    {
        if ($item != 'new') {
            $data = Hotel::findOrFail($item);
            $data->cities = array();
        } else {
            $data = new Hotel;
            $data->cities = array('false' => 'Select a city');
        }

        $cities = City::all();
        foreach($cities as $city) {
            if ($city->id == $data->city_id)
                $data->cities = array($city->id => $city->name) + $data->cities;
            else
                $data->cities = $data->cities + array($city->id => $city->name);
        }

        return view('hotels.form', array('data' => $data));
    }

    public function RoomsIndex($item)
    {
        $data = array();
        $data["data"] = array();
        $data["hotel_id"] = $item;
        $hotel = Hotel::find($item);
        foreach ($hotel->rooms as $room) {
            $data["data"][$room->type->type->name][$room->type->name] = $room->id;
        }
        return view('rooms.list', array('data' => $data));
    }

    public function Save(Request $request, $id)
    {
        $validate = [
            'code' => 'required|max:255',
            'city' => 'required|exists:cities,id',
            'name' => 'required|max:255',
            'rating' => 'required|integer',
            'address' => 'max:255',
            'services_desc' => 'max:9999',
            'add_desc' => 'max:9999',
            'description' => 'max:9999'
        ];

        if ($id == 'new') {
            $hotel = new Hotel;
        } else {
            $hotel = Hotel::findOrFail($id);
        }
        $this->validate($request, $validate);

        $hotel->code = $request->code;
        $hotel->city_id = $request->city;
        $hotel->name = $request->name;
        $hotel->rating = $request->rating;
        $hotel->address = $request->address;
        $hotel->services_desc = $request->services_desc;
        $hotel->add_desc = $request->add_desc;
        $hotel->description = $request->description;

        $hotel->save();

        return $hotel->id;
    }

    public function Delete($id)
    {
        $hotel = Hotel::findOrFail($id);
        if ($return = \App\Http\Traits\HelperTrait::deleteCheckModel($hotel, ['services' => 'Services', 'journeys' => 'Tours']))
            return response($return, 422);
        else
            $hotel->delete();
        return redirect()->back();
    }
}
