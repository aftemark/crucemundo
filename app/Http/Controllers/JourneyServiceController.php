<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Journey;
use App\Service;
use App\Http\Requests;

class JourneyServiceController extends Controller
{
    public function __construct(Request $request)
    {
        $this->year = $request->session()->get('catalog_year_id');
    }

    public function Index(Request $request)
    {
        $this->validate($request, [
            'itinerary_id' => 'required|exists:itineraries,id',
            'journey_id' => 'exists:journeys,id'
        ]);

        if ($request->has('journey_id')) {
            $journey = Journey::find($request->journey_id);
        } else {
            $journey = new Journey;
        }

        $journey->itinerary_id = $request->itinerary_id;

        $services = array();
        foreach ($journey->services as $service) {
            if ($service->itinerary_city_id !== NULL)
                $services[$service->itinerary_city_id][$service->service_id] = $service;
            else
                $services["other"][$service->service_id] = $service;
        }

        $otherServices = array();
        if ($journey->type == 'tour' && isset($journey->hotel)) {
            $otherServices = collect($journey->hotel->services()->where('year_id', $this->year)->get())
                ->merge(collect(Service::where('service_type', 'other')->where('year_id', $this->year)->get()))
                ->merge(collect(Service::where('service_type', 'catering')->where('other', '!=', NULL)->where('year_id', $this->year)->get()))->unique();
        } else if ($journey->type == 'cruise' && isset($journey->ship)) {
            $otherServices = collect($journey->ship->services()->where('year_id', $this->year)->get())
                ->merge(collect(Service::where('service_type', 'other')->where('year_id', $this->year)->get()))
                ->merge(collect(Service::where('service_type', 'catering')->where('other', '!=', NULL)->where('year_id', $this->year)->get()))->unique();
        }

        $days = array();
        $cities = array();

        if (isset($journey->itinerary))
            foreach ($journey->itinerary->relations as $itinerary) {
                $days[$itinerary->day][$itinerary->id] = $itinerary->city;
                $cities[] = $itinerary->id;
            }

        $nonServices = array();
        $counter = 0;
        if (isset($journey->nonDays)) {
            foreach ($journey->nonDays as $nonDay) {
                if (in_array($nonDay->itinerary_city_id, $cities) || $nonDay->itinerary_city_id === NULL) {
                    ++$counter;
                    foreach ($nonDay->services as $nonService) {
                        $nonService->counter = $counter;
                        if ($nonDay->itinerary_city_id)
                            $nonServices[$nonDay->itinerary_city_id][$nonDay->id][$nonService->journey_service_id] = $nonService;
                        else
                            $nonServices["other"][$nonDay->id][$nonService->journey_service_id] = $nonService;
                    }
                }
            }
        }

        return view('journey_services.list', array(
            'data' => $journey,
            'otherServices' => $otherServices,
            'services' => $services,
            'nonServices' => $nonServices,
            'days' => $days
        ));
    }
}