<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\ServicePackage;
use App\PackageService;
use App\Service;
use App\Http\Requests;

class ServicePackageController extends Controller
{
    public function __construct(Request $request)
    {
        $this->year = $request->session()->get('catalog_year_id');
    }

    public function Index(Request $request)
    {
        $this->validate($request, [
            'order' => 'in:asc,desc'
        ]);

        $data = ServicePackage::where('year_id', $this->year);
        if ($request->has('search'))
            $data->search($request->search);
        if ($request->has('order'))
            $data->orderBy('name', $request->order);
        else
            $data->orderBy('updated_at', 'desc');

        return view('packages.list', array('data' => $data->paginate($request->session()->get('packages_number', 10))));
    }

    public function SetNumber(Request $request)
    {
        $this->validate($request, ['number' => 'required|in:10,50,100']);
        $request->session()->put('packages_number', $request->number);

        return redirect()->route('packages_list');
    }

    public function Info($item)
    {
        if ($item != 'new') {
            $package = ServicePackage::findOrFail($item);
        } else {
            $package = new ServicePackage;
        }

        return view('packages.form', array('data' => $package));
    }

    public function PackageServices($item)
    {
        $package = ServicePackage::findOrFail($item);
        $data = array();

        foreach ($package->services as $relation) {
            $relation->service->parent_id = $relation->id;
            $data[] = $relation->service;
        }
        return view('packages.services', array('data' => $data, 'packageid' => $package->id));
    }

    public function PackageVenues($item)
    {
        $data = array();
        $services = ServicePackage::findOrFail($item)->services;

        foreach ($services as $service) {
            $actual = $service->service;
            if (isset($actual->ship))
                $data[$actual->ship->name] = $actual->ship->name;
            else if (isset($actual->city))
                $data[$actual->city->name] = $actual->city->name;
            else if (isset($actual->hotel))
                $data[$actual->hotel->name] = $actual->hotel->name;
        }

        if (!empty($data))
            return $data;
    }

    public function TypeServices(Request $request)
    {
        $this->validate($request, [
            'type' => 'required|in:excursion,transport,catering,other',
            'id' => 'exists:service_packages,id'
        ]);

        $data = array();

        $services = Service::where('year_id', $this->year)->where('service_type', $request->type)->get();

        $packageServices = ServicePackage::find($request->id)->services;
        $exists = count($packageServices) > 0;

        foreach($services as $service) {
            if ($exists)
                $already = $packageServices->where('service_id', $service->id)->first();
            if (!$exists || !isset($already->id))
                $data = $data + array($service->id => $service->name);
        }

        if (!empty($data))
            return $data;
    }

    public function Save(Request $request)
    {
        $validate = [
            'name' => 'required|min:2|max:255',
            'min_pax' => 'required|integer',
            'max_pax' => 'required|integer',
            'price' => 'required|numeric|between:0,9999999.99',
            'available' => 'in:true',
            'board_price' => 'between:0,9999999.99',
            'description' => 'max:9999',
            'condition_desc' => 'max:9999'
        ];

        if ($request->has('id')) {
            $this->validate($request, $validate + array('id' => 'required|exists:service_packages,id'));
            $package = ServicePackage::find($request->id);
        } else {
            $this->validate($request, $validate);
            $package = new ServicePackage;
        }

        $package->name = $request->name;
        $package->min_pax = $request->min_pax;
        $package->max_pax = $request->max_pax;
        
        $package->price = $request->price;

        if ($request->all_pax_cabin == 'true') {
            $package->all_pax_cabin = true;
            if ($request->multiply == 'true')
                $package->multiply = true;
            else
                $package->multiply = false;
        } else {
            $package->all_pax_cabin = false;
            $package->multiply = false;
        }

        if ($request->available == 'true') {
            $package->available = true;
            $package->board_price = $request->board_price;
        }
        else {
            $package->available = false;
            $package->board_price = 0;
        }
        $package->description = $request->description;
        $package->condition_desc = $request->condition_desc;
        $package->year_id = $this->year;
        $package->save();

        if ($request->has('id'))
            return redirect()->route('packages_list');
        else
            return $package->id;
    }
    
    public function SaveService(Request $request, $id)
    {
        $this->validate($request, [
            'service_id' => 'required|exists:services,id'
        ]);

        if (Service::find($request->service_id)->year_id == $this->year) {
            $packageService = new PackageService;
            $packageService->service_id = $request->service_id;
            $packageService->service_package_id = $id;
            $packageService->save();

            return $packageService->id;
        }
    }

    public function Delete($id)
    {
        $package = ServicePackage::findOrFail($id);
        if ($return = \App\Http\Traits\HelperTrait::deleteCheckModel($package, ['journey_services' => 'Journey Services']))
            return response($return, 422);
        else
            $package->delete();
    }

    public function ServiceDelete($id)
    {
        PackageService::findOrFail($id)->delete();
    }
}
