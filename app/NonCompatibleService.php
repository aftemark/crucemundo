<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class NonCompatibleService extends Model
{
    public function day()
    {
        return $this->belongsTo('App\NonCompatible', 'non_compatible_id', 'id');
    }
    
    public function journey_service()
    {
        return $this->belongsTo('App\JourneyService', 'journey_service_id', 'id');
    }
}