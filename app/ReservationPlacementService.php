<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ReservationPlacementService extends Model
{
    public function placement()
    {
        return $this->belongsTo('App\ReservationPlacement', 'reservation_placement_id', 'id');
    }

    public function service()
    {
        return $this->belongsTo('App\JourneyService', 'journey_service_id', 'id');
    }
}
