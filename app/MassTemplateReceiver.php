<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class MassTemplateReceiver extends Model
{
    public $timestamps = false;

    public function client()
    {
        if ($this->client_id)
            return $this->belongsTo('App\Client', 'client_id', 'id');
        else
            return $this->belongsTo('App\ClientUser', 'client_user_id', 'id');
    }
}
