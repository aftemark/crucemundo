<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Document extends Model
{
    public function template()
    {
        return $this->belongsTo('App\CustomTemplate', 'custom_template_id', 'id');
    }
}
