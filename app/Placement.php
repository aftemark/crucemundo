<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Placement extends Model
{
    public function journey()
    {
        return $this->belongsTo('App\Journey', 'journey_id', 'id');
    }

    public function group()
    {
        return $this->belongsTo('App\SupplementGroup', 'supplement_group_id', 'id');
    }

    public function room()
    {
        return $this->belongsTo('App\Room', 'room_id', 'id');
    }

    public function cabin()
    {
        return $this->belongsTo('App\Cabin', 'cabin_id', 'id');
    }

    public function special_relations()
    {
        return $this->hasMany('App\SpecialRelation');
    }

    public function reservation_placement()
    {
        return $this->belongsTo('App\ReservationPlacement', 'reservation_placement_id', 'id');
    }

    public function cruise_type()
    {
        return $this->belongsTo('App\CruiseType', 'cruise_type_id', 'id');
    }

    protected static function boot()
    {
        parent::boot();

        static::deleting(function($placement) {
            $placement->special_relations()->delete();
        });
    }
}
