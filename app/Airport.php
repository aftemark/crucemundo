<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Sofa\Eloquence\Eloquence;

class Airport extends Model
{
    use Eloquence;

    protected $searchableColumns = ['id', 'name'];

    /*protected $searchable = [
        'columns' => [
            'airports.id' => 1,
            'airports.name' => 10,
            'cities.name' => 10
        ],
        'joins' => [
            'cities' => ['airports.city_id','cities.id'],
        ]
    ];*/

    public function city()
    {
        return $this->belongsTo('App\City', 'city_id', 'id');
    }

    public function pax()
    {
        return $this->hasMany('App\ReservationPax');
    }
}
