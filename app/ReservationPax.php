<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ReservationPax extends Model
{
    public function placement ()
    {
        return $this->belongsTo('App\ReservationPlacement', 'reservation_placement_id', 'id');
    }

    public function services()
    {
        return $this->hasMany('App\ReservationPaxService');
    }

    protected static function boot()
    {
        parent::boot();

        static::deleting(function($pax) {
            /*$placement = $pax->placement;

            $newTypeAmount = $placement[$pax->type . 's'] - 1;
            if ($pax->type == 'adult' && $newTypeAmount >= ($placement->childrens + $placement->infants)) {
                $placement[$pax->type . 's'] = $newTypeAmount;
                $placement->save();
            } else if ($pax->type == 'adult')
                return false;
            else {
                $placement[$pax->type . 's'] = $newTypeAmount;
                $placement->save();
            }*/

            $pax->services()->delete();
        });
    }
}
