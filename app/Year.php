<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Year extends Model
{
    public function journeys()
    {
        return $this->hasMany('App\Journey');
    }

    public function services()
    {
        return $this->hasMany('App\Service');
    }

    public function packages()
    {
        return $this->hasMany('App\ServicePackage');
    }
}
