<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Sofa\Eloquence\Eloquence;

class Country extends Model
{
    use Eloquence;

    protected $searchableColumns = ['id', 'name'];

    public function cities()
    {
        return $this->hasMany('App\City');
    }

    public function pax()
    {
        return $this->hasMany('App\ReservationPax');
    }

    public function clients()
    {
        return $this->hasMany('App\Client');
    }
}
