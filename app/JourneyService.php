<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class JourneyService extends Model
{
    public function service()
    {
        return $this->belongsTo('App\Service', 'service_id', 'id');
    }

    public function package()
    {
        return $this->belongsTo('App\ServicePackage', 'service_package_id', 'id');
    }

    public function special_relation()
    {
        return $this->hasMany('App\SpecialRelation');
    }

    public function non_service()
    {
        return $this->hasMany('App\NonCompatibleService');
    }

    public function notifications()
    {
        return $this->hasMany('App\Notification');
    }

    protected static function boot()
    {
        parent::boot();

        static::deleting(function($service) {
            foreach($service->special_relation() as $relation) {
                $relation->service_id = NULL;
                $relation->save();
            }
        });
    }
}
