<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Sofa\Eloquence\Eloquence;

class ServicePackage extends Model
{
    use Eloquence;

    protected $searchableColumns = ['name'];

    public function year()
    {
        return $this->belongsTo('App\Year', 'year_id', 'id');
    }

    public function services()
    {
        return $this->hasMany('App\PackageService');
    }

    public function journey_services()
    {
        return $this->hasMany('App\JourneyService');
    }

    protected static function boot() {
        parent::boot();

        static::deleting(function($package) {
            $package->services()->delete();
        });
    }
}