<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Deck extends Model
{
    public function ship ()
    {
        return $this->belongsTo('App\Ship');
    }

    public function cabins ()
    {
        return $this->hasMany('App\Cabin');
    }

    public function types()
    {
        return $this->hasMany('App\CruiseType');
    }

    public function files()
    {
        return $this->morphMany('App\UserFile', 'imageable');
    }

    protected static function boot() {
        parent::boot();

        static::deleting(function($deck) {
            $deck->cabins()->delete();
            $deck->files()->delete();
        });
    }
}
