<?php

namespace App;

use Illuminate\Foundation\Auth\User as Authenticatable;
use Zizaco\Entrust\Traits\EntrustUserTrait;
use Sofa\Eloquence\Eloquence;
use DB;

class User extends Authenticatable
{
    use Eloquence;
    use EntrustUserTrait;
    use Eloquence, EntrustUserTrait {
        EntrustUserTrait::save insteadof Eloquence;
    }

    protected $searchableColumns = ['name', 'status'];
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    public function client_user()
    {
        return $this->hasOne('App\ClientUser');
    }

    public function notifications()
    {
        return $this->hasMany('App\Notification');
    }

    public function detachAllRoles()
    {
        DB::table('role_user')->where('user_id', $this->id)->delete();

        return $this;
    }
}
