<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Sofa\Eloquence\Eloquence;

class Ship extends Model
{
    use Eloquence;

    protected $searchableColumns = ['id', 'name', 'rating'];

    /*protected $searchable = [
        'columns' => [
            'ships.id' => 10,
            'ships.name' => 10,
            'ships.rating' => 5
        ]
    ];*/

    public function alldecks()
    {
        return $this->hasMany('App\Deck');
    }

    public function services()
    {
        return $this->hasMany('App\Service');
    }

    public function percents()
    {
        return $this->hasMany('App\ClientPercent');
    }

    public function journeys()
    {
        return $this->hasMany('App\Journey');
    }

    protected static function boot() {
        parent::boot();

        static::deleting(function($ship) {
            foreach ($ship->alldecks as $deck) {
                $deck->cabins()->delete();
                $deck->delete();
            }
            $ship->percents()->delete();
        });
    }
}