<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PackageService extends Model
{
    public function servicePackage ()
    {
        return $this->belongsTo('App\ServicePackage', 'service_package_id', 'id');
    }

    public function service ()
    {
        return $this->belongsTo('App\Service', 'service_id', 'id');
    }
}
