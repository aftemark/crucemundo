<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Sofa\Eloquence\Eloquence;
use Illuminate\Database\Eloquent\SoftDeletes;

class Reservation extends Model
{
    use Eloquence;
    use SoftDeletes;

    protected $searchableColumns = ['journey.code', 'client.type', 'journey.itinerary.name', 'language.name', 'release_date', 'reservation_type'];
    protected $dates = ['deleted_at'];

    public function journey()
    {
        return $this->belongsTo('App\Journey', 'journey_id', 'id');
    }

    public function client()
    {
        return $this->belongsTo('App\Client', 'client_id', 'id');
    }

    public function client_user()
    {
        return $this->belongsTo('App\ClientUser', 'client_user_id', 'id');
    }

    public function year()
    {
        return $this->belongsTo('App\Year', 'year_id', 'id');
    }

    public function language()
    {
        return $this->belongsTo('App\Language', 'language_id', 'id');
    }

    public function placements()
    {
        return $this->hasMany('App\ReservationPlacement');
    }

    public function payments()
    {
        return $this->hasMany('App\ReservationPayment');
    }

    public function notifications()
    {
        return $this->hasMany('App\Notification');
    }

    public function fines()
    {
        return $this->hasMany('App\Fine');
    }
}
