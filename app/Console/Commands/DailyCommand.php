<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Journey;
use App\Reservation;
use Carbon\Carbon;
use App\Http\Traits\HelperTrait;

class DailyCommand extends Command
{
    use HelperTrait;
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'reservations:daily';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Reservations daily routine.';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $date30 = Carbon::now()->addDays(30)->toDateString();
        $date14 = Carbon::now()->addDays(14)->toDateString();
        foreach (Journey::all() as $journey) {
            if ($journey->release_date == $date30)
                foreach ($journey->reservations()->whereType('booking')->get() as $reservation) {
                    if ($reservation->with_names) {
                        $cruise_types = [];
                        foreach ($reservation->placements as $placement)
                            if (!isset($placement->assigned))
                                $cruise_types[$placement->cruise_type_id][] = $placement->id;

                        foreach ($reservation->journey->placements as $placement)
                            if (!$placement->reservation_placement_id && !empty($cruise_types[$placement->cruise_type_id])) {
                                $placement->reservation_placement_id = $cruise_types[$placement->cruise_type_id][0];
                                array_shift($cruise_types[$placement->cruise_type_id]);
                                $placement->save();
                            }

                        $this->notify(NULL, $reservation->id, 'action', 'Reservation was assigned - <a href="' . url('/' . $reservation->type . '/' . $reservation->id) . '">' . $reservation->code . '</a>');
                    }

                    if (!$reservation->paid)
                        $this->notify(NULL, $reservation->id, 'reminder', 'Reservation must be paid - <a href="' . url('/' . $reservation->type . '/' . $reservation->id) . '">' . $reservation->code . '</a>');
                }

            if ($journey->depart_date == $date14)
                foreach ($journey->reservations()->whereType('booking')->get() as $reservation)
                    $this->notify(NULL, $reservation->id, 'reminder', 'Reservation will be released in 14 days - <a href="' . url('/' . $reservation->type . '/' . $reservation->id) . '">' . $reservation->code . '</a>');

            if (Carbon::parse($journey->release_date)->isFuture()) {
                if (Carbon::now()->addDays($journey->remind)->toDateString() == $journey->release_date)
                    $this->notify(NULL, NULL, 'reminder', ucfirst($journey->type) . ' release, ' . date('d.m.Y', strtotime($journey->release_date)) . ' - <a href="' . url('/' . $journey->type . '/' . $journey->id) . '">' . $journey->code . '</a>');

                foreach ($journey->services()->where('display', true)->get() as $service)
                    if (Carbon::now()->addDays($service->remind)->toDateString() == $service->release_date) {
                        $service_type = isset($service->service_id) ? 'service' : 'package';
                        $this->notify(NULL, NULL, 'reminder', 'Service release, ' . $service[$service_type]->name . ', ' . date('d.m.Y', strtotime($journey->release_date)) . ' - <a href="' . url('/' . $journey->type . '/' . $journey->id) . '">' . $journey->code . '</a>');
                    }

                foreach ($journey->specials as $special)
                    if (Carbon::parse($special->date_end)->isTomorrow())
                        $this->notify(NULL, NULL, 'reminder', 'Special offer has expired, ' . date('d.m.Y', strtotime($special->date_end)) . ' - <a href="' . url('/' . $journey->type . '/' . $journey->id) . '">' . $journey->code . '</a>');
            }
        }

        foreach (Reservation::where('type', 'option')->where('till', Carbon::now()->toDateString())->get() as $option) {
            $url = '<a href="' . url('/' . $option->type . '/' . $option->id) . '">' . $option->code . '</a>';
            $this->notify(NULL, $option->id, 'update', 'Option was canceled (' . ucfirst($option->journey->type) . ') - ' . $url);
            $this->notify(NULL, $option->id, 'reminder', 'Option was canceled (' . ucfirst($option->journey->type) . ') - ' . $url);
            $this->notify(NULL, $option->id, 'update', 'E-mail was sent (Canceled ' . ucfirst($option->type) . ') - ' . $url, NULL, NULL, [
                'type' => 'cancellation_' . $option->type,
                'reservation' => $option
            ]);
            $option->delete();
        }
    }
}
