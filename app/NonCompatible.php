<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class NonCompatible extends Model
{
    public function journey()
    {
        return $this->belongsTo('App\Journey', 'journey_id', 'id');
    }

    public function services()
    {
        return $this->hasMany('App\NonCompatibleService');
    }

    protected static function boot()
    {
        parent::boot();

        static::deleting(function($day) {
            $day->services()->delete();
        });
    }
}