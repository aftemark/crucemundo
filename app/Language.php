<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Sofa\Eloquence\Eloquence;

class Language extends Model
{
    use Eloquence;

    protected $searchableColumns = ['id', 'name'];

    /*protected $searchable = [
        'columns' => [
            'languages.id' => 1,
            'languages.name' => 1
        ]
    ];*/

    public function clients()
    {
        return $this->hasMany('App\Client');
    }

    public function services()
    {
        return $this->hasMany('App\Service');
    }

    public function reservations()
    {
        return $this->hasMany('App\Reservation');
    }

}
