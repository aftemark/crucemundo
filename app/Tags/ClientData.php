<?php

namespace App\Tags;
use InvalidArgumentException;

class ClientData {

    private $model;

    public function __construct($model) {
        $this->model = $model;
    }

    public function __isset($name) {
        if (!isset($this->model->{$name}))
            $this->model->{$name} = 'undefined';

        return true;
    }

    public function __get($name) {
        return $this->model->{$name};
    }

    public function name() {
        return ($this->code ? $this->code : $this->name) . ($this->second_name ? ' ' . $this->second_name : '');
    }
}