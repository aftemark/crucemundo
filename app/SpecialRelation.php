<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SpecialRelation extends Model
{
    public function special()
    {
        return $this->belongsTo('App\Special', 'special_id', 'id');
    }

    public function placement()
    {
        return $this->belongsTo('App\Placement', 'placement_id', 'id');
    }

    public function service()
    {
        return $this->belongsTo('App\JourneyService', 'journey_service_id', 'id');
    }

    public function journey()
    {
        return $this->belongsTo('App\Journey', 'journey_id', 'id');
    }
}