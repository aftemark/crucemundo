<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ReservationPlacement extends Model
{
    public function placement()
    {
        return $this->belongsTo('App\Placement', 'placement_id', 'id');
    }

    public function reservation()
    {
        return $this->belongsTo('App\Reservation', 'reservation_id', 'id');
    }

    public function cruise_type()
    {
        return $this->belongsTo('App\CruiseType', 'cruise_type_id', 'id');
    }

    public function reservation_paxes()
    {
        return $this->hasMany('App\ReservationPax');
    }

    public function services()
    {
        return $this->hasMany('App\ReservationPlacementService');
    }

    public function notifications()
    {
        return $this->hasMany('App\Notification');
    }

    public function assigned()
    {
        return $this->hasOne('App\Placement');
    }

    protected static function boot()
    {
        parent::boot();

        static::deleting(function($placement) {
            foreach ($placement->reservation_paxes as $pax) {
                $pax->services()->delete();
                $pax->delete();
            }
            $placement->services()->delete();
        });
    }
}
