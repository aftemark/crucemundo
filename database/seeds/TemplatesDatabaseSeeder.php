<?php

use Illuminate\Database\Seeder;
use Carbon\Carbon;

class TemplatesDatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $types = [
            'mail' => ['new_booking', 'new_option', 'convert_reservation', 'invoice', 'invoice_flexible', 'cancellation_booking', 'cancellation_option', 'pax_details_update', 'overbooking'],
            'report' => [
                'Ваучер' => 'voucher',
                'Договор' => 'contract',
                'Счет на полную оплату' => 'full_payment',
                'Счет на депозит' => 'deposit',
                'Руминг-лист' => 'rooming_list',
                'Статистика по бронированию' => 'booking_stats',
                'Релизы' => 'releases',
                'Оплаты' => 'payments',
                'Сверка взаиморасчетов' => 'revise',
                'Статус продаж общий' => 'sales_status',
                'Статистика' => 'statistics'
            ]
        ];

        foreach ($types as $type => $custom_types)
            foreach ($custom_types as $custom_name => $custom_type)
                DB::table('custom_templates')->insert([
                    'type' => $type,
                    'custom_type' => $custom_type,
                    'name' => $type == 'mail' ? $custom_type : $custom_name,
                    'subject' => $type == 'mail' ? 'Subject' : NULL,
                    'text' => '',
                    'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
                    'updated_at' => Carbon::now()->format('Y-m-d H:i:s')
                ]);
    }
}
