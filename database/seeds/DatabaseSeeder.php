<?php

use Illuminate\Database\Seeder;
use Carbon\Carbon;
use Illuminate\Support\Facades\Config;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $modules = Config::get('constants.modules');
        foreach ($modules as $moduleName => $module) {
            $newRow = DB::table('roles')->insertGetId([
                'name' => 'edit_' . $moduleName,
                'display_name' => 'Edit ' . $module['name'],
                'description' => 'Edit ' . $module['name'],
                'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
                'updated_at' => Carbon::now()->format('Y-m-d H:i:s')
            ]);

            DB::table('role_user')->insert([
                'user_id' => '1',
                'role_id' => $newRow
            ]);

            $newRow = DB::table('roles')->insertGetId([
                'name' => 'show_' . $moduleName,
                'display_name' => 'Show ' . $module['name'],
                'description' => 'Show ' . $module['name'],
                'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
                'updated_at' => Carbon::now()->format('Y-m-d H:i:s')
            ]);

            DB::table('role_user')->insert([
                'user_id' => '1',
                'role_id' => $newRow
            ]);

            foreach ($module["children"] as $childName => $children) {
                $create_edit = isset($children['create']) && $children['create'] ? 'create' : 'edit';
                $this_name = $children;
                if (is_array($children)) {
                    foreach ($children['children'] as $children_children_name => $children_children) {
                        if ($children_children['type'] == 'edit') {
                            $newRow = DB::table('roles')->insertGetId([
                                'name' => $children_children_name . '_' . $childName,
                                'display_name' => 'Edit ' . $children_children['name'],
                                'description' => 'Edit ' . $children_children['name'],
                                'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
                                'updated_at' => Carbon::now()->format('Y-m-d H:i:s')
                            ]);

                            DB::table('role_user')->insert([
                                'user_id' => '1',
                                'role_id' => $newRow
                            ]);
                        } else if ($children_children['type'] == 'show') {
                            $newRow = DB::table('roles')->insertGetId([
                                'name' => 'show_' . $childName . '_' . $children_children_name,
                                'display_name' => 'Show ' . $children_children['name'],
                                'description' => 'Show ' . $children_children['name'],
                                'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
                                'updated_at' => Carbon::now()->format('Y-m-d H:i:s')
                            ]);

                            DB::table('role_user')->insert([
                                'user_id' => '1',
                                'role_id' => $newRow
                            ]);
                        }
                    }
                    $this_name = $children['name'];
                }
                $newRow = DB::table('roles')->insertGetId([
                    'name' => $create_edit . '_' . $childName,
                    'display_name' => ucfirst($create_edit) . ' ' . $this_name,
                    'description' => ucfirst($create_edit) . ' ' . $this_name,
                    'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
                    'updated_at' => Carbon::now()->format('Y-m-d H:i:s')
                ]);

                DB::table('role_user')->insert([
                    'user_id' => '1',
                    'role_id' => $newRow
                ]);

                $newRow = DB::table('roles')->insertGetId([
                    'name' => 'show_' . $childName,
                    'display_name' => 'Show ' . $this_name,
                    'description' => 'Show ' . $this_name,
                    'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
                    'updated_at' => Carbon::now()->format('Y-m-d H:i:s')
                ]);

                DB::table('role_user')->insert([
                    'user_id' => '1',
                    'role_id' => $newRow
                ]);
            }
        }
    }
}
