<?php

use Illuminate\Database\Seeder;
use Carbon\Carbon;

class OldDatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('users')->insert([
            'name' => 'admin',
            'email' => 'admin@admin.ru',
            'active' => true,
            'password' => bcrypt('123456'),
            'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at' => Carbon::now()->format('Y-m-d H:i:s')
        ]);

        DB::table('roles')->insert([
            'name' => 'edit_directories',
            'display_name' => 'Edit Directories',
            'description' => 'Edit Directories',
            'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at' => Carbon::now()->format('Y-m-d H:i:s')
        ]);
        DB::table('roles')->insert([
            'name' => 'show_directories',
            'display_name' => 'Show Directories',
            'description' => 'Show Directories',
            'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at' => Carbon::now()->format('Y-m-d H:i:s')
        ]);
        DB::table('roles')->insert([
            'name' => 'edit_administration',
            'display_name' => 'Edit Administration',
            'description' => 'Edit Administration',
            'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at' => Carbon::now()->format('Y-m-d H:i:s')
        ]);
        DB::table('roles')->insert([
            'name' => 'show_administration',
            'display_name' => 'Show Administration',
            'description' => 'Show Administration',
            'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at' => Carbon::now()->format('Y-m-d H:i:s')
        ]);
        DB::table('roles')->insert([
            'name' => 'edit_reservations',
            'display_name' => 'Edit Reservations',
            'description' => 'Edit Reservations',
            'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at' => Carbon::now()->format('Y-m-d H:i:s')
        ]);
        DB::table('roles')->insert([
            'name' => 'show_reservations',
            'display_name' => 'Show Reservations',
            'description' => 'Show Reservations',
            'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at' => Carbon::now()->format('Y-m-d H:i:s')
        ]);
        DB::table('roles')->insert([
            'name' => 'edit_notifications',
            'display_name' => 'Edit Notifications',
            'description' => 'Edit Notifications',
            'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at' => Carbon::now()->format('Y-m-d H:i:s')
        ]);
        DB::table('roles')->insert([
            'name' => 'show_notifications',
            'display_name' => 'Show Notifications',
            'description' => 'Show Notifications',
            'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at' => Carbon::now()->format('Y-m-d H:i:s')
        ]);

        DB::table('roles')->insert([
            'name' => 'edit_catalog',
            'display_name' => 'Edit Catalog',
            'description' => 'Edit Catalog',
            'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at' => Carbon::now()->format('Y-m-d H:i:s')
        ]);
        DB::table('roles')->insert([
            'name' => 'show_catalog',
            'display_name' => 'Show Catalog',
            'description' => 'Show Catalog',
            'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at' => Carbon::now()->format('Y-m-d H:i:s')
        ]);
        DB::table('roles')->insert([
            'name' => 'edit_cities',
            'display_name' => 'Edit Cities',
            'description' => 'Edit Catalog',
            'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at' => Carbon::now()->format('Y-m-d H:i:s')
        ]);
        DB::table('roles')->insert([
            'name' => 'show_cities',
            'display_name' => 'Show Cities',
            'description' => 'Show Cities',
            'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at' => Carbon::now()->format('Y-m-d H:i:s')
        ]);
        DB::table('roles')->insert([
            'name' => 'edit_ships',
            'display_name' => 'Edit Ships',
            'description' => 'Edit Ships',
            'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at' => Carbon::now()->format('Y-m-d H:i:s')
        ]);
        DB::table('roles')->insert([
            'name' => 'show_ships',
            'display_name' => 'Show Ships',
            'description' => 'Show Ships',
            'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at' => Carbon::now()->format('Y-m-d H:i:s')
        ]);
        DB::table('roles')->insert([
            'name' => 'edit_hotels',
            'display_name' => 'Edit Hotels',
            'description' => 'Edit Hotels',
            'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at' => Carbon::now()->format('Y-m-d H:i:s')
        ]);
        DB::table('roles')->insert([
            'name' => 'show_hotels',
            'display_name' => 'Show Hotels',
            'description' => 'Show Hotels',
            'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at' => Carbon::now()->format('Y-m-d H:i:s')
        ]);
        DB::table('roles')->insert([
            'name' => 'edit_users',
            'display_name' => 'Edit Users',
            'description' => 'Edit Users',
            'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at' => Carbon::now()->format('Y-m-d H:i:s')
        ]);
        DB::table('roles')->insert([
            'name' => 'show_users',
            'display_name' => 'Show Users',
            'description' => 'Show Users',
            'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at' => Carbon::now()->format('Y-m-d H:i:s')
        ]);
        DB::table('roles')->insert([
            'name' => 'edit_mailing',
            'display_name' => 'Edit Mailing',
            'description' => 'Edit Mailing',
            'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at' => Carbon::now()->format('Y-m-d H:i:s')
        ]);
        DB::table('roles')->insert([
            'name' => 'show_mailing',
            'display_name' => 'Show Mailing',
            'description' => 'Show Mailing',
            'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at' => Carbon::now()->format('Y-m-d H:i:s')
        ]);
        DB::table('roles')->insert([
            'name' => 'edit_services',
            'display_name' => 'Edit Services',
            'description' => 'Edit Services',
            'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at' => Carbon::now()->format('Y-m-d H:i:s')
        ]);
        DB::table('roles')->insert([
            'name' => 'show_services',
            'display_name' => 'Show Services',
            'description' => 'Show Services',
            'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at' => Carbon::now()->format('Y-m-d H:i:s')
        ]);
        DB::table('roles')->insert([
            'name' => 'edit_packages',
            'display_name' => 'Edit Packages',
            'description' => 'Edit Packages',
            'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at' => Carbon::now()->format('Y-m-d H:i:s')
        ]);
        DB::table('roles')->insert([
            'name' => 'show_packages',
            'display_name' => 'Show Packages',
            'description' => 'Show Packages',
            'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at' => Carbon::now()->format('Y-m-d H:i:s')
        ]);
        DB::table('roles')->insert([
            'name' => 'edit_itineraries',
            'display_name' => 'Edit Itineraries',
            'description' => 'Edit Itineraries',
            'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at' => Carbon::now()->format('Y-m-d H:i:s')
        ]);
        DB::table('roles')->insert([
            'name' => 'show_itineraries',
            'display_name' => 'Show Itineraries',
            'description' => 'Show Itineraries',
            'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at' => Carbon::now()->format('Y-m-d H:i:s')
        ]);
        DB::table('roles')->insert([
            'name' => 'edit_b2c',
            'display_name' => 'Edit B2C Clients',
            'description' => 'Edit B2C Clients',
            'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at' => Carbon::now()->format('Y-m-d H:i:s')
        ]);
        DB::table('roles')->insert([
            'name' => 'show_b2c',
            'display_name' => 'Show B2C Clients',
            'description' => 'Show B2C Clients',
            'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at' => Carbon::now()->format('Y-m-d H:i:s')
        ]);
        DB::table('roles')->insert([
            'name' => 'edit_b2b',
            'display_name' => 'Edit B2B Clients',
            'description' => 'Edit B2B Clients',
            'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at' => Carbon::now()->format('Y-m-d H:i:s')
        ]);
        DB::table('roles')->insert([
            'name' => 'show_b2b',
            'display_name' => 'Show B2B Clients',
            'description' => 'Show B2B Clients',
            'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at' => Carbon::now()->format('Y-m-d H:i:s')
        ]);
        DB::table('roles')->insert([
            'name' => 'edit_year',
            'display_name' => 'Edit Years',
            'description' => 'Edit Years',
            'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at' => Carbon::now()->format('Y-m-d H:i:s')
        ]);
        DB::table('roles')->insert([
            'name' => 'show_year',
            'display_name' => 'Show Archive Years',
            'description' => 'Show Archive Years',
            'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at' => Carbon::now()->format('Y-m-d H:i:s')
        ]);
        DB::table('roles')->insert([
            'name' => 'edit_cruises',
            'display_name' => 'Edit Cruises',
            'description' => 'Edit Cruises',
            'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at' => Carbon::now()->format('Y-m-d H:i:s')
        ]);
        DB::table('roles')->insert([
            'name' => 'show_cruises',
            'display_name' => 'Show Cruises',
            'description' => 'Show Cruises',
            'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at' => Carbon::now()->format('Y-m-d H:i:s')
        ]);
        DB::table('roles')->insert([
            'name' => 'edit_tours',
            'display_name' => 'Edit Tours',
            'description' => 'Edit Tours',
            'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at' => Carbon::now()->format('Y-m-d H:i:s')
        ]);
        DB::table('roles')->insert([
            'name' => 'show_tours',
            'display_name' => 'Show Tours',
            'description' => 'Show Tours',
            'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at' => Carbon::now()->format('Y-m-d H:i:s')
        ]);
        DB::table('roles')->insert([
            'name' => 'edit_cruises_implementation',
            'display_name' => 'Edit Cruises Implementation tab',
            'description' => 'Edit Cruises Implementation tab',
            'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at' => Carbon::now()->format('Y-m-d H:i:s')
        ]);
        DB::table('roles')->insert([
            'name' => 'show_cruises_implementation',
            'display_name' => 'Show Cruises Implementation tab',
            'description' => 'Show Cruises Implementation tab',
            'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at' => Carbon::now()->format('Y-m-d H:i:s')
        ]);
        DB::table('roles')->insert([
            'name' => 'edit_cruises_reservations',
            'display_name' => 'Edit Cruises Reservations tab',
            'description' => 'Edit Cruises Reservations tab',
            'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at' => Carbon::now()->format('Y-m-d H:i:s')
        ]);
        DB::table('roles')->insert([
            'name' => 'show_cruises_reservations',
            'display_name' => 'Show Cruises Reservations tab',
            'description' => 'Show Cruises Reservations tab',
            'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at' => Carbon::now()->format('Y-m-d H:i:s')
        ]);
        DB::table('roles')->insert([
            'name' => 'edit_tours_implementation',
            'display_name' => 'Edit Tour Implementation tab',
            'description' => 'Edit Tour Implementation tab',
            'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at' => Carbon::now()->format('Y-m-d H:i:s')
        ]);
        DB::table('roles')->insert([
            'name' => 'show_tours_implementation',
            'display_name' => 'Show Tour Implementation tab',
            'description' => 'Show Tour Implementation tab',
            'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at' => Carbon::now()->format('Y-m-d H:i:s')
        ]);
        DB::table('roles')->insert([
            'name' => 'edit_tours_reservations',
            'display_name' => 'Edit Tour Reservations tab',
            'description' => 'Edit Tour Reservations tab',
            'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at' => Carbon::now()->format('Y-m-d H:i:s')
        ]);
        DB::table('roles')->insert([
            'name' => 'show_tours_reservations',
            'display_name' => 'Show Tour Reservations tab',
            'description' => 'Show Tour Reservations tab',
            'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at' => Carbon::now()->format('Y-m-d H:i:s')
        ]);
        DB::table('roles')->insert([
            'name' => 'edit_tour_options',
            'display_name' => 'Edit Tour Options',
            'description' => 'Edit Tour Options',
            'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at' => Carbon::now()->format('Y-m-d H:i:s')
        ]);
        DB::table('roles')->insert([
            'name' => 'show_tour_options',
            'display_name' => 'Edit Tour Options',
            'description' => 'Edit Tour Options',
            'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at' => Carbon::now()->format('Y-m-d H:i:s')
        ]);
        DB::table('roles')->insert([
            'name' => 'edit_cruise_options',
            'display_name' => 'Edit Cruise Options',
            'description' => 'Edit Cruise Options',
            'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at' => Carbon::now()->format('Y-m-d H:i:s')
        ]);
        DB::table('roles')->insert([
            'name' => 'show_cruise_options',
            'display_name' => 'Edit Cruise Options',
            'description' => 'Edit Cruise Options',
            'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at' => Carbon::now()->format('Y-m-d H:i:s')
        ]);
        DB::table('roles')->insert([
            'name' => 'edit_tour_inquiries',
            'display_name' => 'Edit Tour Inquiries',
            'description' => 'Edit Tour Inquiries',
            'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at' => Carbon::now()->format('Y-m-d H:i:s')
        ]);
        DB::table('roles')->insert([
            'name' => 'show_tour_inquiries',
            'display_name' => 'Edit Tour Inquiries',
            'description' => 'Edit Tour Inquiries',
            'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at' => Carbon::now()->format('Y-m-d H:i:s')
        ]);
        DB::table('roles')->insert([
            'name' => 'edit_cruise_inquiries',
            'display_name' => 'Edit Cruise Inquiries',
            'description' => 'Edit Cruise Inquiries',
            'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at' => Carbon::now()->format('Y-m-d H:i:s')
        ]);
        DB::table('roles')->insert([
            'name' => 'show_cruise_inquiries',
            'display_name' => 'Edit Cruise Inquiries',
            'description' => 'Edit Cruise Inquiries',
            'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at' => Carbon::now()->format('Y-m-d H:i:s')
        ]);
        DB::table('roles')->insert([
            'name' => 'edit_tour_bookings',
            'display_name' => 'Edit Tour Bookings',
            'description' => 'Edit Tour Bookings',
            'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at' => Carbon::now()->format('Y-m-d H:i:s')
        ]);
        DB::table('roles')->insert([
            'name' => 'show_tour_bookings',
            'display_name' => 'Edit Tour Bookings',
            'description' => 'Edit Tour Bookings',
            'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at' => Carbon::now()->format('Y-m-d H:i:s')
        ]);
        DB::table('roles')->insert([
            'name' => 'edit_cruise_bookings',
            'display_name' => 'Edit Cruise Bookings',
            'description' => 'Edit Cruise Bookings',
            'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at' => Carbon::now()->format('Y-m-d H:i:s')
        ]);
        DB::table('roles')->insert([
            'name' => 'show_cruise_bookings',
            'display_name' => 'Edit Cruise Bookings',
            'description' => 'Show Cruise Bookings',
            'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at' => Carbon::now()->format('Y-m-d H:i:s')
        ]);

        DB::table('roles')->insert([
            'name' => 'edit_notifications_actions',
            'display_name' => 'Edit Notifications Actions',
            'description' => 'Edit Notifications Actions',
            'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at' => Carbon::now()->format('Y-m-d H:i:s')
        ]);
        DB::table('roles')->insert([
            'name' => 'show_notifications_actions',
            'display_name' => 'Show Notifications Actions',
            'description' => 'Show Notifications Actions',
            'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at' => Carbon::now()->format('Y-m-d H:i:s')
        ]);

        DB::table('role_user')->insert([
            'user_id' => '1',
            'role_id' => '1'
        ]);
        DB::table('role_user')->insert([
            'user_id' => '1',
            'role_id' => '2'
        ]);
        DB::table('role_user')->insert([
            'user_id' => '1',
            'role_id' => '3'
        ]);
        DB::table('role_user')->insert([
            'user_id' => '1',
            'role_id' => '4'
        ]);
        DB::table('role_user')->insert([
            'user_id' => '1',
            'role_id' => '5'
        ]);
        DB::table('role_user')->insert([
            'user_id' => '1',
            'role_id' => '6'
        ]);
        DB::table('role_user')->insert([
            'user_id' => '1',
            'role_id' => '7'
        ]);
        DB::table('role_user')->insert([
            'user_id' => '1',
            'role_id' => '8'
        ]);
        DB::table('role_user')->insert([
            'user_id' => '1',
            'role_id' => '9'
        ]);
        DB::table('role_user')->insert([
            'user_id' => '1',
            'role_id' => '10'
        ]);
        DB::table('role_user')->insert([
            'user_id' => '1',
            'role_id' => '11'
        ]);
        DB::table('role_user')->insert([
            'user_id' => '1',
            'role_id' => '12'
        ]);
        DB::table('role_user')->insert([
            'user_id' => '1',
            'role_id' => '13'
        ]);
        DB::table('role_user')->insert([
            'user_id' => '1',
            'role_id' => '14'
        ]);
        DB::table('role_user')->insert([
            'user_id' => '1',
            'role_id' => '15'
        ]);
        DB::table('role_user')->insert([
            'user_id' => '1',
            'role_id' => '16'
        ]);
        DB::table('role_user')->insert([
            'user_id' => '1',
            'role_id' => '17'
        ]);
        DB::table('role_user')->insert([
            'user_id' => '1',
            'role_id' => '18'
        ]);
        DB::table('role_user')->insert([
            'user_id' => '1',
            'role_id' => '19'
        ]);
        DB::table('role_user')->insert([
            'user_id' => '1',
            'role_id' => '20'
        ]);
        DB::table('role_user')->insert([
            'user_id' => '1',
            'role_id' => '21'
        ]);
        DB::table('role_user')->insert([
            'user_id' => '1',
            'role_id' => '22'
        ]);
        DB::table('role_user')->insert([
            'user_id' => '1',
            'role_id' => '23'
        ]);
        DB::table('role_user')->insert([
            'user_id' => '1',
            'role_id' => '24'
        ]);
        DB::table('role_user')->insert([
            'user_id' => '1',
            'role_id' => '25'
        ]);
        DB::table('role_user')->insert([
            'user_id' => '1',
            'role_id' => '26'
        ]);
        DB::table('role_user')->insert([
            'user_id' => '1',
            'role_id' => '27'
        ]);
        DB::table('role_user')->insert([
            'user_id' => '1',
            'role_id' => '28'
        ]);
        DB::table('role_user')->insert([
            'user_id' => '1',
            'role_id' => '29'
        ]);
        DB::table('role_user')->insert([
            'user_id' => '1',
            'role_id' => '30'
        ]);

        DB::table('years')->insert([
            'year' => '2015',
            'status' => 'archive',
            'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at' => Carbon::now()->format('Y-m-d H:i:s')
        ]);
        DB::table('years')->insert([
            'year' => '2016',
            'status' => 'active',
            'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at' => Carbon::now()->format('Y-m-d H:i:s')
        ]);

        DB::table('cities')->insert([
            'country_id' => 1,
            'name' => 'Saint-Petersburg',
            'description' => 'Saint-Petersburg desc',
            'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at' => Carbon::now()->format('Y-m-d H:i:s')
        ]);

        DB::table('cities')->insert([
            'country_id' => 2,
            'name' => 'Odessa',
            'description' => 'Odessa desc',
            'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at' => Carbon::now()->format('Y-m-d H:i:s')
        ]);

        DB::table('languages')->insert([
            'name' => 'Russian',
            'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at' => Carbon::now()->format('Y-m-d H:i:s')
        ]);
        DB::table('languages')->insert([
            'name' => 'English',
            'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at' => Carbon::now()->format('Y-m-d H:i:s')
        ]);

        DB::table('placement_types')->insert([
            'name' => 'Economy',
            'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at' => Carbon::now()->format('Y-m-d H:i:s')
        ]);
        DB::table('placement_types')->insert([
            'name' => 'Business',
            'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at' => Carbon::now()->format('Y-m-d H:i:s')
        ]);
        DB::table('placement_types')->insert([
            'name' => 'VIP',
            'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at' => Carbon::now()->format('Y-m-d H:i:s')
        ]);

        DB::table('type_categories')->insert([
            'name' => 'C',
            'typename' => 'ship',
            'placement_type_id' => 1,
            'pax_amount' => 2,
            'balcony' => 0,
            'bed' => 1,
            'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at' => Carbon::now()->format('Y-m-d H:i:s')
        ]);
        DB::table('type_categories')->insert([
            'name' => 'B+',
            'typename' => 'ship',
            'placement_type_id' => 2,
            'pax_amount' => 3,
            'balcony' => 1,
            'bed' => 1,
            'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at' => Carbon::now()->format('Y-m-d H:i:s')
        ]);
        DB::table('type_categories')->insert([
            'name' => 'A+',
            'typename' => 'hotel',
            'placement_type_id' => 3,
            'pax_amount' => 3,
            'balcony' => 1,
            'bed' => 1,
            'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at' => Carbon::now()->format('Y-m-d H:i:s')
        ]);

        DB::table('airports')->insert([
            'name' => 'Пулково',
            'city_id' => 1,
            'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at' => Carbon::now()->format('Y-m-d H:i:s')
        ]);
        DB::table('airports')->insert([
            'name' => 'Аэропорт',
            'city_id' => 2,
            'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at' => Carbon::now()->format('Y-m-d H:i:s')
        ]);

        DB::table('itineraries')->insert([
            'name' => 'test route',
            'nights' => 2,
            'from_to' => 'Odessa - Saint Petersburg',
            'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at' => Carbon::now()->format('Y-m-d H:i:s')
        ]);
        DB::table('itinerary_cities')->insert([
            'itinerary_id' => 1,
            'city_id' => 1,
            'day' => 1,
            'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at' => Carbon::now()->format('Y-m-d H:i:s')
        ]);
        DB::table('itinerary_cities')->insert([
            'itinerary_id' => 1,
            'city_id' => 1,
            'day' => 2,
            'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at' => Carbon::now()->format('Y-m-d H:i:s')
        ]);
        DB::table('itinerary_cities')->insert([
            'itinerary_id' => 1,
            'city_id' => 2,
            'day' => 2,
            'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at' => Carbon::now()->format('Y-m-d H:i:s')
        ]);
        DB::table('itinerary_cities')->insert([
            'itinerary_id' => 1,
            'city_id' => 2,
            'day' => 3,
            'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at' => Carbon::now()->format('Y-m-d H:i:s')
        ]);

        /*DB::table('permissions')->insert([
            'name' => 'view',
            'display_name' => 2,
            'description' => 2,
            'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at' => Carbon::now()->format('Y-m-d H:i:s')
        ]);*/
    }
}
