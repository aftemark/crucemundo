<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateReservationPlacementsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('reservation_placements', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('reservation_id')->unsigned();
            $table->integer('cruise_type_id')->unsigned()->nullable();
            $table->integer('placement_id')->unsigned()->nullable();
            $table->integer('adults')->default(0);
            $table->integer('childrens')->default(0);
            $table->integer('infants')->default(0);
            $table->boolean('bed')->default(false);
            $table->timestamps();

            $table->foreign('reservation_id')->references('id')->on('reservations')
                ->onUpdate('cascade')->onDelete('cascade');
            $table->foreign('cruise_type_id')->references('id')->on('cruise_types')
                ->onUpdate('cascade')->onDelete('cascade');
            $table->foreign('placement_id')->references('id')->on('placements')
                ->onUpdate('cascade')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('reservation_placements');
    }
}
