<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSpecialsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('specials', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('journey_id')->unsigned();
            $table->date('date_start');
            $table->date('date_end');
            $table->enum('type', array('discount', 'free', 'exception'));
            $table->enum('offer_type', array('percent', 'eur', 'new', 'free_services', 'free_packages', 'no_single', 'no_port', 'free_extensions'));
            $table->decimal('offer_amount', 9, 2)->nullable();
            $table->enum('special_condition', array('group', 'fit', 'both'))->nullable();
            $table->boolean('previous');
            $table->text('description')->nullable();
            $table->timestamps();

            $table->foreign('journey_id')->references('id')->on('journeys')
                ->onUpdate('cascade')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('specials');
    }
}
