<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSpecialRelationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('special_relations', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('special_id')->unsigned();
            $table->integer('placement_id')->unsigned()->nullable();
            $table->integer('journey_service_id')->unsigned()->nullable();
            $table->integer('journey_id')->unsigned()->nullable();
            $table->timestamps();

            $table->unique(array('special_id', 'placement_id'));
            $table->unique(array('special_id', 'journey_service_id'));
            $table->unique(array('special_id', 'journey_id'));
            $table->foreign('special_id')->references('id')->on('specials')
                ->onUpdate('cascade')->onDelete('cascade');
            $table->foreign('placement_id')->references('id')->on('placements')
                ->onUpdate('cascade')->onDelete('cascade');
            $table->foreign('journey_service_id')->references('id')->on('journey_services')
                ->onUpdate('cascade')->onDelete('cascade');
            $table->foreign('journey_id')->references('id')->on('journeys')
                ->onUpdate('cascade')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('special_relations');
    }
}
