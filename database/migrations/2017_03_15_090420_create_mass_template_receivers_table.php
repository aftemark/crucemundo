<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMassTemplateReceiversTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('mass_template_receivers', function (Blueprint $table) {
            $table->integer('mass_template_id')->unsigned();
            $table->integer('client_id')->unsigned()->nullable();
            $table->integer('client_user_id')->unsigned()->nullable();

            $table->unique(array('mass_template_id', 'client_id'));
            $table->unique(array('mass_template_id', 'client_user_id'));
            $table->foreign('mass_template_id')->references('id')->on('mass_templates')
                ->onUpdate('cascade')->onDelete('cascade');
            $table->foreign('client_id')->references('id')->on('clients')
                ->onUpdate('cascade')->onDelete('cascade');
            $table->foreign('client_user_id')->references('id')->on('client_users')
                ->onUpdate('cascade')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('mass_template_receivers');
    }
}
