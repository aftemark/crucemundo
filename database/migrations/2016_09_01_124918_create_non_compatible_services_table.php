<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateNonCompatibleServicesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('non_compatible_services', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('non_compatible_id')->unsigned();
            $table->integer('journey_service_id')->unsigned();
            $table->unique(array('non_compatible_id', 'journey_service_id'));
            $table->timestamps();
            
            $table->foreign('non_compatible_id')->references('id')->on('non_compatibles')
                ->onUpdate('cascade')->onDelete('cascade');
            $table->foreign('journey_service_id')->references('id')->on('journey_services')
                ->onUpdate('cascade')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('non_compatible_services');
    }
}
