<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateReservationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('reservations', function (Blueprint $table) {
            $table->increments('id');
            $table->string('code');
            $table->enum('type', array('option', 'booking', 'inquiry'));
            $table->enum('reservation_type', array('fit', 'group'))->default('fit');
            $table->integer('journey_id')->unsigned();
            $table->integer('client_id')->unsigned();
            $table->integer('client_user_id')->unsigned()->nullable();
            $table->date('till')->nullable();
            $table->boolean('send_email')->default(false);
            $table->boolean('with_names')->default(false);
            $table->integer('pax_number')->nullable();
            $table->integer('placements_number')->nullable();
            $table->integer('language_id')->unsigned()->nullable();
            $table->decimal('offer_amount', 9, 2)->nullable();
            $table->text('description')->nullable();
            $table->boolean('placements_tab')->default(false);
            $table->boolean('services_tab')->default(false);
            $table->enum('payments_status', array('deposit_invoiced', 'deposit_paid', 'total_invoiced', 'total_paid'))->nullable();
            $table->enum('overbooking_status', array('60', '80'))->nullable();
            $table->boolean('paid')->default(false);
            $table->integer('year_id')->unsigned();
            $table->timestamp('deleted_at');
            $table->timestamps();

            $table->foreign('journey_id')->references('id')->on('journeys')
                ->onUpdate('cascade')->onDelete('cascade');
            $table->foreign('client_id')->references('id')->on('clients')
                ->onUpdate('cascade')->onDelete('cascade');
            $table->foreign('client_user_id')->references('id')->on('client_users')
                ->onUpdate('cascade')->onDelete('cascade');
            $table->foreign('language_id')->references('id')->on('languages')
                ->onUpdate('cascade')->onDelete('cascade');
            $table->foreign('year_id')->references('id')->on('years')
                ->onUpdate('cascade')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('reservations');
    }
}
