<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateShipsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('ships', function (Blueprint $table) {
            $table->increments('id');
            $table->string('code');
            $table->string('name');
            $table->integer('rating');
            $table->integer('year_built');
            $table->integer('year_last');
            $table->float('width');
            $table->float('length');
            $table->float('draft');
            $table->integer('pax_amount');
            $table->integer('decks');
            $table->text('description');
            $table->text('services_desc');
            $table->text('add_desc');
            $table->integer('add_bed')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('ships');
    }
}
