<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateServicesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('services', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('language_id')->unsigned()->nullable();
            $table->integer('ship_id')->unsigned()->nullable();
            $table->integer('hotel_id')->unsigned()->nullable();
            $table->integer('city_id')->unsigned()->nullable();
            $table->enum('service_type', array('excursion', 'transport', 'catering', 'other'));
            $table->string('name');
            $table->integer('min_pax');
            $table->integer('max_pax');
            $table->string('other')->nullable();
            $table->boolean('all_pax_cabin')->default(false);
            $table->boolean('multiply')->default(false);
            $table->decimal('price', 9, 2);
            $table->decimal('board_price', 9, 2);
            $table->text('description');
            $table->text('condition_desc');
            $table->integer('year_id')->unsigned();
            $table->timestamps();

            $table->foreign('language_id')->references('id')->on('languages')
                ->onUpdate('cascade')->onDelete('cascade');
            $table->foreign('ship_id')->references('id')->on('ships')
                ->onUpdate('cascade')->onDelete('cascade');
            $table->foreign('hotel_id')->references('id')->on('hotels')
                ->onUpdate('cascade')->onDelete('cascade');
            $table->foreign('city_id')->references('id')->on('cities')
                ->onUpdate('cascade')->onDelete('cascade');
            $table->foreign('year_id')->references('id')->on('years')
                ->onUpdate('cascade')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('services');
    }
}
