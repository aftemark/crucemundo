<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateJourneyServicesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('journey_services', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('journey_id')->unsigned();
            $table->integer('itinerary_city_id')->unsigned()->nullable();
            $table->integer('service_id')->unsigned()->nullable();
            $table->integer('service_package_id')->unsigned()->nullable();
            $table->boolean('included')->default(false);
            $table->boolean('optional')->default(false);
            $table->boolean('display')->default(false);
            $table->date('release_date')->nullable();
            $table->integer('remind')->nullable();
            $table->timestamps();

            $table->unique(array('journey_id', 'itinerary_city_id', 'service_id'));
            $table->unique(array('journey_id', 'itinerary_city_id', 'service_package_id'));
            $table->foreign('journey_id')->references('id')->on('journeys')
                ->onUpdate('cascade')->onDelete('cascade');
            $table->foreign('itinerary_city_id')->references('id')->on('itineraries')
                ->onUpdate('cascade')->onDelete('cascade');
            $table->foreign('service_id')->references('id')->on('services')
                ->onUpdate('cascade')->onDelete('cascade');
            $table->foreign('service_package_id')->references('id')->on('service_packages')
                ->onUpdate('cascade')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('journey_services');
    }
}
