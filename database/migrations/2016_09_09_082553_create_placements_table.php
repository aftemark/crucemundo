<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePlacementsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('placements', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('journey_id')->unsigned();
            $table->integer('cabin_id')->unsigned()->nullable();
            $table->integer('room_id')->unsigned()->nullable();
            $table->boolean('display');
            $table->decimal('price_net', 9, 2)->nullable();
            $table->decimal('price_rec', 9, 2)->nullable();
            $table->integer('amount');
            $table->text('note');
            $table->integer('supplement_group_id')->unsigned()->nullable();
            $table->integer('reservation_placement_id')->unsigned()->nullable();
            $table->integer('cruise_type_id')->unsigned()->nullable();
            $table->timestamps();

            $table->unique(array('journey_id', 'cabin_id'));
            $table->unique(array('journey_id', 'room_id'));
            $table->unique(array('journey_id', 'reservation_placement_id'));
            $table->foreign('journey_id')->references('id')->on('journeys')
                ->onUpdate('cascade')->onDelete('cascade');
            $table->foreign('cabin_id')->references('id')->on('cabins')
                ->onUpdate('cascade')->onDelete('cascade');
            $table->foreign('room_id')->references('id')->on('rooms')
                ->onUpdate('cascade')->onDelete('cascade');
            $table->foreign('supplement_group_id')->references('id')->on('supplement_groups')
                ->onUpdate('cascade')->onDelete('cascade');
            $table->foreign('reservation_placement_id')->references('id')->on('reservation_placements')
                ->onUpdate('cascade')->onDelete('cascade');
            $table->foreign('cruise_type_id')->references('id')->on('cruise_types')
                ->onUpdate('cascade')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('placements');
    }
}
