<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateServicePackagesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('service_packages', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            $table->integer('min_pax');
            $table->integer('max_pax');
            $table->string('other')->nullable();
            $table->boolean('all_pax_cabin')->default(false);
            $table->boolean('multiply')->default(false);
            $table->decimal('price', 9, 2);
            $table->decimal('board_price', 9, 2);
            $table->boolean('available')->default(false);
            $table->text('description');
            $table->text('condition_desc');
            $table->integer('year_id')->unsigned();
            $table->timestamps();

            $table->foreign('year_id')->references('id')->on('years')
                ->onUpdate('cascade')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('service_packages');
    }
}
