<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateNotificationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('notifications', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('user_id')->unsigned()->nullable();
            $table->integer('reservation_id')->unsigned()->nullable();
            $table->integer('journey_service_id')->unsigned()->nullable();
            $table->enum('type', array('action', 'update', 'reminder'));
            $table->enum('reminder_type', array('service_min', 'service_max'))->nullable();
            $table->boolean('completed')->default(false);
            $table->text('text');
            $table->timestamps();

            $table->foreign('user_id')->references('id')->on('users')
                ->onUpdate('cascade')->onDelete('cascade');
            $table->foreign('reservation_id')->references('id')->on('reservations')
                ->onUpdate('cascade')->onDelete('cascade');
            $table->foreign('journey_service_id')->references('id')->on('journey_services')
                ->onUpdate('cascade')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('notifications');
    }
}
