<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateJourneysTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('journeys', function (Blueprint $table) {
            $table->increments('id');
            $table->enum('type', array('tour', 'cruise'));
            $table->string('code');
            $table->integer('ship_id')->unsigned()->nullable();
            $table->integer('hotel_id')->unsigned()->nullable();
            $table->enum('season', array('high', 'middle', 'low'));
            $table->enum('extension_type', array('pre', 'post'))->nullable();
            $table->date('depart_date')->nullable();
            $table->date('release_date')->nullable();
            $table->integer('remind')->nullable();
            $table->text('description');
            $table->integer('itinerary_id')->unsigned()->nullable();
            $table->decimal('port_tax', 9, 2);
            $table->integer('booking_percent')->nullable();
            $table->integer('full_pay')->nullable();
            $table->integer('journey_id')->unsigned()->nullable();
            $table->integer('year_id')->unsigned();
            $table->timestamps();

            $table->foreign('ship_id')->references('id')->on('ships')
                ->onUpdate('cascade')->onDelete('cascade');
            $table->foreign('hotel_id')->references('id')->on('hotels')
                ->onUpdate('cascade')->onDelete('cascade');
            $table->foreign('itinerary_id')->references('id')->on('itineraries')
                ->onUpdate('cascade')->onDelete('cascade');
            $table->foreign('journey_id')->references('id')->on('journeys')
                ->onUpdate('cascade')->onDelete('cascade');
            $table->foreign('year_id')->references('id')->on('years')
                ->onUpdate('cascade')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('journeys');
    }
}
