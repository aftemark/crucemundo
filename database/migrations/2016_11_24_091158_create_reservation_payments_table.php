<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateReservationPaymentsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('reservation_payments', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('reservation_id')->unsigned();
            $table->date('date');
            $table->enum('type', array('deposit', 'full', '100'));
            $table->decimal('amount', 11, 2);
            $table->boolean('email')->default(false);
            $table->timestamps();

            $table->foreign('reservation_id')->references('id')->on('reservations')
                ->onUpdate('cascade')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('reservation_payments');
    }
}
