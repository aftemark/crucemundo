<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateReservationPaxesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('reservation_paxes', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('reservation_placement_id')->unsigned();
            $table->enum('type', array('adult', 'children', 'infant'));
            $table->string('name')->nullable();
            $table->string('surname')->nullable();
            $table->boolean('sex')->nullable();
            $table->date('birth')->nullable();
            $table->string('document_type')->nullable();
            $table->string('document_number')->nullable();
            $table->integer('country_id')->unsigned()->nullable();
            $table->date('validity')->nullable();
            $table->date('arrival')->nullable();
            $table->integer('arrival_airport_id')->unsigned()->nullable();
            $table->string('arrival_number')->nullable();
            $table->date('departure')->nullable();
            $table->integer('departure_airport_id')->unsigned()->nullable();
            $table->string('departure_number')->nullable();
            $table->boolean('bed')->default(false);
            $table->boolean('filled')->default(false);
            $table->timestamps();

            $table->foreign('reservation_placement_id')->references('id')->on('reservation_placements')
                ->onUpdate('cascade')->onDelete('cascade');
            $table->foreign('country_id')->references('id')->on('countries')
                ->onUpdate('cascade')->onDelete('cascade');
            $table->foreign('arrival_airport_id')->references('id')->on('airports')
                ->onUpdate('cascade')->onDelete('cascade');
            $table->foreign('departure_airport_id')->references('id')->on('airports')
                ->onUpdate('cascade')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('reservation_paxes');
    }
}
