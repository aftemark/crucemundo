<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCabinsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('cabins', function (Blueprint $table) {
            $table->increments('id');
            $table->string('code');
            $table->integer('num');
            $table->integer('deck_id')->unsigned();
            $table->integer('type_category_id')->unsigned();
            $table->float('area');
            $table->text('description');
            $table->timestamps();

            $table->unique(array('deck_id', 'num'));
            $table->foreign('deck_id')->references('id')->on('decks')
                ->onUpdate('cascade')->onDelete('cascade');
            $table->foreign('type_category_id')->references('id')->on('type_categories')
                ->onUpdate('cascade')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('cabins');
    }
}
