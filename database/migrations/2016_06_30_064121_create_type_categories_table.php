<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTypeCategoriesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('type_categories', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            $table->enum('typename', array('hotel', 'ship'));
            $table->integer('placement_type_id')->unsigned();
            $table->integer('pax_amount');
            $table->boolean('balcony');
            $table->boolean('bed');
            $table->timestamps();

            $table->foreign('placement_type_id')->references('id')->on('placement_types')
                ->onUpdate('cascade')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('type_categories');
    }
}
