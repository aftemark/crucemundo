<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCustomTemplatesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('custom_templates', function(Blueprint $table) {
            $table->increments('id');
            $table->enum('type', array('mail', 'report'));
            $table->string('custom_type');
            $table->string('subject')->nullable();
            $table->string('name');
            $table->text('text');
            $table->boolean('status')->default(false);
            $table->boolean('send_copy')->default(false);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('custom_templates');
    }
}
