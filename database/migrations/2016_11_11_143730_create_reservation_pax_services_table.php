<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateReservationPaxServicesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('reservation_pax_services', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('reservation_pax_id')->unsigned();
            $table->integer('journey_service_id')->unsigned();
            $table->timestamps();

            $table->foreign('reservation_pax_id')->references('id')->on('reservation_paxes')
                ->onUpdate('cascade')->onDelete('cascade');
            $table->foreign('journey_service_id')->references('id')->on('journey_services')
                ->onUpdate('cascade')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('reservation_pax_services');
    }
}
