<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateNonCompatiblesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('non_compatibles', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('journey_id')->unsigned();
            $table->integer('itinerary_city_id')->unsigned()->nullable();
            $table->timestamps();

            $table->foreign('journey_id')->references('id')->on('journeys')
                ->onUpdate('cascade')->onDelete('cascade');
            $table->foreign('itinerary_city_id')->references('id')->on('itinerary_cities')
                ->onUpdate('cascade')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('non_compatibles');
    }
}
