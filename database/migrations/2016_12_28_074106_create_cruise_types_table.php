<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCruiseTypesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('cruise_types', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('journey_id')->unsigned();
            $table->integer('type_category_id')->unsigned();
            $table->integer('deck_id')->unsigned();
            $table->integer('overbooking');
            $table->decimal('price_net', 9, 2);
            $table->decimal('price_rec', 9, 2);
            $table->timestamps();

            $table->unique(array('journey_id', 'deck_id', 'type_category_id'));
            $table->foreign('journey_id')->references('id')->on('journeys')
                ->onUpdate('cascade')->onDelete('cascade');
            $table->foreign('type_category_id')->references('id')->on('type_categories')
                ->onUpdate('cascade')->onDelete('cascade');
            $table->foreign('deck_id')->references('id')->on('decks')
                ->onUpdate('cascade')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('cruise_types');
    }
}
