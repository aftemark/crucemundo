<div class="modal-header">
    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
    <h4 class="modal-title" id="userModalLabel">
        <div class="col-md-6">
            <div class="row">
                Add user
                @if(isset($data["id"]))
                <div class="checkbox">
                    <label><input id="userActivity" data-id="{{ $data["id"] }}" type="checkbox" name="active" value="true"{{ $data->active ? ' checked' : '' }}>Active</label>
                </div>
                @endif
            </div>
        </div>
        <div class="col-md-6">
            <div class="row">
                @role('edit_administration')
                @role('edit_users')
                <div class="col-md-6">
                    <a class="btn btn-success saveUser">Save</a>
                </div>
                @endrole
                @endrole
                <div class="col-md-6">
                    <a data-dismiss="modal" class="btn btn-danger">Cancel</a>
                </div>
            </div>
        </div>
    </h4>
</div>
<div class="modal-body">
    <ul class="nav nav-pills">
        <li class="active"><a data-toggle="tab" href="#general">General information</a></li>
        <li><a data-toggle="tab" href="#relations">Relations</a></li>
    </ul>
    <div class="tab-content row">
        <div id="general" class="tab-pane fade in active">
            <form id="userInfo">
                <div class="col-md-6">
                    <div class="form-group">
                        <label>Name:</label>
                        <input type="text" name="name" class="form-control" value="{{ $data->name }}">
                    </div>
                    <div class="form-group">
                        <label>Position:</label>
                        <input type="text" name="position" class="form-control" value="{{ $data->position }}">
                    </div>
                    <div class="form-group">
                        <label>Office:</label>
                        <input type="text" name="office" class="form-control" value="{{ $data->office }}">
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="form-group">
                        <label>Login:</label>
                        <input type="text" class="form-control"{{ !isset($data->id) ? 'name=login' : 'disabled value=' . $data->email }}>
                    </div>
                    <div class="form-group">
                        <label>Password:</label>
                        <input type="password" name="password" class="form-control">
                    </div>
                    <div class="form-group">
                        <label>Repeat password:</label>
                        <input type="password" name="password_confirmation" class="form-control">
                    </div>
                </div>
                <input type="hidden" name="_token" value="{{ Session::token() }}">
                @if(isset($data["id"]))
                <input type="hidden" name="id" value="{{ $data->id }}">
                @endif
            </form>
            <div class="col-md-12">
                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group">
                            <label for="userRole">User role:</label>
                            <select id="userRole" class="form-control" name="role">
                                <option value="">Select role template</option>
                            </select>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-12">
                        <h3>Data Access</h3>
                        <ul class="nav nav-tabs">
                        @foreach($modules as $keyName => $module)
                            <li{{ $module == reset($modules) ? ' class=active' : '' }}><a data-toggle="tab" href="#{{ $keyName }}">{{ $module["name"] }}</a></li>
                        @endforeach
                        </ul>
                        <form id="userDataAccess" class="tab-content">
                            @foreach($modules as $keyName => $module)
                            <div id="{{ $keyName }}" class="tab-pane col-md-12 fade{{ $module == reset($modules) ? ' in active' : '' }}">
                                <div class="row">
                                    <div class="checkbox-inline">
                                        <label><input type="checkbox" class="editDirectory" name="{{ 'edit_' . $keyName }}" value="true"{{ $data->hasRole('edit_' . $keyName) ? ' checked' : '' }}>Edit</label>
                                    </div>
                                    <div class="checkbox-inline">
                                        <label><input type="checkbox" class="showDirectory" name="{{ 'show_' . $keyName }}" value="true"{{ $data->hasRole('show_' . $keyName) ? ' checked' : '' }}>Show</label>
                                    </div>
                                    <div class="checkbox-inline">
                                        <p><strong>Full access to the module</strong></p>
                                    </div>
                                </div>
                                @if(!empty($module["children"]))
                                @foreach($module["children"] as $directoryName => $directory)
                                @if(!is_array($directory))
                                <div class="row">
                                    <div class="checkbox-inline">
                                        <label><input type="checkbox" class="editChild" name="{{ 'edit_' . $directoryName }}" value="true"{{ $data->hasRole('edit_' . $directoryName) ? ' checked' : '' }}>Edit</label>
                                    </div>
                                    <div class="checkbox-inline">
                                        <label><input type="checkbox" class="showChild" name="{{ 'show_' . $directoryName }}" value="true"{{ $data->hasRole('show_' . $directoryName) ? ' checked' : '' }}>Show</label>
                                    </div>
                                    <div class="checkbox-inline">
                                        <p>{{ $directory }}</p>
                                    </div>
                                </div>
                                @else
                                @php $create_edit = isset($directory['create']) && $directory['create'] ? 'create' : 'edit'; @endphp
                                <div class="row">
                                    <div class="checkbox-inline">
                                        <label><input type="checkbox" class="editChild" name="{{ $create_edit . '_' . $directoryName }}" value="true"{{ $data->hasRole($create_edit . '_' . $directoryName) ? ' checked' : '' }}>{{ ucfirst($create_edit) }}</label>
                                    </div>
                                    <div class="checkbox-inline">
                                        <label><input type="checkbox" class="showChild" name="{{ 'show_' . $directoryName }}" value="true"{{ $data->hasRole('show_' . $directoryName) ? ' checked' : '' }}>Show</label>
                                    </div>
                                    <div class="checkbox-inline">
                                        <p>{{ $directory['name'] }}</p>
                                    </div>
                                </div>
                                @foreach($directory['children'] as $directory_children_name => $directory_children)
                                <div class="row">
                                    <div class="checkbox-inline">
                                    @if($directory_children['type'] == 'edit')
                                        <label><input type="checkbox" class="editChild" name="{{ $directory_children_name . '_' . $directoryName }}" value="true"{{ $data->hasRole($directory_children_name . '_' . $directoryName) ? ' checked' : '' }}>Edit</label>
                                    @else
                                        <label><input type="checkbox" class="showChild" name="{{ 'show_' . $directoryName . '_' . $directory_children_name  }}" value="true"{{ $data->hasRole('show_' . $directoryName . '_' . $directory_children_name ) ? ' checked' : '' }}>Show</label>
                                    @endif
                                    </div>
                                    <div class="checkbox-inline">
                                        <p>{{ $directory['name'] }} ({{ $directory_children['name'] }})</p>
                                    </div>
                                </div>
                                @endforeach
                                @endif
                                @endforeach
                                @endif
                            </div>
                        @endforeach
                        </form>
                    </div>
                </div>
            </div>
        </div>
        <div id="relations" class="tab-pane fade">
            <form id="userRelations">
                <label>B2B Client</label>
                <select class="form-control load-users">
                    <option value="">Select B2B client</option>
                    @foreach($clients as $client)
                    <option {{ isset($data->client_user) && $data->client_user->client->id == $client->id ? 'selected ' : '' }}value="{{ $client->id }}">{{ $client->code }}</option>
                    @endforeach
                </select>
                <label>User</label>
                <select class="form-control" id="clientUsers" name="client_user">
                    <option value="">Select user</option>
                    @if(isset($data->client_user))
                    <option selected value="{{ $data->client_user->id }}">{{ $data->client_user->name }}</option>
                    @foreach($data->client_user->client->users()->where('user_id', NULL)->get() as $clientUser)
                    <option value="{{ $clientUser->id }}">{{ $clientUser->name }}</option>
                    @endforeach
                    @else
                    @foreach($clients{0}->users()->where('user_id', NULL)->get() as $clientUser)
                    <option value="{{ $clientUser->id }}">{{ $clientUser->name }}</option>
                    @endforeach
                    @endif
                </select>
            </form>
        </div>
    </div>
</div>