@extends('layouts.app')

@section('title')Users @endsection

@section('content')
    <div class="search-block">
        <div class="search">
            @include('includes.breadcrumbs')
        </div>
    </div>
    <div class="base-page-navigation-table">
        <div class="search-cities">
            @include('includes.table-search', ['oldSearch' => Request::get('search'), 'searchKeys' => ['all' => 'All', 'name' => 'Name', 'status' => 'Status'], 'otherKeys' => ['order', 'order_field']])
        </div>
        <div class="base-page-table-label">
            <p>Users</p>
        </div>
        <div class="base-page-table-add-button">
            @role('edit_administration')
            @role('edit_users')
            <button data-id="new" class="base-page-table-add editUser">Add user</button>
            @endrole
            @endrole
        </div>
    </div>
    <table id="users" class="table table-striped custom-table table-city table-iteniraries">
        <thead>
        <tr>
            <th>Name
                @include('includes.table-sort', ['orderField' => 'name', 'sortSearchKeys' => ['field', 'search', 'page']])
            </th>
            <th>Status
                @include('includes.table-sort', ['orderField' => 'status', 'sortSearchKeys' => ['field', 'search', 'page']])
            </th>
        </tr>
        </thead>
        <tbody>
        @foreach ($data as $each)
            <tr data-id="{{ $each->id }}">
                <td>{{ $each->name }}</td>
                <td>
                    <div class="table-floating-left-element">{{ $each->status }}</div>
                    <ul class="custom-dropdown-table">
                        <li class="dropdown">
                            <a class="dropdown-toggle inner-table-button-hovered" data-toggle="dropdown" href="#"><span class="glyphicon glyphicon-option-vertical"></span></a>
                            <ul class="dropdown-menu">
                                <li>
                                    <a href="#" data-id="{{ $each->id }}" class="editUser"><img src="/src/img/pen-icon.png" alt=""></a>
                                </li>
                                @role('edit_administration')
                                @role('edit_users')
                                <li>
                                    <a href="{{ url('/delete/user/' . $each->id) }}" class="delete-check-element"><img src="/src/img/trash-icon.png" alt=""></a>
                                </li>
                                @endrole
                                @endrole
                            </ul>
                        </li>
                    </ul>
                </td>
            </tr>
        @endforeach
        </tbody>
    </table>
    <div class="row">
        <div class="col-md-6">
            {{ $data->links() }}
        </div>
        <div class="col-md-6">
            <p>Display:</p>
            <a href="/users/setnumber?number=10" class="btn btn-default">10</a>
            <a href="/users/setnumber?number=50" class="btn btn-default">50</a>
            <a href="/users/setnumber?number=100" class="btn btn-default">100</a>
        </div>
    </div>
    <div class="modal fade" id="userModal" tabindex="-1" role="dialog" aria-labelledby="userModalLabel">
        <div class="modal-dialog" role="document">
            <div class="modal-content"></div>
        </div>
    </div>
@endsection

@section('script')
    <script src="{{ url('src/js/role-templates.js') }}"></script>
    <script>
        $(document).ready(function() {
            $('body').on('click', '.editUser', function () {
                $('#userModal .modal-content').empty();
                $('#userModal').modal('show');
                var attr = $(this).attr('data-id');

                loadUser(attr);
            });
        });

        function loadUser (attr) {
            $.ajax({
                dataType: "html",
                method: "GET",
                url: "/user/" + attr,
                success: function (data) {
                    $('#userModal .modal-content').append(data);
                    @if(!Auth::user()->hasRole('edit_users'))

                    $('#userModal input, #userModal select, #userModal textarea, #userRelations select').each(function () {
                        $(this).prop('disabled', true);
                    });
                    @endif

                    $('.saveUser').on('click', function () {
                        var href = window.location.href;
                        $.ajax({
                            type: "POST",
                            url: "{{ url('/save/user') }}",
                            data: $('#userInfo, #userDataAccess, #userRelations').serialize(),
                            success: function() {
                                $('#userModal').modal('hide');
                                location.reload();
                            }
                        });
                    });

                    $('#userActivity').on('change', function () {
                        $.get("{{ url('/activity/user') }}/" + $(this).attr('data-id'));
                    });

                    $('.load-users').on('change', function () {
                        var ID = $(this).val();
                        var select = $('#clientUsers');
                        if (ID)
                            $.get("{{ url('/b2b/users') }}/" + ID, function(response) {
                                select.find('option').each(function () {
                                    if ($(this).val())
                                        $(this).remove();
                                });
                                $.each(response, function (id, name) {
                                    select.append('<option value="' + id + '">' + name + '</option>');
                                })
                            });
                        else
                            select.find('option').each(function () {
                                if ($(this).val())
                                    $(this).remove();
                            });
                    });

                    rolesInit();
                }
            });
        }
    </script>
@endsection