@extends('layouts.app')

@section('title')Mass mailing template @endsection

@section('css')
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.3/css/select2.min.css">
@endsection

@section('content')
    <form action="{{ url('/mailing/send') }}" method="POST" class="container">
        <div class="row">
            <div class="col-md-6">
                <h2>Mass Mailing</h2>
            </div>
            <div class="col-md-6">
                <div class="row pull-right">
                    @role('edit_administration')
                    @role('edit_mailing')
                    <button type="submit" class="btn btn-success"><span class="glyphicon glyphicon-ok"></span> Send</button>
                    @endrole
                    @endrole
                    <a href="{{ url('/mailing') }}" class="btn btn-danger"><span class="glyphicon glyphicon-remove"></span> Cancel</a>
                </div>
            </div>
        </div>
        <div class="form-group">
            <select multiple class="form-control emails-select">
                @foreach($emails as $email)
                <option {{ in_array($email['id'], $picked[$email['type']]) ? 'selected ' : '' }}value="{{ $email['type'] . '.' . $email['id'] }}">{{ $email['email'] }} ({{ $email['name'] }})</option>
                @endforeach
            </select>
        </div>
        <div class="form-group">
            <label>Subject</label>
            <input type="text" class="form-control" name="subject" value="{{ $template->subject }}">
        </div>
        <div class="form-group">
            <label>Text</label>
            <textarea name="text" id="emailText">{{ $template->text }}</textarea>
        </div>
        <div class="form-group add-variable">
            <span data-value="@{{name}}">Name - @{{name}}</span>
        </div>
        @if($template->id)
        <input type="hidden" name="id" value="{{ $template->id }}">
        @endif
        {{ csrf_field() }}
    </form>
@endsection

@section('script')
<script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.3/js/select2.min.js"></script>
<script src="{{ url('/src/ckeditor/ckeditor.js') }}"></script>
<script>
    $(document).ready(function() {
        $('.emails-select').select2();
        CKEDITOR.replace('emailText');
        $('.add-variable span').on('click', function() {
            CKEDITOR.instances['emailText'].insertText($(this).attr('data-value'));
        });
        @role('edit_administration')
        @role('edit_mailing')
        $('form').submit(function() {
            var form = $(this);
            form.find('select').val().forEach(function(value) {
                let splitted = value.split('.');
                $('<input/>').attr('type', 'hidden').attr('name', splitted[0] + '[]').attr('value', splitted[1]).appendTo(form);
            });
        });
        @endrole
        @endrole
    });
</script>
@endsection