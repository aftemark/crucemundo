@extends('layouts.app')

@section('title')Notification template @endsection

@section('css')
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.3/css/select2.min.css">
@endsection

@section('content')
    <form action="{{ url('/mailing/save') }}" method="POST" class="container">
        <div class="row">
            <div class="col-md-6">
                <h2>Mass Mailing</h2>
            </div>
            <div class="col-md-6">
                <div class="row pull-right">
                    @role('edit_administration')
                    @role('edit_mailing')
                    <button type="submit" class="btn btn-success"><span class="glyphicon glyphicon-ok"></span> Save</button>
                    @endrole
                    @endrole
                    <a href="{{ url('/mailing') }}" class="btn btn-danger"><span class="glyphicon glyphicon-remove"></span> Cancel</a>
                </div>
            </div>
        </div>
        <div class="form-group">
            <label>Name</label>
            <input type="text" class="form-control" name="name" value="{{ $template->name }}">
        </div>
        <div class="form-group">
            <label>Subject</label>
            <input type="text" class="form-control" name="subject" value="{{ $template->subject }}">
        </div>
        <div class="form-group">
            <label>Text</label>
            <textarea name="text" id="emailText">{{ $template->text }}</textarea>
        </div>
        <input type="hidden" name="id" value="{{ $template->id }}">
        {{ csrf_field() }}
    </form>
@endsection

@section('script')
    <script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.3/js/select2.min.js"></script>
    <script src="{{ url('/src/ckeditor/ckeditor.js') }}"></script>
    <script>
        $(document).ready(function() {
            $('.emails-select').select2();
            CKEDITOR.replace('emailText');
            $('.add-variable span').on('click', function() {
                CKEDITOR.instances['emailText'].insertText($(this).attr('data-value'));
            });
        });
    </script>
@endsection