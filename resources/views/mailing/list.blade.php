@extends('layouts.app')

@section('title')Mailing @endsection

@section('content')
    <div class="container">
        <div class="row">
            <h3>E-mail</h3>
        </div>
        <div class="row center">
            <ul class="nav nav-pills">
                <li class="active"><a data-toggle="tab" href="#notifications">Clients notifications</a></li>
                <li><a data-toggle="tab" href="#mass_mailing">Mass Mailing</a></li>
            </ul>
        </div>
        @if(Session::has('message'))
            <div class="row">
                <div class="col-md-4 col-md-offset-4 success">
                    {{ Session::get('message') }}
                </div>
            </div>
        @endif
        <div class="tab-content">
            <div id="notifications" class="tab-pane fade in active">
                <table class="table table-striped">
                    <thead>
                    <tr>
                        <th>Name</th>
                        <th>Status</th>
                        <th>Send a copy to manager</th>
                        <th></th>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach ($notifications as $notification)
                        <tr>
                            <td>{{ $notification->name }}</td>
                            <td>
                                @if($allow_edit)
                                <form action="{{ url('/mailing/notification/field') }}" method="POST">
                                @endif
                                    <div class="checkbox">
                                        <label><input class="change-template-field" type="checkbox" {{ $notification->status ? 'checked ' : '' }}{{ !$allow_edit ? 'disabled ' : '' }}name="value" value="true">ON</label>
                                    </div>
                                @if($allow_edit)
                                    <input type="hidden" name="field" value="status">
                                    <input type="hidden" name="id" value="{{ $notification->id }}">
                                    <input type="hidden" name="_token" value="{{ Session::token() }}">
                                </form>
                                @endif
                            </td>
                            <td>
                                @if($allow_edit)
                                <form action="{{ url('/mailing/notification/field') }}" method="POST">
                                @endif
                                    <div class="checkbox">
                                        <input class="change-template-field" type="checkbox" {{ $notification->send_copy ? 'checked ' : '' }}{{ !$allow_edit ? 'disabled ' : '' }}name="value" value="true">
                                    </div>
                                @if($allow_edit)
                                    <input type="hidden" name="field" value="send_copy">
                                    <input type="hidden" name="id" value="{{ $notification->id }}">
                                    <input type="hidden" name="_token" value="{{ Session::token() }}">
                                </form>
                                @endif
                            </td>
                            <td>
                                <a href="{{ url('/mailing/notification/' . $notification->id) }}">Template</a>
                            </td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
            </div>
            <div id="mass_mailing" class="tab-pane fade">
                <div class="row text-right">
                    <a href="{{ url('/mailing/mass/new') }}" class="btn btn-success"><span class="glyphicon glyphicon-ok"></span> New Template</a>
                </div>
                <table class="table table-striped">
                    <thead>
                    <tr>
                        <th>Date</th>
                        <th>Subject</th>
                        <th>Number of addresses</th>
                        <th></th>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach ($mass_templates as $mass_template)
                        <tr>
                            <td>{{ $mass_template->created_at }}</td>
                            <td>{{ $mass_template->subject }}</td>
                            <td>{{ $mass_template->receivers->count() }}</td>
                            <td>
                                <a href="{{ url('/mailing/mass/' . $mass_template->id) }}"><span class="glyphicon glyphicon-edit"></span></a>
                            </td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>
@endsection

@if($allow_edit)
@section('script')
    <script>
        $(document).ready(function() {
            $('.change-template-field').on('change', function () {
                var form = $(this).closest('form');
                $.post(form.attr('action'), form.serialize());
            });
        });
    </script>
@endsection
@endif