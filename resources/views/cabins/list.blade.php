@foreach($data as $deckName => $deck)
    <div class="sun-decks clearfix">
        <h3>{{ $deckName }}</h3>
        <table class="table custom-table table-cabins">
            <thead>
            <tr>
                <th>Cabin category</th>
                <th>ID</th>
                <th class="stripped">Cabin number</th>
            </tr>
            </thead>
            <tbody>
            @if(!empty($deck["data"]))
                @foreach($deck["data"] as $categoryName => $category)
                    @php $cabinsCount = 0; $firstCabin = reset($category)[0]; foreach ($category as $type) $cabinsCount += count($type); @endphp
                    <tr>
                        <td rowspan="{{ $cabinsCount }}">{{ $categoryName }}{{ count($category) }}</td>
                        <td rowspan="{{ count(reset($category)) }}">{{ array_keys($category)[0] }}</td>
                        <td>
                            <div class="table-floating-left-element">{{ $firstCabin->num }}</div>
                            <ul class="custom-dropdown-table">
                                <li class="dropdown">
                                    <a class="dropdown-toggle inner-table-button-hovered" data-toggle="dropdown" href="#"><span class="glyphicon glyphicon-option-vertical"></span></a>
                                    <ul class="dropdown-menu">
                                        <li>
                                            <a href="#" data-id="{{ $firstCabin->id }}" class="editCabin"><img src="/src/img/pen-icon.png" alt=""></a>
                                        </li>
                                        @role('edit_directories')
                                        @role('edit_ships')
                                        <li>
                                            <a href="#" data-id="{{ $firstCabin->id }}" class="deleteCabin"><img src="/src/img/trash-icon.png" alt=""></a>
                                        </li>
                                        @endrole
                                        @endrole
                                    </ul>
                                </li>
                            </ul>
                        </td>
                    </tr>
                    @php $typesCount = 0; @endphp
                    @foreach($category as $typeName => $type)
                        @if($typesCount++ != 0)
                            @php $firstCabin = $type[0]; @endphp
                            <tr>
                                <td hidden></td>
                                <td rowspan="{{ count($type) }}">{{ $typeName }}</td>
                                <td>
                                    <div class="table-floating-left-element">{{ $firstCabin->num }}</div>
                                    <ul class="custom-dropdown-table">
                                        <li class="dropdown">
                                            <a class="dropdown-toggle inner-table-button-hovered" data-toggle="dropdown" href="#"><span class="glyphicon glyphicon-option-vertical"></span></a>
                                            <ul class="dropdown-menu">
                                                <li>
                                                    <a href="#" data-id="{{ $firstCabin->id }}" class="editCabin"><img src="/src/img/pen-icon.png" alt=""></a>
                                                </li>
                                                @role('edit_directories')
                                                @role('edit_ships')
                                                <li>
                                                    <a href="#" data-id="{{ $firstCabin->id }}" class="deleteCabin"><img src="/src/img/trash-icon.png" alt=""></a>
                                                </li>
                                                @endrole
                                                @endrole
                                            </ul>
                                        </li>
                                    </ul>
                                </td>
                            </tr>
                        @endif
                        @php $cabinsCount = 0; @endphp
                        @foreach($type as $cabin)
                            @if($cabinsCount++ != 0)
                                <tr>
                                    <td hidden>{{ $cabinsCount }}</td>
                                    <td hidden></td>
                                    <td>
                                        <div class="table-floating-left-element">{{ $cabin->num }}</div>
                                        <ul class="custom-dropdown-table">
                                            <li class="dropdown">
                                                <a class="dropdown-toggle inner-table-button-hovered" data-toggle="dropdown" href="#"><span class="glyphicon glyphicon-option-vertical"></span></a>
                                                <ul class="dropdown-menu">
                                                    <li>
                                                        <a href="#" data-id="{{ $cabin->id }}" class="editCabin"><img src="/src/img/pen-icon.png" alt=""></a>
                                                    </li>
                                                    @role('edit_directories')
                                                    @role('edit_ships')
                                                    <li>
                                                        <a href="#" data-id="{{ $cabin->id }}" class="deleteCabin"><img src="/src/img/trash-icon.png" alt=""></a>
                                                    </li>
                                                    @endrole
                                                    @endrole
                                                </ul>
                                            </li>
                                        </ul>
                                    </td>
                                </tr>
                            @endif
                        @endforeach
                    @endforeach
                @endforeach
            @endif
            </tbody>
        </table>
        @role('edit_directories')
        @role('edit_ships')
        <a href="#" data-deck="{{ $deck["deck_id"] }}" data-id="new" class="base-page-table-add btn-add-cabin editCabin">Add cabin</a>
        @endrole
        @endrole
    </div>
@endforeach
<div class="bed-number">
    <p>Number of additional beds:</p>
    <input id="shipAddBed" class="ships-width-bed" type="text" value="{{ $add_bed }}">
</div>