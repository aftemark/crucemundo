{{ csrf_field() }}
<input id="cabinDeckID" type="hidden" name="deck" value="{{ $data->deck_id }}">
@if(isset($data->id))
    <input type="hidden" name="id" value="{{ $data->id }}">
@endif
<div class="modal-header">
    <div class="modal-header-label modal-left">
        <h4>
            @if(isset($data->id))
                Cabin # {{ $data->num }}
            @else
                Add cabin
            @endif
        </h4>
    </div>
    <div class="modal-header-buttons modal-right">
        <a id="saveCabin" class="btn-create">Save</a>
        <button type="button" class="btn-cancel" data-dismiss="modal">Cancel</button>
    </div>
</div>
<div class="modal-body clearfix">
    <div class="modal-left ships-modal-left">
        <div class="modal-ships-inner-element">
            <label>Category:</label>
            <select id="cabinType">
                @foreach($data->types as $key => $each)
                    <option value="{{ $key }}">{{ $each }}</option>
                @endforeach
            </select>
        </div>
        <div class="modal-ships-inner-element">
            <p>ID:</p>
            <select name="type_category" id="cabinID">
                @foreach($data->categories as $key2 => $each2)
                    <option value="{{ $key2 }}">{{ $each2 }}</option>
                @endforeach
            </select>
        </div>
        <div class="modal-ships-inner-element cabin-input">
            <p>Cabin #:</p>
            <input type="text" name="num" value="{{ $data->num }}">
        </div>
        <div class="modal-ships-inner-element">
            <p>Area:</p>
            <input type="text" name="area" value="{{ $data->area }}">
            <div class="modal-ships-input-value">
                <p>m^2</p>
            </div>
        </div>
        <div class="modal-ships-inner-element">
            <p>Number of pax:</p>
            <input disabled type="text" id="cabinPaxAmount">
            <div class="modal-ships-input-value">
                <p>pax</p>
            </div>
        </div>
        <div class="modal-ships-inner-element">
            <ul>
                <li><label><input type="checkbox" id="cabinBed" disabled>Balcony</label></li>
                <li><label><input type="checkbox" id="cabinBalcony" disabled>Additional bed</label></li>
            </ul>
        </div>
    </div>
    <div class="modal-right ships-modal-right">
        <div class="modal-ships-inner-element">
            <p>Note:</p>
            <textarea name="description" cols="30" rows="10">{{ $data->description }}</textarea>
        </div>
    </div>
</div>