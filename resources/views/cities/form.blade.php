<div class="modal-header">
    <div class="modal-header-title pull-left">
        <h4 class="modal-title" id="addYaerLabel">Add City</h4>
    </div>
    <div class="modal-header-buttons pull-right">
        @role('edit_directories')
        @role('edit_cities')
        <button type="submit" class="btn-create saveCity" form="addCityForm">Save</button>
        @endrole
        @endrole
        <button type="button" class="btn-cancel" data-dismiss="modal">Cancel</button>
    </div>
</div>
<div class="modal-body clearfix">
    <form id="addCityForm" action="{{ url('/save/cities') }}" method="POST">
        {{ csrf_field() }}
        <div class="select-copy-year city-modal-left">
            <div class="modal-input-city">
                <label>City:</label>
                <input type="text" name="city" value="{{ $data->name }}">
            </div>
            <div class="modal-country-select">
                <label>Select a country:</label>
                <select class="form-control" name="country">
                    <option disabled="disabled" selected hidden>Select a country</option>
                    @foreach($countries as $countryID => $countryName)
                    <option {{ $countryID == $data->country_id ? 'selected ' : '' }}value="{{ $countryID }}">{{ $countryName }}</option>
                    @endforeach
                </select>
            </div>
        </div>
        <div class="select-new-year city-modal-right">
            <label for="copy-year">Note:</label>
            <textarea name="description" cols="30" rows="10">{{ $data->description }}</textarea>
        </div>
        @if($data->id)
        <input name="id" type="hidden" value="{{ $data->id }}">
        @endif
    </form>
</div>