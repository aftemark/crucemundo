@extends('layouts.app')

@section('title')Cities @endsection

@section('content')
    <div class="search-block">
        <div class="search">
            @include('includes.breadcrumbs')
        </div>
    </div>
    <div class="base-page-navigation-table">
        <div class="search-cities">
            @include('includes.table-search', ['oldSearch' => Request::get('search'), 'searchKeys' => ['all' => 'All', 'city' => 'City', 'country' => 'Country'], 'otherKeys' => ['order', 'order_field']])
        </div>
        <div class="base-page-table-label">
            <p>List of cities</p>
        </div>
        @role('edit_directories')
        @role('edit_cities')
        <div class="base-page-table-add-button">
            <button data-id="new" class="base-page-table-add edit-city">Add city</button>
        </div>
        @endrole
        @endrole
    </div>
    <table id="cities" class="table table-striped custom-table table-city">
        <thead>
        <tr>
            <th>City
                @include('includes.table-sort', ['orderField' => 'city', 'sortSearchKeys' => ['field', 'search', 'page']])
            </th>
            <th>Country
                @include('includes.table-sort', ['orderField' => 'country', 'sortSearchKeys' => ['field', 'search', 'page']])
            </th>
        </tr>
        </thead>
        <tbody>
        @foreach ($data as $each)
        <tr data-id="{{ $each->id }}">
            <td>{{ $each->name }}</td>
            <td>
                <div class="table-floating-left-element">{{ $each->country->name }}</div>
                <ul class="custom-dropdown-table">
                    <li class="dropdown">
                        <a class="dropdown-toggle inner-table-button-hovered" data-toggle="dropdown" href="#"><span class="glyphicon glyphicon-option-vertical"></span></a>
                        <ul class="dropdown-menu">
                            <li>
                                <a href="#" data-id="{{ $each->id }}" class="edit-city"><img src="/src/img/pen-icon.png" alt=""></a>
                            </li>
                            @role('edit_directories')
                            @role('edit_cities')
                            <li>
                                <a href="{{ url('/delete/city/' . $each->id) }}" data-id="{{ $each->id }}" class="delete-check-element"><img src="/src/img/trash-icon.png" alt=""></a>
                            </li>
                            @endrole
                            @endrole
                        </ul>
                    </li>
                </ul>
            </td>
        </tr>
        @endforeach
        </tbody>
    </table>
    <div class="row navigation-bottom">
        <div class="col-md-6">
            {{ $data->appends(array_except(Request::query(), 'cities_page'))->links() }}
        </div>
        <div class="col-md-6 setnumber">
            <p>Display:</p>
            <a href="{{ url('/cities/setnumber?number=10') }}" class="btn btn-default btn-setnumber">10</a>
            <a href="{{ url('/cities/setnumber?number=50') }}" class="btn btn-default btn-setnumber">50</a>
            <a href="{{ url('/cities/setnumber?number=100') }}" class="btn btn-default btn-setnumber">100</a>
        </div>
    </div>
    <div class="modal fade" id="cityModal" tabindex="-1" role="dialog" aria-labelledby="cityModalLabel">
        <div class="modal-dialog" role="document">
            <div class="modal-content"></div>
        </div>
    </div>
@endsection

@section('script')
    <script src="{{ url('/src/js/jquery.validate.min.js') }}"></script>
    <script>
        $(document).ready(function() {
            $('body').on('click', '.edit-city', function () {
                $('#cityModal .modal-content').empty();
                $('#cityModal').modal('show');
                var attr = $(this).attr('data-id');

                loadCity(attr);
            });
        });

        function loadCity(attr) {
            $.ajax({
                dataType: "html",
                method: "GET",
                url: "/cities/" + attr,
                success: function (data) {
                    $('#cityModal .modal-content').append(data);

                    $('.saveCity').on('click', function (e) {
                        if ($("#addCityForm").valid()) {
                            var form = $('#' + $(this).attr('form'));
                            var href = window.location.href;
                            $.ajax({
                                type: "POST",
                                url: form.attr('action'),
                                data: form.serialize(),
                                success: function () {
                                    $('#cityModal').modal('hide');
                                    $('#cities').load(href + " #cities > *");
                                }
                            });
                        }
                        e.preventDefault();
                    });

                    $("#addCityForm").validate({
                        rules: {
                            country: {
                                required: true
                            },
                            city: {
                                required: true
                            }
                        },
                        messages: {
                            addCitySelect: "Select country",
                            cityName: "Enter the name of the city"
                        }
                    });
                    @if(!Auth::user()->hasRole('edit_cities'))

                    $('#cityModal input, #cityModal select, #cityModal textarea').each(function () {
                        $(this).prop('disabled', true);
                    });
                    @endif
                }
            });
        }
    </script>
@endsection