<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
    <meta name="viewport" content="width=device-width, initial-scale=1" />

    <title>@yield('title')</title>

    <link rel="stylesheet" href="{{ url('/src/css/bootstrap.min.css') }}">
    <link rel="stylesheet" href="{{ url('/src/css/main.css') }}">
    @yield('css')

    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
    <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->
</head>
<body>
    @include('includes.header')
    @yield('content')
    @include('includes.footer')
    @yield('script')
</body>
</html>
