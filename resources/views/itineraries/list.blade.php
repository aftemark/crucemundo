@extends('layouts.app')

@section('title')Itineraries @endsection

@section('css')
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.3/css/select2.min.css">
@endsection

@section('content')
    <div class="search-block">
        <div class="search">
            @include('includes.breadcrumbs')
        </div>
    </div>
    <div class="base-page-navigation-table">
        <div class="search-cities">
            @include('includes.table-search', ['oldSearch' => Request::get('search'), 'searchKeys' => ['all' => 'All', 'name' => 'Name', 'nights' => 'Nights', 'from_to' => 'From - To'], 'otherKeys' => ['order', 'order_field']])
        </div>
        <div class="base-page-table-label">
            <p>List of itineraries</p>
        </div>
        <div class="base-page-table-add-button">
            @role('edit_directories')
            @role('edit_itineraries')
            <button data-id="new" class="base-page-table-add editItinerary">Add an itinerary</button>
            @endrole
            @endrole
        </div>
    </div>
    <table id="itineraries" class="table table-striped custom-table table-city table-iteniraries">
        <thead>
        <tr>
            <th>Itinerary
                @include('includes.table-sort', ['orderField' => 'name', 'sortSearchKeys' => ['field', 'search', 'page']])
            </th>
            <th>Nights
                @include('includes.table-sort', ['orderField' => 'nights', 'sortSearchKeys' => ['field', 'search', 'page']])
            </th>
            <th>From / to
                @include('includes.table-sort', ['orderField' => 'from_to', 'sortSearchKeys' => ['field', 'search', 'page']])
            </th>
        </tr>
        </thead>
        <tbody>
        @foreach ($data as $each)
            <tr data-id="{{ $each->id }}">
                <td>{{ $each->name }}</td>
                <td>{{ $each->nights }}</td>
                <td>
                    <div class="table-floating-left-element">{{ $each->from_to }}</div>
                    <ul class="custom-dropdown-table">
                        <li class="dropdown">
                            <a class="dropdown-toggle inner-table-button-hovered" data-toggle="dropdown" href="#"><span class="glyphicon glyphicon-option-vertical"></span></a>
                            <ul class="dropdown-menu">
                                <li>
                                    <a data-id="{{ $each->id }}" class="editItinerary" href="#"><img src="/src/img/pen-icon.png" alt=""></a>
                                </li>
                                @role('edit_directories')
                                @role('edit_itineraries')
                                <li>
                                    <a href="{{ url('/delete/itinerary/' . $each->id) }}" class="delete-check-element"><img src="/src/img/trash-icon.png" alt=""></a>
                                </li>
                                @endrole
                                @endrole
                            </ul>
                        </li>
                    </ul>
                </td>
            </tr>
        @endforeach
        </tbody>
    </table>
    <div class="row">
        <div class="col-md-6">
            {{ $data->links() }}
        </div>
        <div class="col-md-6">
            <p>Display:</p>
            <a href="/itineraries/setnumber?number=10" class="btn btn-default">10</a>
            <a href="/itineraries/setnumber?number=50" class="btn btn-default">50</a>
            <a href="/itineraries/setnumber?number=100" class="btn btn-default">100</a>
        </div>
    </div>

    <div id="itineraryModal" class="modal fade" role="dialog">
        <div class="modal-dialog" role="document">
            <form class="modal-content" action="{{ url('/save/itinerary') }}" method="POST">
                {{ csrf_field() }}
                <div class="modal-header">
                    <div class="modal-header-title pull-left">
                        <h4 class="modal-title">Itinerary</h4>
                    </div>
                    <div class="modal-header-buttons pull-right">
                        @role('edit_directories')
                        @role('edit_itineraries')
                        <button type="button" class="btn-create saveItinerary">Save</button>
                        @endrole
                        @endrole
                        <button type="button" class="btn-cancel" data-dismiss="modal">Cancel</button>
                    </div>
                </div>
                <div class="modal-body clearfix">
                    <div class="iteniraries-modal-body">
                        <div class="iteniraries-modal-body-left-elem">
                            <label>Name:</label>
                            <input id="itineraryName" type="text" name="name">
                        </div>
                        <div class="iteniraries-modal-body-right-elem">
                            <label>Nights:</label>
                            <input id="itineraryNights" type="number" name="nights">
                        </div>
                    </div>
                    <table id="itineraryCities" class="table table-striped custom-table">
                        <tbody></tbody>
                    </table>
                </div>
            </form>
        </div>
    </div>
@endsection

@section('script')
    <script src="{{ url('/src/js/jquery.validate.min.js') }}"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.3/js/select2.min.js"></script>
    <script>
        $( document ).ready(function() {
            var allCities = {};
            var dayCities = {};

            $.event.special.inputchange = {
                setup: function() {
                    var self = this, val;
                    $.data(this, 'timer', window.setInterval(function() {
                        val = self.value;
                        if ( $.data( self, 'cache') != val ) {
                            $.data( self, 'cache', val );
                            $( self ).trigger( 'inputchange' );
                        }
                    }, 20));
                },
                teardown: function() {
                    window.clearInterval( $.data(this, 'timer') );
                },
                add: function() {
                    $.data(this, 'cache', this.value);
                }
            };

            $('body').on('click', '.editItinerary', function () {
                var attr = $(this).attr('data-id');
                loadItinerary(attr);
            });

            $('body').on('click', '.saveItinerary', function () {
                var form = $('#itineraryModal form');
                if (form.valid()) {
                    $.ajax({
                        type: "POST",
                        dataType: "html",
                        url: form.attr('action'),
                        data: form.serialize(),
                        success: function (data) {
                            $('#itineraryModal').modal('hide');
                            window.location.reload();
                        }
                    });
                }
            });
        });

        function loadItinerary (attr) {
            $.ajax({
                dataType: "json",
                method: "GET",
                url: "/itinerary/" + attr,
                success: function (itineraryData) {
                    var itineraryId = parseInt(attr);
                    allCities = itineraryData.allcities;
                    dayCities = itineraryData.itinerarycities;
                    $('#itineraryCities tbody').empty();
                    $('#itineraryModal').modal('show');

                    if (parseInt(attr) > 0)
                        $('#itineraryModal .modal-body').append('<input name="id" type="hidden" value="' + itineraryId + '">');
                    else
                        $('#itineraryModal .modal-body input[name=id]').remove();

                    $('#itineraryName').val(itineraryData.name);
                    $('#itineraryNights').val(itineraryData.nights);
                    appendCitySelect(0, parseInt(itineraryData.nights));

                    $('#itineraryNights').on('inputchange', function () {
                        if($(this).val() >= 1 && $(this).val() <= 999) {
                            var newLength = parseInt($(this).val());
                            var itineraryCitiesLength = $('#itineraryCities tbody tr').length;
                            //console.log('Now length: ' + itineraryCitiesLength + '. New Length:' + newLength)
                            if (newLength >= itineraryCitiesLength) {
                                appendCitySelect(itineraryCitiesLength, newLength);
                            } else if (newLength < itineraryCitiesLength) {
                                $('#itineraryCities tbody tr').each(function (index){
                                    if (index > newLength) {
                                        $(this).remove();
                                    }
                                });
                            }
                        } else if ($(this).val() < 1)
                            $('#itineraryCities tbody tr').remove();
                    });

                    @if(!Auth::user()->hasRole('edit_itineraries'))

                    $('#itineraryModal input, #itineraryModal select, #itineraryModal textarea').each(function () {
                        $(this).prop('disabled', true);
                    });
                    @endif

                    $("#itineraryModal form").validate({
                        rules: {
                            name: {
                                required: true
                            },
                            nights: {
                                required: true
                            }
                        }
                    });
                }
            });
        }

        function appendCitySelect (from, to) {
            for (i = from + 1; i < to + 2; i++) {
                var options = '';
                $.each(allCities, function (cityID, cityName) {
                    var iftrue = false;
                    $.each(dayCities[i], function (selectIndex, selectVal) {
                        console.log(selectIndex);
                        if (selectVal == cityID)
                            iftrue = true;
                    });

                    if (iftrue)
                        options += '<option selected value="' + cityID + '">' + cityName + '</option>';
                    else
                        options += '<option value="' + cityID + '">' + cityName + '</option>';
                });
                $('#itineraryCities tbody').append('<tr><td><div class="modal-table-iteniraries-left"><label>Day ' + i + ' cities:</label></div><div class="modal-table-iteniraries-right"><select class="modal-table-iteniraries-right-select selectCities" multiple="multiple" name="itinerary_cities[' + i + '][]">' + options + '</select></div></td></tr>')
            }
            $('.selectCities').select2();
        }
    </script>
@endsection