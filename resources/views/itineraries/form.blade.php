<form action="{{ url('/save/itineraries') }}" method="POST">
    <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="itineraryModalLabel">
            <div class="col-md-6">
                Add itinerary
            </div>
            <div class="col-md-6">
                <div class="row">
                    <div class="col-md-6">
                        <input type="hidden" name="_token" value="{{ Session::token() }}">
                        @role('edit_directories')
                        @role('edit_itineraries')
                        <a class="btn btn-success saveItinerary">Save</a>
                        @endrole
                        @endrole
                    </div>
                    <div class="col-md-6">
                        <a data-dismiss="modal" class="btn btn-danger">Cancel</a>
                    </div>
                </div>
            </div>
        </h4>
    </div>
    <div class="modal-body">
        <div class="row">
            <div class="col-md-10">
                <div class="form-group">
                    <label for="itineraryName">Name</label>
                    <input id="itineraryName" type="text" name="name" class="form-control" value="{{ $data->name }}">
                </div>
            </div>
            <div class="col-md-2">
                <div class="form-group">
                    <label for="itineraryNights">Nights</label>
                    <input id="itineraryNights" type="text" name="nights" class="form-control" value="{{ $data->nights }}">
                </div>
            </div>
            @if($data->id != 'new')
                <input name="id" type="hidden" value="{{ $data->id }}">
            @endif
        </div>
        <div id="itineraryCities" class="row">
            <table class="table table-striped">
                <tbody>
                    @foreach($data as $dayName => $day)
                    <tr>
                        <td class="col-md-4">
                            <label>{{ $dayName }}</label>
                        </td>
                        <td class="col-md-8">
                            <select class="form-control selectCities"></select>
                        </td>
                    </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
    </div>
</form>