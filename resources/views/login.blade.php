<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
    <meta name="viewport" content="width=device-width, initial-scale=1" />

    <title>@yield('title')</title>

    <link rel="stylesheet" href="{{ url('src/css/bootstrap.min.css') }}">
    <link rel="stylesheet" href="{{ url('src/css/main.css') }}">

    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
    <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->
</head>
<body>
<div class="login-form">
    <div class="form-logo"><img src="src/img/logo-auth.png" alt="crucemundo"></div>
    <div class="form-main">
        <form id="loginForm" action="{{ url('/signin') }}" method="post">
            {{ csrf_field() }}
            <input class="form-user field" placeholder="Username" type="text" name="email">
            <input class="form-pwd field" placeholder="Password" type="password" name="password">
            @if ($errors->has('email') || $errors->has('password'))
            <label id="password-error" class="error" for="password">Entered data doesn't match any account.</label>
            @endif
            <div class="form-check">
                <input class="form-checkbox" type="checkbox" id="checkbox" name="remember">
                <label for="checkbox"><p>Remember me</p></label>
            </div>
            <div class="captcha-center{{ $errors->has('g-recaptcha-response') ? ' captcha-error' : '' }}">
                <div class="g-recaptcha" data-sitekey="{{ env('RE_CAP_SITE') }}"></div>
            </div>
            <input class="form-button" type="submit" value="login">
        </form>
    </div>
</div>
<script src="https://www.google.com/recaptcha/api.js"></script>
<script src="{{ url('src/js/jquery-3.2.1.min.js') }}"></script>
<script src="{{ url('src/js/jquery.validate.min.js') }}"></script>
<script src="{{ url('src/js/main.js') }}"></script>
<script>
    $("#loginForm").validate({
        rules: {
            email: {
                required: true,
                email: true,
                minlength: 2
            },
            password: {
                required: true,
                minlength: 6
            }
        }
    });
</script>
</body>
</html>