<tr data-id="{{ $data->id }}">
    <td class="edit2-colored" colspan="4">
        <form action="{{ url('/save/airports') }}" method="POST">
            {{ csrf_field() }}
            @if($data->id)
                <input name="id" type="hidden" value="{{ $data->id }}">
            @endif
            <div class="inner-table-edit2">
                <div class="table-edit-element">
                    <p>City:</p>
                    <div class="modal-edit2-input">
                        <select name="city">
                            @foreach($data->cities as $key => $each)
                                <option value="{{ $key }}">{{ $each }}</option>
                            @endforeach
                        </select>
                    </div>
                </div>
                <div class="table-edit-element">
                    <p>Name:</p>
                    <div class="modal-edit2-input">
                        <input type="text" name="name" value="{{ $data->name }}">
                    </div>
                </div>
            </div>
            <div class="inner-table-edit2-buttons">
                @role('edit_directories')
                @role('edit_catalog')
                <button type="button" class="inner-table-confirm2 saveItem"><span class="glyphicon glyphicon-ok"></span></button>
                @endrole
                @endrole
                <button type="button" class="inner-table-decline2 formCancel"><span class="glyphicon glyphicon-remove"></span></button>
            </div>
        </form>
    </td>
</tr>