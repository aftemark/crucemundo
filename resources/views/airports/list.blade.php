<div class="modal-header">
    <div class="country-modal-header">
        <button type="button" class="close country-close" data-dismiss="modal"><img src="/src/img/cancel-icon-md.png" alt="close"></button>
        <h4 class="modal-title">Airports</h4>
    </div>
</div>
<div class="modal-body clearfix">
    <div class="modal-search-block">
        <form class="modal-search-form">
            @role('edit_directories')
            @role('edit_catalog')
            <img id="newItem" class="modal-search-add-button" src="/src/img/plus-modal-icon.png" alt="+">
            @endrole
            @endrole
            <input id="searchCatalogField" class="modal-search-placeholder" type="text" placeholder="Search">
            <button type="reset" class="inner-table-decline inner-modal-search"><span class="glyphicon glyphicon-remove"></span></button>
            <input id="searchCatalog" class="modal-search-submit" type="image" src="/src/img/search-icon.png">
        </form>
    </div>
    <table class="table table-striped custom-table modal-table-airports">
        <thead>
        <tr>
            <th>City</th>
            <th>Name</th>
        </tr>
        </thead>
        <tbody id="catalogTable">
        @foreach ($data as $each)
            <tr data-id="{{ $each->id }}">
                <td>{{ $each->city->name }}</td>
                <td>
                    <div class="table-floating-left-element">{{ $each->name }}</div>
                    <ul class="custom-dropdown-table">
                        <li class="dropdown">
                            <a class="dropdown-toggle inner-table-button-hovered" data-toggle="dropdown" href="#"><span class="glyphicon glyphicon-option-vertical"></span></a>
                            <ul class="dropdown-menu">
                                <li>
                                    <a href="#" class="editItem" data-href="{{ url('/airports/' . $each->id) }}"><img src="/src/img/pen-icon.png" alt=""></a>
                                </li>
                                <li>
                                    <a href="#" class="deleteItem" data-href="{{ url('/delete/airports/' . $each->id) }}"><img src="/src/img/trash-icon.png" alt=""></a>
                                </li>
                            </ul>
                        </li>
                    </ul>
                </td>
            </tr>
        @endforeach
        </tbody>
    </table>
</div>