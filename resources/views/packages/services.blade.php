<div class="row">
    <div class="col-md-4">
        <h3>List of package services</h3>
    </div>
    <div class="col-md-4"></div>
    <div class="col-md-4 right">
        @role('edit_directories')
        @role('edit_packages')
        <button data-toggle="modal" data-target="#newServiceModal" class="btn btn-warning"><span class="glyphicon glyphicon-plus"></span>Add service</button>
        @endrole
        @endrole
    </div>
</div>
<table class="table table-striped">
    <tbody>
    <thead>
    <tr>
        <th>Service type</th>
        <th>Name</th>
        <th>Venue</th>
        <th></th>
    </tr>
    </thead>
    @foreach ($data as $service)
        <tr data-id="{{ $service->id }}">
            @if(isset($service->service_type))
                <td>{{ $service->service_type }}</td>
            @else
                <td>-</td>
            @endif
            @if($service->name)
                <td>{{ $service->name }}</td>
            @else
                <td>-</td>
            @endif
            @if($service->city)
                <td>{{ $service->city->name }}</td>
            @elseif($service->ship)
                <td>{{ $service->ship->name }}</td>
            @elseif($service->hotel)
                <td>{{ $service->hotel->name }}</td>
            @else
                <td>-</td>
            @endif
            <td>
                @role('edit_directories')
                @role('edit_packages')
                <a class="deleteService" data-id="{{ $service->parent_id }}"><span class="glyphicon glyphicon-trash"></span></a>
                @endrole
                @endrole
            </td>
        </tr>
    @endforeach
    </tbody>
</table>

<div class="modal fade" id="newServiceModal" tabindex="-1" role="dialog" aria-labelledby="newServiceModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <form action="{{ url('/save/packageservices/' . $packageid) }}" method="POST">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title" id="newServiceModalLabel">
                        <div class="col-md-6">
                            Add service to package
                        </div>
                        <div class="col-md-6">
                            <div class="row">
                                <div class="col-md-6">
                                    <input type="hidden" name="_token" value="{{ Session::token() }}">
                                    <a class="btn btn-success saveService">Save</a>
                                </div>
                                <div class="col-md-6">
                                    <a data-dismiss="modal" class="btn btn-danger">Cancel</a>
                                </div>
                            </div>
                        </div>
                    </h4>
                </div>
                <div class="modal-body">
                    <div class="row">
                        <div class="form-group col-md-12">
                            <label for="serviceType">Service type</label>
                            <select id="serviceType" class="form-control">
                                <option value="false">Select a service type</option>
                                <option value="excursion">Excursion</option>
                                <option value="transport">Transport</option>
                                <option value="catering">Catering</option>
                                <option value="other">Other</option>
                            </select>
                        </div>
                        <div class="form-group col-md-12">
                            <label for="serviceSelect">Service</label>
                            <select id="serviceSelect" class="form-control" name="service_id">
                                <option value="false">Select a service</option>
                            </select>
                        </div>
                    </div>
                    <div id="newServiceDiv" class="row"> </div>
                </div>
            </form>
        </div>
    </div>
</div>