@extends('layouts.app')

@section('title')Service Packages @endsection

@section('content')
    <div class="search-block">
        <div class="search">
            @include('includes.breadcrumbs')
        </div>
    </div>
    <div class="base-page-navigation-table">
        <div class="search-cities">
            <form>
                <input class="search-placeholder" type="text" name="search" placeholder="Search" value="{{ Request::get('search') }}">
                <input class="search-submit" type="image" src="/src/img/search.png">
            </form>
        </div>
        <div class="base-page-table-label">
            <p>List of service packages</p>
        </div>
        @role('edit_directories')
        @role('edit_packages')
        <div class="base-page-table-add-button">
            <a class="base-page-table-add" href="{{ url('/package/new') }}">Add service package</a>
        </div>
        @endrole
        @endrole
    </div>
    <table id="packages" class="table table-striped custom-table table-serivce-packages">
        <thead>
        <tr>
            <th>Service package
                @include('includes.table-sort', ['sortSearchKeys' => ['search', 'page']])
            </th>
        </tr>
        </thead>
        <tbody>
        @foreach ($data as $each)
        <tr>
            <td>
                <div class="table-floating-left-element">{{ $each->name }}</div>
                <ul class="custom-dropdown-table">
                    <li class="dropdown">
                        <a class="dropdown-toggle inner-table-button-hovered" data-toggle="dropdown" href="#"><span class="glyphicon glyphicon-option-vertical"></span></a>
                        <ul class="dropdown-menu">
                            <li>
                                <a href="{{ url('/package/' . $each->id) }}"><img src="/src/img/pen-icon.png" alt=""></a>
                            </li>
                            @role('edit_directories')
                            @role('edit_packages')
                            <li>
                                <a href="{{ url('/delete/package/' . $each->id) }}" class="delete-check-element"><img src="/src/img/trash-icon.png" alt=""></a>
                            </li>
                            @endrole
                            @endrole
                        </ul>
                    </li>
                </ul>
            </td>
        </tr>
        @endforeach
        </tbody>
    </table>
    <div class="row">
        <div class="col-md-6">
            {{ $data->links() }}
        </div>
        <div class="col-md-6">
            <p>Display:</p>
            <a href="/packages/setnumber?number=10" class="btn btn-default">10</a>
            <a href="/packages/setnumber?number=50" class="btn btn-default">50</a>
            <a href="/packages/setnumber?number=100" class="btn btn-default">100</a>
        </div>
    </div>
@endsection