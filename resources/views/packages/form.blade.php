@extends('layouts.app')

@section('title')Service package @endsection

@section('css')
    <link rel="stylesheet" href="{{ url('/src/css/select2.min.css') }}">
@endsection

@section('content')
    <nav class="navbar second-nav">
        <div class="container-fluid">
            <div class="addships-title pull-left">
                <h3>Service package</h3>
            </div>
            <div class="addships-buttons pull-right">
                @role('edit_directories')
                @role('edit_packages')
                @if($data->id)
                    <button id="savePackage" type="button" class="btn-create">Save</button>
                @else
                    <button id="packageNext" type="button" class="btn-create next-btn">Next</button>
                @endif
                @endrole
                @endrole
                <a class="btn-cancel button-link-cancel" href="{{ url('/packages') }}">Cancel</a>
            </div>
        </div>
    </nav>
    <div class="base-page-table-nav-buttons add-hotel-table-nav">
        <ul id="packageNav" class="nav nav-tabs add-hotel-table-nav-tabs" role="tablist">
            @if(isset($data->id))
                <li class="tab active"><a data-toggle="tab" href="#general">General information</a></li>
                <li class="tab"><a data-toggle="tab" href="#services">Services</a></li>
            @else
                <li class="tab active"><a data-toggle="tab" href="#general">General information</a></li>
            @endif
        </ul>
    </div>
    <div class="tab-content">
        <form id="general" class="tab-pane fade in active">
            {{ csrf_field() }}
            @if($data->id)
                <input name="id" type="hidden" value="{{ $data->id }}">
            @endif
            <div class="add-hotel-table-row1">
                <div class="add-hotel-table-elem add-servicepackage-elem">
                    <p>Name:</p>
                    <input type="text" name="name" value="{{ $data->name }}">
                </div>
            </div>
            <div class="add-hotel-table-row1">
                <div class="add-service-tab-elem">
                    <p>Venue:</p>
                    <select disabled class="add-service-tab-select" id="packageVenue" multiple="multiple"></select>
                </div>
            </div>
            <div class="left-side-details pull-left">
                <div class="modal-left add-service-tab-table-left">
                    <div class="modal-add-excursion-row">
                        <div class="modal-left modal-add-excursion-inner">
                            <div class="modal-add-excursion-inner-element add-service-tab-general-left">
                                <label>Min number of pax:</label>
                                <div class="modal-add-excursion-input-value">
                                    <p>pax</p>
                                </div>
                                <input type="text" name="min_pax" value="{{ $data->min_pax }}">
                            </div>
                        </div>
                        <div class="modal-left modal-add-excursion-inner">
                            <div class="modal-add-excursion-inner-element add-service-tab-general-left">
                                <label>Max number of pax:</label>
                                <div class="modal-add-excursion-input-value">
                                    <p>pax</p>
                                </div>
                                <input type="text" name="max_pax" value="{{ $data->max_pax }}">
                            </div>
                        </div>
                    </div>
                    <div class="modal-add-excursion-inner-element add-service-tab-general-left border-dashed modal-add-catering-price">
                        <label>Price:</label>
                        <div class="modal-add-excursion-input-value">
                            <p><span>eur</span></p>
                        </div>
                        <input type="text" name="price" value="{{ $data->price }}">
                        <div class="cb-property">
                            <div class="base-page-table-nav-buttons md-services-buttons">
                                <label>Condition:</label>
                                <ul id="paxCabinValue" class="nav nav-tabs services-page-tabs md-transport-tabs" role="tablist">
                                    <li class="tab{{ !$data->all_pax_cabin ? ' active' : '' }}"><a href="#" data-toggle="tab" data-value="false">Min 1 pax</a></li>
                                    <li class="tab{{ $data->all_pax_cabin ? ' active' : '' }}"><a href="#" data-toggle="tab" data-value="true">For all pax in the cabins / rooms</a></li>
                                </ul>
                                <input type="hidden" name="all_pax_cabin" value="{{ $data->all_pax_cabin ? 'true' : 'false' }}">
                            </div>
                            <div>
                                <input class="md-other-checkbox add-service-tab-cb" type="checkbox" {{ $data->all_pax_cabin && $data->multiply ? 'checked ' : '' }}{{ !$data->all_pax_cabin ? 'disabled ' : '' }}id="packageMultiply" value="true">
                                <label class="md-other-checkbox-label add-service-tab-cb-label">Multiply price per number of pax</label>
                            </div>
                        </div>
                    </div>
                    <div class="tracking-input-by-cb">
                        <div class="modal-left modal-add-excursion-inner-left">
                            <div class="modal-add-excursion-inner-element add-service-tab-general-left">
                                <input class="tracked-cb add-service-tab-cb" type="checkbox" id="packageAvailability" name="available" value="true" {{ $data->available ? 'checked ' : '' }}>
                                <label class="add-service-tab-cb-label">Available on board</label>
                            </div>
                        </div>
                        <div class="modal-left modal-add-excursion-inner-right">
                            <div class="modal-add-excursion-inner-element add-service-tab-general-left">
                                <div class="modal-add-excursion-input-value">
                                    <p><span>eur</span></p>
                                </div>
                                <input {{ !$data->available && $data->board_price == 0 ? 'disabled ' : '' }}id="packageBoardPrice" name="board_price" type="text" value="{{ $data->board_price }}">

                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="right-side-details pull-right">
                <div class="add-ships-table-row3">
                    <div class="add-service-tab-table-right">
                        <p>Description:</p>
                        <textarea class="add-service-description-ta" name="description">{{ $data->description }}</textarea>
                    </div>
                </div>
                <div class="add-ships-table-row4">
                    <div class="add-service-tab-table-right">
                        <p>Condition:</p>
                        <textarea class="add-service-condition-ta" name="condition_desc">{{ $data->condition_desc }}</textarea>
                    </div>
                </div>
            </div>
        </form>
        <div id="services" class="tab-pane fade"></div>
    </div>
@endsection

@section('script')
    <script src="{{ url('/src/js/select2.min.js') }}"></script>
    <script>
        $( document ).ready(function() {
            $("#packageVenue").select2();

            @if(!Auth::user()->hasRole('edit_packages'))
            $('.content input, .content select, .content textarea').each(function () {
                $(this).prop('disabled', true);
            });

            @endif
            $('#packageAvailability').change(function() {
                $('#packageBoardPrice').attr('disabled', !this.checked)
            });
            $('#paxCabinValue li a').on('click', function () {
                var value = $(this).attr('data-value');
                $('#general input[name=all_pax_cabin]').val(value);
                $('#packageMultiply').attr('disabled', value != 'true');
            });

            @if(isset($data->id))
            var packageID = {{ $data->id }};
            ServicesList(packageID);
            @else
            var packageID = 0;
            @endif
            function ServicesList(id) {
                $.ajax({
                    dataType: "html",
                    method: "GET",
                    cache: false,
                    url: '/packageservices/' + id,
                    success: function (data) {
                        GetVenues(packageID);

                        $("#services").empty().prepend(data);

                        $('#serviceSelect').change(function () {
                            var value = $('#serviceSelect').val();
                            if (value != 'false')
                                ServiceInfo(value);
                        });

                        $('.deleteService').on('click', function() {
                            var dataID = $(this).attr('data-id');
                            $.ajax({
                                dataType: "html",
                                method: "GET",
                                cache: false,
                                url: '/delete/packageservices/' + dataID,
                                success: function () {
                                    ServicesList(packageID);
                                }
                            });
                        });

                        $('#serviceType').change(function () {
                            $('#newServiceDiv').empty();
                            $('#serviceSelect').empty().append('<option value="false">Select a service</option>');
                            if ($(this).val() != 'false') {
                                $.ajax({
                                    dataType: "json",
                                    method: "GET",
                                    cache: false,
                                    url: '/type/services/' + packageID + '?type=' + $(this).val(),
                                    success: function (services) {
                                        $.each(services, function (serviceId ,serviceName) {
                                            $('#serviceSelect').append('<option value="' + serviceId + '">' + serviceName + '</option>');
                                        });
                                        var value = $('#serviceSelect').val();
                                        if (value != 'false')
                                            ServiceInfo(value);
                                    }
                                });
                            }
                        });

                        $('.saveService').on('click', function () {
                            var form = $('#newServiceModal form');
                            $('#newServiceModal').modal('hide');
                            $.ajax({
                                data: form.serialize(),
                                method: form.attr('method'),
                                cache: false,
                                url: form.attr('action'),
                                success: function () {
                                    ServicesList(packageID);
                                }
                            });
                        });
                    }
                });
            }

            function ServiceInfo(id) {
                $.ajax({
                    dataType: "html",
                    method: "GET",
                    cache: false,
                    url: '/serviceinfo/' + id,
                    success: function (serviceinfo) {
                        $('#newServiceDiv').empty().append($(serviceinfo).find('.modal-body'));
                        $('#newServiceDiv input, #newServiceDiv select, #newServiceDiv textarea').each(function() {
                            $(this).attr('disabled', true);
                        });
                    }
                });
            }

            function GetVenues(id) {
                $.ajax({
                    dataType: "json",
                    method: "GET",
                    cache: false,
                    url: '/packages/venues/' + id,
                    success: function (venues) {
                        $("#packageVenue").empty();
                        $.each(venues, function (venueId, venueName) {
                            var option = new Option(venueName, venueId);
                            option.selected = true;

                            $("#packageVenue").append(option).trigger("change");
                        });
                    }
                });
            }

            @if(!isset($data->id))
            $('body').on('click', '#packageNext', function() {
                var packageData = $('#general form').serializeArray();
                var button = $(this);
                $.ajax({
                    type: "POST",
                    url: "/save/package",
                    cache: false,
                    async: false,
                    data: packageData,
                    success: function (ajaxPackageID) {
                        packageID = ajaxPackageID;
                        ServicesList(ajaxPackageID);

                        $('#general form').append('<input type="hidden" name="id" value="' + ajaxPackageID + '">');

                        button.empty().attr('id', 'savePackage').append('<span class="glyphicon glyphicon-ok"></span> Save');

                        $('#packageNav').append('<li><a data-toggle="tab" href="#services">Services</a></li>');
                        $('#packageNav a').eq(1).trigger('click');
                    }
                });
            });
            @endif
            $('body').on('click', '#savePackage', function() {
                $.ajax({
                    type: "POST",
                    url: "/save/package",
                    cache: false,
                    data: $('#general form').serializeArray(),
                    success: function (ajaxHotelID) {
                        window.location.replace('{{ url('/packages') }}');
                    },
                    async: false
                });
            });
        });
    </script>
@endsection