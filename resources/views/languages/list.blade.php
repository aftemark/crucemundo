<div class="modal-header">
    <div class="country-modal-header">
        <button type="button" class="close country-close" data-dismiss="modal"><img src="/src/img/cancel-icon.png" alt="close"></button>
        <h4 class="modal-title">Languages</h4>
    </div>
</div>
<div class="modal-body clearfix">
    <div class="modal-search-block">
        <div class="modal-search-form">
            @role('edit_directories')
            @role('edit_catalog')
            <img id="newItem" class="country-search-add" src="/src/img/plus-modal-icon.png" alt="+">
            @endrole
            @endrole
            <input id="searchCatalogField" class="country-search-placeholder" type="text" placeholder="Search">
            <input id="searchCatalog" class="country-search-submit" type="image" src="/src/img/search-icon.png">
        </div>
    </div>
    <div class="modal-table-language">
        <table class="table table-striped country-table">
            <tbody id="catalogTable">
            @foreach ($data as $each)
                <tr data-id="{{ $each->id }}">
                    <td>
                        <div class="inner-table-notation">
                            <p>{{ $each->name }}</p>
                        </div>
                        <div class="modal-table-links">
                            <a class="table-pen editItem" href="#" data-href="{{ url('/languages/' . $each->id) }}"><img src="/src/img/pen-icon.png" alt=""></a>
                            @role('edit_directories')
                            @role('edit_catalog')
                            <a class="table-trash deleteItem" href="#" data-href="{{ url('/delete/airports/' . $each->id) }}"><img src="/src/img/trash-icon.png" alt=""></a>
                            @endrole
                            @endrole
                        </div>
                    </td>
                </tr>
            @endforeach
            </tbody>
        </table>
    </div>
</div>