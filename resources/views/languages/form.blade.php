<tr data-id="{{ $data->id }}" class="modal-addnew">
    <td>
        <form action="{{ url('/save/languages') }}" method="POST">
            {{ csrf_field() }}
            @if($data->id)
            <input name="id" type="hidden" value="{{ $data->id }}">
            @endif
            <div class="inner-table-edit modal-addnew-value">
                <input type="text" name="name" value="{{ $data->name }}">
            </div>
            <div class="inner-table-edit-buttons">
                @role('edit_directories')
                @role('edit_catalog')
                <button type="button" class="inner-table-confirm saveItem"><span class="glyphicon glyphicon-ok"></span></button>
                @endrole
                @endrole
                <button type="button" class="inner-table-decline formCancel"><span class="glyphicon glyphicon-remove"></span></button>
            </div>
        </form>
    </td>
</tr>