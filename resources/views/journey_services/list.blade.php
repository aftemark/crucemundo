@if(isset($data->itinerary))
    @foreach($days as $day => $cities)
        @php $isFirst = 0; @endphp
        @foreach($cities as $id => $city)
            <div class="service-itinerary-border-dashed">
                <div class="service-itinerary-head-table">
                    <div class="service-itinerary-head-left">
                        @if(++$isFirst == 1)
                        <label>
                            <span class="bold">Day {{ $day }}</span>
                        </label>
                        <br>
                        @endif
                        <label>City: {{ $city->name }}</label>
                    </div>
                    <div class="service-itinerary-head-right">
                        <label>List of services</label>
                    </div>
                </div>
                <form>
                    <table class="table table-striped custom-table service-tabs-table">
                        <thead>
                        <tr>
                            <th>Service name</th>
                            <th>Language</th>
                            <th>Service type</th>
                            <th>Included</th>
                            <th>Optional</th>
                            <th>Display</th>
                            <th>Release date</th>
                            <th>Remind</th>
                        </tr>
                        </thead>
                        @php $packages = []; @endphp
                        <tbody>
                        @foreach ($city->services->where('year_id', Session::get('catalog_year_id')) as $service)
                            <tr>
                                <td>{{ $service->name or '-' }}</td>
                                <td>{{ $service->language->name or '-' }}</td>
                                <td>{{ $service->service_type or '-' }}</td>
                                <td><input class="display-block" type="radio" name="radio_service[{{ $id }}][{{ $service->id }}]" value="included"{{ isset($services[$id]["service"][$service->id]) && $services[$id]["service"][$service->id]["included"] ? ' checked' : '' }}></td>
                                <td><input class="display-block" type="radio" name="radio_service[{{ $id }}][{{ $service->id }}]" value="optional"{{ isset($services[$id]["service"][$service->id]) && $services[$id]["service"][$service->id]["optional"] ? ' checked' : '' }}></td>
                                <td><input class="display-block" type="checkbox" name="display_service[{{ $id }}][{{ $service->id }}]" value="true"{{ isset($services[$id]["service"][$service->id]) && $services[$id]["service"][$service->id]["display"] ? ' checked' : '' }}></td>
                                <td>
                                    <input class="calendar" type="text" name="release_date_service[{{ $id }}][{{ $service->id }}]" value="{{ isset($services[$id]["service"][$service->id]) && $services[$id]["service"][$service->id]["release_date"] ? $services[$id]["service"][$service->id]["release_date"] : '' }}">
                                    <div class="table-calendar-service-tabs modal-add-service-input-value service-general-span">
                                        <span class="glyphicon glyphicon-calendar"></span>
                                    </div>
                                </td>
                                <td><input type="text" name="remind_service[{{ $id }}][{{ $service->id }}]" value="{{ isset($services[$id]["service"][$service->id]) && $services[$id]["service"][$service->id]["remind"] ? $services[$id]["service"][$service->id]["remind"] : '' }}"></td>
                            </tr>
                            @php if (isset($service->packages))
									foreach ($service->packages as $package)
										$packages[$package->package_id] = $package->servicePackage; @endphp
                        @endforeach
                        </tbody>
                        @if(!empty($packages))
                            <thead>
                            <tr>
                                <th colspan="2">Service name</th>
                                <th>Service type</th>
                                <th>Included</th>
                                <th>Optional</th>
                                <th>Dispay</th>
                                <th>Release date</th>
                                <th>Remind</th>
                            </tr>
                            </thead>
                            @foreach ($packages as $package)
                                <tr>
                                    <td colspan="3">{{ $package->name or '-' }}</td>
                                    <td><input class="display-block" type="radio" name="radio_package[{{ $id }}][{{ $package->id }}]" value="included"{{ isset($services[$id]["package"][$package->id]) && $services[$id]["package"][$package->id]["included"] ? ' checked' : '' }}></td>
                                    <td><input class="display-block table-cb" type="radio" name="radio_package[{{ $id }}][{{ $package->id }}]" value="optional"{{ isset($services[$id]["package"][$package->id]) && $services[$id]["package"][$package->id]["optional"] ? ' checked' : '' }}></td>
                                    <td><input class="display-block" type="checkbox" name="display_package[{{ $id }}][{{ $package->id }}]" value="true"{{ isset($services[$id]["package"][$package->id]) && $services[$id]["package"][$package->id]["display"] ? ' checked' : '' }}></td>
                                    <td><input class="calendar" type="text" name="release_date_package[{{ $id }}][{{ $package->id }}]" value="{{ isset($services[$id]["package"][$package->id]) && $services[$id]["package"][$package->id]["release_date"] ? $services[$id]["package"][$package->id]["release_date"] : '' }}"></td>
                                    <td><input type="text" name="remind_package[{{ $id }}][{{ $package->id }}]" value="{{ isset($services[$id]["package"][$package->id]) && $services[$id]["package"][$package->id]["remind"] ? $services[$id]["package"][$package->id]["remind"] : '' }}"></td>
                                </tr>
                            @endforeach
                        @endif
                    </table>
                    @if($data->services->where('itinerary_city_id', $id)->count())
                        <div class="service-itinerary-table-bottom anchor-service-itinerary-insert">
                            @php $countCompatibles = 1; @endphp
                            <p>Non compatible:</p>
                            @if(isset($nonServices[$id]))
                                @foreach($nonServices[$id] as $nonPacks)
                                    <div class="non-compatible">
                                        <div class="service-itinerary-table-bottom-left">
                                            <div class="modal-table-iteniraries-right full-width">
                                                <select name="nonCompatibles[{{ $id }}][{{ $countCompatibles = array_values($nonPacks)[0]->counter }}][]" class="modal-table-iteniraries-right-select nonCompatibles" multiple="multiple">
                                                    @foreach($data->services as $journeyService)
                                                        @if($journeyService->itinerary_city_id == $id)
                                                            <option {{ isset($nonPacks[$journeyService->id]) ? 'selected ' : '' }}value="{{ $journeyService->id }}">{{ $journeyService->service_id === NULL ? $journeyService->package->name : $journeyService->service->name }}</option>
                                                        @endif
                                                    @endforeach
                                                </select>
                                            </div>
                                        </div>
                                        <div class="service-itinerary-table-bottom-right">
                                            <div class="service-itinerary-table-bottom-right-elem">
                                                <span class="glyphicon glyphicon-minus anchor-service-itinerary-remove-select"></span>
                                            </div>
                                        </div>
                                    </div>
                                @endforeach
                            @endif
                            <div class="non-compatible">
                                <div class="service-itinerary-table-bottom-left">
                                    <div class="modal-table-iteniraries-right full-width">
                                        <select name="nonCompatibles[{{ $id }}][{{ isset($nonServices[$day]) ? count($nonServices[$day]) + 1 : '0' }}][]" class="modal-table-iteniraries-right-select nonCompatibles" multiple="multiple">
                                            @foreach($data->services as $journeyService)
                                                @if($journeyService->itinerary_city_id == $id)
                                                    <option value="{{ $journeyService->id }}">{{ $journeyService->service_id === NULL ? $journeyService->package->name : $journeyService->service->name }}</option>
                                                @endif
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                                <div class="service-itinerary-table-bottom-right">
                                    <div class="service-itinerary-table-bottom-right-elem">
                                        <span data-select-type="other" data-select-count="{{ $countCompatibles }}" class="glyphicon glyphicon-plus anchor-service-itinerary-add-select"></span>
                                    </div>
                                </div>
                            </div>
                        </div>
                    @endif
                </form>
            </div>
        @endforeach
    @endforeach
    <form class="service-itinerary-border-dashed">
        <div class="service-itinerary-head-table">
            <div class="service-itinerary-head-left">
                <label>Other services</label>
            </div>
            <div class="service-itinerary-head-right">
                <label>List of services</label>
            </div>
        </div>
        <table class="table table-striped custom-table service-tabs-table other-service-tab-table">
            <thead>
            <tr>
                <th>Service name</th>
                <th>Included</th>
                <th>Optional</th>
                <th>Display</th>
                <th>Release date</th>
                <th>Remind</th>
            </tr>
            </thead>
            <tbody>
            @php $packages = array(); @endphp
            @foreach ($otherServices as $otherService)
                <tr>
                    <td>{{ $otherService->name or '-' }}</td>
                    <td><input class="display-block" type="radio" name="radio_service[other][{{ $otherService->id }}]" value="included"{{ isset($services["other"]["service"][$otherService->id]) && $services["other"]["service"][$otherService->id]["included"] ? ' checked' : '' }}></td>
                    <td><input class="display-block" type="radio" name="radio_service[other][{{ $otherService->id }}]" value="optional"{{ isset($services["other"]["service"][$otherService->id]) && $services["other"]["service"][$otherService->id]["optional"] ? ' checked' : '' }}></td>
                    <td><input class="display-block" type="checkbox" name="display_service[other][{{ $otherService->id }}]" value="true"{{ isset($services["other"]["service"][$otherService->id]) && $services["other"]["service"][$otherService->id]["display"] ? ' checked' : '' }}></td>
                    <td><input type="text" name="release_date_service[other][{{ $otherService->id }}]" value="{{ isset($services["other"]["service"][$otherService->id]) && $services["other"]["service"][$otherService->id]["release_date"] ? $services["other"]["service"][$otherService->id]["release_date"] : '' }}"></td>
                    <td><input type="text" name="remind_service[other][{{ $otherService->id }}]" value="{{ isset($services["other"]["service"][$otherService->id]) && $services["other"]["service"][$otherService->id]["remind"] ? $services["other"]["service"][$otherService->id]["remind"] : '' }}"></td>
                </tr>
                @php if (isset($otherService->packages))
                    foreach ($otherService->packages as $package)
                        $packages[$package->package_id] = $package->servicePackage; @endphp
            @endforeach
            @foreach ($packages as $package)
                <tr>
                    <td>{{ $package->name or '-' }}</td>
                    <td><input class="display-block" type="radio" name="radio_package[other][{{ $package->id }}]" value="included"{{ isset($services["other"]["package"][$package->id]) && $services["other"]["package"][$package->id]["included"] ? ' checked' : '' }}></td>
                    <td><input class="display-block" type="radio" name="radio_package[other][{{ $package->id }}]" value="optional"{{ isset($services["other"]["package"][$package->id]) && $services["other"]["package"][$package->id]["optional"] ? ' checked' : '' }}></td>
                    <td><input class="display-block" type="checkbox" name="display_package[other][{{ $package->id }}]" value="true"{{ isset($services["other"]["package"][$package->id]) && $services["other"]["package"][$package->id]["display"] ? ' checked' : '' }}></td>
                    <td><input type="text" name="release_date_package[other][{{ $package->id }}]" value="{{ isset($services["other"]["package"][$package->id]) && $services["other"]["package"][$package->id]["release_date"] ? $services["other"]["package"][$package->id]["release_date"] : '' }}"></td>
                    <td><input type="text" name="remind_package[other][{{ $package->id }}]" value="{{ isset($services["other"]["package"][$package->id]) && $services["other"]["package"][$package->id]["remind"] ? $services["other"]["package"][$package->id]["remind"] : '' }}"></td>
                </tr>
            @endforeach
            </tbody>
        </table>
    </form>
    <form>
        <p>Non compatible:</p>
        <div class="service-itinerary-table-bottom anchor-service-itinerary-insert">
            @php $countCompatibles = 1; @endphp
            @if(isset($nonServices["other"]))
                @foreach($nonServices["other"] as $nonOtherPacks)
                    <div class="service-itinerary-table-bottom-left">
                        <div class="modal-table-iteniraries-right full-width">
                            <select name="nonCompatibles[other][{{ $countCompatibles = array_values($nonOtherPacks)[0]->counter }}][]" class="modal-table-iteniraries-right-select nonCompatibles" multiple="multiple">
                                @foreach($data->services->where('itinerary_city_id', NULL) as $journeyService)
                                    <option {{ isset($nonOtherPacks[$journeyService->id]) ? 'selected ' : '' }}value="{{ $journeyService->id }}">{{ $journeyService->service_id === NULL ? $journeyService->package->name : $journeyService->service->name }}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>
                    <div class="service-itinerary-table-bottom-right">
                        <div class="service-itinerary-table-bottom-right-elem">
                            <span class="glyphicon glyphicon-minus anchor-service-itinerary-remove-select"></span>
                        </div>
                    </div>
                @endforeach
            @endif
            <div class="service-itinerary-table-bottom-left">
                <div class="modal-table-iteniraries-right full-width">
                    <select name="nonCompatibles[other][0][]" class="modal-table-iteniraries-right-select nonCompatibles" multiple="multiple">
                        @foreach($data->services->where('itinerary_city_id', NULL) as $journeyService)
                            <option value="{{ $journeyService->id }}">{{ $journeyService->service_id === NULL ? $journeyService->package->name : $journeyService->service->name }}</option>
                        @endforeach
                    </select>
                </div>
            </div>
            <div class="service-itinerary-table-bottom-right">
                <div class="service-itinerary-table-bottom-right-elem">
                    <span data-select-type="other" data-select-count="{{ $countCompatibles }}" class="glyphicon glyphicon-plus anchor-service-itinerary-add-select"></span>
                </div>
            </div>
        </div>
    </form>
@endif