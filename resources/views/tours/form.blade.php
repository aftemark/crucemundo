@extends('layouts.app')

@section('title')Service tour @endsection

@section('css')
    <link rel="stylesheet" href="{{ url('/src/css/select2.min.css') }}">
@endsection

@section('content')
    <nav class="navbar second-nav">
        <div class="container-fluid">
            <div class="addships-title pull-left">
                @if(!empty($data->code))
                    <h3>{{ $data->code }}</h3>
                @else
                    <h3>New tour</h3>
                @endif
            </div>
            <div class="addships-buttons pull-right">
                @if(isset($data->id))
                    <a href="{{ url('/copy/tour/' . $data->id) }}" class="btn-create copy-btn">Copy</a>
                @endif
                @role('edit_directories')
                @role('edit_tours')
                @if(isset($data->id))
                    <button type="button" id="saveTour" class="btn-create save-btn">Save</button>
                @else
                    <button type="button" id="tourNext" class="btn-create next-btn">Next</button>
                @endif
                @endrole
                @endrole
                <a class="btn-cancel button-link-cancel" href="{{ url('/tours') }}">Cancel</a>
            </div>
        </div>
    </nav>
    <div class="base-page-table-nav-buttons add-hotel-table-nav">
        <ul id="tourNav" class="nav nav-tabs tour-tabs anchor-tabs-next" role="tablist">
            @if(isset($data->id))
                <li class="tab active"><a href="#general" data-toggle="tab">General information</a></li>
                <li class="tab"><a href="#services" data-toggle="tab">Itinerary and services</a></li>
                <li class="tab"><a href="#rates" data-toggle="tab">Rates</a></li>
                <li class="tab"><a href="#booking" data-toggle="tab">Booking and payment Conditions</a></li>
                @role('show_cruises_implementation')
                <li class="tab"><a href="#implementation" data-toggle="tab">Implementation</a></li>
                @endrole
                <li class="tab"><a href="#specials" data-toggle="tab">Specials</a></li>
                @role('show_cruises_reservations')
                <li class="tab"><a class="anchor-service-tabs-show-save" href="#reservations" data-toggle="tab">Reservations</a></li>
                @endrole
            @else
                <li class="tab active"><a href="#general" data-toggle="tab">General information</a></li>
            @endif
        </ul>
    </div>
    <div class="tab-content">
        <div id="general" class="tab-pane fade in active">
            <form>
                {{ csrf_field() }}
                <input type="hidden" name="type" value="tour">
                @if(isset($data->id))
                    <input name="id" type="hidden" value="{{ $data->id }}">
                @endif
                <div class="service-general-form-body dashed">
                    <div class="left-side-details pull-left">
                        <div class="modal-left add-service-tab-table-left">
                            <div class="modal-add-excursion-row">
                                <div class="modal-left tab-service-general">
                                    <label>ID:</label>
                                    <input type="text" name="code" value="{{ $data->code }}">
                                </div>
                                <div class="modal-left tab-service-general">
                                    <label>Hotel:</label>
                                    <select name="hotel">
                                        @if(!isset($data->hotel_id))
                                            <option value="false">Select hotel</option>
                                        @endif
                                        @foreach($hotels as $hotel)
                                            <option {{ $data->hotel_id == $hotel->id ? 'selected ' : '' }}value="{{ $hotel->id }}">{{ $hotel->name }}</option>
                                        @endforeach
                                    </select>
                                </div>
                                <div class="add-service-general-row dashed">
                                    <div class="modal-left modal-add-service-left-inner">
                                        <div class="modal-add-excursion-inner-element add-service-tab-general-left">
                                            <label>Season:</label>
                                            <select name="season">
                                                @if(!isset($data->season))
                                                    <option value="false">Select season</option>
                                                @endif
                                                <option {{ $data->season == 'high' ? 'selected ' : '' }}value="high">High</option>
                                                <option {{ $data->season == 'middle' ? 'selected ' : '' }}value="middle">Middle</option>
                                                <option {{ $data->season == 'low' ? 'selected ' : '' }}value="low">Low</option>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="modal-left modal-add-service-right-inner">
                                        <div class="modal-add-excursion-inner-element add-service-tab-general-left">
                                            <label>Depature date:</label>
                                            <div class="modal-add-service-input-value service-general-span">
                                                <span class="glyphicon glyphicon-calendar"></span>
                                            </div>
                                            <input type="text" value="{{ $data->depart_date }}" name="depart_date">
                                        </div>
                                    </div>
                                </div>
                                <div class="add-service-general-row">
                                    <div class="modal-left service-general-left-elements">
                                        <div class="service-general-left-element add-service-tab-general-left">
                                            <label>Days:</label>
                                            <input id="tourDays" type="text" disabled value="{{ isset($data->itinerary) ? $data->itinerary->nights + 1 : '' }}">
                                        </div>
                                    </div>
                                    <div class="modal-left service-general-left-elements service-general-center-element">
                                        <div class="service-general-left-element add-service-tab-general-left">
                                            <label>Nights:</label>
                                            <input id="tourNights" type="text" disabled value="{{ isset($data->itinerary) ? $data->itinerary->nights : '' }}">
                                        </div>
                                    </div>
                                    <div class="modal-left modal-add-service-right-inner">
                                        <div class="modal-add-excursion-inner-element add-service-tab-general-left">
                                            <label>Ending date:</label>
                                            <div class="modal-add-service-input-value service-general-span">
                                                <span class="glyphicon glyphicon-calendar"></span>
                                            </div>
                                            <input id="tourEndDate" type="text" disabled value="{{ $data->ending_date }}">
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="right-side-details pull-right">
                        <div class="add-ships-table-row3">
                            <div class="add-service-general-left-row">
                                <div class="modal-add-service-right-element">
                                    <div class="service-general-rdate add-service-tab-general-left">
                                        <label>Release date:</label>
                                        <div class="modal-add-service-input-value service-general-span">
                                            <span class="glyphicon glyphicon-calendar"></span>
                                        </div>
                                        <input name="journey_release_date" type="text" value="{{ $data->release_date }}">
                                    </div>
                                </div>
                                <div class="modal-add-service-left-element">
                                    <div class="service-general-femind add-service-tab-general-left">
                                        <label>Remind:</label>
                                        <div class="modal-add-service-input-value service-general-text-span">
                                            <span>days before release date</span>
                                        </div>
                                        <input name="journey_remind" type="text" value="{{ $data->remind }}">
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="add-ships-table-row4">
                            <div class="add-service-tab-table-right service-general-ta">
                                <p>Note:</p>
                                <textarea name="description" class="add-service-condition-ta">{{ $data->description }}</textarea>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="base-page-table-nav-buttons services-tour-type-tabs">
                    <ul class="nav nav-tabs tour-tabs " role="tablist">
                        <li class="tab active"><a href="#tabTour" data-toggle="tab">Tour</a></li>
                        <li class="tab"><a href="#tabExtansion" data-toggle="tab">Extension</a></li>
                    </ul>
                </div>
                <div class="general-bottom-tabs-left">
                    <label>Cruise:</label>
                    <select name="cruise" {{ $data->journey_id ? '' : 'disabled ' }}>
                        @if(!isset($data->journey_id))
                            <option value="false">Select cruise</option>
                        @endif
                        @foreach($cruises as $cruise)
                            <option {{ $data->journey_id == $cruise->id ? 'selected ' : '' }}value="{{ $cruise->id }}">{{ $cruise->code }}</option>
                        @endforeach
                    </select>
                </div>
                <div class="general-bottom-tabs-right">
                    <label>Extension type:</label>
                    <select name="extension_type" {{ $data->journey_id ? '' : 'disabled ' }}>
                        @if(!isset($data->journey_id))
                            <option value="false">Select extension type</option>
                        @endif
                        <option {{ $data->extension_type == 'pre' && isset($data->journey_id) ? 'selected ' : '' }}value="pre">Pre tour</option>
                        <option {{ $data->extension_type == 'post' && isset($data->journey_id) ? 'selected ' : '' }}value="post">Post tour</option>
                    </select>
                </div>
            </form>
        </div>
        <div id="services" class="tab-pane fade">
            <div class="service-itinerary-body">
                <div class="service-itinerary-head">
                    <div class="service-itinerary-head-elem">
                        <label>Itinerary:</label>
                        <select name="itinerary">
                            @if(!$data->itinerary_id)
                                <option value="false">Select itinerary</option>
                            @endif
                            @foreach($itineraries as $itinerary)
                                <option data-id="{{ $itinerary->from_to }}" {{ $data->itinerary_id == $itinerary->id ? 'selected ' : '' }}value="{{ $itinerary->id }}">{{ $itinerary->name }}</option>
                            @endforeach
                        </select>
                    </div>
                    <div class="service-itinerary-head-right-elems">
                        <div class="service-itinerary-head-elem">
                            <label>Departure - Destination:</label>
                            <input disabled id="journeyFromTo" type="text" value="{{ $data->itinerary->from_to or '' }}">
                        </div>
                    </div>
                    <div class="itinerary_services">
                        @include('journey_services.list')
                    </div>
                </div>
            </div>
        </div>
        <div id="rates" class="tab-pane fade">
            @if(isset($data->hotel))
                <form class="tab-rates-tables">
                    <label class="tab-rates-labels label-service-tabs-uppercase">List of rooms</label>
                    <table class="table table-striped custom-table service-tabs-table other-service-tab-table tour-tab-rates-table">
                        <thead>
                        <tr>
                            <th>ID</th>
                            <th>NET price</th>
                            <th>REC price</th>
                            <th>Number of rooms</th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach ($data->placements as $placement)
                            <tr>
                                <td>{{ $placement->room->type->name }}</td>
                                <td><input type="text" name="netPrice[{{ $placement->room_id }}]" value="{{ $placement->price_net }}"></td>
                                <td><input type="text" name="recPrice[{{ $placement->room_id }}]" value="{{ $placement->price_rec }}"></td>
                                <td><input type="text" name="roomsAmount[{{ $placement->room_id }}]" value="{{ $placement->amount }}"></td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                    <?php $supplements_count  = 0; ?>
                    @foreach($data->groups as $group)
                        <div class="service-rates-table-bottom">
                            <div class="anchor-service-rates-insert">
                                <div class="tour-service-rates-table-bottom-elem">
                                    <div class="modal-add-service-right-element tab-rates-bottom-table-elems">
                                        <div class="service-general-rdate add-service-tab-general-left">
                                            <label>Single supplement:</label>
                                            <div class="modal-add-service-input-value service-general-span">
                                                <span>%</span>
                                            </div>
                                            <input type="text" name="supplement_rate[{{ ++$supplements_count }}]" value="{{ $group->percent }}">
                                        </div>
                                    </div>
                                    <div class="modal-add-service-right-element tab-rates-bottom-table-elems">
                                        <div class="service-general-rdate add-service-tab-general-left">
                                            <div class="service-itinerary-table-bottom-right">
                                                <div class="service-itinerary-table-bottom-right-elem service-rates-bottom-spans">
                                                    <span class="glyphicon glyphicon-minus anchor-service-rates-remove"></span>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="modal-table-iteniraries-right full-width tab-rates-bottom-table">
                                <select class="modal-table-iteniraries-right-select supplementGroup" multiple="multiple" name="supplement_group[{{ $supplements_count }}][]">
                                    @foreach ($group->placements as $placement)
                                        <option selected value="{{ $placement->room_id }}">{{ $placement->room->type->name }}</option>
                                    @endforeach
                                    @foreach ($roomCategories as $id => $roomCategory)
                                        <option {{ in_array($id, $selectedRooms) && $group->room_id != $id ? 'disabled ' : '' }}{{ $group->room_id == $id ? 'selected ' : '' }}value="{{ $id }}">{{ $roomCategory }}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                    @endforeach
                    <div class="service-rates-table-bottom">
                        <div class="anchor-service-rates-insert">
                            <div class="service-rates-table-bottom-elem">
                                <div class="modal-add-service-right-element tab-rates-bottom-table-elems">
                                    <div class="service-general-rdate add-service-tab-general-left">
                                        <label>Single supplement:</label>
                                        <div class="modal-add-service-input-value service-general-span">
                                            <span>%</span>
                                        </div>
                                        <input type="text" name="supplement_rate[0]">
                                    </div>
                                </div>
                                <div class="modal-add-service-right-element tab-rates-bottom-table-elems">
                                    <div class="service-general-rdate add-service-tab-general-left">
                                        <label>Number of rooms available with a surcharge of less than 100%:</label>
                                        <input class="tab-rates-number-of-rooms" type="text" name="supplement_group[0][]">
                                        <div class="service-itinerary-table-bottom-right">
                                            <div class="service-itinerary-table-bottom-right-elem service-rates-bottom-spans">
                                                <span class="glyphicon glyphicon-plus anchor-service-rates-add"></span>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="modal-table-iteniraries-right full-width tab-rates-bottom-table">
                            <select class="modal-table-iteniraries-right-select supplementGroup" multiple="multiple" name="supplement_group[0][]">
                                @foreach ($roomCategories as $id => $roomCategory)
                                    @if(!in_array($id, $selectedRooms) || $group->room_id == $id)
                                        <option value="{{ $id }}">{{ $roomCategory }}</option>
                                    @endif
                                @endforeach
                            </select>
                        </div>
                    </div>
                </form>
            @endif
        </div>
        <div id="booking" class="tab-pane fade">
            <div class="tab-rates-tables">
                <div class="tab-condition-row">
                    <div class="modal-add-service-right-element tab-conditions-elem">
                        <div class="service-general-rdate add-service-tab-general-left tab-conditions-payment">
                            <label>Terms of payment:</label>
                            <div class="modal-add-service-input-value service-general-span">
                                <span>%</span>
                            </div>
                            <input type="text" name="booking_percent" value="{{ $data->booking_percent ? $data->booking_percent : '' }}">
                        </div>
                        <p>on booking ( non- refundable deposit)</p>
                    </div>
                </div>
                <div class="tab-condition-row2">
                    <div class="modal-add-service-right-element tab-conditions-elem">
                        <p>Full payment</p>
                        <div class="service-general-rdate add-service-tab-general-left tab-conditions-payment">
                            <div class="modal-add-service-input-value service-general-span">
                                <span>days</span>
                            </div>
                            <input type="text" name="full_pay" value="{{ $data->full_pay }}">
                        </div>
                        <p>before the tour</p>
                    </div>
                </div>
                <div class="tab-condition-row3">
                    <div class="anchor-service-tab-option-insert">
                        <div class="tab-conditions-elem-label">
                            <label>Cancellation fees:</label>
                        </div>
                        @php $feecount = 0; @endphp
                        @foreach($data->fees as $fee)
                            <div class="modal-add-service-right-element tab-conditions-elem">
                                <p class="first-p">Up to</p>
                                <input class="input-for-upto" type="text" name="fee_from[{{ ++$feecount }}]" value="{{ $fee->from }}">
                                <p class="tab-conditions-elem-dash">-</p>
                                <input {{ isset($fee->to) ? 'checked ' : '' }}type="checkbox">
                                <input {{ isset($fee->to) ? '' : 'disabled ' }}class="input-for-upto feeToInput" type="text" name="fee_to[{{ $feecount }}]" value="{{ $fee->to }}">
                                <p>days before embarkation:</p>
                                <div class="service-general-rdate add-service-tab-general-left tab-conditions-payment-days">
                                    <div class="modal-add-service-input-value service-general-span">
                                        <span>%</span>
                                    </div>
                                    <input type="text" name="fee_percent[{{ $feecount }}]" value="{{ $fee->percent }}">
                                </div>
                                <p>of tour price</p>
                                <div class="service-itinerary-table-bottom-right">
                                    <div class="service-itinerary-table-bottom-right-elem service-rates-bottom-spans">
                                        <span class="glyphicon glyphicon-minus anchor-service-option-remove"></span>
                                    </div>
                                </div>
                            </div>
                        @endforeach
                        <div class="modal-add-service-right-element tab-conditions-elem">
                            <p class="first-p">Up to</p>
                            <input class="input-for-upto" type="text" name="fee_from[0]">
                            <p class="tab-conditions-elem-dash">-</p>
                            <input type="checkbox">
                            <input disabled class="input-for-upto feeToInput" type="text" name="fee_to[0]">
                            <p>days before embarkation:</p>
                            <div class="service-general-rdate add-service-tab-general-left tab-conditions-payment-days">
                                <div class="modal-add-service-input-value service-general-span">
                                    <span>%</span>
                                </div>
                                <input type="text" name="fee_percent[0]">
                            </div>
                            <p>of tour price</p>
                            <div class="service-itinerary-table-bottom-right">
                                <div class="service-itinerary-table-bottom-right-elem service-rates-bottom-spans">
                                    <span class="glyphicon glyphicon-plus anchor-service-option-add"></span>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        @role('show_tours_implementation')
        <form id="implementation" class="tab-pane fade">
            @if(!empty($types_info))
                <div class="tab-implementation-row">
                    <div class="tab-implementation-row-left">
                        <label>Tour details:</label>
                        <table class="table table-striped custom-table tab-implementation-table">
                            <tbody>
                            <tr>
                                <td>
                                    <div class="tab-implementation-table-cell-left">Factual number of rooms / pax:</div>
                                    <div class="tab-implementation-table-cell-right">{{ $types_info["overall"]["factual_rooms"] }}/{{ $types_info["overall"]["factual_pax"] }}</div>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <div class="tab-implementation-table-cell-left">Rooms / Pax to sell:</div>
                                    <div class="tab-implementation-table-cell-right">{{ $types_info["overall"]["sell_rooms"] }}/{{ $types_info["overall"]["sell_pax"] }}</div>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <div class="tab-implementation-table-cell-left">Rooms / Pax with Names:</div>
                                    <div class="tab-implementation-table-cell-right">{{ $types_info["overall"]["names_rooms"] }}/{{ $types_info["overall"]["names_pax"] }}</div>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <div class="tab-implementation-table-cell-left">Rooms / Pax booked (no Names):</div>
                                    <div class="tab-implementation-table-cell-right">{{ $types_info["overall"]["no_names_rooms"] }}/{{ $types_info["overall"]["no_names_pax"] }}</div>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <div class="tab-implementation-table-cell-left">Rooms / Pax on option:</div>
                                    <div class="tab-implementation-table-cell-right">{{ $types_info["overall"]["option_rooms"] }}/{{ $types_info["overall"]["option_pax"] }}</div>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <div class="tab-implementation-table-cell-left">Rooms / Pax booked & option:</div>
                                    <div class="tab-implementation-table-cell-right">{{ $types_info["overall"]["booked_options_rooms"] }}/{{ $types_info["overall"]["booked_options_pax"] }}</div>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <div class="tab-implementation-table-cell-left">Rooms / Pax available:</div>
                                    <div class="tab-implementation-table-cell-right">{{ $types_info["overall"]["available_rooms"] }}/{{ $types_info["overall"]["available_pax"] }}</div>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <div class="tab-implementation-table-cell-left">Rooms / Pax Inquiry:</div>
                                    <div class="tab-implementation-table-cell-right">{{ $types_info["overall"]["inquiry_rooms"] }}/{{ $types_info["overall"]["inquiry_pax"] }}</div>
                                </td>
                            </tr>
                            </tbody>
                        </table>
                    </div>
                    <div class="tab-implementation-row-right"></div>
                </div>
                <div class="tab-implementation-row">
                    @php $countTypes = 0; @endphp
                    @foreach($types_info["types"] as $tour_type)
                        <div class="tab-implementation-row-{{ ++$countTypes % 2 == 0 ? 'right' : 'left' }}">
                            <label>{{ $tour_type["name"] }}:</label>
                            <table class="table table-striped custom-table tab-implementation-table">
                                <tbody>
                                <tr>
                                    <td>
                                        <div class="tab-implementation-table-cell-left">Factual number of rooms / pax:</div>
                                        <div class="tab-implementation-table-cell-right">{{ $tour_type["factual_rooms"] }}/{{ $tour_type["factual_pax"] }}</div>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <div class="tab-implementation-table-cell-left">Rooms / Pax to sell:</div>
                                        <div class="tab-implementation-table-cell-right">{{ $tour_type["sell_rooms"] }}/{{ $tour_type["sell_pax"] }}</div>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <div class="tab-implementation-table-cell-left">Rooms / Pax with Names:</div>
                                        <div class="tab-implementation-table-cell-right">{{ $tour_type["names_rooms"] }}/{{ $tour_type["names_pax"] }}</div>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <div class="tab-implementation-table-cell-left">Rooms / Pax booked (no Names):</div>
                                        <div class="tab-implementation-table-cell-right">{{ $tour_type["no_names_rooms"] }}/{{ $tour_type["no_names_pax"] }}</div>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <div class="tab-implementation-table-cell-left">Rooms / Pax on option:</div>
                                        <div class="tab-implementation-table-cell-right">{{ $tour_type["sell_rooms"] }}/{{ $tour_type["sell_pax"] }}</div>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <div class="tab-implementation-table-cell-left">Rooms / Pax booked & option:</div>
                                        <div class="tab-implementation-table-cell-right">{{ $tour_type["booked_options_rooms"] }}/{{ $tour_type["booked_options_pax"] }}</div>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <div class="tab-implementation-table-cell-left">Rooms / Pax available:</div>
                                        <div class="tab-implementation-table-cell-right">{{ $tour_type["available_rooms"] }}/{{ $tour_type["available_pax"] }}</div>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <div class="tab-implementation-table-cell-left">Rooms / Pax Inquiry:</div>
                                        <div class="tab-implementation-table-cell-right">{{ $tour_type["inquiry_rooms"] }}/{{ $tour_type["inquiry_pax"] }}</div>
                                    </td>
                                </tr>
                                </tbody>
                            </table>
                        </div>
                    @endforeach
                </div>
            @endif
        </form>
        @endrole
        <div id="specials" class="tab-pane fade">
            <div class="tab-specials">
                <div class="base-page-navigation-table">
                    <div class="base-page-table-label">
                        <p>List of specials</p>
                    </div>
                    @role('edit_tours')
                    <div class="base-page-table-add-button">
                        <button data-id="new" class="base-page-table-add editSpecial">Add special</button>
                    </div>
                </div>
                @endrole
            </div>
            <table class="table table-striped custom-table tab-specials-table">
                <thead>
                <tr>
                    <th>Start</th>
                    <th>Expiry</th>
                    <th>Description</th>
                </tr>
                </thead>
                <tbody>
                @if(isset($data->specials))
                    @foreach ($data->specials as $special)
                        <tr>
                            <td>{{ $special->date_start or '-' }}</td>
                            <td>{{ $special->date_end or '-' }}</td>
                            <td>
                                <div class="table-floating-left-element">{{ $special->description or '-'  }}</div>
                                <ul class="custom-dropdown-table">
                                    <li class="dropdown">
                                        <a class="dropdown-toggle inner-table-button-hovered" data-toggle="dropdown" href="#"><span class="glyphicon glyphicon-option-vertical"></span></a>
                                        <ul class="dropdown-menu">
                                            <li>
                                                <a data-id="{{ $special->id }}" class="editSpecial" href="#"><img src="/src/img/pen-icon.png" alt=""></a>
                                            </li>
                                            @role('edit_directories')
                                            @role('edit_tours')
                                            <li>
                                                <a href="#" data-id="{{ $special->id }}" class="deleteSpecial"><img src="/src/img/trash-icon.png" alt=""></a>
                                            </li>
                                            @endrole
                                            @endrole
                                        </ul>
                                    </li>
                                </ul>
                            </td>
                        </tr>
                    @endforeach
                @endif
                </tbody>
            </table>
        </div>
        @role('show_tours_reservations')
        <div id="reservations" class="tab-pane fade">
            @include('reservations.model_list')
        </div>
        @endrole
    </div>

    <div class="modal fade" id="specialModal" tabindex="-1" role="dialog" aria-labelledby="specialModalLabel">
        <div class="modal-dialog" role="document">
            <form class="modal-content" method="POST"></form>
        </div>
    </div>
@endsection

@section('script')
    <script src="{{ url('/src/js/select2.min.js') }}"></script>
    <script>
        $( document ).ready(function() {
            $('input[name=tour_type]').change(function() {
                $('select[name=cruise]').attr('disabled', $(this).val() == 'false');
                $('select[name=extension_type]').attr('disabled', $(this).val() == 'false');
            });

            $('#services select[name=itinerary]').change(function() {
                if ($(this).val() != 'false')
                    $('#journeyFromTo').val($(this).find('option[value=' + $(this).val() + ']').attr('data-id'));
                else
                    $('#journeyFromTo').val('');

                var data = { itinerary_id: $(this).val() };
                if (tourID)
                    data["journey_id"] = tourID;
                $.ajax({
                    dataType: "html",
                    method: "GET",
                    cache: false,
                    url: '/services/tour',
                    data: data,
                    success: function (itineraries_service) {
                        $('#services .itinerary_services').empty().append(itineraries_service);
                        $(".nonCompatibles").select2();
                    }
                });
            });

            $("#tourVenue, .supplementGroup, .nonCompatibles").select2();

            $('body').on('select2:select', '.supplementGroup', function (e) {
                var $this = $(this);
                $this.closest('.tab-rates-tables').find('option[value=' + e.params.data.id + ']').not(e.params.data.element).prop('disabled', true);
                $(".tab-rates-tables .supplementGroup").select2();
            });

            $('body').on('select2:unselect', '.supplementGroup', function (e) {
                $(this).closest('.tab-rates-tables').find('option[value=' + e.params.data.id + ']').prop('disabled', false);
                $(".tab-rates-tables .supplementGroup").select2();
            });

            $('body').on('click', '.anchor-service-rates-remove', function () {
                $(this).closest('.service-rates-table-bottom').remove();
            });

            $('.anchor-service-rates-add').on('click', function () {
                var $supplement = $(this).closest('.service-rates-table-bottom'),
                    $options = $supplement.find('select').clone();

                $options.find('option').removeAttr("selected");
                supplementsCount++;

                $('<div class="service-rates-table-bottom"><div class="anchor-service-rates-insert"><div class="service-rates-table-bottom-elem"><div class="modal-add-service-right-element tab-rates-bottom-table-elems"><div class="service-general-rdate add-service-tab-general-left"><label>Single supplement:</label><div class="modal-add-service-input-value service-general-span"><span>%</span></div><input type="text" name="supplement_rate[' + supplementsCount + ']"></div></div><div class="modal-add-service-right-element tab-rates-bottom-table-elems"><div class="service-general-rdate add-service-tab-general-left"><label>Number of cabins available with a surcharge of less than 100%:</label><input class="tab-rates-number-of-cabins" type="text" name="placements_amount[' + supplementsCount + ']"><div class="service-itinerary-table-bottom-right"><div class="service-itinerary-table-bottom-right-elem service-rates-bottom-spans"><span class="glyphicon glyphicon-minus anchor-service-rates-remove"></span></div></div></div></div></div></div><div class="modal-table-iteniraries-right full-width tab-rates-bottom-table"><select class="modal-table-iteniraries-right-select supplementGroup" multiple="multiple" name="supplement_group[' + supplementsCount + '][]">' + $options.html() + '</select></div></div>').insertBefore($supplement);
                $(".supplementGroup").select2();
            });

            $('.anchor-service-itinerary-add-select').on('click', function() {
                var selectType = $(this).attr('data-select-type'),
                        selectCount = $(this).attr('data-select-count'),
                        $thisBlock = $(this).closest('.non-compatible'),
                        $options = $thisBlock.find('select').clone();
                $(this).attr('data-select-count', parseInt(selectCount) + 1);

                $options.find('option').removeAttr("selected");

                $('<div class="non-compatible"><div class="service-itinerary-table-bottom anchor-service-itinerary-insert anchor-copied"><div class="service-itinerary-table-bottom-left"><div class="modal-table-iteniraries-right full-width"><select data-select-type="' + selectType + '" data-select-count="' + selectCount + '" class="modal-table-iteniraries-right-select" multiple="multiple">' + $options.html() + '</select></div></div><div class="service-itinerary-table-bottom-right"><div class="service-itinerary-table-bottom-right-elem"><span class="glyphicon glyphicon-minus anchor-service-itinerary-remove-select"></span></div></div></div></div>').insertBefore($thisBlock);

                $(".modal-table-iteniraries-right-select").select2();
            });

            @if(!Auth::user()->hasRole('edit_tours'))
            $('.content input, .content select, .content textarea').each(function () {
                $(this).prop('disabled', true);
            });

            @endif
            $('#tourAvailability').change(function() {
                $('#tourBoardPrice').attr('disabled', !this.checked)
            });
            $('input[name=all_pax_cabin]').change(function() {
                if($(this).val() == 'false')
                    $('#tourMultiply').attr('disabled', true)
                else
                    $('#tourMultiply').attr('disabled', false)
            });

            @if(isset($data->id))
            var tourID = '{{ $data->id }}';
            EditDeleteSpecials();
            modelReservations(tourID, 'tour', '{{ Session::token() }}', $('#reservations'), '#reservations');
            var supplementsCount = {{ $supplements_count }};
            @else
            var tourID = 0;
            @endif

            @if(!isset($data->id))
            $('body').on('click', '#tourNext', function() {
                var tourData = $('#general form').serializeArray();
                var button = $(this);
                $.ajax({
                    type: "POST",
                    url: "/save/tour",
                    cache: false,
                    async: false,
                    data: tourData,
                    success: function (ajaxPackageID) {
                        tourID = ajaxPackageID;
                        //ServicesList(ajaxPackageID);

                        $('#general form').append('<input type="hidden" name="id" value="' + ajaxPackageID + '">');

                        button.empty().attr('id', 'saveTour').append('<span class="glyphicon glyphicon-ok"></span> Save');

                        $('#tourNav').append('<li><a data-toggle="tab" href="#services">Services</a></li>');
                        $('#tourNav a').eq(1).trigger('click');
                    }
                });
            });
            @endif
            $('body').on('click', '#saveTour', function() {
                $.ajax({
                    type: "POST",
                    url: "/save/tour",
                    cache: false,
                    data: $('#general form, #services form, #rates form, #booking form, #implementation form, #specials form, #reservations form').serializeArray(),
                    success: function () {
                        window.location.replace('{{ url('/tours') }}');
                    },
                    async: false
                });
            });

            $('.journeyFee [type=checkbox]').on('change', function () {
                if($(this).is(':checked'))
                    $(this).closest('.row').find('.feeToInput').attr('disabled', false);
                else
                    $(this).closest('.row').find('.feeToInput').attr('disabled', true);
            });

            function SpecialsList (id) {
                $( "#specialsTable" ).empty().load( '/tour/' + id + ' #specialsTable', function() {
                    EditDeleteSpecials();
                });
            }

            function SpecialModal(id, custom_data) {
                $('#specialModal .modal-content').empty();

                var url = "/special/" + id;
                var dataType;

                if (id == 'new')
                    url = "/special/" + id + "/" + tourID;

                $.ajax({
                    dataType: "html",
                    method: "GET",
                    cache: false,
                    url: url,
                    data: custom_data,
                    success: function (data) {
                        if (!custom_data)
                            $('#specialModal').modal('show');

                        $("#specialModal .modal-content").prepend(data);
                        @if(!Auth::user()->hasRole('edit_tours'))

                        $('#specialModal input, #specialModal select, #specialModal textarea').each(function () {
                            $(this).prop('disabled', true);
                        });
                        @endif

                        $('#cabinTypes, #specialServices, #specialPackages').select2();

                        $('#specialType').on('change', function () {
                            if ($(this).val() == 'false')
                                $('#specialFields').empty();
                            else if ($(this).val() != 'false')
                                SpecialModal(id, {type: $(this).val()});
                        });

                        $('#offerType').on('change', function () {
                            if($(this).val() == 'false') {
                                $('#offerAmount').attr('disabled', true);
                                $('#specialPackages').attr('disabled', true).closest('div').css('display', 'none');
                                $('#specialServices').attr('disabled', true).closest('div').css('display', 'none');
                            } else {
                                $('#offerAmount').attr('disabled', false);
                            }

                            if ($(this).val() == 'free_services') {
                                $('#specialServices').attr('disabled', false).closest('div').css('display', 'block');
                                $('#specialPackages').attr('disabled', true).closest('div').css('display', 'none');
                            } else if ($(this).val() == 'free_packages') {
                                $('#specialPackages').attr('disabled', false).closest('div').css('display', 'block');
                                $('#specialServices').attr('disabled', true).closest('div').css('display', 'none');
                            }
                        });



                        $('#saveSpecial').on('click', function() {
                            $.ajax({
                                dataType: "html",
                                data: $('#specialModal form').serialize(),
                                method: "POST",
                                cache: false,
                                url: '/save/special',
                                success: function (data) {
                                    SpecialsList(tourID);
                                    $('#specialModal').modal('hide');
                                }
                            });
                        });
                    }
                });
            }

            function EditDeleteSpecials () {
                $('.editSpecial').on('click', function () {
                    SpecialModal($(this).attr('data-id'));
                });

                $('.deleteSpecial').on('click', function() {
                    var dataID = $(this).attr('data-id');
                    $.ajax({
                        dataType: "html",
                        method: "GET",
                        cache: false,
                        url: '/delete/special/' + dataID,
                        success: function (data) {
                            SpecialsList(tourID);
                        }
                    });
                });
            }

        });
    </script>
@endsection