@extends('layouts.app')

@section('title')Tours @endsection

@section('content')
    <div class="search-block">
        <div class="search">
            @include('includes.breadcrumbs')
        </div>
    </div>
    <div class="base-page-navigation-table">
        <div class="search-cities">
            @include('includes.table-search', ['oldSearch' => Request::get('search'), 'searchKeys' => ['all' => 'All', 'depart_date' => 'Departure date', 'hotel.name' => 'hotel', 'itinerary.name' => 'Itinerary', 'itinerary.from_to' => 'From / To', 'itinerary.nights' => 'Duration nights', 'season' => 'Season'], 'otherKeys' => ['order', 'order_field']])
        </div>
        <div class="base-page-table-label">
            <p>List of tours</p>
        </div>
        <div class="base-page-table-add-button">
            @role('edit_directories')
            @role('edit_tours')
            <a href="{{ url('/tour/new') }}" data-id="new" class="base-page-table-add">Add tour</a>
            @endrole
            @endrole
        </div>
    </div>
    <table id="tours" class="table table-striped custom-table table-tours">
        <thead>
        <tr>
            <th>Departure date
                @include('includes.table-sort', ['orderField' => 'depart_date', 'sortSearchKeys' => ['field', 'search', 'page']])
            </th>
            <th>Hotel
                @include('includes.table-sort', ['orderField' => 'hotel.name', 'sortSearchKeys' => ['field', 'search', 'page']])
            </th>
            <th>Itineraries
                @include('includes.table-sort', ['orderField' => 'itinerary.name', 'sortSearchKeys' => ['field', 'search', 'page']])
            </th>
            <th>From / To
                @include('includes.table-sort', ['orderField' => 'itinerary.from_to', 'sortSearchKeys' => ['field', 'search', 'page']])
            </th>
            <th>Duration
                @include('includes.table-sort', ['orderField' => 'itinerary.nights', 'sortSearchKeys' => ['field', 'search', 'page']])
            </th>
            <th>Season
                @include('includes.table-sort', ['orderField' => 'season', 'sortSearchKeys' => ['field', 'search', 'page']])
            </th>
            <th>Available places</th>
            <th>Specials</th>
        </tr>
        </thead>
        <tbody>
        @foreach ($data as $each)
            <tr>
                <td>{{ $each->depart_date or '-' }}</td>
                <td>{{ $each->hotel->name or '-' }}</td>
                <td>{{ $each->itinerary->name or '-' }}</td>
                <td>{{ $each->itinerary->from_to or '-' }}</td>
                @if(isset($each->itinerary))
                    <td>{{ $each->itinerary->nights + 1 }}D/{{ $each->itinerary->nights }}N</td>
                @else
                    <td>-</td>
                @endif
                <td>{{ $each->season or '-' }}</td>
                <td>{{ $each->places or 0 }}</td>
                <td>
                    <div class="table-floating-left-element">{{ $each->specials->count() }}</div>
                    <ul class="custom-dropdown-table">
                        <li class="dropdown">
                            <a class="dropdown-toggle inner-table-button-hovered" data-toggle="dropdown" href="#"><span class="glyphicon glyphicon-option-vertical"></span></a>
                            <ul class="dropdown-menu">
                                <li>
                                    <a href="{{ url('/tour/' . $each->id) }}"><img src="/src/img/pen-icon.png" alt=""></a>
                                </li>
                                @role('edit_directories')
                                @role('edit_tours')
                                <li>
                                    <a href="{{ url('/delete/tour/' . $each->id) }}" class="delete-check-element"><img src="/src/img/trash-icon.png" alt=""></a>
                                </li>
                                @endrole
                                @endrole
                            </ul>
                        </li>
                    </ul>
                </td>
            </tr>
        @endforeach
        </tbody>
    </table>
    <div class="row">
        <div class="col-md-6">
            {{ $data->links() }}
        </div>
        <div class="col-md-6">
            <p>Display:</p>
            <a href="/tours/setnumber?number=10" class="btn btn-default">10</a>
            <a href="/tours/setnumber?number=50" class="btn btn-default">50</a>
            <a href="/tours/setnumber?number=100" class="btn btn-default">100</a>
        </div>
    </div>
@endsection