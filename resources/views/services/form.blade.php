{{ csrf_field() }}
<input type="hidden" name="service_type" value="{{ $service_type }}">
@if(isset($data->id))
    <input name="id" type="hidden" value="{{ $data->id }}">
@endif
<div class="modal-header">
    <div class="modal-header-label modal-left">
        <h4>Add {{ $service_type == 'other' ? $service_type . ' service' : $service_type }}</h4>
    </div>
    <div class="modal-header-buttons modal-right">
        <button form="formExcursion" class="btn-create saveService">Save</button>
        <button type="button" class="btn-cancel" data-dismiss="modal">Cancel</button>
    </div>
</div>
<div class="modal-body clearfix">
    <div class="modal-add-excursion-center">
        <label>Name:</label>
        <input type="text" name="name" value="{{ $data->name }}">
    </div>
    <div class="modal-left modal-add-excursion-left">
        @if($service_type == 'excursion')
            <div class="modal-left modal-add-excursion-row">
                <label>Language:</label>
                <select name="language">
                    @foreach($data->languages as $languageID => $language)
                        <option value="{{ $languageID }}">{{ $language }}</option>
                    @endforeach
                </select>
            </div>
        @endif
        <div class="modal-left modal-add-excursion-row">
            <label>Venue:</label>
            @if($service_type == 'catering' || $service_type == 'other')
                <div class="base-page-table-nav-buttons md-services-buttons">
                    <ul class="nav nav-tabs services-page-tabs md-catering-tabs" id="serviceVenueType" role="tablist">
                        <li class="tab {{ $data->other || !$data->id ? 'active ' : '' }}md-catering-active-select"><a data-type="other" href="#mdCateringOther" data-toggle="tab">Other</a></li>
                        <li class="tab {{ $data->ship_id ? 'active ' : '' }}md-catering-active-select"><a data-type="ships" href="#mdCateringShips" data-toggle="tab">Ships</a></li>
                        <li class="tab {{ $data->hotel_id ? 'active ' : '' }}md-catering-active-select"><a data-type="hotels" href="#mdCateringHotel" data-toggle="tab">Hotel</a></li>
                        <li class="tab {{ $data->city_id ? 'active ' : '' }}md-catering-active-select"><a data-type="cities" href="#mdCateringCity" data-toggle="tab">City</a></li>
                    </ul>
                </div>
                <input id="venueType" type="hidden" name="venue_type" value="{{ array_keys($data->venue_types)[0] }}">
                <div id="venueTabs" class="tab-content">
                    <div id="mdCateringOther" class="tab-pane fade{{ $data->other || !$data->id ? ' in active' : '' }}">
                        <input {{ $data->other || !$data->id ? '' : 'disabled ' }}name="venue" placeholder="Type in the other venue" value="{{ $data->other ? $data->other : '' }}">
                    </div>
                    <div id="mdCateringShips" class="tab-pane fade{{ $data->ship_id ? ' in active' : '' }}">
                        <select {{ $data->ship_id ? '' : 'disabled ' }}name="venue" class="md-catering-tracking-select">
                            <option hidden>Select ship</option>
                            @if($data->ship_id)
                                @foreach($data->venues as $key => $each)
                                    <option {{ $data->ship_id == $key ? 'selected ' : '' }}value="{{ $key }}">{{ $each }}</option>
                                @endforeach
                            @endif
                        </select>
                    </div>
                    <div id="mdCateringHotel" class="tab-pane fade{{ $data->hotel_id ? ' in active' : '' }}">
                        <select {{ $data->hotel_id ? '' : 'disabled ' }}name="venue" class="md-catering-tracking-select">
                            <option hidden>Select hotel</option>
                            @if($data->hotel_id)
                                @foreach($data->venues as $key => $each)
                                    <option {{ $data->hotel_id == $key ? 'selected ' : '' }}value="{{ $key }}">{{ $each }}</option>
                                @endforeach
                            @endif
                        </select>
                    </div>
                    <div id="mdCateringCity" class="tab-pane fade{{ $data->city_id ? ' in active' : '' }}">
                        <select {{ $data->city_id ? '' : 'disabled ' }}name="venue" class="md-catering-tracking-select">
                            <option hidden>Select city</option>
                            @if($data->city_id)
                                @foreach($data->venues as $key => $each)
                                    <option {{ $data->city_id == $key ? 'selected ' : '' }}value="{{ $key }}">{{ $each }}</option>
                                @endforeach
                            @endif
                        </select>
                    </div>
                </div>
            @else
                <select name="venue">
                    @foreach($data->venues as $key => $each)
                        <option value="{{ $key }}">{{ $each }}</option>
                    @endforeach
                </select>
                <input type="hidden" name="venue_type" value="cities">
            @endif
        </div>
        <div class="modal-add-excursion-row">
            <div class="modal-left modal-add-excursion-inner">
                <div class="modal-add-excursion-inner-element">
                    <label>Min number of pax:</label>
                    <div class="modal-add-excursion-input-value">
                        <p>pax</p>
                    </div>
                    <input type="text" value="{{ $data->min_pax }}" name="min_pax">
                </div>
            </div>
            <div class="modal-left modal-add-excursion-inner">
                <div class="modal-add-excursion-inner-element">
                    <label>Min number of pax:</label>
                    <div class="modal-add-excursion-input-value">
                        <p>pax</p>
                    </div>
                    <input type="text" value="{{ $data->max_pax }}" name="max_pax">
                </div>
            </div>
        </div>
        <div class="modal-add-excursion-inner-element border-dashed modal-add-{{ $service_type == 'excursion' ? 'excursion' : 'catering' }}-price">
            <label>Price:</label>
            <input type="text" name="price" value="{{ $data->price }}">
            <div class="modal-add-excursion-input-value">
                <p><span>eur</span></p>
            </div>
            @if($service_type != 'excursion')
                <div class="cb-property">
                    <div class="base-page-table-nav-buttons md-services-buttons">
                        <label>Condition:</label>
                        <ul id="paxCabinValue" class="nav nav-tabs services-page-tabs md-transport-tabs" role="tablist">
                            <li class="tab{{ $data->all_pax_cabin ? ' ' : ' active' }}"><a href="#" data-toggle="tab" data-value="false">Min 1 pax</a></li>
                            <li class="tab{{ $data->all_pax_cabin ? ' active' : '' }}"><a href="#" data-toggle="tab" data-value="true">For all pax in the cabins / rooms</a></li>
                        </ul>
                        <input type="hidden" name="all_pax_cabin" value="{{ $data->all_pax_cabin ? 'true' : 'false' }}">
                    </div>
                    <div>
                        <input class="md-other-checkbox" {{ $data->all_pax_cabin && $data->multiply ? 'checked ' : '' }}{{ !$data->all_pax_cabin ? 'disabled ' : '' }}type="checkbox" id="serviceMultiply" name="multiply">
                        <label class="md-other-checkbox-label">Multiply price per number of pax</label>
                    </div>
                </div>
            @endif
        </div>
        <div class="tracking-input-by-cb">
            <div class="modal-left modal-add-excursion-inner-left">
                <div class="modal-add-excursion-inner-element">
                    <input {{ $data->board_price ? 'checked ' : '' }}id="serviceAvailability" class="tracked-cb" type="checkbox">
                    <label>Available on board</label>
                </div>
            </div>
            <div class="modal-left modal-add-excursion-inner-right">
                <div class="modal-add-excursion-inner-element">
                    <input {{ !$data->board_price ? 'disabled ' : '' }}id="serviceBoardPrice" name="board_price" class="tracked-cb-input" type="text" value="{{ $data->board_price }}">
                    <div class="modal-add-excursion-input-value">
                        <p><span>eur</span></p>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="modal-right modal-add-services-right add-{{ $service_type }}-right">
        <label>Description:</label>
        <textarea name="description">{{ $data->description }}</textarea>
        <label>Conditions:</label>
        <textarea name="condition_desc">{{ $data->condition_desc }}</textarea>
    </div>
</div>