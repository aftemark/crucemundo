@extends('layouts.app')

@section('title')
    Services
@endsection

@section('content')
    <div class="search-block">
        <div class="search">
            @include('includes.breadcrumbs')
        </div>
        <div class="base-page-table-nav-buttons">
            <ul class="nav nav-tabs services-page-tabs" role="tablist">
                <li class="tab{{ Request::get('table') == 'all' || !Request::has('table') ? ' active' : '' }}"><a href="#all" data-toggle="tab">All</a></li>
                <li class="tab{{ Request::get('table') == 'excursion' ? ' active' : '' }}"><a href="#excursion" data-toggle="tab">Excursions</a></li>
                <li class="tab{{ Request::get('table') == 'transport' ? ' active' : '' }}"><a href="#transport" data-toggle="tab">Transport</a></li>
                <li class="tab{{ Request::get('table') == 'catering' ? ' active' : '' }}"><a href="#catering" data-toggle="tab">Catering</a></li>
                <li class="tab{{ Request::get('table') == 'other' ? ' active' : '' }}"><a href="#other" data-toggle="tab">Other services</a></li>
            </ul>
        </div>
    </div>

    <div class="tab-content">
        @foreach($data as $typeName => $serviceType)
            <div id="{{ $typeName }}" class="tab-pane fade{{ Request::get('table') == $typeName || ($typeName == 'all' && !Request::has('table')) ? ' in active' : '' }}">
                <div class="base-page-navigation-table">
                    <div class="search-ships">
                        @php
                            $searchFields = ['all' => 'All', 'name' => 'Name'];
                            if ($typeName == 'all')
                                $searchFields += ['service_type' => 'Service type', 'city' => 'City', 'ship' => 'Ship', 'hotel' => 'Hotel'];
                            else if ($typeName == 'catering' || $typeName == 'other')
                                $searchFields += ['city' => 'City', 'ship' => 'Ship', 'hotel' => 'Hotel'];
                        @endphp
                        @include('includes.table-search', ['oldSearch' => Request::get('table') == 'ships' ? Request::get('search') : '', 'searchKeys' => $searchFields, 'otherKeys' => ['order', 'order_field'], 'tableName' => $typeName])
                    </div>
                    <div class="base-page-elements">
                        <div class="base-page-table-label">
                            <p>List of all {{ $typeName != 'all' ? $typeName : '' }}{{ $typeName != 'excursion' ? ' services' : ''}}</p>
                        </div>
                        <div class="base-page-table-add-button">
                            @role('edit_directories')
                            @role('edit_services')
                            @if($typeName == 'all')
                                <button class="base-page-table-add" data-toggle="modal" data-target="#newServiceModal">Add {{ $typeName != 'all' ? $typeName : '' }}{{ $typeName != 'excursion' ? ' service' : ''}}</button>
                            @else
                                <button class="base-page-table-add load-service" data-toggle="modal" data-target="#serviceModal">Add {{ $typeName != 'all' ? $typeName : '' }}{{ $typeName != 'excursion' ? ' service' : ''}}</button>
                            @endif
                            @endrole
                            @endrole
                        </div>
                    </div>
                </div>
                <table class="table table-striped custom-table table-ships">
                    <thead>
                    <tr>
                        <th>Services type
                            @include('includes.table-sort', ['orderField' => 'service_type', 'sortSearchKeys' => ['field', 'search', 'page'], 'tableName' => $typeName])
                        </th>
                        <th>Name
                            @include('includes.table-sort', ['orderField' => 'name', 'sortSearchKeys' => ['field', 'search', 'page'], 'tableName' => $typeName])
                        </th>
                        <th>Venue</th>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach ($serviceType as $service)
                        <tr>
                            <td>{{ $service->service_type }}</td>
                            <td>{{ $service->name }}</td>
                            <td>
                                <div class="table-floating-left-element">
                                    @if($service->city)
                                        {{ $service->city->name }}
                                    @elseif($service->ship)
                                        {{ $service->ship->name }}
                                    @elseif($service->hotel)
                                        {{ $service->hotel->name }}
                                    @else
                                        -
                                    @endif
                                </div>
                                <ul class="custom-dropdown-table">
                                    <li class="dropdown">
                                        <a class="dropdown-toggle inner-table-button-hovered" data-toggle="dropdown" href="#"><span class="glyphicon glyphicon-option-vertical"></span></a>
                                        <ul class="dropdown-menu">
                                            <li>
                                                <a class="editService" href="#" data-id="{{ $service->id }}"><img src="/src/img/pen-icon.png" alt=""></a>
                                            </li>
                                            <li>
                                                @role('edit_directories')
                                                @role('edit_services')
                                                <a class="deleteService" href="{{ url('/delete/service/' . $service->id) }}" data-id="{{ $service->id }}"><img src="/src/img/trash-icon.png" alt=""></a>
                                                @endrole
                                                @endrole
                                            </li>
                                        </ul>
                                    </li>
                                </ul>
                            </td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
            </div>
        @endforeach
        <div class="row">
            <div class="col-md-6">
                {{ $serviceType->appends(array_except(Request::query(), $typeName . '_services'))->links() }}
            </div>
            <div class="col-md-6">
                <p>Display:</p>
                <a href="{{ url('/services/setnumber?number=10') }}" class="btn btn-default">10</a>
                <a href="{{ url('/services/setnumber?number=50') }}" class="btn btn-default">50</a>
                <a href="{{ url('/services/setnumber?number=100') }}" class="btn btn-default">100</a>
            </div>
        </div>
    </div>

    <div id="newServiceModal" class="modal fade" role="dialog">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <div class="modal-header-label modal-left">
                        <h4>Add Service</h4>
                    </div>
                    <div class="modal-header-buttons modal-right">
                        <button type="button" class="btn-cancel" data-dismiss="modal">Cancel</button>
                    </div>
                </div>
                <div class="modal-body clearfix">
                    <div class="modal-left modal-add-service-inner">
                        <label>Service type:</label>
                        <select id="serviceType">
                            <option value="false">Select a service type</option>
                            <option value="excursion">Excursion</option>
                            <option value="transport">Transport</option>
                            <option value="catering">Catering</option>
                            <option value="other">Other</option>
                        </select>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div id="serviceModal" class="modal fade service-modals" role="dialog">
        <div class="modal-dialog" role="document">
            <form action="{{ url('/save/service') }}" method="POST" class="modal-content modal-add-excursion"></form>
        </div>
    </div>
@endsection

@section('script')
    <script src="{{ url('/src/js/jquery.validate.min.js') }}"></script>
    <script>
        $(document).ready(function() {
            $('#serviceType').change(function() {
                var value = $(this).val();
                if (value != 'false') {
                    $('#newServiceModal').modal('hide');
                    loadService('new', value);
                }
            });

            $('.editService').on('click', function () {
                var attr = $(this).attr('data-id');
                loadService(attr);
            });

            $('.load-service').on('click', function () {
                var type = $(this).closest('.tab-pane').attr('id');
                loadService('new', type);
            });

            $('.deleteService').on('click', function (e) {
                var element = $(this);
                $.get(element.attr('href'), function() {
                    $('[data-id=' + element.attr('data-id') + ']').closest('tr').remove();
                }).error(function(error) {
                    deleteModal(error);
                });
                e.preventDefault();
            });
        });

        function loadService (attr, type) {
            var url = type ? "/service/" + attr + '?service_type=' + type : "/service/" + attr;
            $.ajax({
                dataType: "html",
                method: "GET",
                url: url,
                success: function (data) {
                    $('#serviceModal .modal-content').empty().append(data);
                    @if(!Auth::user()->hasRole('edit_services'))

                    $('#serviceModal input, #serviceModal select, #serviceModal textarea').each(function () {
                        $(this).prop('disabled', true);
                    });

                    @endif
                    $('#serviceModal').modal('show');

                    $("#serviceModal form").validate({
                        rules: {
                            name: {
                                required: true
                            },
                            min_pax: {
                                required: true
                            },
                            max_pax: {
                                required: true
                            },
                            price: {
                                required: true
                            }
                        }
                    });

                    $('#serviceAvailability').change(function() {
                        $('#serviceBoardPrice').attr('disabled', !this.checked)
                    });

                    $('#paxCabinValue li a').on('click', function () {
                        var value = $(this).attr('data-value');
                        $('#serviceModal input[name=all_pax_cabin]').val(value);
                        $('#serviceMultiply').attr('disabled', value != 'true');
                    });

                    $('.saveService').on('click', function () {
                        var form = $(this).closest('form');
                        if ($("#serviceModal form").valid()) {
                            $.ajax({
                                type: "POST",
                                url: form.attr('action'),
                                data: form.serialize(),
                                success: function (data) {
                                    $('#cityModal').modal('hide');
                                    window.location.reload();
                                }
                            });
                        }
                    });

                    $('#serviceVenueType li a').on('click', function () {
                        var selector = $(this).attr('href'),
                            type = $(this).attr('data-type');
                        $('#venueType').val(type);
                        $('#venueTabs .tab-pane').each(function() {
                            var disabled = true;
                            if (selector == '#' + this.id) {
                                disabled = false;
                                if (selector != '#mdCateringOther')
                                    $.getJSON('/venues?type=' + type, null, function(venues) {
                                        var select = $(selector + ' select');
                                        select.empty();
                                        $.each(venues, function(i, item) {
                                            select.append('<option value="' + i + '">' + item + '</option>');
                                        });
                                    });
                            }
                            $(this).find('select, input').attr('disabled', disabled)
                        });
                    });

                    /*$('#serviceVenueType').on('change', function() {
                        var type = $(this).val();
                        if (type != 'other') {
                            $.getJSON('/venues?type=' + type, null, function(venues) {
                                $('input#serviceVenue').remove();
                                if($('select#serviceVenue').length == 0) {
                                    $('#serviceVenueType').after('<select class="form-control" name="venue" id="serviceVenue"></select>');
                                }
                                $('#serviceVenue').empty();
                                $.each(venues, function(i, item) {
                                    $('#serviceVenue').append('<option value="' + i + '">' + item + '</option>')
                                });
                            });
                        } else {
                            $('select#serviceVenue').remove();
                            if($('input#serviceVenue').length == 0)
                                $('#serviceVenueType').after('<input class="form-control" name="venue" id="serviceVenue" placeholder="Type in the other venue">');
                        }
                    });*/
                }
            });
        }
    </script>
@endsection