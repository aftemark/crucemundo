@extends('layouts.app')

@section('title')Accommodations @endsection

@section('content')
    <div class="search-block">
        <div class="search">
            @include('includes.breadcrumbs')
        </div>
        <div class="base-page-table-nav-buttons">
            <ul class="nav nav-tabs" role="tablist">
                <li{!! Request::input('table', NULL) == 'ships' || !Request::has('table') ? ' class="active"' : '' !!}><a data-toggle="tab" href="#ships">Ships</a></li>
                <li{!! Request::input('table', NULL) == 'hotels' ? ' class="active"' : '' !!}><a data-toggle="tab" href="#hotels">Hotels</a></li>
            </ul>
        </div>
    </div>
    <div class="tab-content">
        <div id="ships" class="tab-pane fade{{ !Request::has('table') || Request::get('table') == 'ships' ? ' in active' : '' }}">
            @role('show_ships')
            <div class="base-page-navigation-table">
                <div class="search-ships">
                    @include('includes.table-search', ['oldSearch' => Request::get('table') == 'ships' ? Request::get('search') : '', 'searchKeys' => ['all' => 'All', 'code' => 'ID', 'name' => 'Name', 'rating' => 'Star Rating'], 'otherKeys' => ['order', 'order_field'], 'tableName' => 'ships'])
                </div>
                <div class="base-page-elements">
                    <div class="base-page-table-label">
                        <p>List of ships</p>
                    </div>
                    <div class="base-page-table-add-button">
                        @role('edit_directories')
                        @role('edit_ships')
                        <a href="{{ url('/ship/new') }}" class="base-page-table-add">Add ship</a>
                        @endrole
                        @endrole
                    </div>
                </div>
            </div>
            <table class="table table-striped custom-table table-ships">
                <thead>
                <tr>
                    <th>ID
                        @include('includes.table-sort', ['orderField' => 'code', 'sortSearchKeys' => ['field', 'search', 'page'], 'tableName' => 'ships'])
                    </th>
                    <th>Name
                        @include('includes.table-sort', ['orderField' => 'name', 'sortSearchKeys' => ['field', 'search', 'page'], 'tableName' => 'ships'])
                    </th>
                    <th>Star rating
                        @include('includes.table-sort', ['orderField' => 'rating', 'sortSearchKeys' => ['field', 'search', 'page'], 'tableName' => 'ships'])
                    </th>
                    <th>Maximum capacity (pax)</th>
                </tr>
                </thead>
                <tbody>
                @foreach ($data['ships'] as $each)
                    <tr>
                        <td>{{ $each->code or '-' }}</td>
                        <td>{{ $each->name or '-' }}</td>
                        <td>{{ $each->rating or '-' }}</td>
                        <td>
                            <div class="table-floating-left-element">{{ $each->max_capacity or '-' }}</div>
                            <ul class="custom-dropdown-table">
                                <li class="dropdown">
                                    <a class="dropdown-toggle inner-table-button-hovered" data-toggle="dropdown" href="#"><span class="glyphicon glyphicon-option-vertical"></span></a>
                                    <ul class="dropdown-menu">
                                        <li>
                                            <a href="{{ url('/ship/' . $each->id) }}"><img src="/src/img/pen-icon.png" alt=""></a>
                                        </li>
                                        <li>
                                            <a class="delete-check-element" href="{{ url('/delete/ship/' . $each->id) }}"><img src="/src/img/trash-icon.png" alt=""></a>
                                        </li>
                                    </ul>
                                </li>
                            </ul>
                        </td>
                    </tr>
                @endforeach
                </tbody>
            </table>
            <div class="row">
                <div class="col-md-6">
                    {{ $data['ships']->appends(array_except(Request::query(), 'ships_services'))->links() }}
                </div>
                <div class="col-md-6">
                    <p>Display:</p>
                    <a href="{{ url('/accommodations/setnumber?number=10') }}" class="btn btn-default">10</a>
                    <a href="{{ url('/accommodations/setnumber?number=50') }}" class="btn btn-default">50</a>
                    <a href="{{ url('/accommodations/setnumber?number=100') }}" class="btn btn-default">100</a>
                </div>
            </div>
            @endrole
        </div>
        <div id="hotels" class="tab-pane fade{{ Request::get('table') == 'hotels' ? ' in active' : '' }}">
            @role('show_hotels')
            <div class="base-page-navigation-table">
                <div class="search-ships">
                    @include('includes.table-search', ['oldSearch' => Request::get('table') == 'hotels' ? Request::get('search') : '', 'searchKeys' => ['all' => 'All', 'code' => 'ID', 'name' => 'Name', 'rating' => 'Star Rating'], 'otherKeys' => ['order', 'order_field', 'table'], 'tableName' => 'hotels'])
                </div>
                <div class="base-page-elements">
                    <div class="base-page-table-label">
                        <p>List of hotels</p>
                    </div>
                    <div class="base-page-table-add-button">
                        @role('edit_directories')
                        @role('edit_hotels')
                        <a class="base-page-table-add" href="{{ url('/hotel/new') }}">Add hotel</a>
                        @endrole
                        @endrole
                    </div>
                </div>
            </div>
            <table class="table table-striped custom-table table-hotels">
                <thead>
                <tr>
                    <th>ID
                        <div class="table-sort">
                            @include('includes.table-sort', ['orderField' => 'code', 'sortSearchKeys' => ['field', 'search', 'page'], 'tableName' => 'hotels'])
                        </div>
                    </th>
                    <th>Name
                        <div class="table-sort">
                            @include('includes.table-sort', ['orderField' => 'name', 'sortSearchKeys' => ['field', 'search', 'page'], 'tableName' => 'hotels'])
                        </div>
                    </th>
                    <th>Star rating
                        <div class="table-sort">
                            @include('includes.table-sort', ['orderField' => 'rating', 'sortSearchKeys' => ['field', 'search', 'page'], 'tableName' => 'hotels'])
                        </div>
                    </th>
                    <th>Country
                        <div class="table-sort">
                            @include('includes.table-sort', ['orderField' => 'country', 'sortSearchKeys' => ['field', 'search', 'page'], 'tableName' => 'hotels'])
                        </div>
                    </th>
                    <th>City
                        <div class="table-sort">
                            @include('includes.table-sort', ['orderField' => 'city', 'sortSearchKeys' => ['field', 'search', 'page'], 'tableName' => 'hotels'])
                        </div>
                    </th>
                </tr>
                </thead>
                <tbody>
                @foreach ($data['hotels'] as $each2)
                    <tr>
                        <td>{{ $each2->code or '-' }}</td>
                        <td>{{ $each2->name or '-' }}</td>
                        <td>{{ $each2->rating or '-' }}</td>
                        <td>{{ $each2->city->country->name or '-' }}</td>
                        <td>
                            <div class="table-floating-left-element">{{ $each2->city->name or '-' }}</div>
                            <ul class="custom-dropdown-table">
                                <li class="dropdown">
                                    <a class="dropdown-toggle inner-table-button-hovered" data-toggle="dropdown" href="#"><span class="glyphicon glyphicon-option-vertical"></span></a>
                                    <ul class="dropdown-menu">
                                        <li>
                                            <a href="{{ url('/hotel/' . $each2->id) }}"><img src="/src/img/pen-icon.png" alt=""></a>
                                        </li>
                                        @role('edit_directories')
                                        @role('edit_hotels')
                                        <li>
                                            <a class="delete-check-element" href="{{ url('/delete/hotel/' . $each2->id) }}"><img src="/src/img/trash-icon.png" alt=""></a>
                                        </li>
                                        @endrole
                                        @endrole
                                    </ul>
                                </li>
                            </ul>
                        </td>
                    </tr>
                @endforeach
                </tbody>
            </table>
            <div class="row">
                <div class="col-md-6">
                    {{ $data['hotels']->appends(array_except(Request::query(), 'hotels_services'))->links() }}
                </div>
                <div class="col-md-6">
                    <p>Display:</p>
                    <a href="{{ url('/accommodations/setnumber?number=10') }}" class="btn btn-default">10</a>
                    <a href="{{ url('/accommodations/setnumber?number=50') }}" class="btn btn-default">50</a>
                    <a href="{{ url('/accommodations/setnumber?number=100') }}" class="btn btn-default">100</a>
                </div>
            </div>
            @endrole
        </div>
    </div>
@endsection