@extends('layouts.app')

@section('title')Service cruise @endsection

@section('css')
    <link rel="stylesheet" href="{{ url('/src/css/select2.min.css') }}">
@endsection

@section('content')
    <nav class="navbar second-nav">
        <div class="container-fluid">
            <div class="addships-title pull-left">
                @if(!empty($data->code))
                    <h3>{{ $data->code }}</h3>
                @else
                    <h3>New cruise</h3>
                @endif
            </div>
            <div class="addships-buttons pull-right">
                @if(isset($data->id) && $data->reservations()->where('with_names', true)->where('type', 'booking')->exists())
                    <a href="{{ url('/assignment/' . $data->id) }}" class="btn-create copy-btn">Cabins assignment</a>
                @endif
                @if(isset($data->id))
                    <a href="{{ url('/copy/cruise/' . $data->id) }}" class="btn-create copy-btn">Copy</a>
                @endif
                @role('edit_directories')
                @role('edit_cruises')
                @if(isset($data->id))
                    <button type="button" id="saveCruise" class="btn-create">Save</button>
                @else
                    <button type="button" id="cruiseNext" class="btn-create">Next</button>
                @endif
                @endrole
                @endrole
                <a class="btn-cancel button-link-cancel" href="{{ url('/cruises') }}">Cancel</a>
            </div>
        </div>
    </nav>
    <div class="base-page-table-nav-buttons b2c-nav-buttons">
        <ul class="nav nav-tabs tour-tabs anchor-tabs-next" role="tablist">
            @if(isset($data->id))
                <li class="tab active"><a href="#general" data-toggle="tab">General information</a></li>
                <li class="tab"><a href="#services" data-toggle="tab">Itinerary and services</a></li>
                <li class="tab"><a href="#rates" data-toggle="tab">Rates</a></li>
                <li class="tab"><a href="#booking" data-toggle="tab">Booking and payment Conditions</a></li>
                @role('show_cruises_implementation')
                <li class="tab"><a href="#implementation" data-toggle="tab">Implementation</a></li>
                @endrole
                <li class="tab"><a href="#specials" data-toggle="tab">Specials</a></li>
                @role('show_cruises_reservations')
                <li class="tab"><a class="anchor-service-tabs-show-save" href="#reservations" data-toggle="tab">Reservations</a></li>
                @endrole
            @else
                <li class="tab active"><a href="#general" data-toggle="tab">General information</a></li>
            @endif
        </ul>
    </div>

    <div class="tab-content">
        <div id="general" class="clearfix b2c-general tab-pane active">
            <form>
                {{ csrf_field() }}
                <input type="hidden" name="type" value="cruise">
                @if(isset($data->id))
                    <input name="id" type="hidden" value="{{ $data->id }}">
                @endif
                <div class="left-side-cruises pull-left">
                    <div class="b2c-table-row2 cruises-info-left clearfix">
                        <div class="id-info-input">
                            <p>ID:</p>
                            <input type="text" name="code" value="{{ $data->code }}">
                        </div>
                        <div class="ship-info-input">
                            <p>Ship:</p>
                            <select name="ship">
                                @if(!isset($data->ship_id))
                                    <option value="false">Select ship</option>
                                @endif
                                @foreach($ships as $ship)
                                    <option {{ $data->ship_id == $ship->id ? 'selected ' : '' }}value="{{ $ship->id }}">{{ $ship->name }}</option>
                                @endforeach
                            </select>
                        </div>
                        <div class="season-date-cruise clearfix">
                            <div class="season-input">
                                <p>Season:</p>
                                <select name="season">
                                    @if(!isset($data->season))
                                        <option value="false">Select season</option>
                                    @endif
                                    <option {{ $data->season == 'high' ? 'selected ' : '' }}value="high">High</option>
                                    <option {{ $data->season == 'middle' ? 'selected ' : '' }}value="middle">Middle</option>
                                    <option {{ $data->season == 'low' ? 'selected ' : '' }}value="low">Low</option>
                                </select>
                            </div>
                            <div class="departure-date-input">
                                <p>Departure date:</p>
                                <input type="text" name="depart_date" value="{{ $data->depart_date }}">
                                <div class="cruises-date-btn"><span class="glyphicon glyphicon-calendar"></span></div>
                            </div>
                        </div>
                        <div class="ending-date-cruise clearfix">
                            <div class="days-info-input">
                                <p>Days:</p>
                                <input id="cruiseDays" type="text" disabled value="{{ isset($data->itinerary) ? $data->itinerary->nights + 1 : '' }}">
                            </div>
                            <div class="nights-info-input">
                                <p>Nights:</p>
                                <input id="cruiseNights" type="text" disabled value="{{ isset($data->itinerary) ? $data->itinerary->nights : '' }}">
                            </div>
                            <div class="departure-date-input">
                                <p>Ending date:</p>
                                <input id="cruiseEndDate" type="text" disabled value="{{ $data->ending_date }}">
                                <div class="cruises-date-btn"><span class="glyphicon glyphicon-calendar"></span></div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="right-side-cruises pull-right">
                    <div class="b2c-table-row3">
                        <div class="cruises-table-elem">
                            <div class="release-info-right clearfix">
                                <div class="release-date-input">
                                    <p>Release date:</p>
                                    <input name="journey_release_date" type="text" value="{{ $data->release_date }}">
                                    <div class="cruises-date-btn"><span class="glyphicon glyphicon-calendar"></span></div>
                                </div>
                                <div class="remind-date-input">
                                    <p>Remind date:</p>
                                    <input name="journey_remind" type="text" value="{{ $data->remind }}">
                                    <div class="cruises-date-btn"><span class="">days before release date</span></div>
                                </div>
                            </div>
                            <div class="cruises-note">
                                <p>Note:</p>
                                <textarea name="description" cols="30" rows="10">{{ $data->description }}</textarea>
                            </div>
                        </div>
                    </div>
                </div>
            </form>
        </div>
        <div id="services" class="tab-pane fade">
            <div class="service-itinerary-body">
                <form class="service-itinerary-head">
                    <div class="service-itinerary-head-elem">
                        <label>Itinerary:</label>
                        <select name="itinerary">
                            @if(!$data->itinerary_id)
                                <option value="false">Select itinerary</option>
                            @endif
                            @foreach($itineraries as $itinerary)
                                <option data-id="{{ $itinerary->from_to }}" {{ $data->itinerary_id == $itinerary->id ? 'selected ' : '' }}value="{{ $itinerary->id }}">{{ $itinerary->name }}</option>
                            @endforeach
                        </select>
                    </div>
                    <div class="service-itinerary-head-right-elems">
                        <div class="service-itinerary-head-elem">
                            <label>Departure - Destination:</label>
                            <input disabled id="journeyFromTo" type="text" value="{{ $data->itinerary->from_to or '' }}">
                        </div>
                    </div>
                </form>
                <div class="itinerary_services">
                    @include('journey_services.list')
                </div>
            </div>
        </div>
        <div id="rates" class="reservations tab-pane fade clearfix">
            @if(isset($data->ship))
                @php $supplements_count = 0; @endphp
                @foreach($data->ship->alldecks as $deck)
                    <form class="tab-rates-tables">
                        <label class="tab-rates-labels label-service-tabs-uppercase">{{ $deck->name }}</label>
                        <table class="table table-striped custom-table service-tabs-table other-service-tab-table service-tab-rates-table">
                            <thead>
                            <tr>
                                <th>ID</th>
                                <th>NET price</th>
                                <th>REC price</th>
                                <th>Cabin #</th>
                                <th></th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach ($deck->types->where('journey_id', $data->id) as $type_category)
                                @php $isFirst = true; @endphp
                                @foreach($type_category->placements as $placement)
                                    <tr>
                                        @if($isFirst)
                                            @php $countPlacements = count($type_category->placements); $isFirst = false; @endphp
                                            <td rowspan="{{ $countPlacements }}">{{ $type_category->type->name }}</td>
                                            <td rowspan="{{ $countPlacements }}"><input type="text" name="netPrice[{{ $type_category->id }}]" value="{{ $type_category->price_net }}"></td>
                                            <td rowspan="{{ $countPlacements }}"><input type="text" name="recPrice[{{ $type_category->id }}]" value="{{ $type_category->price_rec }}"></td>
                                        @else
                                            <td hidden></td>
                                            <td hidden></td>
                                            <td hidden></td>
                                        @endif
                                        <td>{{ $placement->cabin->num }}</td>
                                        <td><input type="checkbox" name="cabinInclude[{{ $placement->cabin_id }}]" {{ $placement->display ? 'checked ' : '' }}class="display-block" value="true"></td>
                                    </tr>
                                @endforeach
                            @endforeach
                            </tbody>
                        </table>
                        @foreach($data->groups()->where('deck_id', $deck->id)->get() as $group)
                            <div class="service-rates-table-bottom">
                                <div class="anchor-service-rates-insert">
                                    <div class="service-rates-table-bottom-elem">
                                        <div class="modal-add-service-right-element tab-rates-bottom-table-elems">
                                            <div class="service-general-rdate add-service-tab-general-left">
                                                <label>Single supplement:</label>
                                                <div class="modal-add-service-input-value service-general-span">
                                                    <span>%</span>
                                                </div>
                                                <input type="text" value="{{ $group->percent }}" name="supplement_rate[{{ ++$supplements_count }}]">
                                            </div>
                                        </div>
                                        <div class="modal-add-service-right-element tab-rates-bottom-table-elems">
                                            <div class="service-general-rdate add-service-tab-general-left">
                                                <label>Number of cabins available with a surcharge of less than 100%:</label>
                                                <input class="tab-rates-number-of-cabins" type="text" name="placements_amount[{{ $supplements_count }}]" value="{{ $group->placements_amount }}">
                                                <div class="service-itinerary-table-bottom-right">
                                                    <div class="service-itinerary-table-bottom-right-elem service-rates-bottom-spans">
                                                        <span class="glyphicon glyphicon-minus anchor-service-rates-remove"></span>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="modal-table-iteniraries-right full-width tab-rates-bottom-table">
                                    <select class="modal-table-iteniraries-right-select supplementGroup" multiple="multiple" name="supplement_group[{{ $supplements_count }}][]">
                                        @foreach ($deck->types->where('journey_id', $data->id) as $type_category)
                                            @foreach ($type_category->placements as $placement)
                                                <option {{ in_array($placement->cabin_id, $selectedCabins) && $group->id != $placement->supplement_group_id ? 'disabled ' : '' }}{{ $group->id == $placement->supplement_group_id ? 'selected ' : '' }}value="{{ $placement->cabin_id }}">{{ $placement->cabin->num }}</option>
                                            @endforeach
                                        @endforeach
                                    </select>
                                </div>
                                <input type="hidden" name="deck_id[{{ $supplements_count }}]" value="{{ $deck->id }}">
                            </div>
                        @endforeach
                        <div class="service-rates-table-bottom">
                            <div class="anchor-service-rates-insert">
                                <div class="service-rates-table-bottom-elem">
                                    <div class="modal-add-service-right-element tab-rates-bottom-table-elems">
                                        <div class="service-general-rdate add-service-tab-general-left">
                                            <label>Single supplement:</label>
                                            <div class="modal-add-service-input-value service-general-span">
                                                <span>%</span>
                                            </div>
                                            <input type="text" name="supplement_rate[{{ ++$supplements_count }}]">
                                        </div>
                                    </div>
                                    <div class="modal-add-service-right-element tab-rates-bottom-table-elems">
                                        <div class="service-general-rdate add-service-tab-general-left">
                                            <label>Number of cabins available with a surcharge of less than 100%:</label>
                                            <input class="tab-rates-number-of-cabins" type="text" name="placements_amount[{{ $supplements_count }}]">
                                            <div class="service-itinerary-table-bottom-right">
                                                <div class="service-itinerary-table-bottom-right-elem service-rates-bottom-spans">
                                                    <span data-deck-id="{{ $deck->id }}" class="glyphicon glyphicon-plus anchor-service-rates-add"></span>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="modal-table-iteniraries-right full-width tab-rates-bottom-table">
                                <select class="modal-table-iteniraries-right-select supplementGroup" multiple="multiple" name="supplement_group[{{ $supplements_count }}][]">
                                    @foreach ($deck->types->where('journey_id', $data->id) as $type_category)
                                        @foreach ($type_category->placements as $placement)
                                            <option {{ in_array($placement->cabin_id, $selectedCabins) ? 'disabled ' : '' }}value="{{ $placement->cabin_id }}">{{ $placement->cabin->num }}</option>
                                        @endforeach
                                    @endforeach
                                </select>
                            </div>
                            <input type="hidden" name="deck_id[{{ $supplements_count }}]" value="{{ $deck->id }}">
                        </div>
                    </form>
                @endforeach
            @endif
        </div>
        <div id="booking" class="reservations tab-pane fade clearfix">
            <div class="tab-rates-tables">
                <div class="tab-condition-row">
                    <div class="modal-add-service-right-element tab-conditions-elem">
                        <div class="service-general-rdate add-service-tab-general-left tab-conditions-payment">
                            <label>Terms of payment:</label>
                            <div class="modal-add-service-input-value service-general-span">
                                <span>%</span>
                            </div>
                            <input type="text" name="booking_percent" value="{{ $data->booking_percent ? $data->booking_percent : '' }}">
                        </div>
                        <p>on booking ( non- refundable deposit)</p>
                    </div>
                </div>
                <div class="tab-condition-row2">
                    <div class="modal-add-service-right-element tab-conditions-elem">
                        <p>Full payment</p>
                        <div class="service-general-rdate add-service-tab-general-left tab-conditions-payment">
                            <div class="modal-add-service-input-value service-general-span">
                                <span>days</span>
                            </div>
                            <input type="text" name="full_pay" value="{{ $data->full_pay }}">
                        </div>
                        <p>before cruise</p>
                    </div>
                </div>
                <div class="tab-condition-row3">
                    <div class="anchor-service-tab-option-insert">
                        <div class="tab-conditions-elem-label">
                            <label>Cancellation fees:</label>
                        </div>
                        @php $feecount = 0; @endphp
                        @foreach($data->fees as $fee)
                            <div class="modal-add-service-right-element tab-conditions-elem journeyFee">
                                <p class="first-p">Up to</p>
                                <input class="input-for-upto" type="text" name="fee_from[{{ ++$feecount }}]" value="{{ $fee->from }}">
                                <p class="tab-conditions-elem-dash">-</p>
                                <input {{ isset($fee->to) ? 'checked ' : '' }}type="checkbox">
                                <input {{ isset($fee->to) ? '' : 'disabled ' }}class="input-for-upto feeToInput" type="text" value="{{ $fee->to }}" name="fee_to[{{ $feecount }}]">
                                <p>days before embarkation:</p>
                                <div class="service-general-rdate add-service-tab-general-left tab-conditions-payment-days">
                                    <div class="modal-add-service-input-value service-general-span">
                                        <span>%</span>
                                    </div>
                                    <input type="text" name="fee_percent[{{ $feecount }}]" value="{{ $fee->percent }}">
                                </div>
                                <p>of tour price</p>
                                <div class="service-itinerary-table-bottom-right">
                                    <div class="service-itinerary-table-bottom-right-elem service-rates-bottom-spans">
                                        <span class="glyphicon glyphicon-minus anchor-service-option-remove"></span>
                                    </div>
                                    <div class="service-itinerary-table-bottom-right-elem service-rates-bottom-spans">
                                        <span class="glyphicon glyphicon-plus anchor-service-option-add"></span>
                                    </div>
                                </div>
                            </div>
                        @endforeach
                        <div class="modal-add-service-right-element tab-conditions-elem journeyFee">
                            <p class="first-p">Up to</p>
                            <input class="input-for-upto" type="text" name="fee_from[0]">
                            <p class="tab-conditions-elem-dash">-</p>
                            <input type="checkbox">
                            <input class="input-for-upto feeToInput" type="text" name="fee_to[0]">
                            <p>days before embarkation:</p>
                            <div class="service-general-rdate add-service-tab-general-left tab-conditions-payment-days">
                                <div class="modal-add-service-input-value service-general-span">
                                    <span>%</span>
                                </div>
                                <input type="text" name="fee_percent[0]" value="">
                            </div>
                            <p>of tour price</p>
                            <div class="service-itinerary-table-bottom-right">
                                <div class="service-itinerary-table-bottom-right-elem service-rates-bottom-spans">
                                    <span class="glyphicon glyphicon-minus anchor-service-option-remove"></span>
                                </div>
                                <div class="service-itinerary-table-bottom-right-elem service-rates-bottom-spans">
                                    <span class="glyphicon glyphicon-plus anchor-service-option-add"></span>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        @role('show_cruises_implementation')
        <form id="implementation" class="reservations tab-pane fade clearfix">
            @if(!empty($types_info))
                <div class="tab-implementation-row">
                    <div class="tab-implementation-row-left">
                        <label>Cruise details:</label>
                        <table class="table table-striped custom-table tab-implementation-table">
                            <tbody>
                            <tr>
                                <td>
                                    <div class="tab-implementation-table-cell-left">Factual number of cabins / pax:</div>
                                    <div class="tab-implementation-table-cell-right">{{ $types_info["overall"]["factual_cabins"] }}/{{ $types_info["overall"]["factual_pax"] }}</div>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <div class="tab-implementation-table-cell-left">Cabins / Pax overbooked:</div>
                                    <div class="tab-implementation-table-cell-right">{{ $types_info["overall"]["overbooked_cabins"] }}/{{ $types_info["overall"]["overbooked_pax"] }}</div>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <div class="tab-implementation-table-cell-left">Cabins / Pax to sell:</div>
                                    <div class="tab-implementation-table-cell-right">{{ $types_info["overall"]["sell_cabins"] }}/{{ $types_info["overall"]["sell_pax"] }}</div>
                                </td>
                                </tr>
                                <tr>
                                    <td>
                                        <div class="tab-implementation-table-cell-left">Cabins / Pax with Names:</div>
                                        <div class="tab-implementation-table-cell-right">{{ $types_info["overall"]["names_cabins"] }}/{{ $types_info["overall"]["names_pax"] }}</div>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <div class="tab-implementation-table-cell-left">Cabins / Pax booked (no Names):</div>
                                        <div class="tab-implementation-table-cell-right">{{ $types_info["overall"]["no_names_cabins"] }}/{{ $types_info["overall"]["no_names_pax"] }}</div>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <div class="tab-implementation-table-cell-left">Cabins / Pax on option:</div>
                                        <div class="tab-implementation-table-cell-right">{{ $types_info["overall"]["option_cabins"] }}/{{ $types_info["overall"]["option_pax"] }}</div>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <div class="tab-implementation-table-cell-left">Cabins / Pax booked & option:</div>
                                        <div class="tab-implementation-table-cell-right">{{ $types_info["overall"]["booked_options_cabins"] }}/{{ $types_info["overall"]["booked_options_pax"] }}</div>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <div class="tab-implementation-table-cell-left">Cabins / Pax available:</div>
                                        <div class="tab-implementation-table-cell-right">{{ $types_info["overall"]["available_cabins"] }}/{{ $types_info["overall"]["available_pax"] }}</div>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <div class="tab-implementation-table-cell-left">Cabins / Pax Inquiry:</div>
                                        <div class="tab-implementation-table-cell-right">{{ $types_info["overall"]["inquiry_cabins"] }}/{{ $types_info["overall"]["inquiry_pax"] }}</div>
                                    </td>
                                </tr>
                                </tbody>
                                </table>
                            </div>
                            <div class="tab-implementation-row-right"></div>
                        </div>
                        <div class="tab-implementation-row">
                            @php $countTypes = 0; @endphp
                            @foreach($data->types as $cruise_type)
                                <div class="tab-implementation-row-{{ ++$countTypes % 2 == 0 ? 'right' : 'left' }}">
                                    <label>A+:</label>
                                    <table class="table table-striped custom-table tab-implementation-table">
                                        <tbody>
                                        <tr>
                                            <td>
                                                <div class="tab-implementation-table-cell-left">Factual number of cabins / pax:</div>
                                                <div class="tab-implementation-table-cell-right">{{ $types_info["types"][$cruise_type->id]["factual_cabins"] }}/{{ $types_info["types"][$cruise_type->id]["factual_pax"] }}</div>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <div class="tab-implementation-table-cell-left">Cabins / Pax overbooked:</div>
                                                <div class="tab-implementation-table-cell-right"><input type="text" name="overbooking[{{ $cruise_type->id }}]" value="{{ $types_info["types"][$cruise_type->id]["overbooked_cabins"] }}"></span>/{{ $types_info["types"][$cruise_type->id]["overbooked_pax"] }}</div>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <div class="tab-implementation-table-cell-left">Cabins / Pax to sell:</div>
                                                <div class="tab-implementation-table-cell-right">{{ $types_info["types"][$cruise_type->id]["sell_cabins"] }}/{{ $types_info["types"][$cruise_type->id]["sell_pax"] }}</div>
                                        </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <div class="tab-implementation-table-cell-left">Cabins / Pax with Names:</div>
                                                <div class="tab-implementation-table-cell-right">{{ $types_info["types"][$cruise_type->id]["names_cabins"] }}/{{ $types_info["types"][$cruise_type->id]["names_pax"] }}</div>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <div class="tab-implementation-table-cell-left">Cabins / Pax booked (no Names):</div>
                                                <div class="tab-implementation-table-cell-right">{{ $types_info["types"][$cruise_type->id]["no_names_cabins"] }}/{{ $types_info["types"][$cruise_type->id]["no_names_pax"] }}</div>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <div class="tab-implementation-table-cell-left">Cabins / Pax on option:</div>
                                                <div class="tab-implementation-table-cell-right">{{ $types_info["types"][$cruise_type->id]["option_cabins"] }}/{{ $types_info["types"][$cruise_type->id]["option_pax"] }}</div>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <div class="tab-implementation-table-cell-left">Cabins / Pax booked & option:</div>
                                                <div class="tab-implementation-table-cell-right">{{ $types_info["types"][$cruise_type->id]["booked_options_cabins"] }}/{{ $types_info["types"][$cruise_type->id]["booked_options_pax"] }}</div>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <div class="tab-implementation-table-cell-left">Cabins / Pax available:</div>
                                                <div class="tab-implementation-table-cell-right">{{ $types_info["types"][$cruise_type->id]["available_cabins"] }}/{{ $types_info["types"][$cruise_type->id]["available_pax"] }}</div>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <div class="tab-implementation-table-cell-left">Cabins / Pax Inquiry:</div>
                                                <div class="tab-implementation-table-cell-right">{{ $types_info["types"][$cruise_type->id]["inquiry_cabins"] }}/{{ $types_info["types"][$cruise_type->id]["inquiry_pax"] }}</div>
                                            </td>
                                        </tr>
                                    </tbody>
                                </table>
                            </div>
                        @endforeach
                    </div>
                @endif
            </form>
        @endrole
        <div id="specials" class="reservations tab-pane fade clearfix">
            <div class="base-page-navigation-cruises">
                <div class="base-page-table-label">
                    <p>List of specials</p>
                </div>
                @role('edit_cruises')
                <div class="base-page-table-add-button">
                    <button type="button" data-id="new" class="base-page-table-add editSpecial">Add special</button>
                </div>
                @endrole
            </div>
            <table id="specialsTable" class="table table-striped custom-table table-specials">
                <thead>
                <tr>
                    <th>Start</th>
                    <th>Expiry</th>
                    <th>Description</th>
                </tr>
                </thead>
                <tbody>
                @if(isset($data->specials))
                    @foreach ($data->specials as $special)
                        <tr>
                            <td>{{ $special->date_start or '-' }}</td>
                            <td>{{ $special->date_end or '-' }}</td>
                            <td>
                                <div class="table-floating-left-element">{{ $special->description or '-'  }}</div>
                                <ul class="custom-dropdown-table">
                                    <li class="dropdown">
                                        <a class="dropdown-toggle inner-table-button-hovered" data-toggle="dropdown" href="#"><span class="glyphicon glyphicon-option-vertical"></span></a>
                                        <ul class="dropdown-menu">
                                            <li>
                                                <a data-id="{{ $special->id }}" class="editSpecial" href="#"><img src="/src/img/pen-icon.png" alt=""></a>
                                            </li>
                                            @role('edit_directories')
                                            @role('edit_cruises')
                                            <li>
                                                <a data-id="{{ $special->id }}" class="deleteSpecial" href="#"><img src="/src/img/trash-icon.png" alt=""></a>
                                            </li>
                                            @endrole
                                            @endrole
                                        </ul>
                                    </li>
                                </ul>
                            </td>
                        </tr>
                    @endforeach
                @endif
                </tbody>
            </table>
        </div>
        @role('show_reservations')
        @role('show_cruises_reservations')
        <div id="reservations" class="tab-pane fade">
            @include('reservations.model_list')
        </div>
        @endrole
        @endrole
    </div>

    <div class="modal fade" id="specialModal" tabindex="-1" role="dialog" aria-labelledby="specialModalLabel">
        <div class="modal-dialog" role="document">
            <div class="modal-content"></div>
        </div>
    </div>
@endsection

@section('script')
    <script src="{{ url('/src/js/select2.min.js') }}"></script>
    <script>
        $( document ).ready(function() {
            $('input[name=cruise_type]').change(function() {
                $('select[name=cruise]').attr('disabled', $(this).val() == 'false');
                $('select[name=extensions_type]').attr('disabled', $(this).val() == 'false');
            });

            $('#services select[name=itinerary]').change(function() {
                if ($(this).val() != 'false')
                    $('#journeyFromTo').val($(this).find('option[value=' + $(this).val() + ']').attr('data-id'));
                else
                    $('#journeyFromTo').val('');

                var data = { itinerary_id: $(this).val() };
                if (cruiseID)
                    data["journey_id"] = cruiseID;
                $.ajax({
                    dataType: "html",
                    method: "GET",
                    cache: false,
                    url: '/services/cruise',
                    data: data,
                    success: function (itineraries_service) {
                        $('#services .itinerary_services').empty().append(itineraries_service);
                        $(".nonCompatibles").select2();
                    }
                });
            });

            $("#cruiseVenue, .supplementGroup, .nonCompatibles").select2();

            $('body').on('select2:select', '.supplementGroup', function (e) {
                var $this = $(this);
                $this.closest('.tab-rates-tables').find('option[value=' + e.params.data.id + ']').not(e.params.data.element).prop('disabled', true);
                $(".tab-rates-tables .supplementGroup").select2();
            });

            $('body').on('select2:unselect', '.supplementGroup', function (e) {
                $(this).closest('.tab-rates-tables').find('option[value=' + e.params.data.id + ']').prop('disabled', false);
                $(".tab-rates-tables .supplementGroup").select2();
            });

            $('body').on('click', '.anchor-service-rates-remove', function () {
                $(this).closest('.service-rates-table-bottom').remove();
            });

            $('.anchor-service-rates-add').on('click', function () {
                var $supplement = $(this).closest('.service-rates-table-bottom'),
                    deckId = parseInt($(this).attr('data-deck-id')),
                    $options = $supplement.find('select').clone();

                $options.find('option').removeAttr("selected");
                supplementsCount++;

                $('<div class="service-rates-table-bottom"><div class="anchor-service-rates-insert"><div class="service-rates-table-bottom-elem"><div class="modal-add-service-right-element tab-rates-bottom-table-elems"><div class="service-general-rdate add-service-tab-general-left"><label>Single supplement:</label><div class="modal-add-service-input-value service-general-span"><span>%</span></div><input type="text" name="supplement_rate[' + supplementsCount + ']"></div></div><div class="modal-add-service-right-element tab-rates-bottom-table-elems"><div class="service-general-rdate add-service-tab-general-left"><label>Number of cabins available with a surcharge of less than 100%:</label><input class="tab-rates-number-of-cabins" type="text" name="placements_amount[' + supplementsCount + ']"><div class="service-itinerary-table-bottom-right"><div class="service-itinerary-table-bottom-right-elem service-rates-bottom-spans"><span class="glyphicon glyphicon-minus anchor-service-rates-remove"></span></div></div></div></div></div></div><div class="modal-table-iteniraries-right full-width tab-rates-bottom-table"><select class="modal-table-iteniraries-right-select supplementGroup" multiple="multiple" name="supplement_group[' + supplementsCount + '][]">' + $options.html() + '</select></div><input type="hidden" name="deck_id[' + supplementsCount + ']" value="' + deckId + '"></div>').insertBefore($supplement);
                $(".supplementGroup").select2();
            });

            @if(!Auth::user()->hasRole('edit_cruises'))
            $('.content input, .content select, .content textarea').each(function () {
                $(this).prop('disabled', true);
            });
            @endif
            $('#cruiseAvailability').change(function() {
                $('#cruiseBoardPrice').attr('disabled', !this.checked)
            });
            $('input[name=all_pax_cabin]').change(function() {
                if($(this).val() == 'false')
                    $('#cruiseMultiply').attr('disabled', true)
                else
                    $('#cruiseMultiply').attr('disabled', false)
            });

            @if(isset($data->id))
            var cruiseID = '{{ $data->id }}';
            modelReservations(cruiseID, 'cruise', '{{ Session::token() }}', $('#reservations'), '#reservations');
            var supplementsCount = {{ $supplements_count }};
            @else
            var cruiseID = 0;
            @endif

            @if(!isset($data->id))
            $('body').on('click', '#cruiseNext', function() {
                var cruiseData = $('#general form').serializeArray();
                var button = $(this);
                $.ajax({
                    type: "POST",
                    url: "/save/cruise",
                    cache: false,
                    async: false,
                    data: cruiseData,
                    success: function (ajaxPackageID) {
                        cruiseID = ajaxPackageID;
                        //ServicesList(ajaxPackageID);

                        $('#general form').append('<input type="hidden" name="id" value="' + ajaxPackageID + '">');

                        button.empty().attr('id', 'saveCruise').append('<span class="glyphicon glyphicon-ok"></span> Save');

                        $('#cruiseNav').append('<li><a data-toggle="tab" href="#services">Services</a></li>');
                        $('#cruiseNav a').eq(1).trigger('click');
                    }
                });
            });
            @endif
            $('body').on('click', '#saveCruise', function() {
                $.ajax({
                    type: "POST",
                    url: "/save/cruise",
                    cache: false,
                    data: $('#general form, #services form, #rates form, #booking form, #implementation, #specials form, #reservations form').serializeArray(),
                    success: function () {
                        window.location.replace('{{ url('/cruises') }}');
                    },
                    async: false
                });
            });

            $('.anchor-service-itinerary-add-select').on('click', function() {
                var selectType = $(this).attr('data-select-type'),
                    selectCount = $(this).attr('data-select-count'),
                    $thisBlock = $(this).closest('.non-compatible'),
                    $options = $thisBlock.find('select').clone();
                $(this).attr('data-select-count', parseInt(selectCount) + 1);

                $options.find('option').removeAttr("selected");

                $('<div class="non-compatible"><div class="service-itinerary-table-bottom anchor-service-itinerary-insert anchor-copied"><div class="service-itinerary-table-bottom-left"><div class="modal-table-iteniraries-right full-width"><select data-select-type="' + selectType + '" data-select-count="' + selectCount + '" class="modal-table-iteniraries-right-select" multiple="multiple">' + $options.html() + '</select></div></div><div class="service-itinerary-table-bottom-right"><div class="service-itinerary-table-bottom-right-elem"><span class="glyphicon glyphicon-minus anchor-service-itinerary-remove-select"></span></div></div></div></div>').insertBefore($thisBlock);

                $(".modal-table-iteniraries-right-select").select2();
            });

            $('body').on('click', '.anchor-service-itinerary-remove-select', function() {
                $(this).closest('.non-compatible').remove();
            });

            $('.journeyFee [type=checkbox]').on('change', function () {
                if($(this).is(':checked'))
                    $(this).closest('.row').find('.feeToInput').attr('disabled', false);
                else
                    $(this).closest('.row').find('.feeToInput').attr('disabled', true);
            });

            function SpecialsList (id) {
                $( "#specialsTable" ).empty().load( '/cruise/' + id + ' #specialsTable', function() {
                });
            }

            function SpecialModal(id, custom_data) {
                $('#specialModal .modal-content').empty();

                var url = "/special/" + id;
                var dataType;

                if (id == 'new')
                    url = "/special/" + id + "/" + cruiseID;

                $.ajax({
                    dataType: "html",
                    method: "GET",
                    cache: false,
                    url: url,
                    data: custom_data,
                    success: function (data) {
                        if (!custom_data)
                            $('#specialModal').modal('show');

                        $("#specialModal .modal-content").prepend(data);
                        @if(!Auth::user()->hasRole('edit_cruises'))

                        $('#specialModal input, #specialModal select, #specialModal textarea').each(function () {
                            $(this).prop('disabled', true);
                        });
                        @endif

                        $('#cabinTypes, #specialServices, #specialPackages, #specialExtensions').select2();

                        $('#specialType').on('change', function () {
                            if ($(this).val() == 'false')
                                $('#specialFields').empty();
                            else if ($(this).val() != 'false')
                                SpecialModal(id, {type: $(this).val()});
                        });

                        $('#offerType').on('change', function () {
                            if($(this).val() == 'false') {
                                $('#offerAmount').attr('disabled', true);
                                $('#specialPackages').attr('disabled', true).closest('div').css('display', 'none');
                                $('#specialServices').attr('disabled', true).closest('div').css('display', 'none');
                            } else {
                                $('#offerAmount').attr('disabled', false);
                            }

                            if ($(this).val() == 'free_services') {
                                $('#specialServices').attr('disabled', false).closest('div').css('display', 'block');
                                $('#specialPackages').attr('disabled', true).closest('div').css('display', 'none');
                                $('#specialExtensions').attr('disabled', true).closest('div').css('display', 'none');
                            } else if ($(this).val() == 'free_packages') {
                                $('#specialPackages').attr('disabled', false).closest('div').css('display', 'block');
                                $('#specialServices').attr('disabled', true).closest('div').css('display', 'none');
                                $('#specialExtensions').attr('disabled', true).closest('div').css('display', 'none');
                            } else if ($(this).val() == 'free_extensions') {
                                $('#specialExtensions').attr('disabled', false).closest('div').css('display', 'block');
                                $('#specialServices').attr('disabled', true).closest('div').css('display', 'none');
                                $('#specialPackages').attr('disabled', true).closest('div').css('display', 'none');
                            }
                        });

                        $('#saveSpecial').on('click', function() {
                            $.ajax({
                                dataType: "html",
                                data: $('#specialModal form').serialize(),
                                method: "POST",
                                cache: false,
                                url: '/save/special',
                                success: function (data) {
                                    SpecialsList(cruiseID);
                                    $('#specialModal').modal('hide');
                                }
                            });
                        });
                    }
                });
            }

            $(document).on('click', '.editSpecial', function () {
                SpecialModal($(this).attr('data-id'));
            });

            $(document).on('click', '.deleteSpecial', function() {
                var dataID = $(this).attr('data-id');
                $.ajax({
                    dataType: "html",
                    method: "GET",
                    cache: false,
                    url: '/delete/special/' + dataID,
                    success: function (data) {
                        SpecialsList(cruiseID);
                    }
                });
            });
        });
    </script>
@endsection