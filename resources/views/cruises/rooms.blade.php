<table id="rooms" class="table table-striped">
    <tbody>
    <thead>
    <tr>
        <th>ID</th>
        <th>NET price</th>
        <th>REC price</th>
        <th>Number of rooms</th>
    </tr>
    </thead>
    @foreach ($data as $each)
        <tr data-id="{{ $each->id }}">
            @if(isset($each->name))
                <td>{{ $each->name }}</td>
            @else
                <td>-</td>
            @endif
            <td>
                <a href="{{ url('/package/' . $each->id) }}"><span class="glyphicon glyphicon-edit"></span></a>
                @role('edit_directories')
                @role('edit_packages')
                <a href="{{ url('/delete/package/' . $each->id) }}"><span class="glyphicon glyphicon-trash"></span></a>
                @endrole
                @endrole
            </td>
        </tr>
        @endforeach
        </tbody>
</table>