@extends('layouts.app')

@section('title')Cruises @endsection

@section('content')
    <div class="search-block">
        <div class="search">
            @include('includes.breadcrumbs')
        </div>
    </div>
    <div class="base-page-navigation-table">
        <div class="search-cities">
            @include('includes.table-search', ['oldSearch' => Request::get('search'), 'searchKeys' => ['all' => 'All', 'depart_date' => 'Departure date', 'ship.name' => 'Ship', 'itinerary.name' => 'Itinerary', 'itinerary.from_to' => 'From / To', 'itinerary.nights' => 'Duration nights', 'season' => 'Season'], 'otherKeys' => ['order', 'order_field']])
        </div>
        <div class="base-page-table-label">
            <p>List of cruises</p>
        </div>
        <div class="base-page-table-add-button">
            @role('edit_directories')
            @role('edit_cruises')
            <a href="{{ url('/cruise/new') }}" data-id="new" class="base-page-table-add">Add cruise</a>
            @endrole
            @endrole
        </div>
    </div>
    <table id="cruises" class="table table-striped custom-table table-cruises">
        <thead>
        <tr>
            <th>Departure date
                @include('includes.table-sort', ['orderField' => 'depart_date', 'sortSearchKeys' => ['field', 'search', 'page']])
            </th>
            <th>Ship
                @include('includes.table-sort', ['orderField' => 'ship.name', 'sortSearchKeys' => ['field', 'search', 'page']])
            </th>
            <th>Itineraries
                @include('includes.table-sort', ['orderField' => 'itinerary.name', 'sortSearchKeys' => ['field', 'search', 'page']])
            </th>
            <th>From / To
                @include('includes.table-sort', ['orderField' => 'itinerary.from_to', 'sortSearchKeys' => ['field', 'search', 'page']])
            </th>
            <th>Duration
                @include('includes.table-sort', ['orderField' => 'itinerary.nights', 'sortSearchKeys' => ['field', 'search', 'page']])
            </th>
            <th>Season
                @include('includes.table-sort', ['orderField' => 'season', 'sortSearchKeys' => ['field', 'search', 'page']])
            </th>
            <th>Available places</th>
            <th>Specials</th>
        </tr>
        </thead>
        <tbody>
        @foreach ($data as $each)
            <tr>
                <td>{{ $each->depart_date or '-' }}</td>
                <td>{{ $each->ship->name or '-' }}</td>
                <td>{{ $each->itinerary->name or '-' }}</td>
                <td>{{ $each->itinerary->from_to or '-' }}</td>
                @if(isset($each->itinerary))
                    <td>{{ $each->itinerary->nights + 1 }}D/{{ $each->itinerary->nights }}N</td>
                @else
                    <td>-</td>
                @endif
                <td>{{ $each->season or '-' }}</td>
                <td>{{ $each->places or 0 }}</td>
                <td>
                    <div class="table-floating-left-element">{{ $each->specials->count() }}</div>
                    <ul class="custom-dropdown-table">
                        <li class="dropdown">
                            <a class="dropdown-toggle inner-table-button-hovered" data-toggle="dropdown" href="#"><span class="glyphicon glyphicon-option-vertical"></span></a>
                            <ul class="dropdown-menu">
                                <li>
                                    <a href="{{ url('/cruise/' . $each->id) }}"><img src="/src/img/pen-icon.png" alt=""></a>
                                </li>
                                @role('edit_directories')
                                @role('edit_cruises')
                                <li>
                                    <a href="{{ url('/delete/cruise/' . $each->id) }}" class="delete-check-element"><img src="/src/img/trash-icon.png" alt=""></a>
                                </li>
                                @endrole
                                @endrole
                            </ul>
                        </li>
                    </ul>
                </td>
            </tr>
        @endforeach
        </tbody>
    </table>
    <div class="row">
        <div class="col-md-6">
            {{ $data->links() }}
        </div>
        <div class="col-md-6">
            <p>Display:</p>
            <a href="/cruises/setnumber?number=10" class="btn btn-default">10</a>
            <a href="/cruises/setnumber?number=50" class="btn btn-default">50</a>
            <a href="/cruises/setnumber?number=100" class="btn btn-default">100</a>
        </div>
    </div>
@endsection