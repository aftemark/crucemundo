@extends('layouts.app')

@section('title')
    Statistics
@endsection

@section('content')
    <div class="container">
        <div class="content">
            <h1>Statistics</h1>
            <p>{{ Session::get('catalog_year') }}</p>
        </div>
    </div>
@endsection