@extends('layouts.app')

@section('title')Hotel @endsection

@section('content')
    <div class="add-navigation">
        <div class="label-add-navigation">
            Hotel
        </div>
        <div class="buttons-add-navigation">
            @role('edit_directories')
            @role('edit_hotels')
            @if(isset($data->id))
                <button id="saveHotel" class="btn-create"><a href="#">Save</a></button>
            @else
                <button data-id="general" id="hotelNext" class="btn-create"><a href="#">Next</a></button>
            @endif
            @endrole
            @endrole
            <button class="btn-cancel"><a href="{{ url('/accommodations') }}">Cancel</a></button>
        </div>
    </div>
    <form id="hotelInfo">
        <div class="add-hotel-table-row1">
            <div class="add-hotel-table-elem">
                <p>ID:</p>
                <input type="text" name="code" value="{{ $data->code }}">
            </div>
            <div class="add-hotel-table-elem">
                <p>Hotel name:</p>
                <input type="text" name="name" value="{{ $data->name }}">
            </div>
            <div class="add-hotel-table-elem">
                <p>Star rating:</p>
                <input type="text" name="rating" value="{{ $data->rating }}">
            </div>
        </div>
        <div class="base-page-table-nav-buttons add-hotel-table-nav">
            <ul id="hotelNav" class="nav nav-tabs add-hotel-table-nav-tabs" role="tablist">
                @if(isset($data->id))
                    <li class="tab active"><a href="#general" data-toggle="tab">General information</a></li>
                    <li class="tab"><a href="#rooms" data-toggle="tab">Rooms</a></li>
                @else
                    <li class="tab active"><a href="#general" data-toggle="tab">General information</a></li>
                @endif
            </ul>
        </div>
    </form>
    <div class="tab-content">
        <div id="general" class="tab-pane fade in active">
            <form>
                {{ csrf_field() }}
                <div class="add-hotel-table-row2">
                    <div class="add-hotel-table-elem">
                        <p>City:</p>
                        <select name="city">
                            @foreach($data->cities as $cityId => $city)
                                <option value="{{ $cityId }}">{{ $city }}</option>
                            @endforeach
                        </select>
                    </div>
                    <div class="add-hotel-table-elem">
                        <p>Address:</p>
                        <input type="text" name="address" value="{{ $data->address }}">
                    </div>
                </div>
                <div class="add-hotel-table-row3">
                    <div class="add-hotel-table-elem">
                        <p>Services:</p>
                        <textarea name="services_desc" cols="30" rows="10">{{ $data->services_desc }}</textarea>
                    </div>
                </div>
                <div class="add-hotel-table-row4">
                    <div class="add-hotel-table-elem">
                        <p>Accommodation:</p>
                        <textarea name="add_desc" cols="30" rows="10">{{ $data->add_desc }}</textarea>
                    </div>
                </div>
                <div class="add-hotel-table-row5">
                    <div class="add-hotel-table-elem">
                        <p>Note:</p>
                        <textarea name="description" cols="30" rows="10">{{ $data->description }}</textarea>
                    </div>
                </div>
            </form>
        </div>
        <div id="rooms" class="tab-pane fade"></div>

    <div class="modal fade" id="roomModal" tabindex="-1" role="dialog" aria-labelledby="roomModalLabel">
        <div class="modal-dialog" role="document">
            <form class="modal-content"></form>
        </div>
    </div>
@endsection

@section('script')
    <script>
        $( document ).ready(function() {
            @if(!Auth::user()->hasRole('edit_hotels'))
            $('.content input, .content select, .content textarea').each(function () {
                $(this).prop('disabled', true);
            });

            @endif
            @if(isset($data->id))
            var hotelID = {{ $data->id }};
            @else
            var hotelID = 0;
            @endif

            @if(isset($data->id))
                RoomsList(hotelID);
            @endif
            function RoomsList (id) {
                $.ajax({
                    dataType: "html",
                    method: "GET",
                    cache: false,
                    url: '/rooms/' + id,
                    success: function (data) {
                        $("#rooms").empty();
                        $("#rooms").prepend(data);

                        $('.editRoom').on('click', function () {
                            roomModal($(this).attr('data-id'));
                        });

                        $('.deleteRoom').on('click', function() {
                            var dataID = $(this).attr('data-id');
                            $.ajax({
                                dataType: "html",
                                method: "GET",
                                cache: false,
                                url: '/delete/room/' + dataID,
                                success: function (data) {
                                    RoomsList(hotelID);
                                }
                            });
                        });

                        function roomModal(id) {
                            $('#roomModal .modal-content').empty();

                            $.ajax({
                                dataType: "html",
                                method: "GET",
                                cache: false,
                                url: "/room/" + id,
                                success: function (data) {
                                    $('#roomModal').modal('show');
                                    $("#roomModal .modal-content").prepend(data);
                                    @if(!Auth::user()->hasRole('edit_hotels'))

                                    $('#roomModal input, #roomModal select, #roomModal textarea').each(function () {
                                        $(this).prop('disabled', true);
                                    });
                                    @endif

                                    if($("#roomModal [name=type_category]").val() != 'false')
                                        TypeCategoryInfo($("#roomModal [name=type_category]").val());

                                    $('#hotelID').val(hotelID);

                                    $('[name=type_category]').on('change', function() {
                                        TypeCategoryInfo(parseInt($(this).val()));
                                    });

                                    $('#roomType').on('change', function() {
                                        if($(this).val() != 'false')
                                            PlacementTypeInfo(parseInt($(this).val()));
                                        else {
                                            $('#roomID').empty().append('<option value="false">Select ID</option>');
                                            $('#roomPaxAmount').val('');
                                            $('#roomBed').prop('checked', false);
                                        }
                                    });

                                    $('#saveRoom').on('click', function() {
                                        $.ajax({
                                            dataType: "html",
                                            data: $('#roomModal form').serialize(),
                                            method: "POST",
                                            cache: false,
                                            url: '/save/room',
                                            success: function (data) {
                                                RoomsList(hotelID);
                                                $('#roomModal').modal('hide');
                                            }
                                        });
                                    });
                                }
                            });
                        }

                    }
                });
            }

            function TypeCategoryInfo (id) {
                $.ajax({
                    dataType: "json",
                    method: "GET",
                    cache: false,
                    url: "/typecategory/" + id,
                    success: function (typeData) {
                        $('#roomPaxAmount').val('');
                        $('#roomBed').prop('checked', false);

                        $('#roomPaxAmount').val(typeData["PaxAmount"]);
                        if(typeData["Bed"] == '1')
                            $('#roomBed').prop('checked', true);
                    }
                });
            }

            function PlacementTypeInfo (typeId) {
                $.ajax({
                    dataType: "json",
                    method: "GET",
                    cache: false,
                    url: '/placementtypehotel/' + typeId,
                    success: function (typeData) {
                        $('#roomID').empty();
                        var count = 0;
                        $.each( typeData, function( key, value ) {
                            if(count == 0)
                                TypeCategoryInfo(key);
                            count++;
                            $('#roomID').append('<option value="' + key + '">' + value + '</option>');
                        });
                    }
                });
            }

            @if(!isset($data->id))
            $('body').on('click', '#hotelNext', function() {
                var hotelData = $('#general form, #hotelInfo').serializeArray();
                var button = $(this);
                $.ajax({
                    type: "POST",
                    url: "/save/hotel/new",
                    cache: false,
                    async: false,
                    data: hotelData,
                    success: function (ajaxHotelID) {
                        hotelID = ajaxHotelID;
                        RoomsList(ajaxHotelID);

                        button.attr('data-id', '').empty().attr('id', 'saveHotel').append('Save');

                        $('#hotelNav').append('<li><a data-toggle="tab" href="#rooms">Rooms</a></li>');
                        $('#hotelNav a').eq(1).trigger('click');
                    }
                });
            });
            @endif
            $('body').on('click', '#saveHotel', function() {
                $.ajax({
                    type: "POST",
                    url: "/save/hotel/" + hotelID,
                    cache: false,
                    data: $('#general form, #hotelInfo').serializeArray(),
                    success: function (ajaxHotelID) {
                        window.location.replace('{{ url('/accommodations') }}');
                    },
                    async: false
                });
            });
        });
    </script>
@endsection