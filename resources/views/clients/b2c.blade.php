@extends('layouts.app')

@section('title') {{ $client->name ? 'B2C Client - ' . $client->name : 'New B2C Client' }} @endsection

@section('content')
    <nav class="navbar second-nav">
        <div class="container-fluid">
            <div class="addships-title pull-left">
                <h3>{{ $client->name ? 'B2C Client - ' . $client->name : 'New B2C Client' }}</h3>
            </div>
            <div class="addships-buttons pull-right">
                @role('edit_directories')
                @role('edit_b2c')
                <button id="saveClient" type="button" class="btn-create btn-create">Save</button>
                @endrole
                @endrole
                <a href="{{ url('/clients') }}" class="btn-cancel">Cancel</a>
            </div>
        </div>
    </nav>
    <div class="base-page-table-nav-buttons b2c-nav-buttons">
        <ul class="nav nav-tabs b2c-page-tabs" role="tablist">
            @if(isset($client->id))
                <li class="tab active"><a href="#general" data-toggle="tab">General information</a></li>
                <li class="tab"><a href="#reservations" data-toggle="tab">Reservations</a></li>
                <li class="tab"><a href="#notifications" data-toggle="tab">Notifications</a></li>
            @else
                <li class="active"><a data-toggle="tab" href="#general">General information</a></li>
            @endif
        </ul>
    </div>
    </div>
    <div class="tab-content">
        <div id="general" class="clearfix b2c-general tab-pane active">
            <form class="tab-pane fade in active">
                <input type="hidden" name="type" value="b2c">
                {{ csrf_field() }}
                @if($client->id)
                    <input type="hidden" name="id" value="{{ $client->id }}">
                @endif
                <div class="left-side-b2c pull-left">
                    <div class="b2c-table-row2">
                        <p>Name:</p>
                        <input class="b2c-name-placeholder" type="text" name="name" value="{{ $client->name }}">
                        <p>Surname:</p>
                        <input class="b2c-surname-placeholder" type="text" name="second_name" value="{{ $client->second_name }}">
                        <p>Language:</p>
                        <select class="b2c-language-placeholder" name="language">
                            @if(!$client->id)
                                <option value="false">Select language</option>
                            @endif
                            @foreach($languages as $language)
                                <option {{ $client->language_id == $language->id ? 'selected ' : '' }}value="{{ $language->id }}">{{ $language->name }}</option>
                            @endforeach
                        </select>
                        <p>Country:</p>
                        <select class="b2c-country-placeholder" name="country">
                            @if(!$client->id)
                                <option value="false">Select country</option>
                            @endif
                            @foreach($countries as $country)
                                <option {{ $client->country_id == $country->id ? 'selected ' : '' }}value="{{ $country->id }}">{{ $country->name }}</option>
                            @endforeach
                        </select>
                        <p>Discount:</p>
                        <input class="b2c-discount-placeholder" type="text" name="percent" value="{{ $client->percent }}">
                        <p>Loyality card #:</p>
                        <input class="b2c-card-placeholder" type="text" name="loyal_number" value="{{ $client->loyal_number }}">
                        <div class="b2c-card-percent"><p>%</p></div>
                        <p>E-mail:</p>
                        <input class="b2c-email-placeholder" type="text" name="email" value="{{ $client->email }}">
                    </div>
                </div>
                <div class="right-side-b2c pull-right">
                    <div class="add-ships-table-row3 b2c-table-row3">
                        <div class="b2c-table-elem">
                            <p>Contacts:</p>
                            <textarea cols="30" rows="10" name="contact_info">{{ $client->contact_info }}</textarea>
                        </div>
                    </div>
                </div>
            </form>
        </div>
        @if($client->id)
            <div id="reservations" class="reservations tab-pane fade clearfix">
                @include('reservations.model_list')
            </div>
            <div id="notifications" class="reservations tab-pane fade clearfix">
                <table class="table table-striped custom-table table-notifications">
                    <thead>
                    <tr>
                        <th>Date and time</th>
                        <th>User</th>
                        <th>Action</th>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach ($reminders as $reminder)
                        <tr>
                            <td>{{ $reminder->created_at or '' }}</td>
                            <td>{{ $reminder->user->name or '' }}</td>
                            <td>{!! $reminder->text or '' !!}</td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
            </div>
        @endif
    </div>
@endsection

@section('script')
    <script>
        $(document).ready(function() {
            @if(!Auth::user()->hasRole('edit_b2c'))
            $('.content input, .content select, .content textarea').each(function () {
                $(this).prop('disabled', true);
            });

            @endif
            @if(isset($client->id))
            var clientID = '{{ $client->id }}';
            modelReservations(clientID, 'b2c', '{{ Session::token() }}', $('#reservations'));
            @else
            var clientID = 0;
            @endif

            @if(!isset($client->id))
            $('body').on('click', '#clientNext', function() {
                var hotelData = $('#general form').serializeArray();
                var button = $(this);
                $.ajax({
                    type: "POST",
                    url: "/save/client",
                    cache: false,
                    async: false,
                    data: hotelData,
                    success: function (ajaxClientID) {
                        clientID = ajaxClientID;
                        $('#general form').append('<input type="hidden" name="id" value="' + ajaxClientID + '">');

                        button.attr('data-id', '').empty().attr('id', 'saveClient').append('<span class="glyphicon glyphicon-ok"></span> Save');

                        $('#clientNav').append('<li><a data-toggle="tab" href="#reservations">Reservations</a></li><li><a data-toggle="tab" href="#notifications">Notifications</a></li>');
                        $('#clientNav a').eq(1).trigger('click');
                    }
                });
            });
            @endif
            $('body').on('click', '#saveClient', function() {
                $.ajax({
                    type: "POST",
                    url: "/save/client",
                    cache: false,
                    data: $('#general form').serializeArray(),
                    success: function () {
                        window.location.replace('{{ url('/clients') }}');
                    },
                    async: false
                });
            });
        });
    </script>
@endsection