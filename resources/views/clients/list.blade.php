@extends('layouts.app')

@section('title')Clients @endsection

@section('content')
    <div class="search-block">
        <div class="search">
            @include('includes.breadcrumbs')
        </div>
        <div class="base-page-table-nav-buttons">
            <ul class="nav nav-tabs" role="tablist">
                @role('show_b2c')
                <li class="tab{!! Request::input('table', NULL) == 'b2c' || !Request::has('table') ? ' active' : '' !!}"><a href="#b2c" data-toggle="tab">B2C</a></li>
                @endrole
                @role('show_b2b')
                <li class="tab{!! Request::input('table', NULL) == 'b2b' || !Auth::user()->hasRole('show_b2c') ? ' active' : '' !!}"><a href="#b2b" data-toggle="tab">B2B</a></li>
                @endrole
            </ul>
        </div>
    </div>
    <div class="tab-content">
        @role('show_b2c')
        <div id="b2c" class="tab-pane fade{!! Request::input('table', NULL) == 'b2c' || !Request::has('table') ? ' in active' : '' !!}">
            <div class="base-page-navigation-table">
                <div class="search-ships">
                    @include('includes.table-search', ['oldSearch' => Request::get('table') == 'b2c' ? Request::get('search') : '', 'searchKeys' => ['all' => 'All', 'name' => 'Name', 'country' => 'Country', 'language' => 'Language', 'percent' => 'Percent', 'loyal_number' => 'Loyal number'], 'otherKeys' => ['order', 'order_field'], 'tableName' => 'b2c'])
                </div>
                <div class="base-page-elements">
                    <div class="base-page-table-label">
                        <p>List of B2C clients</p>
                    </div>
                    <div class="base-page-table-add-button">
                        @role('edit_directories')
                        @role('edit_b2c')
                        <form action="{{ url('/b2c/new') }}">
                            <button class="base-page-table-add">Add B2C client</button>
                        </form>
                        @endrole
                        @endrole
                    </div>
                </div>
            </div>
            <table class="table table-striped custom-table table-clients">
                <thead>
                <tr>
                    <th>Name
                        @include('includes.table-sort', ['orderField' => 'name', 'sortSearchKeys' => ['field', 'search', 'page'], 'tableName' => 'b2c'])
                    </th>
                    <th>Country
                        @include('includes.table-sort', ['orderField' => 'country', 'sortSearchKeys' => ['field', 'search', 'page'], 'tableName' => 'b2c'])
                    </th>
                    <th>Language
                        @include('includes.table-sort', ['orderField' => 'language', 'sortSearchKeys' => ['field', 'search', 'page'], 'tableName' => 'b2c'])
                    </th>
                    <th>Discount
                        @include('includes.table-sort', ['orderField' => 'percent', 'sortSearchKeys' => ['field', 'search', 'page'], 'tableName' => 'b2c'])
                    </th>
                    <th>Loyal Number
                        @include('includes.table-sort', ['orderField' => 'loyal_number', 'sortSearchKeys' => ['field', 'search', 'page'], 'tableName' => 'b2c'])
                    </th>
                </tr>
                </thead>
                <tbody>
                @foreach ($data['b2c'] as $b2c)
                    <tr>
                        <td>{{ $b2c->name }} {{ $b2c->second_name }}</td>
                        <td>{{ $b2c->country->name }}</td>
                        <td>{{ $b2c->language->name }}</td>
                        <td>{{ $b2c->percent }} %</td>
                        <td>
                            <div class="table-floating-left-element">{{ $b2c->loyal_number }}</div>
                            <ul class="custom-dropdown-table">
                                <li class="dropdown">
                                    <a class="dropdown-toggle inner-table-button-hovered" data-toggle="dropdown" href="#"><span class="glyphicon glyphicon-option-vertical"></span></a>
                                    <ul class="dropdown-menu">
                                        <li>
                                            <a href="{{ url('/b2c/' . $b2c->id) }}"><img src="/src/img/pen-icon.png" alt=""></a>
                                        </li>
                                        @role('edit_directories')
                                        @role('edit_b2c')
                                        <li>
                                            <a href="{{ url('/delete/client/' . $b2c->id) }}" class="delete-check-element"><img src="/src/img/trash-icon.png" alt=""></a>
                                        </li>
                                        @endrole
                                        @endrole
                                    </ul>
                                </li>
                            </ul>
                        </td>
                    </tr>
                @endforeach
                </tbody>
            </table>
            <div class="row">
                <div class="col-md-6">{{ $data['b2c']->appends(array_except(Request::query(), 'b2c_clients'))->links() }}</div>
                <div class="col-md-6">
                    <a href="/clients/setnumber?number=10" class="btn btn-default{{ Session::get('clients_number') == '10' ? ' active' : '' }}">10</a>
                    <a href="/clients/setnumber?number=50" class="btn btn-default{{ Session::get('clients_number') == '50' ? ' active' : '' }}">50</a>
                    <a href="/clients/setnumber?number=100" class="btn btn-default{{ Session::get('clients_number') == '100' ? ' active' : '' }}">100</a>
                </div>
            </div>
        </div>
        @endrole
        @role('show_b2b')
        <div id="b2b" class="tab-pane fade{!! Request::input('table', NULL) == 'b2b' || !Auth::user()->hasRole('show_b2c') ? ' in active' : '' !!}">
            <div class="base-page-navigation-table">
                <div class="search-ships">
                    @include('includes.table-search', ['oldSearch' => Request::get('table') == 'b2b' ? Request::get('search') : '', 'searchKeys' => ['all' => 'All', 'name' => 'Name', 'country' => 'Country'], 'otherKeys' => ['order', 'order_field'], 'tableName' => 'b2b'])
                </div>
                <div class="base-page-elements">
                    <div class="base-page-table-label">
                        <p>List of B2B clients</p>
                    </div>
                    <div class="base-page-table-add-button">
                        @role('edit_directories')
                        @role('edit_b2b')
                        <form action="{{ url('/b2b/new') }}">
                            <button class="base-page-table-add">Add B2B client</button>
                        </form>
                        @endrole
                        @endrole
                    </div>
                </div>
            </div>
            <table class="table table-striped custom-table table-clients">
                <thead>
                <tr>
                    <th>Company Name
                        @include('includes.table-sort', ['orderField' => 'name', 'sortSearchKeys' => ['field', 'search', 'page'], 'tableName' => 'b2b'])
                    </th>
                    <th>Country
                        @include('includes.table-sort', ['orderField' => 'country', 'sortSearchKeys' => ['field', 'search', 'page'], 'tableName' => 'b2b'])
                    </th>
                    <th>Totally pax reserved</th>
                    <th>Total reservation price</th>
                    <th>Main accommodation</th>
                </tr>
                </thead>
                <tbody>
                @foreach ($data['b2b'] as $b2b)
                    <tr>
                        <td>{{ $b2b->name }}</td>
                        <td>{{ $b2b->country->name }}</td>
                        <td>{{ $b2b->total_pax or '-' }}</td>
                        <td>{{ $b2b->total_price or '-' }}</td>
                        <td>
                            <div class="table-floating-left-element">{{ $b2b->accommodation or '-' }}</div>
                            <ul class="custom-dropdown-table">
                                <li class="dropdown">
                                    <a class="dropdown-toggle inner-table-button-hovered" data-toggle="dropdown" href="#"><span class="glyphicon glyphicon-option-vertical"></span></a>
                                    <ul class="dropdown-menu">
                                        <li>
                                            <a href="{{ url('/b2b/' . $b2b->id) }}"><img src="/src/img/pen-icon.png" alt=""></a>
                                        </li>
                                        @role('edit_directories')
                                        @role('edit_b2b')
                                        <li>
                                            <a href="{{ url('/delete/client/' . $b2b->id) }}" class="delete-check-element"><img src="/src/img/trash-icon.png" alt=""></a>
                                        </li>
                                        @endrole
                                        @endrole
                                    </ul>
                                </li>
                            </ul>
                        </td>
                    </tr>
                @endforeach
                </tbody>
            </table>
            <div class="row">
                <div class="col-md-6">{{ $data['b2b']->appends(array_except(Request::query(), 'b2b_clients'))->links() }}</div>
                <div class="col-md-6">
                    <a href="/clients/setnumber?number=10" class="btn btn-default{{ Session::get('clients_number') == '10' ? ' active' : '' }}">10</a>
                    <a href="/clients/setnumber?number=50" class="btn btn-default{{ Session::get('clients_number') == '50' ? ' active' : '' }}">50</a>
                    <a href="/clients/setnumber?number=100" class="btn btn-default{{ Session::get('clients_number') == '100' ? ' active' : '' }}">100</a>
                </div>
            </div>
        </div>
        @endrole
    </div>
@endsection