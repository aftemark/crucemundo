<div>
    <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="userModalLabel">
            <div class="col-md-6">
                {{ isset($user->name) ? 'User ' . $user->name : 'New user' }}
            </div>
            <div class="col-md-6">
                <div class="row">
                    <div class="col-md-6">
                        @role('edit_directories')
                        @role('edit_b2b')
                        @if(isset($user->id))
                        <a id="saveUser" class="btn btn-success">Save</a>
                        @else
                        <a id="nextUser" class="btn btn-success">Next</a>
                        @endif
                        @endrole
                        @endrole
                    </div>
                    <div class="col-md-6">
                        <a data-dismiss="modal" class="btn btn-danger">Cancel</a>
                    </div>
                </div>
            </div>
        </h4>
    </div>
    <div class="modal-body">
        <ul class="nav nav-pills">
            <li class="active"><a data-toggle="tab" href="#userGeneral">General information</a></li>
            @if(isset($user->id))
            <li><a data-toggle="tab" href="#userReservations">Reservations</a></li>
            @if(isset($user->user))
            <li><a data-toggle="tab" href="#userNotifications">Notifications</a></li>
            <li><a data-toggle="tab" href="#userHistory">History</a></li>
            @endif
            @endif
        </ul>

        <div class="tab-content">
            <form action="{{ url('/b2b/save/user') }}" method="POST" id="userGeneral" class="tab-pane fade in active">
                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group">
                            <label>Name:</label>
                            <input type="text" name="name" class="form-control" value="{{ $user->name }}">
                        </div>
                        <div class="form-group">
                            <label>Status:</label>
                            <input type="text" name="status" class="form-control" value="{{ $user->status }}">
                        </div>
                        <div class="form-group">
                            <label>Post:</label>
                            <input type="text" name="post" class="form-control" value="{{ $user->post }}">
                        </div>
                        <div class="form-group">
                            <label>E-mail:</label>
                            <input type="text" name="email" class="form-control" value="{{ $user->email }}">
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            <label>Contacts:</label>
                            <textarea rows="6" type="text" name="contact_info" class="form-control">{{ $user->contact_info }}</textarea>
                        </div>
                    </div>
                </div>
                @if(isset($user->id))
                    <input type="hidden" name="id" value="{{ $user->id }}">
                    <input type="hidden" name="client_id" value="{{ $user->client_id }}">
                @endif
                <input type="hidden" name="_token" value="{{ Session::token() }}">
            </form>
            <div id="userReservations" class="tab-pane fade">
                @include('reservations.model_list')
            </div>
            @if(isset($user->user))
            <div id="userNotifications" class="tab-pane fade"></div>
            <div id="userHistory" class="tab-pane fade">
                <table class="table table-striped">
                    <thead>
                        <tr>
                            <th>Date and time</th>
                            <th>Action</th>
                        </tr>
                    </thead>
                    <tbody>
                    @foreach ($user->user->notifications()->where('type', 'action')->get() as $notification)
                        <tr>
                            <td>{{ $notification->created_at }}</td>
                            <td>{!! $notification->text !!}</td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
            </div>
            @endif
        </div>
    </div>
</div>