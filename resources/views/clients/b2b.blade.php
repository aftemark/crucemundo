@extends('layouts.app')

@section('title') {{ $client->name ? 'B2B Client - ' . $client->name : 'New B2B Client' }} @endsection

@section('css')
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.3/css/select2.min.css">
@endsection

@section('content')
    <nav class="navbar second-nav">
        <div class="container-fluid">
            <div class="addships-title pull-left">
                <h3>{{ $client->name ? 'B2B Client - ' . $client->name : 'New B2B Client' }}</h3>
            </div>
            <div class="addships-buttons pull-right">
                @role('edit_directories')
                @role('edit_b2b')
                @if(isset($client->id))
                    <button id="saveClient" type="button" class="btn-create">Save</button>
                @else
                    <button data-id="general" id="clientNext" type="button" class="btn-create next-btn">Next</button>
                @endif
                @endrole
                @endrole
                <a href="{{ url('/clients') }}" class="btn-cancel">Cancel</a>
            </div>
        </div>
    </nav>
    <div class="base-page-table-nav-buttons b2c-nav-buttons">
        <ul id="clientNav" class="nav nav-tabs b2c-page-tabs" role="tablist">
            @if(isset($client->id))
                <li class="active"><a data-toggle="tab" href="#general">General information</a></li>
                <li><a data-toggle="tab" href="#commission">Commission</a></li>
                <li><a data-toggle="tab" href="#bank_details">Bank details</a></li>
                <li><a data-toggle="tab" href="#users">Users</a></li>
                <li><a data-toggle="tab" href="#reservations">Reservations</a></li>
                <li><a data-toggle="tab" href="#notifications">Notifications</a></li>
                <li><a data-toggle="tab" href="#history">History</a></li>
            @else
                <li class="active"><a data-toggle="tab" href="#general">General information</a></li>
            @endif
        </ul>
    </div>
    <div class="tab-content">
        <form id="general" class="clearfix tab-pane active">
            {{ csrf_field() }}
            <input type="hidden" name="type" value="b2b">
            <div class="b2b-general clearfix">
                <div class="left-side-b2c pull-left">
                    <div class="b2c-table-row2">
                        <p>Company name:</p>
                        <input name="name" class="b2c-name-placeholder" type="text" value="{{ $client->name }}">
                        <p>ID:</p>
                        <input name="code" class="b2c-surname-placeholder" type="text" value="{{ $client->code }}">
                        <p>Country:</p>
                        <select class="b2c-country-placeholder" name="country">
                            @if(!$client->country_id)
                                <option value="false">Select country</option>
                            @endif
                            @foreach($countries as $country)
                                <option {{ $client->country_id == $country->id ? 'selected ' : '' }}value="{{ $country->id }}">{{ $country->name }}</option>
                            @endforeach
                        </select>
                        <p>Main E-mail:</p>
                        <input class="b2c-email-placeholder" name="email" type="text" value="{{ $client->email }}">
                    </div>
                </div>
                <div class="right-side-b2c pull-right">
                    <div class="add-ships-table-row3 b2c-table-row3">
                        <div class="b2b-table-elem">
                            <p>Contacts:</p>
                            <textarea name="contact_info" cols="30" rows="10">{{ $client->contact_info }}</textarea>
                        </div>
                    </div>
                </div>
            </div>
            @if(isset($client->id))
                <table class="table table-striped b2b-table-general">
                    <tbody>
                    <tr>
                        <td>Totaly invoiced:</td>
                        <td>0</td>
                    </tr>
                    <tr>
                        <td>Totaly received:</td>
                        <td>0</td>
                    </tr>
                    <tr>
                        <td>Debt:</td>
                        <td>0</td>
                    </tr>
                    <tr>
                        <td>Total Reservations Price:</td>
                        <td>0</td>
                    </tr>
                    <tr>
                        <td>Totaly pax carried:</td>
                        <td>0</td>
                    </tr>
                    </tbody>
                </table>
            @endif
        </form>
        <form id="commission" class="clearfix tab-pane">
            @role('edit_directories')
            @role('edit_b2b')
            <div class="row" style="text-align: right;">
                <button class="btn btn-warning addCommission">Add commission</button>
            </div>
            @endrole
            @endrole
            @if(isset($client->id))
                @foreach ($commissions as $percent => $commission)
                    <div class="commission">
                        <div class="commission-level clearfix">
                            <p>Commission:</p>
                            <input type="text" class="commissionPercent" name="commission" value="{{ $percent }}">
                            <div class="commission-percent"><span class="percent">%</span></div>
                            @role('edit_directories')
                            @role('edit_b2b')
                            <a class="remove-commission" href="#"><span class="glyphicon glyphicon-trash"></span></a>
                            @endrole
                            @endrole
                        </div>
                        <div class="commission-ships">
                            <p>Ships:</p>
                            <select multiple name="ships" class="selectShips">
                                @if(!empty($commission["ships"]))
                                    @foreach($commission["ships"] as $shipid => $shipname)
                                        <option selected value="{{ $shipid }}">{{ $shipname }}</option>
                                    @endforeach
                                @endif
                            </select>
                        </div>
                        <div class="commission-hotel">
                            <p>Hotel:</p>
                            <select multiple class="selectHotels" name="hotels">
                                @if(!empty($commission["hotels"]))
                                    @foreach($commission["hotels"] as $hotelid => $hotelname)
                                        <option selected value="{{ $hotelid }}">{{ $hotelname }}</option>
                                    @endforeach
                                @endif
                            </select>
                        </div>
                    </div>
                @endforeach
            @endif
        </form>
        <form id="bank_details" class="tab-pane">
            <textarea class="bank-details" name="pay_info" cols="30" rows="10">{{ $client->pay_info }}</textarea>
        </form>
            <div id="users" class="tab-pane">
                <form class="b2b-users">
                    <select name="type">
                        <option value="all">All</option>
                        <option value="name">Name</option>
                        <option value="country">Post</option>
                        <option value="language">Status</option>
                    </select>
                    <input class="search-placeholder" type="text" placeholder="Search" value="{{ Request::get('search') }}">
                    <input class="search-submit" type="image" src="/src/img/search.png">
                    @role('edit_directories')
                    @role('edit_b2b')
                    <button type="button" data-id="new" class="base-page-table-add editUser">Add user</button>
                    @endrole
                    @endrole
                </form>
                <table class="table table-striped custom-table table-b2b-add-user">
                    <thead>
                    <tr>
                        <th>Name</th>
                        <th>Post</th>
                        <th>Status</th>
                    </tr>
                    </thead>
                    <tbody>
                    @if(isset($client->id))
                    @foreach ($users as $user)
                        <tr data-id="{{ $user->id }}">
                            <td>{{ $user->name }}</td>
                            <td>{{ $user->post }}</td>
                            <td>
                                <div class="table-floating-left-element">{{ $user->status }}</div>
                                <ul class="custom-dropdown-table">
                                    <li class="dropdown">
                                        <a class="dropdown-toggle inner-table-button-hovered" data-toggle="dropdown" href="#"><span class="glyphicon glyphicon-option-vertical"></span></a>
                                        <ul class="dropdown-menu">
                                            <li>
                                                <a class="editUser" data-id="{{ $user->id }}"><img src="/src/img/pen-icon.png" alt=""></a>
                                            </li>
                                            <li>
                                                @role('edit_directories')
                                                @role('edit_b2b')
                                                <a class="delete-check-element" href="{{ url('/b2b/delete/user/' . $user->id) }}"><img src="/src/img/trash-icon.png" alt=""></a>
                                                @endrole
                                                @endrole
                                            </li>
                                        </ul>
                                    </li>
                                </ul>
                            </td>
                        </tr>
                    @endforeach
                    @endif
                    </tbody>
                </table>
            </div>
            @if(isset($client->id))
            <div id="reservations" class="tab-pane fade">
                @include('reservations.model_list')
            </div>
            <div id="notifications" class="tab-pane fade">
                <table class="table table-striped">
                    <thead>
                    <tr>
                        <th>Date and time</th>
                        <th>User</th>
                        <th>Action</th>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach ($reminders as $reminder)
                        <tr>
                            <td>{{ $reminder->created_at or '' }}</td>
                            <td>{{ $reminder->user->name or '' }}</td>
                            <td>{!! $reminder->text or '' !!}</td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
            </div>
            <div id="history" class="tab-pane fade">
                <table class="table table-striped">
                    <thead>
                    <tr>
                        <th>Date and time</th>
                        <th>Action</th>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach ($notifications as $notification)
                        <tr>
                            <td>{{ $notification->created_at }}</td>
                            <td>{!! $notification->text !!}</td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
            </div>
        @endif
    </div>

    <div class="modal fade" id="userModal" tabindex="-1" role="dialog" aria-labelledby="userModalLabel">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
            </div>
        </div>
    </div>
@endsection

@section('script')
    <script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.3/js/select2.min.js"></script>
    <script>
        $( document ).ready(function() {
            $('.selectShips').select2();
            $('.selectHotels').select2();
            var allHotels = <?php echo json_encode($allHotels); ?>,
            allShips = <?php echo json_encode($allShips); ?>;

            $(document).on("select2:opening", '.selectShips', function () {
                var select = $(this);
                select.find('option').each(function () {
                    if (!$(this).prop('selected'))
                        $(this).remove();
                });

                $.each(allShips, function (shipID, shipVal) {
                    if (!shipVal['selected'])
                        select.append('<option value="' + shipID + '">' + shipVal['name'] + '</option>');
                });
            });
            $(document).on("select2:select select2:unselect", '.selectShips', function (e) {
                var select = $(this);

                select.find('option').each(function () {
                    if ($(this).prop('selected')) {
                        allShips[$(this).val()]['selected'] = true;
                    } else {
                        allShips[$(this).val()]['selected'] = false;
                        $(this).remove();
                    }
                });
            });

            $(document).on("select2:opening", '.selectHotels', function () {
                var select = $(this);
                select.find('option').each(function () {
                    if (!$(this).prop('selected'))
                        $(this).remove();
                });

                $.each(allHotels, function (hotelID, hotelVal) {
                    if (!hotelVal['selected'])
                        select.append('<option value="' + hotelID + '">' + hotelVal['name'] + '</option>');
                });
            });
            $(document).on("select2:select select2:unselect", '.selectHotels', function (e) {
                var select = $(this);

                select.find('option').each(function () {
                    if ($(this).prop('selected')) {
                        allHotels[$(this).val()]['selected'] = true;
                    } else {
                        allHotels[$(this).val()]['selected'] = false;
                        $(this).remove();
                    }
                });
            });
            

            $('body').on('click', '.removeCommission', function () {
                var form = $(this).closest('form');

                form.find('.selectShips option').each(function () {
                    if ($(this).prop('selected')) {
                        allShips[$(this).val()]['selected'] = false;
                    }
                });
                form.find('.selectHotels option').each(function () {
                    if ($(this).prop('selected')) {
                        allHotels[$(this).val()]['selected'] = false;
                    }
                });

                form.remove();
            });

            $('.addCommission').on('click', function () {
                $('#commission').append('<form class="row form-inline"><div class="col-md-2"><div class="form-group"><label>Commission:</label><input type="text" class="form-control commissionPercent" name="commission"></div></div><div class="col-md-4"><div class="form-group"><label>Ships:</label><select multiple class="form-control selectShips" name="ships"></select></div></div><div class="col-md-4"><div class="form-group"><label>Hotels:</label><select multiple class="form-control selectHotels" name="hotels"></select></div></div><div class="col-md-2"><a class="removeCommission" href="#"><span class="glyphicon glyphicon-trash"></span></a></div></form>');

                $('.selectShips').select2();
                $('.selectHotels').select2();
            });

            @if(!Auth::user()->hasRole('edit_b2b'))
            $('.content input, .content select, .content textarea').each(function () {
                $(this).prop('disabled', true);
            });

            @endif
            @if(isset($client->id))
            var clientID = '{{ $client->id }}';
            modelReservations(clientID, 'b2b', '{{ Session::token() }}', $('#reservations'), '#reservations');
            @else
            var clientID = 0;
            @endif

            function saveUser(userForm) {
                $.ajax({
                    type: "POST",
                    url: "/b2b/save/user",
                    data: userForm.serialize(),
                    success: function (userID) {
                        $('#users').load("/b2b/" + clientID + " #users > *");
                        getUser(userID);
                    }
                });
            }

            function getUser(userID) {
                $.ajax({
                    type: "GET",
                    dataType: "html",
                    url: "/b2b/user/" + userID,
                    cache: false,
                    success: function (userData) {
                        $('#userModal .modal-content').empty().append(userData);
                        modelReservations(userID, 'client_user', '{{ Session::token() }}', $('#userReservations'));

                        if (userID == 'new')
                            $('#userModal .modal-content form').append('<input type="hidden" name="client_id" value="' + clientID + '">');

                        $('#saveUser').on('click', function() {
                            saveUser($('#userModal .modal-content form'));
                            $('#userModal').modal('hide');
                        });

                        $('#nextUser').on('click', function() {
                            saveUser($('#userModal .modal-content form'));
                            $('#userModal').modal('hide');
                        });
                    }
                });
            }

            $('document').on('change', '#userReservations .reservations-filters select', function () {
                var this_select = $(this);
                console.log('shit');
                var form_data = $(this.form).serializeArray();
                form_data.push(
                        {name: 'id', value: userID},
                        {name: 'type', value: 'client_user'},
                        {name: '_token', value: '{{ Session::token() }}'}
                    );
                $.ajax({
                    dataType: "html",
                    method: "POST",
                    cache: false,
                    url: '/model/reservations',
                    data: form_data,
                    success: function (html) {
                        this_select.closest('#userReservations').empty().append(html);
                    }
                });
            });

            @if(!isset($client->id))
            $('body').on('click', '#clientNext', function() {
                var hotelData = $('#general').serializeArray();
                var button = $(this);
                var dataId = button.attr('data-id');
                $.ajax({
                    type: "POST",
                    url: "/save/client",
                    data: hotelData,
                    success: function (ajaxClientID) {
                        if (dataId == 'general') {
                            clientID = ajaxClientID;
                            $('#general').append('<input type="hidden" name="id" value="' + ajaxClientID + '">');

                            button.attr('data-id', 'commission');

                            $('#clientNav').append('<li><a data-toggle="tab" href="#commission">Commission</a></li>');
                            $('#clientNav a').eq(1).trigger('click');
                        } else if (dataId == 'commission') {
                            button.attr('data-id', 'bank_details');

                            $('#clientNav').append('<li><a data-toggle="tab" href="#bank_details">Bank details</a></li>');
                            $('#clientNav a').eq(2).trigger('click');
                        } else if (dataId == 'bank_details') {
                            button.attr('data-id', '').empty().attr('id', 'saveClient').append('Save');

                            $('#users').load("/b2b/" + ajaxClientID + " #users > *");

                            $('#clientNav').append('<li><a data-toggle="tab" href="#users">Users</a></li>');
                            $('#clientNav a').eq(3).trigger('click');
                        }
                        //button.attr('data-id', 'commission').empty().attr('id', 'saveClient').append('<span class="glyphicon glyphicon-ok"></span> Save');
                        //$('#clientNav').append('<li><a data-toggle="tab" href="#commission">Commission</a></li><li><a data-toggle="tab" href="#bank_details">Bank details</a></li><li><a data-toggle="tab" href="#users">Users</a></li>');
                    }
                });
            });
            @endif
            $('body').on('click', '.editUser', function() {
                var userID = $(this).attr('data-id');
                getUser(userID);
                $('#userModal').modal('show');
            });

            $(document).on('click', '#saveClient', function() {
                $('#commission').each(function () {
                    var commission = $(this).find('.commissionPercent').val();
                    $(this).find('.commissionPercent').attr('name', 'commission[' + commission + ']');
                    $(this).find('.selectShips').attr('name', 'ships[' + commission + '][]');
                    $(this).find('.selectHotels').attr('name', 'hotels[' + commission + '][]');
                });

                $.ajax({
                    type: "POST",
                    url: "/save/client",
                    data: $('#general, #bank_details, #commission').serializeArray(),
                    success: function () {
                        window.location.replace('{{ url('/clients') }}');
                    }
                });
            });
        });
    </script>
@endsection