{{ csrf_field() }}
<input id="hotelID" type="hidden" name="hotel" value="{{ $data->hotel_id }}">
@if(isset($data->id))
    <input type="hidden" name="id" value="{{ $data->id }}">
@endif
<div class="modal-header">
    <div class="modal-header-label modal-left">
        <h4>Add room</h4>
    </div>
    <div class="modal-header-buttons modal-right">
        @role('edit_directories')
        @role('edit_hotels')
        <button id="saveRoom" type="button" class="btn-create">Save</button>
        @endrole
        @endrole
        <button type="button" class="btn-cancel" data-dismiss="modal">Cancel</button>
    </div>
</div>
<div class="modal-body clearfix">
    <div class="modal-left modal-addroom-left">
        <div class="modal-addroom-inner-element">
            <label>Category:</label>
            <select id="roomType">
                @foreach($data->types as $key => $each)
                    <option value="{{ $key }}">{{ $each }}</option>
                @endforeach
            </select>
        </div>
        <div class="modal-addroom-inner-element">
            <label>ID:</label>
            <select name="type_category" id="roomID">
                @foreach($data->categories as $key2 => $each2)
                    <option value="{{ $key2 }}">{{ $each2 }}</option>
                @endforeach
            </select>
        </div>
        <div class="modal-addroom-inner-element">
            <label>Area:</label>
            <input type="text" name="area" value="{{ $data->area }}">
            <div class="modal-addroom-input-value">
                <p>m<sup>2</sup></p>
            </div>
        </div>
        <div class="modal-addroom-inner-element">
            <label>Number of pax:</label>
            <input id="roomPaxAmount" type="text" value="2" disabled>
            <div class="modal-addroom-input-value">
                <p>pax</p>
            </div>
        </div>
        <div class="modal-addroom-inner-element">
            <label class="modal-addroom-checkbox-label">Additional bed</label>
            <input id="roomBed" type="checkbox" disabled>
        </div>
    </div>
    <div class="modal-right modal-addroom-right">
        <div class="modal-addroom-inner-element">
            <label>Note:</label>
            <textarea name="description" cols="30" rows="10">{{ $data->description }}</textarea>
        </div>
    </div>
</div>