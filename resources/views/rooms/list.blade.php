@if(count($data["data"]) > 0)
    <table class="table custom-table table-rooms">
        <thead>
        <tr>
            <th>Room category</th>
            <th class="stripped">ID</th>
        </tr>
        </thead>
        <tbody>
        @foreach($data["data"] as $catName => $room)
            <tr>
                <td rowspan="{{ count($room) }}">{{ $catName }}</td>
                @php $count = 0; @endphp
                @foreach($room as $roomName => $roomID)
                    @if($count++ == 0)
                        <td class="table-rooms-first">
                            <div class="table-floating-left-element">{{ $roomName }}</div>
                            <ul class="custom-dropdown-table">
                                <li class="dropdown">
                                    <a class="dropdown-toggle inner-table-button-hovered" data-toggle="dropdown" href="#"><span class="glyphicon glyphicon-option-vertical"></span></a>
                                    <ul class="dropdown-menu">
                                        <li>
                                            <a href="#" data-id="{{ $roomID }}" class="editRoom"><img src="/src/img/pen-icon.png" alt=""></a>
                                        </li>
                                        <li>
                                            @role('edit_directories')
                                            @role('edit_hotels')
                                            <a href="#" data-id="{{ $roomID }}" class="deleteRoom"><img src="/src/img/trash-icon.png" alt=""></a>
                                            @endrole
                                            @endrole
                                        </li>
                                    </ul>
                                </li>
                            </ul>
                        </td>
            </tr>
            @else
                <tr>
                    <td hidden></td>
                    <td>
                        <div class="table-floating-left-element">{{ $roomName }}</div>
                        <ul class="custom-dropdown-table">
                            <li class="dropdown">
                                <a class="dropdown-toggle inner-table-button-hovered" data-toggle="dropdown" href="#"><span class="glyphicon glyphicon-option-vertical"></span></a>
                                <ul class="dropdown-menu">
                                    <li>
                                        <a href="#" data-id="{{ $roomID }}" class="editRoom"><img src="/src/img/pen-icon.png" alt=""></a>
                                    </li>
                                    <li>
                                        @role('edit_directories')
                                        @role('edit_hotels')
                                        <a href="#" data-id="{{ $roomID }}" class="deleteRoom"><img src="/src/img/trash-icon.png" alt=""></a>
                                        @endrole
                                        @endrole
                                    </li>
                                </ul>
                            </li>
                        </ul>
                    </td>
                </tr>
            @endif
        @endforeach
        @endforeach
        </tbody>
    </table>
@endif
@role('edit_directories')
@role('edit_hotels')
<div class="base-page-table-add-button hotel-room-add-button">
    <a href="#" data-deck="{{ $data["hotel_id"] }}" data-id="new" class="base-page-table-add editRoom">Add room</a>
</div>
@endrole
@endrole