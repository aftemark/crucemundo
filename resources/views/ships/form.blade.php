@extends('layouts.app')

@section('title')Accommodations @endsection

@section('content')
    <nav class="navbar second-nav">
        <div class="container-fluid">
            <div class="addships-title pull-left">
                @if(isset($data->id))
                    <h3>Ship - {{ $data->name }}</h3>
                @else
                    <h3>Add ship</h3>
                @endif
            </div>
            <div class="addships-buttons pull-right">
                @role('edit_directories')
                @role('edit_ships')
                @if(isset($data->id))
                    <button id="saveShip" class="btn-create">Save</button>
                @else
                    <button data-id="details" id="shipNext" class="btn-create next-btn">Next</button>
                @endif
                @endrole
                @endrole
                <a href="{{ url('/accommodations') }}" class="btn-cancel">Cancel</a>
            </div>
        </div>
    </nav>
    <form id="shipInfo" class="add-hotel-table-row1">
        <div class="add-hotel-table-elem">
            <p>ID:</p>
            <input type="text" name="code" value="{{ $data->code }}">
        </div>
        <div class="add-hotel-table-elem">
            <p>Ship name:</p>
            <input type="text" name="name" value="{{ $data->name }}">
        </div>
        <div class="add-hotel-table-elem">
            <p>Star rating:</p>
            <input type="text" name="rating" value="{{ $data->rating }}">
        </div>
    </form>
    <div class="base-page-table-nav-buttons add-hotel-table-nav">
        <ul id="shipNav" class="nav nav-tabs add-hotel-table-nav-tabs" role="tablist">
            @if(isset($data->id))
                <li class="active"><a data-toggle="tab" href="#details">Technical details</a></li>
                <li><a data-toggle="tab" href="#decks">Decks</a></li>
                <li><a data-toggle="tab" href="#cabins">Cabins</a></li>
            @else
                <li class="active"><a data-toggle="tab" href="#details">Technical details</a></li>
            @endif
        </ul>
    </div>
    <div class="tab-content">
        <div id="details" class="tab-pane fade in active">
            <form>
                {{ csrf_field() }}
                <div class="left-side-details pull-left">
                    <div class="add-ships-table-row2">
                        <p>Built:</p>
                        <input class="ships-built-placeholder" type="text" name="year_built" value="{{ $data->year_built }}">
                        <input class="ships-meter-btn" type="button" value="year">
                        <p>Last renovation:</p>
                        <input class="ships-renovation-placeholder" type="text" name="year_last" value="{{ $data->year_last }}">
                        <input class="ships-meter-btn" type="button" value="year">
                        <p>Width:</p>
                        <input class="ships-width-placeholder" type="text" name="width" value="{{ $data->width }}">
                        <input class="ships-meter-btn" type="button" value="meter">
                        <p>Length:</p>
                        <input class="ships-length-placeholder" type="text" name="length" value="{{ $data->length }}">
                        <input class="ships-meter-btn" type="button" value="meter">
                        <p>Draft:</p>
                        <input class="ships-draugth-placeholder" type="text" name="draft" value="{{ $data->draft }}">
                        <input class="ships-meter-btn" type="button" value="meter">
                        <p>Number of decks:</p>
                        @if(isset($data->id))
                            <input id="shipDecksNumber" class="ships-decks-placeholder" type="text" disabled value="{{ count($data->alldecks) }}">
                        @else
                            <input id="shipDecksNumber" class="ships-decks-placeholder" type="text" name="decks">
                        @endif
                        <p>Number of cabins:</p>
                        <input id="ShipCabinsNumber" class="ships-cabins-placeholder" type="text" disabled>
                        <p>Number of pax:</p>
                        <input id="ShipPaxNumber" type="text" disabled>
                    </div>
                </div>
                <div class="right-side-details pull-right">
                    <div class="add-ships-table-row3">
                        <div class="add-ships-table-elem">
                            <p>Services on board:</p>
                            <textarea name="services_desc" cols="30" rows="10">{{ $data->services_desc }}</textarea>
                        </div>
                    </div>
                    <div class="add-ships-table-row4">
                        <div class="add-ships-table-elem">
                            <p>Accomidation:</p>
                            <textarea name="add_desc" cols="30" rows="10">{{ $data->add_desc }}</textarea>
                        </div>
                    </div>

                    <div class="add-ships-table-row5">
                        <div class="add-ships-table-elem">
                            <p>Note:</p>
                            <textarea name="description" cols="30" rows="10">{{ $data->description }}</textarea>
                        </div>
                    </div>
                </div>
            </form>
        </div>
        <div id="decks" class="tab-pane fade">
            @if(isset($data->id))
                @foreach($data->alldecks as $deck)
                    <form class="decks-block clearfix">
                        <input type="hidden" name="id" value="{{ $deck->id }}">
                        <div class="add-ships-table-row1">
                            <div class="add-ships-table-elem">
                                <p>ID:</p>
                                <input type="text" name="code" name="code" value="{{ $deck->code }}">
                            </div>
                            <div class="add-ships-table-elem">
                                <p>Deck name:</p>
                                <input type="text" name="name" value="{{ $deck->name }}">
                            </div>
                            <div class="add-ships-table-elem">
                                <p>Number of cabins:</p>
                                <input disabled type="text" name="cabins" value="{{ count($deck->cabins) }}">
                            </div>
                        </div>
                        <div class="decks-left-side pull-left add-ships-table-elem">
                            <p>Decks plan:</p>
                            <button class="btn btn-select">Select</button>
                            <textarea cols="30" rows="10"></textarea>
                        </div>
                        <div class="decks-right-side pull-right add-ships-table-elem">
                            <p>Description:</p>
                            <textarea name="description" cols="30" rows="10">{{ $deck->description }}</textarea>
                        </div>
                    </form>
                @endforeach
            @endif
        </div>
        <div id="cabins" class="tab-pane fade"></div>
    </div>

    <div class="modal fade" id="cabinModal" tabindex="-1" role="dialog" aria-labelledby="cabinModalLabel">
        <div class="modal-dialog" role="document">
            <form class="modal-content" action="{{ url('/save/cabins') }}" method="POST"></form>
        </div>
    </div>
@endsection

@section('script')
<script>
    $(document).ready(function() {
        @if(!Auth::user()->hasRole('edit_ships'))
        $('.content input, .content select, .content textarea').each(function () {
            $(this).prop('disabled', true);
        });

        @endif
        @if(isset($data->id))
        var shipID = {{ $data->id }};
        @else
        var shipID = 0;
        @endif

        @if(isset($data->id))
            CabinsList(shipID);
            ShipData(shipID);
        @endif
        function CabinsList (id) {
            $.ajax({
                dataType: "html",
                method: "GET",
                cache: false,
                url: '/decks/' + id,
                success: function (data) {
                    $("#cabins").empty();
                    $("#cabins").prepend(data);

                    @if(!Auth::user()->hasRole('edit_ships'))
                    $('#cabins input, #cabins select, #cabins textarea').each(function () {
                        $(this).prop('disabled', true);
                    });

                    @endif

                    $('.editCabin').on('click', function () {
                        cabinModal($(this).attr('data-id'), $(this).attr('data-deck'));
                    });

                    $('.deleteCabin').on('click', function() {
                        var dataID = $(this).attr('data-id');
                        $.ajax({
                            dataType: "html",
                            method: "GET",
                            cache: false,
                            url: '/delete/cabin/' + dataID,
                            success: function (data) {
                                CabinsList(shipID);
                                ShipData(shipID);
                            }
                        });
                    });

                    function cabinModal(id, deck) {
                        $('#cabinModal .modal-content').empty();

                        $.ajax({
                            dataType: "html",
                            method: "GET",
                            cache: false,
                            url: "/cabin/" + id,
                            success: function (data) {
                                $('#cabinModal').modal('show');
                                $("#cabinModal .modal-content").prepend(data);
                                @if(!Auth::user()->hasRole('edit_ships'))

                                $('#cabinModal input, #cabinModal select, #cabinModal textarea').each(function () {
                                    $(this).prop('disabled', true);
                                });
                                @endif

                                if($("#cabinModal [name=type_category]").val() != 'false')
                                    TypeCategoryInfo($("#cabinModal [name=type_category]").val());

                                if(deck)
                                    $('#cabinDeckID').val(deck);

                                $('[name=type_category]').on('change', function() {
                                    TypeCategoryInfo(parseInt($(this).val()));
                                });

                                $('#cabinType').on('change', function() {
                                    if($(this).val() != 'false')
                                        PlacementTypeInfo(parseInt($(this).val()));
                                    else {
                                        $('#cabinID').empty().append('<option value="false">Select ID</option>');
                                        $('#cabinPaxAmount').val('');
                                        $('#cabinBed').prop('checked', false);
                                    }
                                });

                                $('#saveCabin').on('click', function() {
                                    var data = $('#cabinModal form').serialize();
                                    $.post('/save/cabin', data, function () {
                                        CabinsList(shipID);
                                        ShipData(shipID);

                                        $('#cabinModal').modal('hide');
                                    });
                                });
                            }
                        });
                    }

                }
            });
        }

        function TypeCategoryInfo (id) {
            $.ajax({
                dataType: "json",
                method: "GET",
                cache: false,
                url: "/typecategory/" + id,
                success: function (typeData) {
                    $('#cabinPaxAmount').val('');
                    $('#cabinBalcony').prop('checked', false);
                    $('#cabinBed').prop('checked', false);

                    $('#cabinPaxAmount').val(typeData["PaxAmount"]);
                    if(typeData["Balcony"] == '1')
                        $('#cabinBalcony').prop('checked', true);
                    if(typeData["Bed"] == '1')
                        $('#cabinBed').prop('checked', true);
                }
            });
        }

        function PlacementTypeInfo (typeId) {
            $.ajax({
                dataType: "json",
                method: "GET",
                cache: false,
                url: '/placementtypeship/' + typeId,
                success: function (typeData) {
                    $('#cabinID').empty();
                    var count = 0;
                    $.each( typeData, function( key, value ) {
                        if(count == 0)
                            TypeCategoryInfo(key);
                        count++;
                        $('#cabinID').append('<option value="' + key + '">' + value + '</option>');
                    });
                }
            });
        }

        function ShipData (id) {
            $.ajax({
                dataType: "json",
                method: "GET",
                cache: false,
                url: "/shipdata/" + id,
                success: function (shipData) {
                    $('#ShipCabinsNumber').val(0);
                    $('#ShipPaxNumber').val(0);

                    $('#decks form').each(function() {
                        var input = $(this).find('[name=cabins]');

                        input.val(0);
                        input.val(shipData["decks"][$(this).find('[name=id]').val()]);
                    });

                    $('#ShipCabinsNumber').val(shipData["cabins"]);
                    $('#ShipPaxNumber').val(shipData["pax"]);
                }
            });
        }

        @if(!isset($data->id))
        $('body').on('click', '#shipNext', function() {
            if($(this).attr('data-id') == 'details') {
                $('#shipNext').attr('data-id', 'decks');
                $('#shipDecksNumber').prop('disabled', true);
                $('#shipNav').append('<li><a data-toggle="tab" href="#decks">Decks</a></li>');
                $('#shipNav a').eq(1).trigger('click');

                for (var i = 0; i < $('#shipDecksNumber').val(); i++) {
                    var mainrow = $("<form />", { class : "row" });
                    var row = $("<div />", { class : "row" });
                    var row2 = $("<div />", { class : "row" });

                    var formGroupID = $("<div />", { class : "col-md-4 form-group" });
                    var labelID = $("<label />").text("ID:");
                    var IDInput = $("<input>", { type : "text", name : "code", class : "form-control" });
                    formGroupID.append(labelID).append(IDInput);

                    var formGroupName = $("<div />", { class : "col-md-4 form-group" });
                    var labelName = $("<label />").text("Decks name:");
                    var NameInput = $("<input>", { type : "text", name : "name", class : "form-control" });
                    formGroupName.append(labelName).append(NameInput);

                    var formGroupCabins = $("<div />", { class : "col-md-4 form-group" });
                    var labelCabins = $("<label />").text("Number of cabins:");
                    var CabinsInput = $("<input>", { type : "text", class : "form-control", name : "cabins" }).prop('disabled', true);
                    formGroupCabins.append(labelCabins).append(CabinsInput);

                    var col7 = $("<div />", { class : "col-md-7" });
                    var col5 = $("<div />", { class : "col-md-5" });
                    var formGroup = $("<div />", { class : "form-group" });
                    var descLabel = $("<label />", { class : "form-group" }).text("Description:");
                    var textarea = $("<textarea />", { type : "text", name : "description", class : "form-control", rows : "5" });

                    mainrow.appendTo('#decks');
                    row.append(formGroupID).append(formGroupName).append(formGroupCabins).appendTo(mainrow);
                    col7.append(descLabel).append(textarea);
                    row2.append(col5).append(col7).appendTo(mainrow);
                    $(this).attr('data-id', 'decks')
                }
            } else if ($(this).attr('data-id') == 'decks') {
                var shipData = $('#details form, #shipInfo').serializeArray();
                shipData.push({ name: "decks", value: $('#shipDecksNumber').val() });
                $.ajax({
                    type: "POST",
                    url: "/save/ship/new",
                    cache: false,
                    async: false,
                    data: shipData,
                    success: function (ajaxShipID) {
                        shipID = ajaxShipID;

                        $('#shipDecksNumber').attr('name', '');

                        $('#decks form').each(function() {
                            var deckData = $(this).serializeArray(),
                            thisDeck = $(this);
                            deckData.push({ name: "ship_id", value: ajaxShipID });
                            deckData.push({ name: "_token", value: "{{ Session::token() }}" });

                            $.ajax({
                                type: "POST",
                                url: "/save/deck/new",
                                data: deckData,
                                cache: false,
                                async: false,
                                success: function (ajaxDeckID) {
                                    thisDeck.append('<input type="hidden" name="id" value="' + ajaxDeckID + '">');
                                }
                            });
                        });
                        CabinsList(ajaxShipID);
                        ShipData(ajaxShipID);
                    }
                });

                $(this).attr('data-id', '').empty().attr('id', 'saveShip').append('Save');

                $('#shipNav').append('<li><a data-toggle="tab" href="#cabins">Cabins</a></li>');
                $('#shipNav a').eq(2).trigger('click');
            }
        });
        @endif
        $('body').on('click', '#saveShip', function() {
            var shipData = $('#details form, #shipInfo').serializeArray();
            shipData.push({ name: "add_bed", value: $('#shipAddBed').val() });
            $.ajax({
                type: "POST",
                url: "/save/ship/" + shipID,
                cache: false,
                data: shipData,
                success: function (ajaxShipID) {
                    $('#decks form').each(function() {
                        var deckData = $(this).serializeArray();
                        deckData.push({ name: "ship_id", value: ajaxShipID });
                        deckData.push({ name: "_token", value: "{{ Session::token() }}" });

                        $.ajax({
                            type: "POST",
                            url: "/save/deck/" + $(this).find('[name=id]').val(),
                            data: deckData,
                            cache: false,
                            async: false,
                        });
                    });
                    /*CabinsList(ajaxShipID);
                    ShipData(ajaxShipID);*/
                    window.location.replace('{{ url('/accommodations') }}');
                },
                async: false
            });
        });
    });
</script>
@endsection