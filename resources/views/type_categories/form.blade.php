<tr data-id="{{ $data->id }}">
    <td class="edit2-colored" colspan="{{ $type == 'ship' ? '5' : '4' }}">
        <form action="{{ url('/save/categories') }}" method="POST">
            {{ csrf_field() }}
            @if($data->id)
                <input type="hidden" name="id" value="{{ $data->id }}">
            @endif
            <input type="hidden" name="typename" value="{{ $type }}">
            <div class="inner-table-edit2">
                <div class="table-edit-element">
                    <p>Category:</p>
                    <div class="modal-edit2-input">
                        <select name="category">
                            @foreach($data->types as $key => $each)
                                <option value="{{ $key }}">{{ $each }}</option>
                            @endforeach
                        </select>
                    </div>
                </div>
                <div class="table-edit-element">
                    <p>ID:</p>
                    <div class="modal-edit2-input">
                        <input type="text" name="name" value="{{ $data->name }}">
                    </div>
                </div>
                <div class="table-edit-element">
                    <p>Number of pax:</p>
                    <div class="modal-edit2-input">
                        <input type="text" name="pax_amount" value="{{ $data->pax_amount }}">
                    </div>
                </div>
                <div class="table-edit-element">
                    <p>Additional bed:</p>
                    <div class="modal-edit2-checkbox">
                        <input {{ $data->bed ? 'checked ' : '' }}type="checkbox" name="bed">
                    </div>
                </div>
                @if($type == 'ship')
                    <div class="table-edit-element">
                        <p>Balcony:</p>
                        <div class="modal-edit2-checkbox">
                            <input {{ $data->balcony ? 'checked ' : '' }}type="checkbox" name="balcony">
                        </div>
                    </div>
                @endif
            </div>
            <div class="inner-table-edit2-buttons">
                @role('edit_directories')
                @role('edit_catalog')
                <button type="button" class="inner-table-confirm2 saveItem"><span class="glyphicon glyphicon-ok"></span></button>
                @endrole
                @endrole
                <button type="button" class="inner-table-decline2 formCancel"><span class="glyphicon glyphicon-remove"></span></button>
            </div>
        </form>
    </td>
</tr>