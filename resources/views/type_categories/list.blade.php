<div class="modal-header">
    <div class="country-modal-header">
        <button type="button" class="close country-close" data-dismiss="modal"><img src="/src/img/cancel-icon-md.png" alt="close"></button>
        @if($type == 'hotel')
            <h4 class="modal-title">Room IDs</h4>
        @elseif($type == 'ship')
            <h4 class="modal-title">Cabin IDs</h4>
        @endif
    </div>
</div>
<div class="modal-body clearfix">
    <div class="modal-search-block">
        <div class="modal-search-form">
            @role('edit_directories')
            @role('edit_catalog')
            <img id="newItem" class="modal-search-add-button" src="/src/img/plus-modal-icon.png" alt="+">
            @endrole
            @endrole
            <input id="searchCatalogField" class="modal-search-placeholder" type="text" placeholder="Search">
            <button type="reset" class="inner-table-decline inner-modal-search"><span class="glyphicon glyphicon-remove"></span></button>
            <input id="searchCatalog" class="modal-search-submit" type="image" src="/src/img/search-icon.png">
        </div>
    </div>
    <table class="table table-striped custom-table modal-table-rooms">
        <thead>
        <tr>
            <th>Category</th>
            <th>ID</th>
            <th>Number of pax</th>
            @if($type == 'ship')
                <th>Balcony</th>
            @endif
            <th>Additional bed</th>
        </tr>
        </thead>
        <tbody id="catalogTable">
        @foreach ($data as $each)
            <tr data-id="{{ $each->id }}">
                <td>{{ $each->type->name }}</td>
                <td>{{ $each->name }}</td>
                <td>{{ $each->pax_amount }}</td>
                @if($type == 'ship')
                    <td>{{ $each->bed ? 'Y' : 'N' }}</td>
                @endif
                <td>
                    <div class="table-floating-left-element">{{ $each->bed ? 'Y' : 'N' }}</div>
                    <ul class="custom-dropdown-table">
                        <li class="dropdown">
                            <a class="dropdown-toggle inner-table-button-hovered" data-toggle="dropdown" href="#"><span class="glyphicon glyphicon-option-vertical"></span></a>
                            <ul class="dropdown-menu">
                                <li>
                                    <a class="editItem" data-href="{{ url('/' . $each->typename . 'categories/' . $each->id) }}" href="#"><img src="/src/img/pen-icon.png" alt=""></a>
                                </li>
                                @role('edit_directories')
                                @role('edit_catalog')
                                <li>
                                    <a class="deleteItem" data-href="{{ url('/delete/categories/' . $each->id) }}" href="#"><img src="/src/img/trash-icon.png" alt=""></a>
                                </li>
                                @endrole
                                @endrole
                            </ul>
                        </li>
                    </ul>
                </td>
            </tr>
        @endforeach
        </tbody>
    </table>
</div>