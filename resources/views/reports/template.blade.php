@extends('layouts.app')

@section('title'){{ $template->name }} (Template) @endsection

@section('content')
    <form action="{{ url('/reports/save/template') }}" method="POST" class="container">
        <div class="row">
            <div class="col-md-6">
                <h2>{{ $template->name }} (Template)</h2>
            </div>
            <div class="col-md-6">
                <div class="row pull-right">
                    @role('edit_reports')
                    <button type="submit" class="btn btn-success"><span class="glyphicon glyphicon-ok"></span> Save</button>
                    @endrole
                    <a href="{{ url('/reports/templates') }}" class="btn btn-danger"><span class="glyphicon glyphicon-remove"></span> Cancel</a>
                </div>
            </div>
        </div>
        <div class="form-group">
            <label>Text</label>
            <textarea name="text" id="reportText">{{ $template->text }}</textarea>
        </div>
        <input type="hidden" name="id" value="{{ $template->id }}">
        {{ csrf_field() }}
    </form>
@endsection

@section('script')
    <script src="{{ url('/src/ckeditor/ckeditor.js') }}"></script>
    <script>
        CKEDITOR.replace('reportText');
        $(document).ready(function() {
            $('.add-variable span').on('click', function() {
                CKEDITOR.instances['emailText'].insertText($(this).attr('data-value'));
            });
        });
    </script>
@endsection