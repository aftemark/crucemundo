@extends('layouts.app')

@section('title')Report templates @endsection

@section('content')
    <div class="container">
        <div class="row">
            <h3>Report templates</h3>
        </div>
        <div class="row">
            <table class="table table-striped">
                <thead>
                <tr>
                    <th>Template</th>
                    <th></th>
                </tr>
                </thead>
                <tbody>
                    @foreach ($templates as $template)
                    <tr>
                        <td>{{ $template->name }}</td>
                        <td>
                            <a href="{{ url('/reports/template/' . $template->id) }}"><span class="glyphicon glyphicon-edit"></span></a>
                        </td>
                    </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
    </div>
@endsection