<form action="{{ url('reports/save/document') }}" method="POST">
    <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="documentModalLabel">
            <div class="col-md-6">New Document</div>
            <div class="col-md-6">
                <div class="row">
                    <div class="col-md-6">
                        @role('edit_reports')
                        <button class="btn btn-success">Save</button>
                        @endrole
                    </div>
                    <div class="col-md-6">
                        <a data-dismiss="modal" class="btn btn-danger">Cancel</a>
                    </div>
                </div>
            </div>
        </h4>
    </div>
    <div class="modal-body">
        <div class="form-group">
            <label>Document type</label>
            <select class="form-control" name="template">
                @foreach($templates as $template)
                <option value="{{ $template->id }}">{{ $template->name }}</option>
                @endforeach
            </select>
        </div>
        <hr>
        <div class="form-group">
            <label>#</label>
            <input type="text" class="form-control" name="number">
        </div>
        <div class="form-group">
            <label>Date</label>
            <input type="text" class="form-control" name="date">
        </div>
        <input type="hidden" name="_token" value="{{ Session::token() }}">
    </div>
</form>