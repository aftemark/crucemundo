@extends('layouts.app')

@section('title')Report documents @endsection

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-6">
                <h3>Report documents</h3>
            </div>
            <div class="col-md-6 right">
                @role('edit_reports')
                <a id="newDocument" href="{{ url('/reports/document') }}" class="btn btn-warning">Add document</a>
                @endrole
            </div>
        </div>
        <div class="row">
            <table class="table table-striped">
                <thead>
                <tr>
                    <th>#</th>
                    <th>Date</th>
                    <th>Document</th>
                    <th></th>
                </tr>
                </thead>
                <tbody>
                    @foreach ($documents as $document)
                    <tr>
                        <td>{{ $document->number }}</td>
                        <td>{{ date('d.m.Y', strtotime($document->date)) }}</td>
                        <td>{{ $document->name }}</td>
                        <td>
                            <a href="{{ url('/reports/delete/document/' . $document->id) }}"><span class="glyphicon glyphicon-trash"></span></a>
                            <a href="{{ url('/reports/download/' . $document->id) }}"><span class="glyphicon glyphicon-download-alt"></span></a>
                        </td>
                    </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
        <div class="row">
            <div class="col-md-6">{{ $documents->links() }}</div>
            <div class="col-md-6">
                <a href="/reports/documents/setnumber?number=1" class="btn btn-default{{ Session::get('documents_number') == '10' ? ' active' : '' }}">10</a>
                <a href="/reports/documents/setnumber?number=50" class="btn btn-default{{ Session::get('documents_number') == '50' ? ' active' : '' }}">50</a>
                <a href="/reports/documents/setnumber?number=100" class="btn btn-default{{ Session::get('documents_number') == '100' ? ' active' : '' }}">100</a>
            </div>
        </div>
    </div>
    <div class="modal fade" id="documentModal" tabindex="-1" role="dialog" aria-labelledby="documentModalLabel">
        <div class="modal-dialog" role="document">
            <div class="modal-content"></div>
        </div>
    </div>
@endsection

@section('script')
    <script>
        $(document).ready(function() {
            $('body').on('click', '#newDocument', function (e) {
                var modal = $('#documentModal');
                modal.find('.modal-content').empty();
                modal.modal('show');

                $.get($(this).attr('href'), function (data) {
                    modal.find('.modal-content').append(data);
                });
                e.preventDefault();
            });
        });
    </script>
@endsection