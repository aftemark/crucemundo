@extends('layouts.app')

@section('title'){{ $reservation_info["name"] }} @endsection

@section('css')
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.3/css/select2.min.css">
@endsection

@section('content')
    <div class="container">
        <div class="content">
            <div class="row">
                <div class="col-md-5">
                    @if(isset($data->id))
                    <h2>{{ $reservation_info["name"] }} - {{ $data->code }}</h2>
                    @if($data->type == 'booking' && $data->with_names)
                    <p>With names</p>
                    @endif
                    @else
                    <h2>Add {{ $reservation_info["name"] }}</h2>
                    @endif
                </div>
                <div class="col-md-2">
                    @if(isset($data->id))
                    @if(!$data->deleted_at && $edit_role)
                    <a href="{{ url('/cancel/reservation/' . $data->id) }}" class="btn btn-default">Cancel</a>
                    @endif
                    @if($edit_role)
                    <a href="{{ url('/copy/reservation/' . $data->id) }}" data-id="#copyModal" class="btn btn-default copy-convert-reservation">Copy</a>
                    @endif
                    @endif
                    @if($data->type == 'booking' && $data->journey->type == 'cruise' && $data->with_names && Auth::user()->hasRole('show_cruise_bookings_assignment'))
                    <a href="{{ url('/assignment/' . $data->journey->id) }}" class="btn btn-default">Cabins assignment</a>
                    @endif
                    @if(!$data->deleted_at && $edit_role)
                    @if($data->type == 'option')
                    <a href="{{ url('/convert/' . $data->id . '?to=booking') }}" data-id="#convertModal" class="btn btn-default copy-convert-reservation">Convert to booking</a>
                    @elseif($data->type == 'inquiry')
                    <a href="{{ url('/convert/' . $data->id . '?to=option') }}" data-id="#convertModal" class="btn btn-default copy-convert-reservation">Convert to option</a>
                    <a href="{{ url('/convert/' . $data->id . '?to=booking') }}" data-id="#convertModal" class="btn btn-default copy-convert-reservation">Convert to booking</a>
                    @endif
                    @endif
                    @if($data->deleted_at && $data->type == 'booking' && $edit_role)
                    <form action="{{ url('/reservation/status') }}" class="checkbox" method="POST">
                        <label>
                            <input class="payed-status" name="value" value="true" {{ $data->paid ? 'checked ' : '' }}type="checkbox">Paid
                        </label>
                        <input type="hidden" name="id" value="{{ $data->id }}">
                        <input type="hidden" name="field" value="paid">
                        <input type="hidden" name="_token" value="{{ Session::token() }}">
                    </form>
                    @endif
                </div>
                <div class="col-md-5">
                    <div class="row pull-right">
                        @if(!$data->deleted_at)
                        @if(isset($data->id) && $create_role && ($edit_role || !$data->services_tab || ($show_rooms_cabins ? !$data->placements_tab : false)))
                        <a href="{{ url('/save/reservation') }}" id="saveReservation" class="btn btn-success"><span class="glyphicon glyphicon-ok"></span> Save</a>
                        @elseif(!isset($data->id))
                        <a href="{{ url('/' . $reservation_info["type_value"]) }}/" id="reservationNext" class="btn btn-success"><span class="glyphicon glyphicon-ok"></span> Next</a>
                        @endif
                        @endif
                        <a href="{{ url('/reservations') }}" class="btn btn-danger"><span class="glyphicon glyphicon-remove"></span> Cancel</a>
                    </div>
                </div>
            </div>
            <div class="row">
                <form class="form-inline">
                    <div class="form-group">
                        <label>Made on:</label>
                        <input class="form-control" disabled type="text" value="{{ $data->created_at or '' }}">
                    </div>
                    <div class="form-group">
                        <label>Release date:</label>
                        <input class="form-control" disabled type="text" name="release_date" value="{{ $data->journey->release_date or '' }}">
                    </div>
                    <div class="form-group">
                        <label>ID:</label>
                        <input class="form-control" disabled type="text" name="code" value="{{ $data->code or '' }}">
                    </div>
                </form>
            </div>
            <div class="row center">
                <div class="col-md-8">
                    <ul id="reservationNav" class="nav nav-pills">
                        <li class="active"><a data-toggle="tab" href="#general">General information</a></li>
                        @if(isset($data->id))
                        @if($show_rooms_cabins)
                        <li><a data-toggle="tab" href="#placements">Rooms & Pax Information</a></li>
                        @endif
                        <li><a data-toggle="tab" href="#services">Services</a></li>
                        <li><a data-toggle="tab" href="#details">Details</a></li>
                        <li><a data-toggle="tab" href="#history">History of actions</a></li>
                        @endif
                    </ul>
                </div>
                @if(isset($data->id) && !$data->deleted_at && $create_role && ($edit_role || !$data->services_tab || ($show_rooms_cabins ? !$data->placements_tab : false)))
                <div class="col-md-4">
                    <button type="button" class="btn btn-default edit-tab">
                        <span class="glyphicon glyphicon-pencil" aria-hidden="true"></span> Edit
                    </button>
                </div>
                @endif
            </div>

            <div class="tab-content">
                <form action="{{ url('/save/reservation') }}" id="general" class="tab-pane fade in active" method="POST">
                    <div class="row">
                        <div class="col-md-6">
                            <div class="row">
                                <div class="form-group">
                                    <label>Accommodation:</label>
                                    @if(Auth::user()->hasRole('create_cruise_' . $reservation_info["role_name"]))
                                    <label class="radio-inline"><input type="radio" class="data-load" data-load="#reservation_journeys" name="journey_type" {{ isset($data->journey) && $data->journey->type == 'cruise' ? 'checked ' : '' }}value="cruise">Cruise</label>
                                    @endif
                                    @if(Auth::user()->hasRole('create_tour_' . $reservation_info["role_name"]))
                                    <label class="radio-inline"><input type="radio" class="data-load" data-load="#reservation_journeys" name="journey_type" {{ isset($data->journey) && $data->journey->type == 'tour' ? 'checked ' : '' }}value="tour">Tour</label>
                                    @endif
                                </div>
                                <div class="form-group">
                                    <select name="journey" id="reservation_journeys" class="form-control">
                                        @if(isset($data->journey))
                                            <option value="false">Select {{ $data->journey->type }}</option>
                                        @else
                                            <option value="false">Select journey type</option>
                                        @endif
                                        @if(isset($journeys))
                                        @foreach($journeys as $journey)
                                            <option {{ $data->journey_id == $journey->id ? 'selected ' : '' }}value="{{ $journey->id }}">{{ $journey->code }}</option>
                                        @endforeach
                                        @endif
                                    </select>
                                </div>
                            </div>
                            <div class="form-group">
                                <label>Reservation type:</label>
                                <label class="radio-inline"><input type="radio" name="reservation_type" {{ $data->reservation_type == 'fit' ? 'checked ' : '' }}value="fit">Fit</label>
                                <label class="radio-inline"><input type="radio" name="reservation_type" {{ $data->reservation_type == 'group' ? 'checked ' : '' }}value="group">Group</label>
                            </div>
                            @if($reservation_info["type_value"] == 'option')
                            <div class="form-group">
                                <label>Option till:</label>
                                <input class="form-control" type="text" name="till" value="{{ $data->till }}">
                            </div>
                            @endif
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label>Client:</label>
                                <label class="radio-inline"><input type="radio" class="data-load" data-load="#reservation_clients" name="client_type" {{ isset($data->client) && $data->client->type == 'b2b' ? 'checked ' : '' }}value="b2b">B2B Client</label>
                                <label class="radio-inline"><input type="radio" class="data-load" data-load="#reservation_clients" name="client_type" {{ isset($data->client) && $data->client->type == 'b2c' ? 'checked ' : '' }}value="b2c">B2C Client</label>
                            </div>
                            <div class="row" id="reservation_clients">
                                <div class="form-group">
                                    <select name="client" class="form-control data-load" data-load="#reservation_commission">
                                        <option value="false">Select client</option>
                                        @if(isset($clients))
                                        @foreach($clients as $client)
                                        <option {{ $data->client_id == $client->id ? 'selected ' : '' }}value="{{ $client->id }}">{{ $client->name }}</option>
                                        @endforeach
                                        @endif
                                    </select>
                                </div>
                                <div class="form-inline" id="reservation_commission">
                                    <div class="form-group">
                                        <label>User:</label>
                                        <select name="client_user" class="form-control">
                                            <option value="false">Select user</option>
                                            @if(isset($client_users))
                                            @foreach($client_users as $client_user)
                                            <option {{ $data->client_user_id == $client_user->id ? 'selected ' : '' }}value="{{ $client_user->id }}">{{ $client_user->name }}</option>
                                            @endforeach
                                            @endif
                                        </select>
                                    </div>
                                    <div class="form-group">
                                        <label>Commission level:</label>
                                        @if(isset($data->client) && $data->client->type == 'b2c')
                                        <input class="form-control always-disabled" type="text" disabled value="{{ $data->client->percent }}">
                                        @elseif(isset($data->client) && $data->client->type == 'b2b' && $data->reservation_type == 'fit' && $client_user_percent > 0)
                                        <input class="form-control always-disabled" type="text" disabled value="{{ $client_user_percent }}">
                                        @else
                                        <input value="-" class="form-control always-disabled" type="text" disabled>
                                        @endif
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <label><input type="checkbox" {{ $data->send_email ? 'checked ' : '' }}value="true" name="send_email">Send e-mail notifications when status change</label>
                            </div>
                        </div>
                    </div>
                    <input type="hidden" name="_token" value="{{ Session::token() }}">
                    <input type="hidden" name="type" value="{{ $reservation_info["type_value"] }}">
                    @if(isset($data->id))
                    <input name="edit_tab" type="hidden" value="general">
                    <input name="id" type="hidden" value="{{ $data->id }}">
                    @endif
                </form>
                @if(isset($data->id))
                @if($show_rooms_cabins)
                <form id="placements" class="tab-pane fade">
                    <div class="form-inline">
                        <div class="form-group">
                            <label>Number of pax:</label>
                            <input type="text" class="form-control always-disabled" disabled value="{{ $reservation_info["pax_count"] }}">
                        </div>
                        <div class="form-group">
                            <label>Number of placements:</label>
                            <select class="form-control load-placements" name="placements_number">
                                <option value="false">Select number of placements</option>
                                @if($reservation_info["min_placements"])
                                @for($i = $reservation_info["min_placements"]; $i <= $reservation_info["max_placements"]; $i++)
                                <option {{ $reservation_info["placements_number"] == $i ? 'selected ' : '' }}value="{{ $i }}">{{ $i }}</option>
                                @endfor
                                @endif
                            </select>
                        </div>
                        <div class="form-group">
                            <label>Language:</label>
                            <select class="form-control" name="language">
                                <option value="false">Select language</option>
                                @foreach($languages as $language)
                                <option {{ $data->language_id == $language->id ? 'selected ' : '' }}value="{{ $language->id }}">{{ $language->name }}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>
                    <div class="row" id="reservation_placements">
                        <div class="col-md-7">
                            @foreach($reservation_placements as $count => $placement)
                            <div class="row reservation-placement">
                                <div class="row">
                                    <div class="col-md-6">
                                        <h3>{{ $reservation_info["placement_name"] }} {{ $count }}</h3>
                                    </div>
                                    <div class="col-md-6">
                                    @if(isset($placement->id) && !$data->deleted_at && $edit_role)
                                        <input type="button" href="{{ url('/delete/placement/' . $placement->id) }}" class="btn btn-default delete-reservation-element" value="Delete">
                                    @endif
                                    </div>
                                </div>
                                <div class="form-inline">
                                    @if($data->journey->type == 'cruise')
                                    <div class="form-group">
                                        <label>{{ $reservation_info["placement_name"] }} category:</label>
                                        <select name="placements[{{ $count }}][category]" class="form-control load-placements">
                                            <option value="">Select {{ $reservation_info["placement_key"] }} category</option>
                                            @foreach($reservation_categories as $categoryID => $category)
                                                <option {{ (isset($cruise_types[$placement->cruise_type_id]) && $cruise_types[$placement->cruise_type_id]->type->type->id == $categoryID) || (isset($picked_categories[$count]) && $picked_categories[$count] == $categoryID) ? 'selected ' : '' }}value="{{ $category["category"]->id }}">{{ $category["category"]->name }}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                    <div class="form-group">
                                        <label>{{ $reservation_info["placement_name"] }} type:</label>
                                        <select name="placements[{{ $count }}][cruise_type]" class="form-control load-placements">
                                            <option value="">Select {{ $reservation_info["placement_key"] }} type</option>
                                            @if(isset($cruise_types[$placement->cruise_type_id]) && isset($reservation_categories[$cruise_types[$placement->cruise_type_id]->type->type->id]))
                                            @foreach($reservation_categories[$cruise_types[$placement->cruise_type_id]->type->type->id]["types"] as $type)
                                            @if($type["amount"] || $placement->cruise_type_id == $type["id"])
                                                <option {{ $placement->cruise_type_id == $type["id"] ? 'selected ' : '' }}value="{{ $type["id"] }}">{{ $type["name"] }}</option>
                                            @endif
                                            @endforeach
                                            @elseif(isset($picked_categories[$count]))
                                            @foreach($reservation_categories[$picked_categories[$count]]["types"] as $type)
                                            @if($type["amount"])
                                                <option value="{{ $type["id"] }}">{{ $type["name"] }}</option>
                                            @endif
                                            @endforeach
                                            @endif
                                        </select>
                                    </div>
                                    @elseif($data->journey->type == 'tour')
                                    <div class="form-group">
                                        <label>{{ $reservation_info["placement_name"] }} category:</label>
                                        <select name="placements[{{ $count }}][category]" class="form-control load-placements">
                                            <option value="">Select {{ $reservation_info["placement_key"] }} category</option>
                                            @foreach($reservation_categories as $categoryID => $category)
                                                @if(!empty($category))
                                                    <option {{ (isset($placement->placement[$reservation_info["placement_key"]]->type->type) && $placement->placement[$reservation_info["placement_key"]]->type->type->id == $categoryID) || (isset($picked_categories[$count]) && $picked_categories[$count] == $categoryID) ? 'selected ' : '' }}value="{{ $category["category"]->id }}">{{ $category["category"]->name }}</option>
                                                @endif
                                            @endforeach
                                            @if(isset($placement->placement[$reservation_info["placement_key"]]->type->type) && !isset($reservation_categories[$placement->placement[$reservation_info["placement_key"]]->type->type->id]))
                                                <option selected value="{{ $placement->placement[$reservation_info["placement_key"]]->type->type->id }}">{{ $placement->placement[$reservation_info["placement_key"]]->type->type->name }}</option>
                                            @endif
                                        </select>
                                    </div>
                                    <div class="form-group">
                                        <label>{{ $reservation_info["placement_name"] }} type:</label>
                                        <select name="placements[{{ $count }}][placement]" class="form-control load-placements">
                                            <option value="">Select {{ $reservation_info["placement_key"] }} type</option>
                                            @if(isset($placement->placement[$reservation_info["placement_key"]]->type->type) && isset($reservation_categories[$placement->placement[$reservation_info["placement_key"]]->type->type->id]))
                                                @foreach($reservation_categories[$placement->placement[$reservation_info["placement_key"]]->type->type->id]["types"] as $types)
                                                    @foreach($types as $type)
                                                        @if($type->amount > 0 || $placement->placement->id == $type->id)
                                                            <option {{ $placement->placement->id == $type->id ? 'selected ' : '' }}value="{{ $type->id }}">{{ $type[$reservation_info["placement_key"]]->type->name }}</option>
                                                        @endif
                                                    @endforeach
                                                @endforeach
                                            @elseif(isset($picked_categories[$count]))
                                                @foreach($reservation_categories[$picked_categories[$count]]["types"] as $types)
                                                    @foreach($types as $type)
                                                        @if($type->amount > 0)
                                                            <option value="{{ $type->id }}">{{ $type[$reservation_info["placement_key"]]->type->name }}</option>
                                                        @endif
                                                    @endforeach
                                                @endforeach
                                            @endif
                                        </select>
                                    </div>
                                    @endif
                                </div>
                                @if(isset($placement->placement) || isset($placement->cruise_type))
                                @php $this_model = $data->journey->type == 'tour' ? $placement->placement->room : $placement->cruise_type; @endphp
                                <div class="list-group">
                                    @if($data->journey->type == 'cruise')
                                    <div class="list-group-item">
                                        <span class="badge">{{ implode(', ', $cabins_info[$this_model->type->id]["numbers"]) }}</span>
                                        {{ $reservation_info["placement_name"] }}s numbers:
                                    </div>
                                    <div class="list-group-item">
                                        <span class="badge">{{ $cabins_info[$this_model->type->id]["min_area"] == $cabins_info[$this_model->type->id]["max_area"] ? $cabins_info[$this_model->type->id]["min_area"] : $cabins_info[$this_model->type->id]["min_area"] . ' - ' . $cabins_info[$this_model->type->id]["max_area"] }} m2</span>
                                        Area:
                                    </div>
                                    @endif
                                    <div class="list-group-item">
                                        <span class="badge">{{ $this_model->type->pax_amount }}<span {{ $placement->bed ? '' : 'style=display:none; '  }}class="plus-bed">+1</span></span>
                                        Number of pax:
                                    </div>
                                    @if($data->journey->type == 'cruise')
                                    <div class="list-group-item">
                                        <span class="badge">{{ $this_model->type->balcony ? 'Yes' : 'No' }}</span>
                                        Balcony:
                                    </div>
                                    @endif
                                    <div class="list-group-item">
                                        <span class="badge">@if($this_model->type->bed) <label><input {{ $placement->bed ? 'checked ' : '' }}name="placements[{{ $count }}][bed]" class="placement-bed" type="checkbox">Yes</label> @else No @endif</span>
                                        Additional bed:
                                    </div>
                                    @if($data->journey->type == 'tour')
                                    <div class="list-group-item">
                                        <span class="badge">{{ $data->reservation_type == 'group' ? $placement->placement->price_rec : $placement->placement->price_net }} EUR</span>
                                        Price per pax:
                                    </div>
                                    @else
                                    <div class="list-group-item">
                                        <span class="badge">{{ $data->reservation_type == 'group' ? $this_model->price_rec : $this_model->price_net }} EUR</span>
                                        Price per pax:
                                    </div>
                                    @endif
                                </div>
                                <div class="form-inline">
                                    <div class="form-group">
                                        <label>Adults:</label>
                                        <input type="text" class="form-control" name="placements[{{ $count }}][adults]" value="{{ $placement->adults }}">
                                    </div>
                                    <div class="form-group">
                                        <label>Children:</label>
                                        <input type="text" class="form-control" name="placements[{{ $count }}][childrens]" value="{{ $placement->childrens }}">
                                    </div>
                                    <div class="form-group">
                                        <label>Infants:</label>
                                        <input type="text" class="form-control" name="placements[{{ $count }}][infants]" value="{{ $placement->infants }}">
                                    </div>
                                </div>
                                @endif
                                @if($placement->reservation_paxes->count())
                                    <div class="col-md-12">
                                        <div class="form-inline">
                                            <label>Names</label>
                                            @if(!$data->deleted_at && $edit_role)
                                            <div class="form-group">
                                                <input type="button" class="btn btn-default edit-names" value="Edit" data-href="{{ url('/placement/pax/' . $placement->id) }}">
                                            </div>
                                            @endif
                                        </div>
                                        @foreach($placement->reservation_paxes as $reservation_pax)
                                            <div class="row">
                                                <div class="col-md-6">
                                                    <p>{{ $reservation_pax->name }} {{ $reservation_pax->surname or '' }} ({{ $reservation_pax->type }})</p>
                                                </div>
                                                @if($placement->bed && $reservation_pax->type != 'infant')
                                                <div class="col-md-3">
                                                    <label><input type="radio" class="form-control" value="{{ $reservation_pax->id }}" {{ $reservation_pax->bed ? 'checked ' : '' }}name="placements[{{ $count }}][pax_bed]"> Bed</label>
                                                </div>
                                                @endif
                                                @if(!$data->deleted_at && $edit_role)
                                                <div class="col-md-3">
                                                    <input type="button" href="{{ url('/delete/pax/' . $reservation_pax->id) }}" class="btn btn-default delete-reservation-element" value="Delete">
                                                </div>
                                                @endif
                                            </div>
                                        @endforeach
                                    </div>
                                    <div class="row">
                                        {{ $placements_data["ids"][$placement->id]["description_first"] }}
                                    </div>
                                    <div class="row">
                                        {{ $placements_data["ids"][$placement->id]["description_second"] }}
                                    </div>
                                    <div class="row">
                                        {{ $placements_data["ids"][$placement->id]["commission_discount"] }}
                                    </div>
                                    <div class="row">
                                        {{ $placements_data["ids"][$placement->id]["to_pay"] }}
                                    </div>
                                @endif
                                @if(isset($placement->id))
                                    <input type="hidden" name="placements[{{ $count }}][id]" value="{{ $placement->id }}">
                                @endif
                            </div>
                            @endforeach
                        </div>
                        <div class="col-md-5">
                            @if(isset($placements_data["ids"]))
                            <h3>Accommodation:</h3>
                            @foreach($placements_data["ids"] as $placement_data)
                                <p>{{ $placement_data["description_first"] }}</p>
                            @endforeach
                            @foreach($placements_data["ids"] as $placement_data)
                                <p>{{ $placement_data["description_second"] }}</p>
                            @endforeach
                            <p>Sub total: {{ $placements_data["total"]["sub_total"] }} EUR</p>
                            <p>{{ $placements_data["total"]["commission_discount"] }}</p>
                            @foreach($placements_data["fines"] as $fine)
                            <p>{{ $fine }}</p>
                            @endforeach
                            <p>{{ $placements_data["total"]["to_pay"] }}</p>
                            @endif
                        </div>
                    </div>
                    <input name="edit_tab" type="hidden" value="placements">
                    <input type="hidden" name="_token" value="{{ Session::token() }}">
                </form>
                @endif
                @if(isset($data->journey))
                <form id="services" class="tab-pane fade">
                    <table class="table table-striped">
                        <thead>
                        <tr>
                            <th>{{ $reservation_info["placement_name"] }} number</th>
                            <th>{{ $reservation_info["placement_name"] }} type</th>
                            <th>Name</th>
                            @foreach($journey_services as $journeyService)
                                <th>{{ $journeyService["name"] }} {{ $journeyService["value"] }} EUR</th>
                            @endforeach
                            <th>Amount</th>
                        </tr>
                        </thead>
                        <tbody>
                        <?php $services_array["normal"] = []; ?>
                        @foreach($reservation_placements as $placementCount => $placement)
                            @if(isset($placement->id))
                                @php $pax_count = $placement->reservation_paxes->count(); @endphp
                                <tr>
                                    <td>{{ $reservation_info["placement_name"] }} {{ $placementCount }}</td>
                                    <td>{{ $reservation_info["journey_type"] == 'tour' ? $placement->placement[$reservation_info["placement_key"]]->type->name : $placement->cruise_type->type->name }}</td>
                                    <td>
                                        @foreach($placement->reservation_paxes as $reservation_pax)
                                            <div class="row">{{ $reservation_pax->name }} {{ $reservation_pax->surname or '' }}</div>
                                            <?php $prices[$reservation_pax->id] = 0.00; ?>
                                        @endforeach
                                    </td>
                                    @if($pax_count)
                                    @foreach($journey_services as $journeyServiceID => $journeyService)
                                        @if(isset($journeyService["placement"]))
                                            <td>
                                                <input type="checkbox" data-price="{{ isset($journeyService["multiply"]) ? $add = number_format($journeyService["value"]/$pax_count, 2) : $add = number_format($journeyService["value"], 2) }}" data-id="all" {{ !empty($journeyService["placement"][$placement->id]) ? 'checked ' : '' }}value="{{ $journeyServiceID }}" name="service[placement][{{ $placement->id }}][]">
                                            </td>
                                            <?php if(!empty($journeyService["placement"][$placement->id]) && isset($journeyService["multiply"])) { $services_array["multiply"][$journeyServiceID][$placement->id]["count"] = $placement->reservation_paxes->count(); $services_array["multiply"][$journeyServiceID][$placement->id]["price"] = $add; } ?>
                                            @foreach($placement->reservation_paxes as $placement_reservation_pax)
                                                <?php if (!empty($journeyService["placement"][$placement->id])) {
                                                    $prices[$placement_reservation_pax->id] += $add;
                                                    if(!isset($journeyService["multiply"])) $services_array["normal"][$journeyServiceID] = isset($services_array["normal"][$journeyServiceID]) ? $services_array["normal"][$journeyServiceID] + 1 : 1;
                                                } ?>
                                            @endforeach
                                        @elseif(isset($journeyService["pax"]))
                                            <td>
                                            @foreach($placement->reservation_paxes as $placement_reservation_pax)
                                                <input type="checkbox" data-price="{{ $add = number_format($journeyService["value"], 2) }}" data-id="{{ $placement_reservation_pax->id }}" {{ !empty($journeyService["pax"][$placement->id][$placement_reservation_pax->id]) ? 'checked ' : '' }}value="{{ $journeyServiceID }}" name="service[pax][{{ $placement->id }}][{{ $placement_reservation_pax->id }}][]">
                                                <?php $prices[$placement_reservation_pax->id] += !empty($journeyService["pax"][$placement->id][$placement_reservation_pax->id]) ? $add : 0 ; ?>
                                                <?php if (!empty($journeyService["pax"][$placement->id][$placement_reservation_pax->id])) $services_array["normal"][$journeyServiceID] = isset($services_array["normal"][$journeyServiceID]) ? $services_array["normal"][$journeyServiceID] + 1 : 1; ?>
                                            @endforeach
                                            </td>
                                        @endif
                                    @endforeach
                                    @endif
                                    <td class="services-amount">
                                    @foreach($placement->reservation_paxes as $placement_reservation_pax)
                                        <div class="row user-price-{{ $placement_reservation_pax->id }}" data-amount="{{ number_format($prices[$placement_reservation_pax->id], 2) }}">{{ number_format($prices[$placement_reservation_pax->id], 2) }}</div>
                                    @endforeach
                                    </td>
                                </tr>
                            @endif
                        @endforeach
                        </tbody>
                    </table>
                    @if(isset($services_data["included"]))
                    <h3>Included services:</h3>
                    @foreach($services_data["included"] as $services_line)
                        <p>{{ $services_line }}</p>
                    @endforeach
                    @endif
                    @if(isset($services_data["optional"]))
                    <h3>Extra services:</h3>
                    @foreach($services_data["optional"] as $services_line)
                        <p>{{ $services_line }}</p>
                    @endforeach
                    @endif
                    <p>Services Sub total: {{ number_format($services_data["total"], 2) }} EUR</p>
                    <p>Total to pay: {{ number_format($services_data["total"] + $placements_data["total"]["total"], 2) }} EUR</p>
                    <input name="edit_tab" type="hidden" value="services">
                </form>
                @endif
                <form id="details" class="tab-pane fade">
                    <h2>Terms of payment:</h2>
                    <div class="row">- {{ $data->journey->booking_percent or 0 }}% on booking (non-refundable deposit)</div>
                    <div class="row">- Full payment {{ $data->journey->full_pay or 0 }} days before the {{ $data->journey->type }}</div>
                    <h2>Cancellation fees:</h2>
                    @foreach($data->journey->fees as $fee)
                        <div class="row">- Up to {{ $fee->from }}{{ $fee->to !== NULL ? ' - ' . $fee->to : '' }} days before embarkation - {{ $fee->percent }}% of cruise price</div>
                    @endforeach
                    <div class="row">- Less than {{ $data->journey->full_pay or 0 }} days before embarkation -100% penalty</div>
                    @if($reservation_info["type_value"] == 'booking')
                        <div class="row">
                            <div class="col-md-6">
                                <h2>Payment Information</h2>
                            </div>
                            <div class="col-md-6">
                                <input type="button" href="{{ url('/payment/new') }}" class="btn btn-default payment-info" value="Add payment information">
                            </div>
                        </div>
                        <div class="row">
                            <label>To pay till:</label>
                            <input type="text" class="form-control" value="{{ $data->till }}">
                        </div>
                        <table class="table table-striped" id="details-table">
                            <tbody>
                                <thead>
                                    <tr>
                                        <th>Type</th>
                                        <th>Date</th>
                                        <th>Amount</th>
                                        <th>Debt</th>
                                        <th></th>
                                    </tr>
                                </thead>
                                @foreach($data->payments()->orderBy('created_at', 'desc')->get() as $payment)
                                    <tr>
                                        @if($payment->type == 'deposit')
                                            <td>Deposit</td>
                                        @elseif($payment->type == 'full')
                                            <td>Full payment</td>
                                        @elseif($payment->type == '100')
                                            <td>100% payment</td>
                                        @else
                                            <td> - </td>
                                        @endif
                                        <td>{{ $payment->date }}</td>
                                        <td>{{ $payment->amount }} EUR</td>
                                        <td>{{ number_format($debts[$payment->id], 2) }} EUR</td>
                                        <td>
                                            <input type="button" href="{{ url('/payment/' . $payment->id) }}" class="btn btn-default payment-info" value="Edit">
                                            @if($edit_role)
                                                <input type="button" href="{{ url('/delete/payment/' . $payment->id) }}" class="btn btn-default delete-payment" value="Delete">
                                            @endif
                                        </td>
                                    </tr>
                                @endforeach
                            </tbody>
                        </table>
                    @endif
                    <hr>
                    <div class="row">
                        <div class="col-md-4">
                            <p>Reservation {{ $data->code }}</p>
                            <p>{{ $data->journey[$reservation_info["journey_accommodation"]]->name }}</p>
                            <p>{{ date('Y.m.d', strtotime($data->journey->depart_date)) }} - {{ date('Y.m.d', strtotime($data->journey->depart_date . ' + ' . ($data->journey->itinerary->nights) . ' days')) }} {{ $data->journey->itinerary->from_to }} ({{ $data->journey->itinerary->nights }} nights)</p>
                            @if(isset($data->client) && $data->client->type == 'b2c')
                            <p>Client name - {{ $data->client->name or '' }} {{ $data->client->second_name or '' }}</p>
                            @elseif(isset($data->client) && $data->client->type == 'b2b' && $client_user_percent > 0)
                            <p>Company name - {{ $data->client_user->client->code or '' }}</p>
                            <p>Operator name - {{ $data->client_user->name or '' }}</p>
                            @endif
                            <p>{{ $reservation_info["pax_count"] }} pax/{{ $reservation_info["placements_number"] }} cabins</p>
                        </div>
                        <div class="col-md-4">
                            @if(isset($placements_data["ids"]))
                            <h3>Accommodation:</h3>
                            @foreach($placements_data["ids"] as $placement_data)
                            <p>{{ $placement_data["description_first"] }}</p>
                            @endforeach
                            @foreach($placements_data["ids"] as $placement_data)
                            <p>{{ $placement_data["description_second"] }}</p>
                            @endforeach
                            <p>Sub total: {{ $placements_data["total"]["sub_total"] }} EUR</p>
                            <p>{{ $placements_data["total"]["commission_discount"] }}</p>
                            @foreach($placements_data["fines"] as $fine)
                            <p>{{ $fine }}</p>
                            @endforeach
                            <p>{{ $placements_data["total"]["to_pay"] }}</p>
                            @endif
                        </div>
                        <div class="col-md-4">
                            @if(isset($services_data["included"]))
                                <h3>Included services:</h3>
                                @foreach($services_data["included"] as $services_line)
                                    <p>{{ $services_line }}</p>
                                @endforeach
                            @endif
                            @if(isset($services_data["optional"]))
                                <h3>Extra services:</h3>
                                @foreach($services_data["optional"] as $services_line)
                                    <p>{{ $services_line }}</p>
                                @endforeach
                            @endif
                            <p>Services Sub total: {{ number_format($services_data["total"], 2) }} EUR</p>
                            <p>Total to pay: {{ $reservation_info["cancel_fine"] === NULL ? number_format($services_data["total"] + $placements_data["total"]["total"], 2) : 'Booking was canceled, with fine - ' . $reservation_info["cancel_fine"]->amount }} EUR</p>
                        </div>
                    </div>
                </form>
                <div id="history" class="tab-pane fade">
                    <table class="table table-striped">
                        <thead>
                        <tr>
                            <th>Date and time</th>
                            <th>User</th>
                            <th>Action</th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach ($data->notifications()->where('type', '!=', 'reminder')->get() as $notification)
                            <tr>
                                <td>{{ $notification->created_at or '' }}</td>
                                <td>{{ $notification->user->name or 'Program' }}</td>
                                <td>{!! $notification->text or '' !!}</td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                </div>
                @endif
            </div>
        </div>
    </div>

    <div class="modal fade" id="paxesModal" tabindex="-1" role="dialog" aria-labelledby="paxesModalLabel">
        <div class="modal-dialog" role="document">
            <div class="modal-content"></div>
        </div>
    </div>

    <div class="modal fade" id="paymentModal" tabindex="-1" role="dialog" aria-labelledby="paymentModalLabel">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <form class="row" action="{{ url('/save/payment') }}" method="POST">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                        <h4 class="modal-title" id="paymentModalLabel">
                            <div class="col-md-6">
                                Payment Information
                            </div>
                            <div class="col-md-6">
                                <div class="row">
                                    <div class="col-md-6">
                                        <input type="hidden" name="_token" value="{{ Session::token() }}">
                                        @if($edit_role)
                                            <a class="btn btn-success save-payment">Save</a>
                                        @endif
                                    </div>
                                    <div class="col-md-6">
                                        <a data-dismiss="modal" class="btn btn-danger">Cancel</a>
                                    </div>
                                </div>
                            </div>
                        </h4>
                    </div>
                    <div class="modal-body">
                        <div class="row">
                            <div class="col-md-3">
                                <label>Date:</label>
                                <input {{ $edit_role ? '' : 'disabled ' }}type="text" name="date" class="form-control">
                            </div>
                            <div class="col-md-6">
                                <label>Date:</label>
                                <select {{ $edit_role ? '' : 'disabled ' }}name="type" class="form-control">
                                    <option value="deposit">Deposit</option>
                                    <option value="full">Full payment</option>
                                    <option value="100">100% payment</option>
                                </select>
                            </div>
                            <div class="col-md-3">
                                <label>Amount:</label>
                                <input {{ $edit_role ? '' : 'disabled ' }}type="text" name="amount" class="form-control">
                            </div>
                        </div>
                        <div class="row">
                            <label><input {{ $edit_role ? '' : 'disabled ' }}type="checkbox" value="true" name="email">Send e-mail notifications</label>
                        </div>
                        <input {{ $edit_role ? '' : 'disabled ' }}type="hidden" name="id">
                        <input {{ $edit_role ? '' : 'disabled ' }}type="hidden" name="reservation_id" value="{{ $data->id }}">
                    </div>
                </form>
            </div>
        </div>
    </div>

    @if($edit_role && isset($data->id))
        <div class="modal fade" id="copyModal" tabindex="-1" role="dialog" aria-labelledby="copyModalLabel">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                        <h4 class="modal-title" id="copyModalLabel">
                            <div class="col-md-6">Reservation copy</div>
                            <div class="col-md-6">
                                <div class="row">
                                    <div class="col-md-6">
                                        <a href="{{ url('/copy/reservation/' . $data->id . '?copy=true') }}" data-id="#copyModal" class="btn btn-success copy-convert-reservation">Copy</a>
                                    </div>
                                    <div class="col-md-6">
                                        <a data-dismiss="modal" class="btn btn-danger">Cancel</a>
                                    </div>
                                </div>
                            </div>
                        </h4>
                    </div>
                    <div class="modal-body"></div>
                </div>
            </div>
        </div>

        <div class="modal fade" id="convertModal" tabindex="-1" role="dialog" aria-labelledby="convertModalLabel">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                        <h4 class="modal-title" id="convertModalLabel">
                            <div class="col-md-6">Reservation convert</div>
                            <div class="col-md-6">
                                <div class="row">
                                    <a data-dismiss="modal" class="btn btn-danger">Cancel</a>
                                </div>
                            </div>
                        </h4>
                    </div>
                    <div class="modal-body"></div>
                </div>
            </div>
        </div>
    @endif
@endsection

@section('script')
    @if(!$data->deleted_at)
    <script>
        $(document).ready(function() {
            @if(isset($data->id))
            var forms = "#general :input[value!=''], #placements :input[value!=''], #services :input[value!='']",
                servicesAllowed = false,
                nonCompatibles;

            $.ajax({
                type: "GET",
                url: '{{ url('/reservation/noncompatibles/' . $data->id) }}',
                cache: false,
                dataType: "json",
                success: function (result) {
                    nonCompatibles = result;
                    $('#services tbody tr').each(function () {
                        checkServices($(this));
                    });
                }
            });

            $(document).on('click', '.edit-tab', function () {
                var tab = $('#reservationNav .active a');
                if (tab.attr('data-edited') != 'true') {
                    $(tab.attr('href') + ' input, ' + tab.attr('href') + ' select').not('.always-disabled').prop('disabled', false);
                    tab.attr('data-edited', 'true');
                    $(this).html('<span class="glyphicon glyphicon-ok"></span> Save').removeClass('edit-tab').addClass('save-tab');
                    servicesAllowed = !$('#services [name=edit_services]').prop('disabled');
                }
            });

            $(document).on('click', '.save-tab', function () {
                $.ajax({
                    type: "POST",
                    url: "{{ url('/save/reservation') }}",
                    cache: false,
                    data: $(forms).serializeArray(),
                    success: function () {
                        location.reload(true);
                    }
                });
            });

            $('body').on('click', '#saveReservation', function(e) {
                var button = $(this);
                $.ajax({
                    type: "POST",
                    url: button.attr('href'),
                    cache: false,
                    data: $(forms).serializeArray(),
                    success: function () {
                        window.location.replace('{{ url('/reservations') }}');
                    }
                });
                e.preventDefault();
            });

            $('body').on('change', '.load-placements', function() {
                loadPlacements();
            });

            $('body').on('click', '.delete-reservation-element', function(event) {
                var placement = $(this).closest('.reservation-placement');
                $.ajax({
                    type: "GET",
                    url: $(this).attr('href'),
                    cache: false,
                    dataType: "html",
                    success: function () {
                        placement.remove();
                        $.ajax({
                            type: "GET",
                            url: window.location.href,
                            cache: false,
                            dataType: "html",
                            success: function (html) {
                                $('#placements').empty().append($(html).find('#placements').html());
                            },
                            error: function (error) {
                                deleteModal(error);
                            }
                        });
                    }
                });
                event.preventDefault();
            });

            $('body').on('click', '.edit-names', function() {
                $('#paxesModal').modal('show');
                $('#paxesModal .modal-content').empty().load($(this).attr('data-href'));
            });

            $('body').on('click', '#services [type=checkbox]', function() {
                var row = $(this).closest('tr');
                checkServices(row);

                if (!$(this).hasClass('always-disabled')) {
                    var id = $(this).attr('data-id');
                    var userRow = id != 'all' ? $('.user-price-' + id) : row.find('.services-amount .row');
                    var price = parseFloat($(this).attr('data-price'));

                    if ($(this).prop('checked'))
                        userRow.each(function () {
                            var new_price = parseFloat($(this).attr('data-amount')) + price;
                            $(this).attr('data-amount', parseFloat(new_price).toFixed(2)).html(parseFloat(new_price).toFixed(2));
                        });
                    else
                        userRow.each(function () {
                            var new_price = parseFloat($(this).attr('data-amount')) - price;
                            $(this).attr('data-amount', parseFloat(new_price).toFixed(2)).html(parseFloat(new_price).toFixed(2));
                        });
                }
            });

            $('body').on('click', '.placement-bed', function() {
                if ($(this).prop('checked'))
                    $(this).closest('.list-group').find('.plus-bed').css('display', 'inline-block');
                else
                    $(this).closest('.list-group').find('.plus-bed').css('display', 'none');
            });

            @if($reservation_info["type_value"] != 'option')
            $('body').on('click', '.payment-info', function(event) {
                $.ajax({
                    type: "GET",
                    url: $(this).attr('href'),
                    cache: false,
                    dataType: "json",
                    success: function (payment_info) {
                        var modal = $('#paymentModal');
                        modal.find('[name=amount]').val(payment_info.amount);
                        modal.find('[name=date]').val(payment_info.date);
                        modal.find('option').removeProp("selected");
                        modal.find('option[value=' + payment_info.type + ']').prop("selected", "selected");
                        if (payment_info.email)
                            modal.find('[name=email]').prop("checked", "checked");
                        else
                            modal.find('[name=email]').removeProp("checked");
                        if (payment_info.id)
                            modal.find('[name=id]').val(payment_info.id);
                        else
                            modal.find('[name=id]').val('');

                        modal.modal('show');
                    }
                });
                event.preventDefault();
            });
            @endif

            @if($edit_role)
            $('body').on('click', '.save-paxes, .save-payment', function() {
                var btn = $(this);
                $.ajax({
                    type: "POST",
                    url: btn.closest('form').attr('action'),
                    cache: false,
                    data: btn.closest('form').serialize(),
                    success: function () {
                        btn.closest('.modal').modal('hide');
                        reloadPayments();
                    }
                });
            });

            $('body').on('click', '.delete-payment', function() {
                $.ajax({
                    type: "GET",
                    url: $(this).attr('href'),
                    cache: false,
                    success: function () {
                        reloadPayments();
                    }
                });
            });

            function reloadPayments () {
                $('#details-table').load(location.href + ' #details-table');
            }
            @endif

            function loadPlacements() {
                var placementsData = $("#placements :input[value!='']").serializeArray();
                placementsData.push({ name: "placementsData", value: "true" });
                $.ajax({
                    type: "POST",
                    url: window.location.href,
                    cache: false,
                    data: placementsData,
                    dataType: "html",
                    success: function (html) {
                        $('#placements').empty().append($(html).find('#placements').html());
                    }
                });
            }

            function checkServices(row) {
                row.find('[type=checkbox]').each(function () {
                    $(this).removeClass('always-disabled');
                    if (servicesAllowed) $(this).prop('disabled', false);
                });
                row.find('[type=checkbox]').each(function () {
                    if ($(this).prop('checked') && nonCompatibles[$(this).val()]) {
                        nonCompatibles[$(this).val()].forEach(function (service) {
                            row.find('[value=' + service + ']').addClass('always-disabled').prop('disabled', true).prop('checked', false);
                        });
                    }
                });
            }
            @else
            var forms = "#general";

            $('body').on('click', '#reservationNext', function(e) {
                var button = $(this);
                $.ajax({
                    type: $('#general').attr('method'),
                    url: $('#general').attr('action'),
                    cache: false,
                    data: $(forms).serialize(),
                    success: function (ajaxID) {
                        window.location.replace(button.attr('href') + ajaxID);
                    }
                });
                e.preventDefault();
            });
            @endif

            $('body').on('change', '.data-load', function () {
                loadData($(this));
            });

            function loadData (field) {
                $(field.attr('data-load')).load('?' + field.attr('name') + '=' + field.val() + ' ' + field.attr('data-load') + ' >*');
            }
        });
    </script>
    @endif
    @if(isset($data->id))
    <script>
        $(document).ready(function() {
            $(".tab-content input, .tab-content select").not(":input[name=id], :input[name=_token]").prop('disabled', true);

            $('body').on('click', '.copy-convert-reservation', function(e) {
                var element = $(this);
                $.get(element.attr('href'), function(response) {
                    var modal = $(element.attr('data-id'));
                    if (response.failed_entities) {
                        modal.find('.modal-body').html(response.failed_entities);
                        modal.modal('show');
                    } else {
                        window.location.href = response;
                    }
                });
                e.preventDefault();
            });

            $('.payed-status').on('change', function() {
                var form = $(this).closest('form');
                $.post($(this).closest('form').attr('action'), $(this).closest('form').serialize());
            });
        });
    </script>
    @endif
@endsection