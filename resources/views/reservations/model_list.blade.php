<div class="row">
    <form class="col-md-6 reservations-filters">
        <div class="col-md-6">
            <select name="reservation_type" class="form-control">
                <option value="">Select reservation type</option>
                <option {{ Request::get('reservation_type') == 'inquiry' ? 'selected ' : '' }}value="inquiry">Inquiry</option>
                <option {{ Request::get('reservation_type') == 'option' ? 'selected ' : '' }}value="option">Option</option>
                <option {{ Request::get('reservation_type') == 'booking' ? 'selected ' : '' }}value="booking">Booking</option>
            </select>
        </div>
        <div class="col-md-6">
            <select name="status" class="form-control">
                <option value="">Select status</option>
            </select>
        </div>
    </form>
    <div class="col-md-6 right">
        @role('edit_reservations')
        @if(Auth::user()->hasRole(['edit_cruise_inquiries', 'edit_tour_inquiries']))
            <a href="{{ url('/inquiry/new') }}" class="btn btn-warning">Add Inquiry</a>
        @endif
        @if(Auth::user()->hasRole(['edit_cruise_options', 'edit_tour_options']))
            <a href="{{ url('/option/new') }}" class="btn btn-warning">Add Option</a>
        @endif
        @if(Auth::user()->hasRole(['edit_cruise_bookings', 'edit_tour_bookings']))
            <a href="{{ url('/booking/new') }}" class="btn btn-warning">Add Booking</a>
        @endif
        @endrole
    </div>
</div>
<div class="row">
    <h3>List of reservations</h3>
</div>
<table id="reservations" class="table table-striped">
    <tbody>
    <thead>
    <tr>
        <th>Made on</th>
        <th>Number of pax</th>
        <th>Reservation type</th>
        <th>Status</th>
        <th></th>
    </tr>
    </thead>
    @foreach ($reservations as $each)
        <tr>
            <td>{{ $each->created_at or '-' }}</td>
            <td>{{ $each->pax_number or 0 }}</td>
            <td>{{ $each->type or '-' }}</td>
            <td>{{ $each->status or '-' }}</td>
            <td>
                <a href="{{ url('/' . $each->type . '/' . $each->id) }}"><span class="glyphicon glyphicon-edit"></span></a>
                @role('edit_reservations')
                @role('edit_' . $each->journey->type . '_' . $each->role_name)
                <a href="{{ url('/delete/reservation/' . $each->id) }}"><span class="glyphicon glyphicon-trash"></span></a>
                @endrole
                @endrole
            </td>
        </tr>
        @endforeach
        </tbody>
</table>