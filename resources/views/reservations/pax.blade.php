<form class="row" action="{{ url('/save/placement/' . $placementID) }}" method="POST">
    <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="paxesModalLabel">
            <div class="col-md-6">
                Pax details
            </div>
            <div class="col-md-6">
                <div class="row">
                    <div class="col-md-6">
                        <input type="hidden" name="_token" value="{{ Session::token() }}">
                        @role('edit_reservations')
                        <a class="btn btn-success save-paxes">Save</a>
                        @endrole
                    </div>
                    <div class="col-md-6">
                        <a data-dismiss="modal" class="btn btn-danger">Cancel</a>
                    </div>
                </div>
            </div>
        </h4>
    </div>
    <div class="modal-body">
        @foreach($paxes as $count => $pax)
        <div class="col-md-12">
            {{ $pax->name or '' }} {{ $pax->surname or '' }}
            <div class="row">
                <div class="col-md-4">
                    <label>Surname:</label>
                    <input type="text" class="form-control" name="pax[{{ $count }}][surname]" value="{{ $pax->surname }}">
                </div>
                <div class="col-md-4">
                    <label>Name:</label>
                    <input type="text" class="form-control" name="pax[{{ $count }}][name]" value="{{ $pax->name }}">
                </div>
                <div class="col-md-2">
                    <label>Sex:</label>
                    <select class="form-control" name="pax[{{ $count }}][sex]">
                        <option value="">Select sex</option>
                        <option {{ $pax->sex === 0 ? 'selected ' : '' }}value="0">Female</option>
                        <option {{ $pax->sex === 1 ? 'selected ' : '' }}value="1">Male</option>
                    </select>
                </div>
                <div class="col-md-2">
                    <label>Date of birth:</label>
                    <input type="text" class="form-control" name="pax[{{ $count }}][birth]" value="{{ $pax->birth ? date('d.m.Y', strtotime($pax->birth)) : '' }}">
                </div>
            </div>
            <div class="row">
                <div class="col-md-4">
                    <label>Document type:</label>
                    <input type="text" class="form-control" name="pax[{{ $count }}][document_type]" value="{{ $pax->document_type }}">
                </div>
                <div class="col-md-4">
                    <label>Serial number:</label>
                    <input type="text" class="form-control" name="pax[{{ $count }}][document_number]" value="{{ $pax->document_number }}">
                </div>
                <div class="col-md-2">
                    <label>Citizenship:</label>
                    <select class="form-control" name="pax[{{ $count }}][country_id]">
                        <option value="">Select citizenship</option>
                        @foreach($countries as $country)
                        <option {{ $pax->country_id == $country->id ? 'selected ' : '' }}value="{{ $country->id }}">{{ $country->name }}</option>
                        @endforeach
                    </select>
                </div>
                <div class="col-md-2">
                    <label>Validity:</label>
                    <input type="text" class="form-control" name="pax[{{ $count }}][validity]" value="{{ $pax->validity ? date('d.m.Y', strtotime($pax->validity)) : '' }}">
                </div>
            </div>
            <hr>
            <div class="row">
                <div class="col-md-2">
                    <label>Date of arrival:</label>
                    <input type="text" class="form-control" name="pax[{{ $count }}][arrival]" value="{{ $pax->arrival ? date('d.m.Y', strtotime($pax->arrival)) : '' }}">
                </div>
                <div class="col-md-8">
                    <label>Airport:</label>
                    <select class="form-control" name="pax[{{ $count }}][arrival_airport_id]">
                        <option value="">Select airport</option>
                        @foreach($airports as $airport)
                        <option {{ $pax->arrival_airport_id == $airport->id ? 'selected ' : '' }}value="{{ $airport->id }}">{{ $airport->name }}</option>
                        @endforeach
                    </select>
                </div>
                <div class="col-md-2">
                    <label>Flight number:</label>
                    <input type="text" class="form-control" name="pax[{{ $count }}][arrival_number]" value="{{ $pax->arrival_number }}">
                </div>
            </div>
            <div class="row">
                <div class="col-md-2">
                    <label>Date of departure:</label>
                    <input type="text" class="form-control" name="pax[{{ $count }}][departure]" value="{{ $pax->departure ? date('d.m.Y', strtotime($pax->departure)) : '' }}">
                </div>
                <div class="col-md-8">
                    <label>Airport:</label>
                    <select class="form-control" name="pax[{{ $count }}][departure_airport_id]">
                        <option value="">Select airport</option>
                        @foreach($airports as $airport)
                        <option {{ $pax->departure_airport_id == $airport->id ? 'selected ' : '' }}value="{{ $airport->id }}">{{ $airport->name }}</option>
                        @endforeach
                    </select>
                </div>
                <div class="col-md-2">
                    <label>Flight number:</label>
                    <input type="text" class="form-control" name="pax[{{ $count }}][departure_number]" value="{{ $pax->departure_number }}">
                </div>
            </div>
            @if(isset($pax->id))
            <input type="hidden" name="pax[{{ $count }}][id]" value="{{ $pax->id }}">
            @endif
        </div>
        @endforeach
    </div>
</form>