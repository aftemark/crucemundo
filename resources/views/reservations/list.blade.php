@extends('layouts.app')

@section('title'){{ 'Reservations' }}@endsection

@section('content')
    <div class="container">
        <div class="content">
            <form class="row">
                <div class="col-md-6">
                    <select onchange="this.form.submit()" name="canceled" class="form-control">
                        <option value="false">Active reservations</option>
                        <option {{ Request::get('canceled') == 'true' ? 'selected ' : '' }}value="true">Canceled reservations</option>
                    </select>
                </div>
                <div class="col-md-6 filters">
                    <div class="col-md-6">
                        <select onchange="this.form.submit()" name="journey" class="form-control">
                            <option value="">Select journey type</option>
                            <option {{ Request::get('journey') == 'cruise' ? 'selected ' : '' }}value="cruise">Cruise</option>
                            <option {{ Request::get('journey') == 'tour' ? 'selected ' : '' }}value="tour">Tour</option>
                        </select>
                    </div>
                    <div class="col-md-6">
                        <select onchange="this.form.submit()" name="reservation_type" class="form-control">
                            <option value="">Select reservation type</option>
                            <option {{ Request::get('reservation_type') == 'inquiry' ? 'selected ' : '' }}value="inquiry">Inquiry</option>
                            <option {{ Request::get('reservation_type') == 'option' ? 'selected ' : '' }}value="option">Option</option>
                            <option {{ Request::get('reservation_type') == 'booking' ? 'selected ' : '' }}value="booking">Booking</option>
                        </select>
                    </div>
                </div>
            </form>
            <div class="row">
                <form class="col-md-6">
                    <div class="col-md-3">
                        <select class="form-control" name="type">
                            <option {{ Request::get('type') == 'all' ? 'selected ' : '' }}value="all">All</option>
                            <option {{ Request::get('type') == 'journey.code' ? 'selected ' : '' }}value="journey.code">Accommodation</option>
                            <option {{ Request::get('type') == 'client.type' ? 'selected ' : '' }}value="client.type">Client</option>
                            <option {{ Request::get('type') == 'journey.itinerary.name' ? 'selected ' : '' }}value="journey.itinerary.name">Itinerary</option>
                            <option {{ Request::get('type') == 'language.name' ? 'selected ' : '' }}value="language.name">Language</option>
                            <option {{ Request::get('type') == 'release_date' ? 'selected ' : '' }}value="release_date">Release date</option>
                            <option {{ Request::get('type') == 'reservation_type' ? 'selected ' : '' }}value="reservation_type">Reservation type</option>
                        </select>
                    </div>
                    <div class="col-md-8">
                        <input class="form-control" type="text" name="search" placeholder="Search" value="{{ Request::get('search') }}">
                    </div>
                    <div class="col-md-1">
                        <button type="submit" class="btn btn-default"><span class="glyphicon glyphicon-search"></span></button>
                    </div>
                </form>
                <div class="col-md-6 pull-right">
                    @role('edit_reservations')
                    @if(Auth::user()->hasRole(['edit_cruise_inquiries', 'edit_tour_inquiries']))
                        <a href="{{ url('/inquiry/new') }}" class="btn btn-warning">Add Inquiry</a>
                    @endif
                    @if(Auth::user()->hasRole(['edit_cruise_options', 'edit_tour_options']))
                        <a href="{{ url('/option/new') }}" class="btn btn-warning">Add Option</a>
                    @endif
                    @if(Auth::user()->hasRole(['edit_cruise_bookings', 'edit_tour_bookings']))
                        <a href="{{ url('/booking/new') }}" class="btn btn-warning">Add Booking</a>
                    @endif
                    @endrole
                </div>
            </div>
            <div class="row">
                <div class="col-md-6">
                    <h3>List of reservations</h3>
                </div>
            </div>
            <table id="reservations" class="table table-striped">
                <tbody>
                <thead>
                <tr>
                    <th>Accommodation</th>
                    <th>Client</th>
                    <th>Itinerary</th>
                    <th>Departure date</th>
                    <th>Language</th>
                    <th>Additional service</th>
                    <th>Reservation type</th>
                    <th>Status</th>
                    <th>Payments</th>
                    <th></th>
                </tr>
                </thead>
                @foreach ($data as $each)
                    <tr{{ $each->deleted_at && !$each->paid ? ' style=background-color:#ff0000;' : '' }}>
                        <td>{{ $each->journey->code or '-' }}</td>
                        <td>{{ $each->client->type or '-' }}</td>
                        <td>{{ $each->journey->itinerary->name or '-' }}</td>
                        <td>{{ $each->journey->depart_date or '-' }}</td>
                        <td>{{ $each->language->name or '-' }}</td>
                        <td>{{ $services_amount[$each->id] or '-' }}</td>
                        <td>{{ $each->reservation_type or '-' }}</td>
                        <td>{{ ucfirst($each->type) }}{{ $each->type == 'booking' && $each->with_names ? ' (with names)' : '' }}</td>
                        <td>
                            @if($each->type == 'booking' && !$each->deleted_at && Auth::user()->hasRole(['edit_' . $each->journey->type . '_' . $each->role_name]))
                            <form action="{{ url('/reservation/status') }}" method="POST">
                                <select name="value" class="form-control change-payments-status">
                                    <option value="">-</option>
                                    <option {{ $each->payments_status == 'deposit_invoiced' ? 'selected ' : '' }}value="deposit_invoiced">Deposit invoiced</option>
                                    <option {{ $each->payments_status == 'deposit_paid' ? 'selected ' : '' }}value="deposit_paid">Deposit paid</option>
                                    <option {{ $each->payments_status == 'total_invoiced' ? 'selected ' : '' }}value="total_invoiced">Total invoiced</option>
                                    <option {{ $each->payments_status == 'total_paid' ? 'selected ' : '' }}value="total_paid">Total paid</option>
                                </select>
                                <input type="hidden" name="id" value="{{ $each->id }}">
                                <input type="hidden" name="field" value="payments_status">
                                <input type="hidden" name="_token" value="{{ Session::token() }}">
                            </form>
                            @endif
                        </td>
                        <td>
                            @if(Auth::user()->hasRole(['show_' . $each->journey->type . '_' . $each->role_name]))
                            <a href="{{ url('/' . $each->type . '/' . $each->id) }}"><span class="glyphicon glyphicon-edit"></span></a>
                            @endif
                            {{--
                            @role('edit_reservations')
                            @if(Auth::user()->hasRole(['edit_' . $each->journey->type . '_' . $each->role_name]))
                            <a href="{{ url('delete/reservation/' . $each->id) }}"><span class="glyphicon glyphicon-trash"></span></a>
                            @endif
                            @endrole
                            --}}
                        </td>
                    </tr>
                    @endforeach
                    </tbody>
            </table>
            <div class="row">
                <div class="col-md-6">{{ $data->links() }}</div>
                <div class="col-md-6">
                    <a href="/reservations/setnumber?number=10" class="btn btn-default{{ Session::get('reservations_number') == '10' ? ' active' : '' }}">10</a>
                    <a href="/reservations/setnumber?number=50" class="btn btn-default{{ Session::get('reservations_number') == '50' ? ' active' : '' }}">50</a>
                    <a href="/reservations/setnumber?number=100" class="btn btn-default{{ Session::get('reservations_number') == '100' ? ' active' : '' }}">100</a>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('script')
    <script>
        $(document).ready(function() {
            $('.change-payments-status').on('change', function () {
                var form = $(this).closest('form');
                $.post(form.attr('action'), form.serialize());
            });
        });
    </script>
@endsection