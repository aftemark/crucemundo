<div id="catalogModal" class="modal fade" role="dialog">
    <div class="modal-dialog" role="document">
        <div class="modal-content"></div>
    </div>
</div>
<div class="modal fade" id="deleteModal" tabindex="-1" role="dialog" aria-labelledby="deleteModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="deleteModalLabel">Error</h4>
            </div>
            <div class="modal-body"></div>
        </div>
    </div>
</div>

<script src="{{ url('/src/js/jquery-3.2.1.min.js') }}"></script>
<script src="{{ url('/src/js/bootstrap.min.js') }}"></script>
<script src="{{ url('/src/js/scripts.js') }}"></script>
@role('show_catalog')
@if(in_array(Route::current()->getName(), config('constants.directories_second_nav_routes')))
<script src="{{ url('/src/js/catalog.js') }}"></script>
<script>
    var attr = '',
    search = '',
    permission = {{ Auth::user()->hasRole('edit_catalog') ? 1 : 0 }};
    CatalogModal(permission);
</script>
@endrole
@endif