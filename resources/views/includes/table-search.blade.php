<form method="GET">
    <select name="field">
        @foreach($searchKeys as $searchKey => $searchKeyName)
        <option {{ Request::input('field', NULL) == $searchKey && (!isset($tableName) || Request::input('table', NULL) == $tableName) ? 'selected ' : '' }}value="{{ $searchKey }}">{{ $searchKeyName }}</option>
        @endforeach
    </select>
    <input class="search-placeholder" name="search" type="text" placeholder="Search" value="{{ $oldSearch }}">
    <input class="search-submit" type="image" src="/src/img/search.png">
    @if(!isset($tableName) || Request::input('table', NULL) == $tableName)
    @foreach($otherKeys as $otherKey)
    @if(Request::has($otherKey))
    <input type="hidden" name="{{ $otherKey }}" value="{{ Request::get($otherKey) }}">
    @endif
    @endforeach
    @endif
    @if(isset($tableName))
    <input type="hidden" name="table" value="{{ $tableName }}">
    @endif
</form>