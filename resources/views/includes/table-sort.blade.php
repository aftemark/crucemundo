@php
    $routeName = Route::current()->getName();
    $params = [];
    $request = Request::all();
    $isTable = !isset($tableName) || (isset($request['table']) && $request['table'] == $tableName);
    if (isset($tableName))
        $params['table'] = $tableName;
    $isAsc = Request::input('order', NULL) == 'asc' && $isTable && (!isset($orderField) || Request::input('order_field', NULL) == $orderField);
    $isDesc = Request::input('order', NULL) == 'desc' && $isTable && (!isset($orderField) || Request::input('order_field', NULL) == $orderField);
    if ($isTable) {
        foreach ($sortSearchKeys as $key)
            if (isset($request[$key]))
                $params[$key] = $request[$key];
    }
@endphp
<div class="table-sort">
    <a href="{{ route($routeName, $isAsc ? $params : $params + ['order' => 'asc'] + (isset($orderField) ? ['order_field' => $orderField] : [])) }}"><span class="sort-top glyphicon glyphicon-menu-up{{ $isAsc ? ' glyphicon-active' : '' }}"></span></a>
    <a href="{{ route($routeName, $isDesc ? $params : $params + ['order' => 'desc'] + (isset($orderField) ? ['order_field' => $orderField] : [])) }}"><span class="sort-bottom glyphicon glyphicon-menu-down{{ $isDesc ? ' glyphicon-active' : '' }}"></span></a>
</div>