@php $routeName = Route::current()->getName(); @endphp
<header>
    <nav class="navbar main-nav main-navigation">
        <div class="navbar-header">
            <a class="nav-logo main-navigation-logo" href="{{ url('/') }}"><img src="/src/img/logo.png" alt="CRUCEMUNDO"></a>
            </a>
        </div>
        @if($routeName != 'years')
        <ul class="nav navbar-nav main-navigation">
            @role('show_directories')
            <li class="dropdown">
                <a class="dropdown-toggle active main-nav-menu-item main-nav-menu-first" data-toggle="dropdown" href="#">Directories
                    <span class="glyphicon glyphicon-menu-down"></span></a>
                <ul class="dropdown-menu main-nav-dropdown-menu">
                    @role('show_cities')
                    <li><a href="{{ url('/cities') }}">Cities</a></li>
                    @endrole
                    @if(Auth::user()->hasRole('show_hotels') || Auth::user()->hasRole('show_ships'))
                        <li><a href="{{ url('/accommodations') }}">Accomodation</a></li>
                    @endif
                    @role('show_services')
                    <li><a href="{{ url('/services') }}">Services</a></li>
                    @endrole
                    @if(Auth::user()->hasRole('show_b2b') || Auth::user()->hasRole('show_b2c'))
                        <li><a href="{{ url('/clients') }}">Clients</a></li>
                    @endif
                    @role('show_itineraries')
                    <li><a href="{{ url('/itineraries') }}">Itineraries</a></li>
                    @endrole
                    @role('show_packages')
                    <li><a href="{{ url('/packages') }}">Service packages</a></li>
                    @endrole
                    @role('show_cruises')
                    <li><a href="{{ url('/cruises') }}">Cruises</a></li>
                    @endrole
                    @role('show_tours')
                    <li><a href="{{ url('/tours') }}">Tour</a></li>
                    @endrole
                </ul>
            </li>
            @endrole
            @role('show_reservations')
            <li><a class="main-nav-menu-item" href="{{ url('/reservations') }}">Reservation</a></li>
            @endrole
            <li class="dropdown">
                <a class="main-nav-menu-item main-nav-item-with-badge" data-toggle="dropdown" href="#">Notification{{--<span class="badge main-nav-menu-badge">3</span>--}}
                    <span class="glyphicon glyphicon-menu-down main-nav-hide-menu-down"></span></a>
                <ul class="dropdown-menu ddm-notification main-nav-dropdown-menu">
                    <li><a href="{{ url('/notifications/actions') }}">User action</a></li>
                    <li><a href="{{ url('/notifications/updates') }}">Program updates<span class="badge main-nav-badge">3</span></a></li>
                    <li><a href="{{ url('/notifications/reminders') }}">Reminders</a></li>
                </ul>
            </li>
            @role('show_reports')
            <li><a class="main-nav-menu-item" href="#">Reports</a></li>
            @endrole
            @role('show_administration')
            <li class="dropdown">
                <a class="dropdown-toggle main-nav-menu-item" data-toggle="dropdown" href="#">Administration
                    <span class="glyphicon glyphicon-menu-down"></span></a>
                <ul class="dropdown-menu main-nav-dropdown-menu">
                    @role('show_users')
                    <li><a href="{{ url('/users') }}">Users</a></li>
                    @endrole
                    @role('show_mailing')
                    <li><a href="{{ url('/mailing') }}">E-mail</a></li>
                    @endrole
                </ul>
            </li>
            @endrole
        </ul>
        @endif
        <div class="main-nav-login-block">
            <div class="main-nav-user-block">
                <ul>
                    <li class="login-name">{{ isset(Auth::user()->name) ? Auth::user()->name : Auth::user()->email }}</li>
                    <li class="login-settings">
                        <a href="#"><img src="/src/img/settings-icon.png" alt="settings"></a>
                    </li>
                    <li class="logout">
                        <a href="{{ url('/logout') }}"><img src="/src/img/logout-icon.png" alt="logout"></a>
                    </li>
                </ul>
            </div>
            <ul>
                <li class="dropdown main-nav-select">
                    <a class="main-nav-item-current" data-toggle="dropdown" href="#">{{ Session::has('catalog_year') ? 'Year ' . Session::get('catalog_year') : 'Select Year' }}
                        <span class="glyphicon glyphicon-menu-down"></span></a>
                    <ul class="dropdown-menu main-nav-drop-select">
                        @foreach($nav_years["active"] as $nav_year)
                            <li><a class="main-nav-item-target" href="{{ url('/years/select/' . $nav_year->year) }}">Year {{ $nav_year->year }}</a></li>
                        @endforeach
                        <li><a class="main-nav-item-target" href="{{ url('/years') }}">All years</a></li>
                    </ul>
                </li>
            </ul>
        </div>
    </nav>
</header>

@role('show_directories')
@if(in_array($routeName, config('constants.directories_second_nav_routes')))
    <nav class="navbar second-nav">
        <div class="container-fluid">
            <ul class="nav navbar-nav second-nav-menu-items">
                <li class="second-nav-menu-item"><a {!! $routeName == 'cities_list' ? 'class="second-nav-menu-item-active" ' : '' !!}href="{{ url('/cities') }}">Cities</a></li>
                <li class="second-nav-menu-item"><a {!! $routeName == 'accommodations_list' || $routeName == 'ship' || $routeName == 'hotel' ? 'class="second-nav-menu-item-active" ' : '' !!}href="{{ url('/accommodations') }}">Accommodation</a></li>
                <li class="second-nav-menu-item"><a {!! $routeName == 'services_list' ? 'class="second-nav-menu-item-active" ' : '' !!}href="{{ url('/services') }}">Services</a></li>
                <li class="second-nav-menu-item"><a {!! $routeName == 'clients_list' || $routeName == 'b2c' || $routeName == 'b2c.new' || $routeName == 'b2b' || $routeName == 'b2b.new' ? 'class="second-nav-menu-item-active" ' : '' !!}href="{{ url('/clients') }}">Clients</a></li>
                <li class="second-nav-menu-item"><a {!! $routeName == 'itineraries_list' ? 'class="second-nav-menu-item-active" ' : '' !!}href="{{ url('/itineraries') }}">Itineraries</a></li>
                <li class="second-nav-menu-item"><a {!! $routeName == 'packages_list' || $routeName == 'package' ? 'class="second-nav-menu-item-active" ' : '' !!}href="{{ url('/packages') }}">Server packages</a></li>
                <li class="second-nav-menu-item"><a {!! $routeName == 'cruises_list' || $routeName == 'cruise' ? 'class="second-nav-menu-item-active" ' : '' !!}href="{{ url('/cruises') }}">Cruises</a></li>
                <li class="second-nav-menu-item"><a {!! $routeName == 'tours_list' || $routeName == 'tour' ? 'class="second-nav-menu-item-active" ' : '' !!}href="{{ url('/tours') }}">Tours</a></li>
                </li>
                @role('show_catalog')
                <li class="dropdown second-nav-dropdown-nav">
                    <a class="dropdown-toggle" data-toggle="dropdown" href="#"><img src="/src/img/books-icon.png" alt="">
                    </a>
                    <ul class="dropdown-menu second-nav-dropped-menu-items showModals">
                        <li><a data-id="countries" href="#">Countries</a></li>
                        <li><a data-id="languages" href="#">Languages</a></li>
                        <li><a data-id="placementtypes" href="#">Cabin/Room Categories</a></li>
                        <li><a data-id="shipcategories" href="#">Cabin IDs</a></li>
                        <li><a data-id="hotelcategories" href="#">Room IDs</a></li>
                        <li><a data-id="airports" href="#">Airports</a></li>
                    </ul>
                </li>
                @endrole
            </ul>
        </div>
    </nav>
@endif
@endrole