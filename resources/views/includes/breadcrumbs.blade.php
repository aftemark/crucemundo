<div class="links">
    <ol class="base-page-breadcrumb">
        @foreach(config('constants.breadcrumbs.' . Route::current()->getName()) as $crumbRouteName => $crumbRoute)
            @if($crumbRoute !== NULL)
                <li><a href="{{ route($crumbRoute) }}(">{{ $crumbRouteName }}</a></li>
            @else
                <li class="active">{{ $crumbRouteName }}</li>
            @endif
        @endforeach
    </ol>
</div>