@extends('layouts.app')

@section('title')Program updates @endsection

@section('content')
    <div class="container">
        <div class="content">
            <div class="row">
                <h3>Reminders</h3>
            </div>
            <div class="row">
                <ul class="nav nav-pills">
                    <li class="active"><a data-toggle="tab" href="#acting">Acting</a></li>
                    <li><a data-toggle="tab" href="#overdue">Overdue</a></li>
                    <li><a data-toggle="tab" href="#completed">Completed</a></li>
                </ul>
            </div>
            <div class="tab-content">
                @foreach($notifications as $notifications_type => $notifications_collection)
                <div id="{{ $notifications_type }}" class="tab-pane fade in{{ $notifications_type == 'acting' ? ' active' : '' }}">
                    <form class="row">
                        <div class="col-md-6">
                            <div class="col-md-3">
                                <select class="form-control" name="{{ $notifications_type }}_search">
                                    <option value="all">All</option>
                                </select>
                            </div>
                            <div class="col-md-8">
                                <input class="form-control" type="text" name="{{ $notifications_type }}_search" placeholder="Search" value="{{ Request::get('search') }}">
                            </div>
                            <div class="col-md-1">
                                <button type="submit" class="btn btn-default"><span class="glyphicon glyphicon-search"></span></button>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <select onchange="this.form.submit()" class="form-control" name="{{ $notifications_type }}_date">
                                <option {{ Request::get($notifications_type . '_date') == 'today' ? 'selected ' : '' }}value="today">Today</option>
                                <option {{ Request::get($notifications_type . '_date') == 'yesterday' ? 'selected ' : '' }}value="yesterday">Yesterday</option>
                                <option {{ Request::get($notifications_type . '_date') == 'week' ? 'selected ' : '' }}value="week">Week</option>
                            </select>
                        </div>
                    </form>
                    <table class="table table-striped">
                        <thead>
                        <tr>
                            <th>Date and time</th>
                            <th>Text</th>
                            @if($notifications_type != 'completed')
                            <th></th>
                            @endif
                        </tr>
                        </thead>
                        <tbody>
                        @foreach ($notifications_collection as $notification)
                            <tr>
                                <td>{{ $notification->created_at or '' }}</td>
                                <td>{!! $notification->text or '' !!}</td>
                                @if($notifications_type != 'completed')
                                <td>
                                    <a href="{{ url('/notifications/reminders/completed/' . $notification->id) }}">Set Completed</a>
                                </td>
                                @endif
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                </div>
                @if(!empty($notifications_collection->links()))
                <div class="row">
                    <div class="col-md-6">{{ $notifications_collection->links() }}</div>
                </div>
                @endif
                @endforeach
            </div>
            <div class="row">
                <div class="col-md-6">
                    <a href="/notifications/setnumber?page=reminders&number=10" class="btn btn-default{{ Session::get('notifications_reminders') == '10' ? ' active' : '' }}">10</a>
                    <a href="/notifications/setnumber?page=reminders&number=50" class="btn btn-default{{ Session::get('notifications_reminders') == '50' ? ' active' : '' }}">50</a>
                    <a href="/notifications/setnumber?page=reminders&number=100" class="btn btn-default{{ Session::get('notifications_reminders') == '100' ? ' active' : '' }}">100</a>
                </div>
            </div>
        </div>
    </div>
@endsection

{{--@section('script')
    <script>
        $('.set-completed').on('click', function() {
            var element = $(this);
            $.get('{{ url('/notifications/reminders/completed') }}/' + element.attr('data-id'), function() {
                element.closest('td').remove().closest('tr').appendTo('#completed table tbody');
                element.remove();
            });
        });
    </script>
@endsection--}}