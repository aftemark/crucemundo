@extends('layouts.app')

@section('title')
    Select year
@endsection

@section('content')
    <div class="add-year-block">
        <div class="clearfix">
            <h4 class="text-center">Select year</h4>
            <button type="button" class="pull-right add-year" data-toggle="modal" data-target="#addYear">Add year</button>
        </div>
        <div class="actual-year">
            @foreach($years["active"] as $year)
                <button type="button"><a href="{{ url('/years/select/' . $year->year) }}">{{ $year->year }}</a></button>
            @endforeach
        </div>
    </div>

    <div class="archive-years">
        <div class="clearfix">
            <h4 class="text-center">Archive</h4>
            <button type="button" class="pull-right add-archive" data-toggle="modal" data-target="#addArchive">Add archive</button>
        </div>
        <div class="old-years">
            @foreach($years["archive"] as $year)
                <button type="button"><a href="{{ url('/years/select/' . $year->year) }}">{{ $year->year }}</a></button>
            @endforeach
        </div>
    </div>

    <div id="addYear" class="modal fade" role="dialog">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <div class="modal-header-label modal-left">
                        <h4>Add year</h4>
                    </div>
                    <div class="modal-header-buttons modal-right">
                        <button type="submit" class="btn-create" form="selectYear">Create</button>
                        <button type="button" class="btn-cancel" data-dismiss="modal">Cancel</button>
                    </div>
                </div>
                <div class="modal-body clearfix">
                    <form id="selectYear" action="{{ url('/years/archive') }}" method="POST">
                        {{ csrf_field() }}
                        <div class="modal-left select-copy-year">
                            <label>Year for copy:</label>
                            <select class="form-control" name="copy">
                                <option disabled="disabled" selected hidden class="style-select">Select a year for copy</option>
                                @foreach($years['active'] as $yearArchive)
                                <option value="{{ $yearArchive->id }}">{{ $yearArchive->year }}</option>
                                @endforeach
                            </select>
                        </div>
                        <div class="modal-right select-new-year">
                            <label>New year:</label>
                            <input class="form-control" name="year">
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>

    <div id="addArchive" class="modal fade" role="dialog">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <div class="modal-header-label modal-left">
                        <h4>Add year to archive</h4>
                    </div>
                    <div class="modal-header-buttons modal-right">
                        <button type="submit" class="btn-add" form="selectYearArchive">Add</button>
                        <button type="button" class="btn-cancel" data-dismiss="modal">Cancel</button>
                    </div>
                </div>
                <div class="modal-body clearfix">
                    <form id="selectYearArchive" action="{{ url('/years/archive') }}" method="POST">
                        {{ csrf_field() }}
                        <div class="select-year-archive pull-left">
                            <label>Year:</label>
                            <select class="form-control" name="year">
                                <option disabled="disabled" selected hidden class="style-select">Select a year</option>
                                @foreach($years['active'] as $yearArchive)
                                    <option value="{{ $yearArchive->id }}">{{ $yearArchive->year }}</option>
                                @endforeach
                            </select>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('script')
    <script src="{{ url('/src/js/jquery.validate.min.js') }}"></script>
    <script>
        $("#selectYear").validate({
            rules: {
                copy: {
                    required: true
                },
                year: {
                    required: true
                }
            },
            messages: {
                copy: "Select a year",
                year: "Select a year"
            }
        });

        $("#selectYearArchive").validate({
            rules: {
                year: {
                    required: true
                }
            },
            messages: {
                addArchive: "Select a year"
            }
        });
    </script>
@endsection