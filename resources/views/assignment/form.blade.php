<div class="modal-header">
    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
    <div class="row modal-title" id="assignmentModalLabel">
        <div class="col-md-6">
            <h4>Cabin assignment</h4>
        </div>
        <div class="col-md-6">
            <div class="row">
                <div class="col-md-6">
                    <a id="saveAssignment" class="btn btn-success">Save</a>
                </div>
                <div class="col-md-6">
                    <a data-dismiss="modal" class="btn btn-danger">Cancel</a>
                </div>
            </div>
        </div>
    </div>
</div>
<form action="{{ url('/save/assignment') }}" method="POST" class="modal-body">
    <div class="row">
        <div class="col-md-6">
            <table class="table table-striped">
                <tbody>
                    <tr>
                        <td>Cabin category:</td>
                        <td>{{ $placement->cabin->type->type->name }}</td>
                    </tr>
                    <tr>
                        <td>Cabin type:</td>
                        <td>{{ $placement->cabin->type->name }}</td>
                    </tr>
                    <tr>
                        <td>Cabin number:</td>
                        <td>{{ $placement->cabin->num }}</td>
                    </tr>
                    <tr>
                        <td>Area:</td>
                        <td>{{ $placement->cabin->area }} m<sup>2</sup></td>
                    </tr>
                    <tr>
                        <td>Number of pax:</td>
                        <td>{{ $placement->cabin->type->pax_amount }}</td>
                    </tr>
                    <tr>
                        <td>Balcony:</td>
                        <td>{{ $placement->cabin->type->balcony ? 'Yes' : 'No' }}</td>
                    </tr>
                    <tr>
                        <td>Additional bed:</td>
                        <td>{{ $placement->cabin->type->bed ? 'Yes' : 'No' }}</td>
                    </tr>
                </tbody>
            </table>
        </div>
        <div class="col-md-6">
            <div class="form-group">
                <label>Note:</label>
                <textarea class="form-control" rows="5" name="note">{{ $placement->note }}</textarea>
            </div>
        </div>
    </div>
    @if(isset($placement->reservation_placement))
    @php $reservation_pax_count = $placement->reservation_placement->reservation_paxes->count(); @endphp
    <div class="row">
        <h3><b>BOOKING - {{ date('d.m.Y', strtotime($placement->reservation_placement->reservation->created_at)) }}</b>({{ $placement->reservation_placement->reservation->code }})</h3>
        <table class="table table-striped">
            <thead>
                <tr>
                    <th>Cabin category</th>
                    <th>ID</th>
                    <th>Name</th>
                    <th>Services</th>
                    <th>Amount</th>
                    <th></th>
                </tr>
            </thead>
            <tbody>
                <tr>
                    <td rowspan="{{ $reservation_pax_count }}">{{ $placement->reservation_placement->cruise_type->type->type->name }}</td>
                    <td rowspan="{{ $reservation_pax_count }}">{{ $placement->reservation_placement->cruise_type->type->name }}</td>
                    <td>{{ $placement->reservation_placement->reservation_paxes()->first()->name }} {{ $placement->reservation_placement->reservation_paxes->first()->surname }}</td>
                    <td rowspan="{{ $reservation_pax_count }}">{{ $placement->reservation_placement->services_names }}</td>
                    <td rowspan="{{ $reservation_pax_count }}">{{ number_format($placement->reservation_placement->amount, 2) }} EUR</td>
                    <td rowspan="{{ $reservation_pax_count }}"><input type="radio" class="form-control" name="reservation_placement" value="{{ $placement->reservation_placement_id }}" checked></td>
                </tr>
                @foreach($placement->reservation_placement->reservation_paxes as $pax)
                @if($placement->reservation_placement->reservation_paxes()->first()->id != $pax->id)
                <tr>
                    <td>{{ $pax->name }} {{ $pax->surname }}</td>
                </tr>
                @endif
                @endforeach
            </tbody>
        </table>
    </div>
    @endif
    @if(!empty($reservations))
    <hr>
    <h3>NOT ASSIGNED</h3>
    @foreach($reservations as $reservation)
    <h3><b>BOOKING - {{ date('d.m.Y', strtotime($reservation["reservation"]["created_at"])) }}</b>({{ $reservation["reservation"]["code"] }})</h3>
    <table class="table table-striped">
        <thead>
        <tr>
            <th>Cabin category</th>
            <th>ID</th>
            <th>Name</th>
            <th>Services</th>
            <th>Amount</th>
            <th></th>
        </tr>
        </thead>
        <tbody>
            @foreach($reservation["placements"] as $other_placement)
            @php $reservation_pax_count = $other_placement->reservation_paxes->count(); @endphp
            <tr>
                <td rowspan="{{ $reservation_pax_count }}">{{ $other_placement->cruise_type->type->type->name }}</td>
                <td rowspan="{{ $reservation_pax_count }}">{{ $other_placement->cruise_type->type->name }}</td>
                <td>{{ $other_placement->reservation_paxes()->first()->name }} {{ $other_placement->reservation_paxes->first()->surname }}</td>
                <td rowspan="{{ $reservation_pax_count }}">{{ $other_placement->services_names }}</td>
                <td rowspan="{{ $reservation_pax_count }}">{{ number_format($other_placement->amount, 2) }} EUR</td>
                <td rowspan="{{ $reservation_pax_count }}"><input type="radio" name="reservation_placement" value="{{ $other_placement->id }}"></td>
            </tr>
            @foreach($other_placement->reservation_paxes as $pax)
            @if($other_placement->reservation_paxes()->first()->id != $pax->id)
            <tr>
                <td>{{ $pax->name }} {{ $pax->surname }}</td>
            </tr>
            @endif
            @endforeach
            @endforeach
        </tbody>
    </table>
    @endforeach
    @endif
    <input type="hidden" name="id" value="{{ $placement->id }}">
    <input type="hidden" name="_token" value="{{ Session::token() }}">
</form>