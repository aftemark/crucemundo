@extends('layouts.app')

@section('title'){{ 'Cabins Assignment' }}@endsection

@section('content')
    <div class="container">
        <div class="content">
            <div class="row">
                <div class="col-md-6">
                    <h2>Cabins Assignment ({{ $journey_info["ship_name"] }})</h2>
                    <p>Date: {{ $journey_info["date"] }}</p>
                </div>
                <div class="col-md-6 right">
                    <a href="{{ url('/reservations') }}" class="btn btn-danger">Cancel</a>
                </div>
            </div>
            @foreach($decks as $deck)
            <div class="row">
                <h3>{{ $deck["name"] }}</h3>
            </div>
            <div class="row">
                <table class="table table-striped">
                    <tbody>
                        <thead>
                            <tr>
                                <th>Cabin category</th>
                                <th>ID</th>
                                <th>Cabin #</th>
                                <th>Names</th>
                                <th>Status</th>
                                <th></th>
                            </tr>
                        </thead>
                        @foreach ($deck["placements"] as $placement)
                        <tr>
                            <td>{{ $placement["category"] or '-' }}</td>
                            <td>{{ $placement["type"] or '-' }}</td>
                            <td>{{ $placement["num"] or '-' }}</td>
                            <td>{{ $placement["paxes"] or '-' }}</td>
                            <td>{{ $placement["status"] or '-' }}</td>
                            <td>
                                @if($placement["status"] == 'Assigned')
                                    <a class="edit-assignment" href="{{ url('/assignment/placement/' . $placement["id"]) }}">Assign</a>
                                    <a class="clear-assignment" href="{{ url('/clear/assignment/' . $placement["id"]) }}">Clear</a>
                                @else
                                    <a class="edit-assignment" href="{{ url('/assignment/placement/' . $placement["id"]) }}">Assign</a>
                                @endif
                            </td>
                        </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
            @endforeach
        </div>
    </div>

    <div class="modal fade" id="assignmentModal" tabindex="-1" role="dialog" aria-labelledby="assignmentModalLabel">
        <div class="modal-dialog" role="document">
            <div class="modal-content"></div>
        </div>
    </div>
@endsection

@section('script')
    <script>
        var assignmentModal = '#assignmentModal';
        $('.edit-assignment').on('click', function(e) {
            $(assignmentModal + ' .modal-content').empty();
            var href = $(this).attr('href');
            $.get(href, function (html) {
                $(assignmentModal).modal('show').find('.modal-content').append(html);
            });
            e.preventDefault();
        });

        $('.clear-assignment').on('click', function(e) {
            var href = $(this).attr('href');
            $.get(href, function () {
                location.reload(true);
            });
            e.preventDefault();
        });

        $('body').on('click', '#saveAssignment', function(e) {
            var form = $(assignmentModal + ' form');
            $.post(form.attr('action'), form.serialize(), function () {
                location.reload(true);
            });
            e.preventDefault();
        });
    </script>
@endsection