@extends('layouts.app')

@section('title')Program updates @endsection

@section('content')
    <div class="container">
        <div class="content">
            <div class="row">
                <h3>Program updates</h3>
            </div>
            <form class="row">
                <dic class="col-md-6" method="GET">
                    <div class="col-md-3">
                        <select class="form-control" name="search">
                            <option value="all">All</option>
                        </select>
                    </div>
                    <div class="col-md-8">
                        <input class="form-control" type="text" name="search" placeholder="Search" value="{{ Request::get('search') }}">
                    </div>
                    <div class="col-md-1">
                        <button type="submit" class="btn btn-default"><span class="glyphicon glyphicon-search"></span></button>
                    </div>
                </dic>
                <div class="col-md-6">
                    <select onchange="this.form.submit()" class="form-control" name="date">
                        <option {{ Request::get('date') == 'today' ? 'selected ' : '' }}value="today">Today</option>
                        <option {{ Request::get('date') == 'yesterday' ? 'selected ' : '' }}value="yesterday">Yesterday</option>
                        <option {{ Request::get('date') == 'week' ? 'selected ' : '' }}value="week">Week</option>
                    </select>
                </div>
            </form>
            <table class="table table-striped">
                <thead>
                    <tr>
                        <th>Date and time</th>
                        <th>Action</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach ($notifications as $notification)
                    <tr>
                        <td>{{ $notification->created_at or '' }}</td>
                        <td>{!! $notification->text or '' !!}</td>
                    </tr>
                    @endforeach
                </tbody>
            </table>
            <div class="row">
                <div class="col-md-6">{{ $notifications->links() }}</div>
                <div class="col-md-6">
                    <a href="/notifications/setnumber?page=updates&number=10" class="btn btn-default{{ Session::get('notifications_updates') == '10' ? ' active' : '' }}">10</a>
                    <a href="/notifications/setnumber?page=updates&number=50" class="btn btn-default{{ Session::get('notifications_updates') == '50' ? ' active' : '' }}">50</a>
                    <a href="/notifications/setnumber?page=updates&number=100" class="btn btn-default{{ Session::get('notifications_updates') == '100' ? ' active' : '' }}">100</a>
                </div>
            </div>
        </div>
    </div>
@endsection