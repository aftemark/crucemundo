{{ csrf_field() }}
<input id="journeyID" type="hidden" name="journey" value="{{ $special->journey_id }}">
@if(isset($special->id))
    <input type="hidden" name="id" value="{{ $special->id }}">
@endif
<div class="modal-header">
    <div class="country-modal-header">
        <div class="modal-header-label modal-left">
            <h4 class="modal-title">Add special</h4>
        </div>
        <div class="modal-header-buttons modal-right">
            @role('edit_directories')
            @role('edit_tours')
            <button type="button" class="btn-add" id="saveSpecial">Save</button>
            @endrole
            @endrole
            <button type="button" class="btn-cancel" data-dismiss="modal">Cancel</button>
        </div>
    </div>
</div>
<div class="modal-body history-modal clearfix">
    <div class="select-special">
        <p>Special type:</p>
        <select name="type" id="specialType">
            <option value="false">Select type</option>
            <option {{ $special->type == 'discount' ? 'selected ' : '' }}value="discount">Discount</option>
            <option {{ $special->type == 'free' ? 'selected ' : '' }}value="free">Free service package</option>
            <option {{ $special->type == 'exception' ? 'selected ' : '' }}value="exception">Supplement exception</option>
        </select>
    </div>
    <div id="specialFields">
        @if($special->type)
            <div class="date-expire clearfix">
                <div class="start-date-input">
                    <p>Start:</p>
                    <input type="text" name="date_start" value="{{ $special->date_start }}">
                    <div class="cruises-date-btn"><span class="glyphicon glyphicon-calendar"></span></div>
                </div>
                <div class="expire-date-input">
                    <p>Expiry:</p>
                    <input type="text" name="date_end" value="{{ $special->date_end }}">
                    <div class="cruises-date-btn"><span class="glyphicon glyphicon-calendar"></span></div>
                </div>
            </div>
            <div class="cabine-type">
                @if($special->journey->type == 'tour')
                    <p>Room types:</p>
                @elseif($special->journey->type == 'cruise')
                    <p>Cabin types:</p>
                @endif
                <select id="cabinTypes" class="cabine-type-select" multiple="multiple" name="cabin_types[]">
                    @foreach($special->journey->placements as $placement)
                        <option {{ in_array($placement->id, $specialPlacements) ? 'selected  ' : '' }}value="{{ $placement->id }}">{{ $placement[$special_info["placement_name"]]->type->name }}</option>
                    @endforeach
                </select>
            </div>
            <div class="select-special-block clearfix">
                <div class="choose-special">
                    <p>Special:</p>
                    <select name="offer_type" id="offerType">
                        <option value="false">Select a special</option>
                        @if($special->type == 'discount')
                            <option {{ $special->offer_type == 'percent' ? 'selected  ' : '' }}value="percent">Discount %</option>
                            <option {{ $special->offer_type == 'eur' ? 'selected  ' : '' }}value="eur">Discount EUR</option>
                            <option {{ $special->offer_type == 'new' ? 'selected  ' : '' }}value="new">New price</option>
                        @endif
                        @if($special->type == 'free')
                            <option {{ $special->offer_type == 'free_services' ? 'selected  ' : '' }}value="free_services">Free services</option>
                            <option {{ $special->offer_type == 'free_packages' ? 'selected  ' : '' }}value="free_packages">Free excursion package</option>
                            @if($special->journey->type == 'cruise')
                                <option {{ $special->offer_type == 'free_extensions' ? 'selected  ' : '' }}value="free_extensions">Free extensions</option>
                            @endif
                        @endif
                        @if($special->type == 'exception')
                            <option {{ $special->offer_type == 'no_single' ? 'selected  ' : '' }}value="no_single">No single supplement</option>
                            @if($special->journey->type == 'cruise')
                                <option {{ $special->offer_type == 'no_port' ? 'selected  ' : '' }}value="no_port">No port tax</option>
                            @endif
                        @endif
                    </select>
                </div>
                @if($special->type == 'discount')
                    <div class="special-input">
                        <input {{ $special->offer_type ? '' : 'disabled ' }}type="text" id="offerAmount" name="offer_amount" value="{{ $special->offer_amount }}">
                        <div class="cruises-date-btn"><span class="">-</span></div>
                    </div>
                @endif
                @if($special->type == 'free')
                    <div class="select-wine"{!! $special->offer_type == 'free_services' ? '' : ' style="display:none;"' !!}>
                        <select class="select-wine-select" multiple="multiple" name="services[]" id="specialServices"{{ $special->offer_type == 'free_services' ? '' : ' disabled' }}>
                            @foreach($journeyServices as $serviceID => $journeyService)
                                <option {{ in_array($serviceID, $specialServices) ? 'selected  ' : '' }}value="{{ $serviceID }}">{{ $journeyService }}</option>
                            @endforeach
                        </select>
                    </div>
                    <div class="select-wine"{!! $special->offer_type == 'free_packages' ? '' : ' style="display:none;"' !!}>
                        <select class="select-wine-select" multiple="multiple" name="services[]" id="specialPackages"{{ $special->offer_type == 'free_packages' ? '' : ' disabled' }}>
                            @foreach($journeyPackages as $packageID => $journeyPackage)
                                <option {{ in_array($packageID, $specialServices) ? 'selected  ' : '' }}value="{{ $packageID }}">{{ $journeyPackage }}</option>
                            @endforeach
                        </select>
                    </div>
                    <div class="select-wine"{!! $special->offer_type == 'free_extensions' ? '' : ' style="display:none;"' !!}>
                        <select class="select-wine-select" multiple="multiple" name="services[]" id="specialExtensions"{{ $special->offer_type == 'free_extensions' ? '' : ' disabled' }}>
                            @foreach($tours as $tour)
                                <option {{ in_array($tour->id, $specialServices) ? 'selected  ' : '' }}value="{{ $tour->id }}">{{ $tour->code }}</option>
                            @endforeach
                        </select>
                    </div>
                @endif
            </div>
            @if($special->type == 'discount' || $special->type == 'free')
                <div class="conditions">
                    <p>Condition:</p>
                    <select name="condition">
                        <option value="false">Select a condition</option>
                        <option {{ $special->special_condition == 'group' ? 'selected  ' : '' }}value="group">Group</option>
                        <option {{ $special->special_condition == 'fit' ? 'selected  ' : '' }}value="fit">Fit</option>
                        <option {{ $special->special_condition == 'both' ? 'selected  ' : '' }}value="both">Group & Fit</option>
                    </select>
                </div>
            @endif
            <div class="apply-special">
                <input {{ $special->previous ? 'checked ' : '' }}type="checkbox" name="previous" value="true">
                <p>Apply to previous bookings:</p>
            </div>
        @endif
    </div>
</div>