<?php

return [
    'modules' => [
        'directories' => [
                'name' => 'Directories',
                'children' => [
                    'catalog' => 'Constants directory',
                    'cities' => 'Cities directory',
                    'ships' => 'Ships directory',
                    'hotels' => 'Hotels directory',
                    'services' => 'Services directory',
                    'packages' => 'Packages directory',
                    'itineraries' => 'Itineraries directory',
                    'b2c' => 'B2C clients directory',
                    'b2b' => 'B2B clients directory',
                    'cruises' => 'Cruises directory',
                    'cruises_implementation' => 'Cruises Implementation tab',
                    'cruises_reservations' => 'Cruises Reservations tab',
                    'tours' => 'Tours directory',
                    'tours_implementation' => 'Tours Implementation tab',
                    'tours_reservations' => 'Tours Reservations tab',
                ]
            ],
            'administration' => [
                'name' => 'Administration',
                'children' => [
                    'users' => 'Users page',
                    'mailing' => 'Mailing',
                ]
            ],
            'reservations' => [
                'name' => 'Reservations',
                'children' => [
                    'cruise_bookings' => [
                        'name' => 'Cruise Bookings',
                        'create' => true,
                        'children' => [
                            'rooms_cabins' => [
                                'name' => 'Cabins & Pax Information',
                                'type' => 'show'
                            ],
                            'assignment' => [
                                'name' => 'Cabins Assignment',
                                'type' => 'show'
                            ],
                            'edit' => [
                                'name' => 'Edit',
                                'type' => 'edit'
                            ]
                        ]
                    ],
                    'cruise_bookings_canceled' => [
                        'name' => 'Cruise Bookings (Canceled)',
                        'children' => [
                            'rooms_cabins' => [
                                'name' => 'Cabins & Pax Information',
                                'type' => 'show'
                            ]
                        ]
                    ],
                    'tour_bookings' => [
                        'name' => 'Tour Bookings',
                        'create' => true,
                        'children' => [
                            'rooms_cabins' => [
                                'name' => 'Rooms & Pax Information',
                                'type' => 'show'
                            ],
                            'edit' => [
                                'name' => 'Edit',
                                'type' => 'edit'
                            ]
                        ]
                    ],
                    'tour_bookings_canceled' => [
                        'name' => 'Tour Bookings (Canceled)',
                        'children' => [
                            'rooms_cabins' => [
                                'name' => 'Rooms & Pax Information',
                                'type' => 'show'
                            ]
                        ]
                    ],
                    'cruise_options' => [
                        'name' => 'Cruise Options',
                        'create' => true,
                        'children' => [
                            'rooms_cabins' => [
                                'name' => 'Cabins & Pax Information',
                                'type' => 'show'
                            ],
                            'edit' => [
                                'name' => 'Edit',
                                'type' => 'edit'
                            ]
                        ]
                    ],
                    'cruise_options_canceled' => [
                        'name' => 'Cruise Options (Canceled)',
                        'children' => [
                            'rooms_cabins' => [
                                'name' => 'Rooms & Pax Information',
                                'type' => 'show'
                            ]
                        ]
                    ],
                    'tour_options' => [
                        'name' => 'Tour Options',
                        'create' => true,
                        'children' => [
                            'rooms_cabins' => [
                                'name' => 'Rooms & Pax Information',
                                'type' => 'show'
                            ],
                            'edit' => [
                                'name' => 'Edit',
                                'type' => 'edit'
                            ]
                        ]
                    ],
                    'tour_options_canceled' => [
                        'name' => 'Tour Options (Canceled)',
                        'children' => [
                            'rooms_cabins' => [
                                'name' => 'Rooms & Pax Information',
                                'type' => 'show'
                            ]
                        ]
                    ],
                    'tour_inquiries' => [
                        'name' => 'Tour Inquiries',
                        'create' => true,
                        'children' => [
                            'rooms_cabins' => [
                                'name' => 'Rooms & Pax Information',
                                'type' => 'show'
                            ],
                            'edit' => [
                                'name' => 'Edit',
                                'type' => 'edit'
                            ]
                        ]
                    ],
                    'tour_inquiries_canceled' => [
                        'name' => 'Tour Inquiries (Canceled)',
                        'children' => [
                            'rooms_cabins' => [
                                'name' => 'Rooms & Pax Information',
                                'type' => 'show'
                            ]
                        ]
                    ],
                    'cruise_inquiries' => [
                        'name' => 'Cruise Inquiries',
                        'create' => true,
                        'children' => [
                            'rooms_cabins' => [
                                'name' => 'Cabins & Pax Information',
                                'type' => 'show'
                            ],
                            'edit' => [
                                'name' => 'Edit',
                                'type' => 'edit'
                            ]
                        ]
                    ],
                    'cruise_inquiries_canceled' => [
                        'name' => 'Cruise Inquiries (Canceled)',
                        'children' => [
                            'rooms_cabins' => [
                                'name' => 'Cabins & Pax Information',
                                'type' => 'show'
                            ]
                        ]
                    ],
                ]
            ],
            'notifications' => [
                'name' => 'Notifications',
                'children' => [
                    'notifications_actions' => 'Notifications Actions',
                    'notifications_updates' => 'Notifications Updates',
                    'notifications_reminders' => 'Notifications Reminders',
                ]
            ],
            'year' => [
                'name' => 'Year navigation',
                'children' => []
            ],
            'reports' => [
                'name' => 'Reports',
                'children' => []
            ],
    ],
    'fines' => [
        'pax_type_change' => 50,
        'pax_amount_change' => 50,
        'pax_details_replacement' => 50
    ],
    'directories_second_nav_routes' => [
        'cities_list',
        'accommodations_list',
        'ship',
        'hotel',
        'services_list',
        'clients_list',
        'b2c',
        'b2c_new',
        'b2b',
        'b2b_new',
        'itineraries_list',
        'packages_list',
        'package',
        'cruises_list',
        'cruise',
        'tours_list',
        'tour'
    ],
    'breadcrumbs' => [
        'cities_list' => ['Directories' => NULL, 'Cities' => NULL],
        'accommodations_list' => ['Directories' => NULL, 'Accommodations' => NULL],
        'services_list' => ['Directories' => NULL, 'Services' => NULL],
        'clients_list' => ['Directories' => NULL, 'Clients' => NULL],
        'itineraries_list' => ['Directories' => NULL, 'Itineraries' => NULL],
        'packages_list' => ['Directories' => NULL, 'Packages' => NULL],
        'cruises_list' => ['Directories' => NULL, 'Cruises' => NULL],
        'tours_list' => ['Directories' => NULL, 'Tours' => NULL],
        'users_list' => ['Administration' => NULL, 'Users' => NULL],
        //'_list' => ['Directories' => NULL, '' => NULL],
    ]
];